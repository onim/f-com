CC=mpiifort
CFLAGS=-c -extend-source -fpp -save-temps \
-heap-arrays 10 -g -openmp -O2 -xHost -static \
-I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include/intel64/lp64 -L"/opt/intel/composer_xe_2013_sp1.0.080/mkl/../compiler/lib/intel64" -liomp5 -lpthread -ldl

DCFLAGS=-c -fpp -extend-source -heap-arrays 10 -g -warn all -check bounds -traceback -save-temps
LDFLAGS=-openmp -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include/intel64/lp64 /opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64/libmkl_lapack95_lp64.a "/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64"/libmkl_intel_lp64.a -Wl,--start-group "/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64"/libmkl_intel_thread.a "/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64"/libmkl_core.a -Wl,--end-group -L"/opt/intel/composer_xe_2013_sp1.0.080/mkl/../compiler/lib/intel64" -liomp5 -lpthread -ldl

SOURCES=\
9.Module_50Read_Commented.f90 \
9.Module_01Fundamental_Constant.f \
9.Module_51Thermo.f \
9.Module_03BlockPointData.f90 \
9.Module_04Thermal_Property.f \
9.Module_05Chemistry_Data.f90 \
9.Module_07Control_Data.f \
9.Module_08Boundary_Data_OOP.f90 \
9.Module_52MPIDataContainer.f \
9.Module_53MPI.f \
9.Module_09Status_Check.f \
9.Module_90Error.f \
9.Module_80AuxilarySubprogram.f \
4.Transport_0Module.f90 \
4.Transport_6Modeling_LES.f90 \
4.Transport_6Modeling_RANS.f90 \
4.Transport_6Modeling.f90 \
1.Prepare_1CalcSt.f90 1.Prepare_1CalcSt_Running.f90 \
1.Prepare_2PreSpc.f90 \
1.Prepare_3ArrayAlloc.f90 \
1.Prepare_4GridGen.f90 1.Prepare_4GridGen_CoordDifference.f90 1.Prepare_4GridGen_ExpandPoint.f90 1.Prepare_4GridGen_Metric.f90 \
1.Prepare_5IniGen.f90 1.Prepare_5IniGen_ApplyBoundaryCondition.f90 1.Prepare_5IniGen_ReadCondition.f90 1.Prepare_5IniGen_ReadPrevResult.f90 1.Prepare_5IniGen_Generation.f90 \
2.Setting_1ExpVar.f90 \
2.Setting_1ExpVar_Wrapper.f90 \
3.Convection_0Module.f90 \
3.Convection_1Convect.f90 \
1.Prepare_6ConfigCheck.f90 \
4.Transport_1Transport.f90 \
4.Transport_2Gradient.f90 \
4.Transport_3Viscosity.f90 \
4.Transport_4Diffusion.f90 \
4.Transport_5Conduction.f90 \
5.Reaction_0Combustion.f90 \
5.Reaction_3JonesLindstedt_original_modified.f90 \
5.Reaction_5FastChemistryIrrev.f90 \
5.Reaction_6Li.f90 \
6.TimeMarch_1Residual.f90 \
6.TimeMarch_2FirstFwEuler.f90 \
7.Finishing_1AddRHS.f90 \
7.Finishing_2BoundCond.f90 \
7.Finishing_3CalcSummary.f90 \
7.Finishing_4OutputResult.f90 \
7.Finishing_5StatusCheck.f90 \
main.f90

OBJECTS_1=$(SOURCES:.f=.o)
OBJECTS=$(OBJECTS_1:.f90=.o)
INTERMEDIATE_1=$(SOURCES:.f=.i)
INTERMEDIATE=$(INTERMEDIATE_1:.f90=.i90)
EXECUTABLE=scramjet

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

.f.o:
	$(CC) $(CFLAGS) $< -o $@

%.o %.mod: %.f
	$(CC) $(CFLAGS) $< -o $@

.f90.o:
	$(CC) $(CFLAGS) $< -o $@

%.o %.mod: %.f90
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS) $(INTERMEDIATE) *.mod $(EXECUTABLE) *__genmod.f90

