#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define _forAllBlocks_ DO ib=1,nb; nx=nx_gl(ib);ny=ny_gl(ib);nz=nz_gl(ib);
#define _forMyBlocks_ DO ib=1,nb; nx=nx_gl(ib);ny=ny_gl(ib);nz=nz_gl(ib);IF (myBlockIs(ib)) THEN

#define _endAllBlocks_ ENDDO
#define _endMyBlocks_ ENDIF; ENDDO


#define _forAllPoints_ DO k=1,nz+2*nGS;DO j=1,ny+2*nGS;DO i=1,nx+2*nGS
#define _forInnerPoints_ DO k=1+nGS,nz+nGS;DO j=1+nGS,ny+nGS;DO i=1+nGS,nx+nGS

#define _end3_ ENDDO;ENDDO;ENDDO
#define _endAllPoints_ ENDDO;ENDDO;ENDDO
#define _endInnerPoints_ ENDDO;ENDDO;ENDDO

#endif
