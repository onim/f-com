!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Scramjet Simulation Program
!           - Compressible, turbulent, reactive gaseous flow simulation-
!
!     Developed by Minho Choi
!           Tsue-Nakaya laboratory
!           The University of Tokyo
!
!     For inquiry
!     contact 0695356468@mail.ecc.u-tokyo.ac.jp
!             lieberstukov@gmail.com
!
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
PROGRAM Scramjet
! Main Program
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!  Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,nx_gl,ny_gl,nz_gl,     &
                                      it,itrBgn,itrEnd,reactionModel, &
                                      itvcnv,itvrsl,dqNorm,dqCri,     &
                                      forcedStop,enable_Diffusion,    &
                                      ib,nb,dt
   USE fundamental_Constants,    ONLY:title
   USE Message_Passing_Interface
   USE M_BlockPointData,         ONLY:bl
IMPLICIT NONE

!$$$$$$$$$$$$$$$
!  Main
!$$$$$$$$$$$$$$$
   CALL startMPI           ! Start MPI session from here

!############### Preparation for the start of calculation #######################
   CALL configload         ! Loads configuration. Includes parameterload subroutine
   CALL PRESPC             ! Loads physical properties.
   CALL arrayalloc         ! Allocate global variable arrays
   CALL GRDGEN             ! Generate grid arrays
   CALL IniGen             ! Initialize major variables
   CALL BCSLT              ! Apply boundary condition
   IF (IamPrimeNode()) CALL configcheck      ! Output loaded configuration
   IF (IamPrimeNode()) WRITE(*,'(A)') title
   it=itrBgn

!########################  ITERATION    ################################
   mainLoop: DO            ! Starting of the loop
      CALL parameterload   ! Load variable configurations
      CALL ExpVarWrapper   ! Expand all variables by calculating their values

      _forMyBlocks_
         ! Solve convective terms
         CALL CONVECT
         CALL RSZERO

         ! Solve diffusive terms
         IF (enable_Diffusion) THEN
            CALL VISCOUS
            CALL RSZERO
         ENDIF

         ! Solve temporal term
         CALL DTRHS
         CALL firstfweuler
         CALL RSZERO

         ! Obtain next step values
         CALL ADDRHS

         ! Solve chemical reaction terms
         CALL combustionWrapper
         CALL RSZERO

      _endMyBlocks_

      ! Apply boundary conditions
      CALL BCSLT

      ! Assess convergence
      CALL RESID1

      ! Determine whether to stop the loop or continue
      IF((IT.GT.(itrBgn+itvrsl)).AND.(dqNorm.LT.dqCri)) EXIT mainloop
      IF(forcedStop.or.it.ge.itrEnd) EXIT mainloop

      ! Output the result intervally
      IF((MOD(it,itvrsl).EQ.0)) CALL OUTRSL

      ! Calculation status check
      CALL statout

      it=it+1

   ENDDO mainloop

   CALL OUTRSL
   CALL statout

   CALL endMPI          ! End MPI session here

END PROGRAM
