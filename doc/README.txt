F-com

Minho Choi

This program is intended to be used for computational fluid dynamics(CFD) of supersonic, reacting, turbulent flows. This program solves continuity eq., Navier-Stokes eq., energy eq., and chemical reaction in a given domain. Several number of blocks consists of the domain, and these blocks can be solved simultaneously with parallel computing using OpenMP and MPI. Based on finite difference method. Therefore, only structured mesh can be used. 


* Language
Only Fortran has been used. 

* Theoretical background
Approximate Riemann solver: AUSM+-up
Interpolation for fluxes: MUSCL
Turbulence model: LES with Smagorinsky model
Specific heat calculation: NASA Glenn coefficient?
Time marching scheme: Forward Euler

* Auxillary programs
geometry generator
result converter to VTK format

* Requirements
OS: linux
compiler: ifort
gfortran may be used, with appropriate change of options in makefile. Calculation status output to standard output may be strange, so you should change code for the output.

