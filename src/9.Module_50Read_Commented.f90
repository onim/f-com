! Macro part
! All read subroutines has identical structure
! Intrinsic type of integer, real, double precision, logical, character* 
!   can be read.
#define _reading_ \
   IF(PRESENT(form)) THEN;         \
      READ (UnitNum, form) getter; \
   ELSE;                           \
      READ (UnitNum, *) getter;    \
   ENDIF

#define _statements_ \
   ReadComments: do; \
      READ (UnitNum, '(A)') line; \
      IF (line (1:1).ne.commentCharacter) EXIT ReadComments; \
   ENDDO ReadComments; \
   BACKSPACE (UnitNum); \
   select type (getter); \
   class is (integer); \
      _reading_; \
   class is (real); \
      _reading_; \
   class is (real*8); \
      _reading_; \
   class is (logical); \
      _reading_; \
   class is (character(len=*)); \
      _reading_; \
   class default; \
      print *,"Reader failed"; \
   end select

! Module part
MODULE readcommentedfile
IMPLICIT NONE
   ! Define comment character - can be modified
   CHARACTER,PARAMETER,PRIVATE::commentCharacterDefault='!'
   CHARACTER,PRIVATE::commentCharacter=commentCharacterDefault
   
   ! Generic subroutine readc
   interface readc
      module procedure readc0
      module procedure readc1
      module procedure readc2
      module procedure readc3
   end interface

CONTAINS

   ! Modify comment character
   subroutine changeCommentChar(newChar)
   implicit none
      character::newchar
   
      commentCharacter=newChar
   end subroutine

   ! Main subroutines (only difference is dimension of getter)
   ! dimension 0
   SUBROUTINE readc0 (UnitNum, getter, form)
   IMPLICIT NONE
      INTEGER, INTENT (in) :: UnitNum
      CLASS(*), INTENT(OUT) :: getter
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      CHARACTER (LEN=300) :: line

      _statements_
   END SUBROUTINE

   ! dimension 1
   SUBROUTINE readc1 (UnitNum, getter, form)
   IMPLICIT NONE
      INTEGER, INTENT (in) :: UnitNum
      CLASS(*), INTENT(OUT), dimension(:) :: getter
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      CHARACTER (LEN=300) :: line

      _statements_
   END SUBROUTINE

   ! dimension 2
   SUBROUTINE readc2 (UnitNum, getter, form)
   IMPLICIT NONE
      INTEGER, INTENT (in) :: UnitNum
      CLASS(*), INTENT(OUT), dimension(:,:) :: getter
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      CHARACTER (LEN=300) :: line

      _statements_
   END SUBROUTINE

   ! dimension 3
   SUBROUTINE readc3 (UnitNum, getter, form)
   IMPLICIT NONE

      INTEGER, INTENT (in) :: UnitNum
      CLASS(*), INTENT(OUT), dimension(:,:,:) :: getter
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      CHARACTER (LEN=300) :: line

      _statements_
   END SUBROUTINE



END MODULE readcommentedfile

