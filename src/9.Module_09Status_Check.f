!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      MODULE Status_Check
!        check calculation status
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      USE Message_Passing_Interface

      IMPLICIT NONE

!$$$$$$$$$$$$$$$$$$$$$
!     Variables
!$$$$$$$$$$$$$$$$$$$$$
!     20expvar
      INTEGER::tempExcess, newtonRapson
!     31ausm
      INTEGER::AUSMFlag
!     42viscosity
      REAL(8)::visc_Ratio,meanViscosity
!     44conduction
      REAL(8)::meanConductivity
!     70addrhs
      LOGICAL::wrongConcentr,voidNode
      REAL(8)::rocons, rucons, rvcons, rwcons, recons
      REAL(8),ALLOCATABLE,DIMENSION(:)::rfscons

!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type declaration
!$$$$$$$$$$$$$$$$$$$$$
      TYPE minmaxQuant
      SEQUENCE
         INTEGER,DIMENSION(4)::
     &      roMaxLoc, velMaxLoc, energyMaxLoc, enthalpyMaxLoc, tempMaxLoc,
     &      roMinLoc, velMinLoc, energyMinLoc, enthalpyMinLoc, tempMinLoc

         REAL(8)::
     &      roMaxVal, velMaxVal, energyMaxVal, enthalpyMaxVal, tempMaxVal,
     &      roMinVal, velMinVal, energyMinVal, enthalpyMinVal, tempMinVal,
     &      qChemMaxVal, qChemMinVal, qchemSumVal
      ENDTYPE

      TYPE(minmaxQuant),ALLOCATABLE::mm(:)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


!$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE findminmax(this)
!$$$$$$$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
      USE M_BlockPointData,         ONLY:bl
      USE control_Data,             ONLY:ib

      IMPLICIT NONE

      TYPE(minmaxQuant)::this

      ASSOCIATE(c=>bl(ib)%p%c)

      this%roMaxVal      =MAXVAL(c%ro)
      this%velMaxVal     =MAXVAL(DSQRT(c%ru**2+c%rv**2+c%rw**2)/c%ro)
      this%energyMaxVal  =MAXVAL(c%re/c%ro-0.5d0*(c%u**2+c%v**2+c%w**2))
      this%enthalpyMaxVal=MAXVAL(c%h-0.5d0*(c%u**2+c%v**2+c%w**2))
      this%tempMaxVal    =MAXVAL(c%t)

      this%roMinVal      =MINVAL(c%ro)
      this%velMinVal     =MINVAL(DSQRT(c%ru**2+c%rv**2+c%rw**2)/c%ro)
      this%energyMinVal  =MINVAL(c%re/c%ro-0.5d0*(c%u**2+c%v**2+c%w**2))
      this%enthalpyMinVal=MINVAL(c%h-0.5d0*(c%u**2+c%v**2+c%w**2))
      this%tempMinVal    =MINVAL(c%t)

      this%roMaxLoc      =(/ib, MAXLOC(c%ro)/)
      this%velMaxLoc     =(/ib, MAXLOC(DSQRT(c%ru**2+c%rv**2+c%rw**2)/c%ro)/)
      this%energyMaxLoc  =(/ib, MAXLOC(c%re/c%ro-0.5d0*(c%u**2+c%v**2+c%w**2))/)
      this%enthalpyMaxLoc=(/ib, MAXLOC(c%h-0.5d0*(c%u**2+c%v**2+c%w**2))/)
      this%tempMaxLoc    =(/ib, MAXLOC(c%t)/)

      this%roMinLoc      =(/ib, MINLOC(c%ro)/)
      this%velMinLoc     =(/ib, MINLOC(DSQRT(c%ru**2+c%rv**2+c%rw**2)/c%ro)/)
      this%energyMinLoc  =(/ib, MINLOC(c%re/c%ro-0.5d0*(c%u**2+c%v**2+c%w**2))/)
      this%enthalpyMinLoc=(/ib, MINLOC(c%h-0.5d0*(c%u**2+c%v**2+c%w**2))/)
      this%tempMinLoc    =(/ib, MINLOC(c%t)/)

      this%qChemMaxVal   =MAXVAL(c%qchem)
      this%qChemMinVal   =MINVAL(c%qchem)
      this%qchemSumVal   =SUM(c%qchem)
      END ASSOCIATE
      ENDSUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE mergeMinmax(this, mm)
!$$$$$$$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nb

      IMPLICIT NONE

      INTEGER::i

      TYPE(minmaxQuant)::this
      TYPE(minmaxQuant),DIMENSION(nb)::mm

      this%roMaxVal=mm(1)%roMaxVal
      this%roMaxLoc=mm(1)%roMaxLoc
      this%velMaxVal=mm(1)%velMaxVal
      this%velMaxLoc=mm(1)%velMaxLoc
      this%energyMaxVal=mm(1)%energyMaxVal
      this%energyMaxLoc=mm(1)%energyMaxLoc
      this%enthalpyMaxVal=mm(1)%enthalpyMaxVal
      this%enthalpyMaxLoc=mm(1)%enthalpyMaxLoc
      this%tempMaxVal=mm(1)%tempMaxVal
      this%tempMaxLoc=mm(1)%tempMaxLoc

      this%roMinVal=mm(1)%roMinVal
      this%roMinLoc=mm(1)%roMinLoc
      this%velMinVal=mm(1)%velMinVal
      this%velMinLoc=mm(1)%velMinLoc
      this%energyMinVal=mm(1)%energyMinVal
      this%energyMinLoc=mm(1)%energyMinLoc
      this%enthalpyMinVal=mm(1)%enthalpyMinVal
      this%enthalpyMinLoc=mm(1)%enthalpyMinLoc
      this%tempMinVal=mm(1)%tempMinVal
      this%tempMinLoc=mm(1)%tempMinLoc

      this%qChemMaxVal=mm(1)%qChemMaxVal
      this%qChemMinVal=mm(1)%qChemMinVal

      DO i=2,nb
            IF(this%roMaxVal.lt.mm(i)%roMaxVal) THEN
                  this%roMaxVal=mm(i)%roMaxVal
                  this%roMaxLoc=mm(i)%roMaxLoc
            ENDIF
            IF(this%velMaxVal.lt.mm(i)%velMaxVal) THEN
                  this%velMaxVal=mm(i)%velMaxVal
                  this%velMaxLoc=mm(i)%velMaxLoc
            ENDIF
            IF(this%energyMaxVal.lt.mm(i)%energyMaxVal) THEN
                  this%energyMaxVal=mm(i)%energyMaxVal
                  this%energyMaxLoc=mm(i)%energyMaxLoc
            ENDIF
            IF(this%enthalpyMaxVal.lt.mm(i)%enthalpyMaxVal) THEN
                  this%enthalpyMaxVal=mm(i)%enthalpyMaxVal
                  this%enthalpyMaxLoc=mm(i)%enthalpyMaxLoc
            ENDIF
            IF(this%tempMaxVal.lt.mm(i)%tempMaxVal) THEN
                  this%tempMaxVal=mm(i)%tempMaxVal
                  this%tempMaxLoc=mm(i)%tempMaxLoc
            ENDIF

            IF(this%roMinVal.gt.mm(i)%roMinVal) THEN
                  this%roMinVal=mm(i)%roMinVal
                  this%roMinLoc=mm(i)%roMinLoc
            ENDIF
            IF(this%velMinVal.gt.mm(i)%velMinVal) THEN
                  this%velMinVal=mm(i)%velMinVal
                  this%velMinLoc=mm(i)%velMinLoc
            ENDIF
            IF(this%energyMinVal.gt.mm(i)%energyMinVal) THEN
                  this%energyMinVal=mm(i)%energyMinVal
                  this%energyMinLoc=mm(i)%energyMinLoc
            ENDIF
            IF(this%enthalpyMinVal.gt.mm(i)%enthalpyMinVal) THEN
                  this%enthalpyMinVal=mm(i)%enthalpyMinVal
                  this%enthalpyMinLoc=mm(i)%enthalpyMinLoc
            ENDIF
            IF(this%tempMinVal.gt.mm(i)%tempMinVal) THEN
                  this%tempMinVal=mm(i)%tempMinVal
                  this%tempMinLoc=mm(i)%tempMinLoc
            ENDIF

            IF(this%qchemMaxVal.gt.mm(i)%qchemMaxVal) THEN
                  this%qchemMaxVal=mm(i)%qchemMaxVal
            ENDIF
            IF(this%qchemMinVal.lt.mm(i)%qchemMinVal) THEN
                  this%qchemMinVal=mm(i)%qchemMinVal
            ENDIF
      ENDDO

      this%qchemSumVal=SUM(mm%qchemSumVal)

      ENDSUBROUTINE

      ENDMODULE
