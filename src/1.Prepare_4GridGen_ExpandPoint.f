!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Expand coordinate information onto ghost nodes
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ExpandPointSimple(gg,nx,ny,nz)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data,                ONLY:gridData,geo_gl
      USE control_Data,             ONLY:nx_gl,ny_gl,nz_gl
      USE fundamental_Constants,    ONLY:nGS


      IMPLICIT NONE

      TYPE(gridData),INTENT(INOUT)::gg
      INTEGER,INTENT(IN)::nx,ny,nz

      INTEGER::i

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     Simple expasion
!$$$$$
      ASSOCIATE (xd => gg%xd_(:,:,:),
     &           yd => gg%xd_(:,:,:),
     &           zd => gg%xd_(:,:,:))

      xd(0,:,:) =  xd(1,:,:)
     &           -(xd(2,:,:) -xd(1,:,:))
      yd(0,:,:) =  yd(1,:,:)
     &           -(yd(2,:,:) -yd(1,:,:))
      zd(0,:,:) =  zd(1,:,:)
     &           -(zd(2,:,:) -zd(1,:,:))

      END ASSOCIATE

      gg%xd_(0,:,:) =gg%xd_(1,:,:)
     &             -(gg%xd_(2,:,:) -gg%xd_(1,:,:))
      gg%yd_(0,:,:) =gg%yd_(1,:,:)
     &             -(gg%yd_(2,:,:) -gg%yd_(1,:,:))
      gg%zd_(0,:,:) =gg%zd_(1,:,:)
     &             -(gg%zd_(2,:,:) -gg%zd_(1,:,:))
            DO i=2,nGS
      gg%xd_(1-i,:,:) =gg%xd_(0,:,:)
     &               -(gg%xd_(i,:,:) -gg%xd_(1,:,:))
      gg%yd_(1-i,:,:) =gg%yd_(0,:,:)
     &               -(gg%yd_(i,:,:) -gg%yd_(1,:,:))
      gg%zd_(1-i,:,:) =gg%zd_(0,:,:)
     &               -(gg%zd_(i,:,:) -gg%zd_(1,:,:))
            ENDDO

!$$$$$


!$$$$$
      gg%xd_(nx+1,:,:) =gg%xd_(nx,:,:)
     &                -(gg%xd_(nx-1,:,:) -gg%xd_(nx,:,:))
      gg%yd_(nx+1,:,:) =gg%yd_(nx,:,:)
     &                -(gg%yd_(nx-1,:,:) -gg%yd_(nx,:,:))
      gg%zd_(nx+1,:,:) =gg%zd_(nx,:,:)
     &                -(gg%zd_(nx-1,:,:) -gg%zd_(nx,:,:))
            DO i=2,nGS
      gg%xd_(nx+i,:,:) =gg%xd_(nx+1,:,:)
     &                -(gg%xd_(nx+1-i,:,:) -gg%xd_(nx,:,:))
      gg%yd_(nx+i,:,:) =gg%yd_(nx+1,:,:)
     &                -(gg%yd_(nx+1-i,:,:) -gg%yd_(nx,:,:))
      gg%zd_(nx+i,:,:) =gg%zd_(nx+1,:,:)
     &                -(gg%zd_(nx+1-i,:,:) -gg%zd_(nx,:,:))
            ENDDO
!$$$$$


!$$$$$
      gg%xd_(:,0,:) =gg%xd_(:,1,:)
     &                   -(gg%xd_(:,2,:) -gg%xd_(:,1,:))
      gg%yd_(:,0,:) =gg%yd_(:,1,:)
     &                   -(gg%yd_(:,2,:) -gg%yd_(:,1,:))
      gg%zd_(:,0,:) =gg%zd_(:,1,:)
     &                   -(gg%zd_(:,2,:) -gg%zd_(:,1,:))
            DO i=2,nGS
      gg%xd_(:,1-i,:) =gg%xd_(:,0,:)
     &               -(gg%xd_(:,i,:) -gg%xd_(:,1,:))
      gg%yd_(:,1-i,:) =gg%yd_(:,0,:)
     &               -(gg%yd_(:,i,:) -gg%yd_(:,1,:))
      gg%zd_(:,1-i,:) =gg%zd_(:,0,:)
     &               -(gg%zd_(:,i,:) -gg%zd_(:,1,:))
            ENDDO
!$$$$$

!$$$$$
      gg%xd_(:,ny+1,:) =gg%xd_(:,ny,:)
     &                -(gg%xd_(:,ny-1,:) -gg%xd_(:,ny,:))
      gg%yd_(:,ny+1,:) =gg%yd_(:,ny,:)
     &                -(gg%yd_(:,ny-1,:) -gg%yd_(:,ny,:))
      gg%zd_(:,ny+1,:) =gg%zd_(:,ny,:)
     &                -(gg%zd_(:,ny-1,:) -gg%zd_(:,ny,:))
            DO i=2,nGS
      gg%xd_(:,ny+i,:) =gg%xd_(:,ny+1,:)
     &                -(gg%xd_(:,ny+1-i,:) -gg%xd_(:,ny,:))
      gg%yd_(:,ny+i,:) =gg%yd_(:,ny+1,:)
     &                -(gg%yd_(:,ny+1-i,:) -gg%yd_(:,ny,:))
      gg%zd_(:,ny+i,:) =gg%zd_(:,ny+1,:)
     &                -(gg%zd_(:,ny+1-i,:) -gg%zd_(:,ny,:))
            ENDDO
!$$$$$


!$$$$$
      gg%xd_(:,:,0) =gg%xd_(:,:,1)
     &             -(gg%xd_(:,:,2) -gg%xd_(:,:,1))
      gg%yd_(:,:,0) =gg%yd_(:,:,1)
     &             -(gg%yd_(:,:,2) -gg%yd_(:,:,1))
      gg%zd_(:,:,0) =gg%zd_(:,:,1)
     &             -(gg%zd_(:,:,2) -gg%zd_(:,:,1))
            DO i=2,nGS
      gg%xd_(:,:,1-i) =gg%xd_(:,:,0)
     &               -(gg%xd_(:,:,i) -gg%xd_(:,:,1))
      gg%yd_(:,:,1-i) =gg%yd_(:,:,0)
     &               -(gg%yd_(:,:,i) -gg%yd_(:,:,1))
      gg%zd_(:,:,1-i) =gg%zd_(:,:,0)
     &               -(gg%zd_(:,:,i) -gg%zd_(:,:,1))
            ENDDO
!$$$$$


!$$$$$
      gg%xd_(:,:,nz+1) =gg%xd_(:,:,nz)
     &                -(gg%xd_(:,:,nz-1) -gg%xd_(:,:,nz))
      gg%yd_(:,:,nz+1) =gg%yd_(:,:,nz)
     &                -(gg%yd_(:,:,nz-1) -gg%yd_(:,:,nz))
      gg%zd_(:,:,nz+1) =gg%zd_(:,:,nz)
     &                -(gg%zd_(:,:,nz-1) -gg%zd_(:,:,nz))

            DO i=2,nGS
      gg%xd_(:,:,nz+i) =gg%xd_(:,:,nz+1)
     &                -(gg%xd_(:,:,nz+1-i) -gg%xd_(:,:,nz))
      gg%yd_(:,:,nz+i) =gg%yd_(:,:,nz+1)
     &                -(gg%yd_(:,:,nz+1-i) -gg%yd_(:,:,nz))
      gg%zd_(:,:,nz+i) =gg%zd_(:,:,nz+1)
     &                -(gg%zd_(:,:,nz+1-i) -gg%zd_(:,:,nz))
            ENDDO
!$$$$$

      END SUBROUTINE ExpandPointSimple
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Expand coordinate information onto ghost nodes
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ExpandPointCopy(gg,nx,ny,nz)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data,                ONLY:gridData,geo_gl
      USE control_Data,             ONLY:nx_gl,ny_gl,nz_gl,ib
      USE boundary_Data_OOP,        ONLY:bF
      USE fundamental_Constants


      IMPLICIT NONE

      TYPE(gridData),INTENT(INOUT)::gg
      INTEGER,INTENT(IN)::nx,ny,nz

!     cB    conBlock
      INTEGER::i,j,cB,m
      REAL(8),DIMENSION(:),ALLOCATABLE::dx, dy, dz

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     Copy from adjacent block
!     The code should be modified if the grid has a singular point.
!     e.g. O-grid
!
!     face orientation:
!                       fo_north
!              |-----------------------|
!              |                       |
!     fo_west  |         block         | fo_east
!              |                       |
!              |-----------------------|
!     eta               fo_south
!     ^
!     |--> xi
!
!
      IF (bf(ib,fo_west)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_west)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_west)%connBlock
!        Matching case
         IF    (ny.eq.ny_gl(cb)) THEN
            DO i=1,nGS
               gg%xd_(1-i,:,:)=geo_gl(cB)%xd_(nx_gl(cb)-i+1,:,:)
               gg%yd_(1-i,:,:)=geo_gl(cB)%yd_(nx_gl(cb)-i+1,:,:)
               gg%zd_(1-i,:,:)=geo_gl(cB)%zd_(nx_gl(cb)-i+1,:,:)
            ENDDO
!        Non-matching case - coarse
         ELSEIF(ny.lt.ny_gl(cb)) THEN
            m=ny_gl(cB)/ny
            DO i=1,nGS
               DO j=1,ny
                  gg%xd_(1-i,j,:)=SUM(geo_gl(cB)%xd_(nx_gl(cb)-i+1,m*j-m+1:m*j,:),DIM=1)/m
                  gg%yd_(1-i,j,:)=SUM(geo_gl(cB)%yd_(nx_gl(cb)-i+1,m*j-m+1:m*j,:),DIM=1)/m
                  gg%zd_(1-i,j,:)=SUM(geo_gl(cB)%zd_(nx_gl(cb)-i+1,m*j-m+1:m*j,:),DIM=1)/m
               ENDDO
               gg%xd_(1-i,(/0,ny+1/),:)=gg%xd_(1-i,(/1,ny/),:)-(gg%xd_(1-i,(/2,ny-1/),:)-gg%xd_(1-i,(/1,ny/),:))
               gg%yd_(1-i,(/0,ny+1/),:)=gg%yd_(1-i,(/1,ny/),:)-(gg%yd_(1-i,(/2,ny-1/),:)-gg%yd_(1-i,(/1,ny/),:))
               gg%zd_(1-i,(/0,ny+1/),:)=gg%zd_(1-i,(/1,ny/),:)-(gg%zd_(1-i,(/2,ny-1/),:)-gg%zd_(1-i,(/1,ny/),:))
               gg%xd_(1-i,(/-1,ny+2/),:)=gg%xd_(1-i,(/0,ny+1/),:)-(gg%xd_(1-i,(/2,ny-1/),:)-gg%xd_(1-i,(/1,ny/),:))
               gg%yd_(1-i,(/-1,ny+2/),:)=gg%yd_(1-i,(/0,ny+1/),:)-(gg%yd_(1-i,(/2,ny-1/),:)-gg%yd_(1-i,(/1,ny/),:))
               gg%zd_(1-i,(/-1,ny+2/),:)=gg%zd_(1-i,(/0,ny+1/),:)-(gg%zd_(1-i,(/2,ny-1/),:)-gg%zd_(1-i,(/1,ny/),:))
            ENDDO
!        Non-matching case - fine
         ELSEIF(ny.gt.ny_gl(cb)) THEN
            m=ny/ny_gl(cB)
            IF (ALLOCATED(dx)) DEALLOCATE(dx, dy, dz)
            ALLOCATE(dx(1-nGS:nz+nGS),dy(1-nGS:nz+nGS),dz(1-nGS:nz+nGS))
            DO i=1,nGS
               DO j=1-ngs,ny+ngs
                  dx= geo_gl(cB)%xd_(nx_gl(cb)-i+1,(j-1)/m+2,:)
     &               -geo_gl(cB)%xd_(nx_gl(cb)-i+1,(j-1)/m  ,:)
                  dy= geo_gl(cB)%yd_(nx_gl(cb)-i+1,(j-1)/m+2,:)
     &               -geo_gl(cB)%yd_(nx_gl(cb)-i+1,(j-1)/m  ,:)
                  dz= geo_gl(cB)%zd_(nx_gl(cb)-i+1,(j-1)/m+2,:)
     &               -geo_gl(cB)%zd_(nx_gl(cb)-i+1,(j-1)/m  ,:)
                  gg%xd_(1-i,j,:)= geo_gl(cB)%xd_(nx_gl(cb)-i+1,(j-1)/m+1,:)
     &                            +dx*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%yd_(1-i,j,:)= geo_gl(cB)%yd_(nx_gl(cb)-i+1,(j-1)/m+1,:)
     &                            +dy*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%zd_(1-i,j,:)= geo_gl(cB)%zd_(nx_gl(cb)-i+1,(j-1)/m+1,:)
     &                            +dz*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
               ENDDO
            ENDDO
         ENDIF

      ENDIF

      IF (bf(ib,fo_east)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_east)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_east)%connBlock
!        Matching case
         IF    (ny.eq.ny_gl(cb)) THEN
            DO i=1,nGS
               gg%xd_(nx+i,:,:)=geo_gl(cB)%xd_(i,:,:)
               gg%yd_(nx+i,:,:)=geo_gl(cB)%yd_(i,:,:)
               gg%zd_(nx+i,:,:)=geo_gl(cB)%zd_(i,:,:)
            ENDDO
!        Non-matching case - coarse
         ELSEIF(ny.lt.ny_gl(cb)) THEN
            m=ny_gl(cB)/ny
            DO i=1,nGS
               DO j=1,ny
                  gg%xd_(nx+i,j,:)=SUM(geo_gl(cB)%xd_(i,m*j-m+1:m*j,:),DIM=1)/m
                  gg%yd_(nx+i,j,:)=SUM(geo_gl(cB)%yd_(i,m*j-m+1:m*j,:),DIM=1)/m
                  gg%zd_(nx+i,j,:)=SUM(geo_gl(cB)%zd_(i,m*j-m+1:m*j,:),DIM=1)/m
               ENDDO
               gg%xd_(nx+i,(/0,ny+1/),:)=gg%xd_(nx+i,(/1,ny/),:)-(gg%xd_(nx+i,(/2,ny-1/),:)-gg%xd_(nx+i,(/1,ny/),:))
               gg%yd_(nx+i,(/0,ny+1/),:)=gg%yd_(nx+i,(/1,ny/),:)-(gg%yd_(nx+i,(/2,ny-1/),:)-gg%yd_(nx+i,(/1,ny/),:))
               gg%zd_(nx+i,(/0,ny+1/),:)=gg%zd_(nx+i,(/1,ny/),:)-(gg%zd_(nx+i,(/2,ny-1/),:)-gg%zd_(nx+i,(/1,ny/),:))
               gg%xd_(nx+i,(/-1,ny+2/),:)=gg%xd_(nx+i,(/0,ny+1/),:)-(gg%xd_(nx+i,(/2,ny-1/),:)-gg%xd_(nx+i,(/1,ny/),:))
               gg%yd_(nx+i,(/-1,ny+2/),:)=gg%yd_(nx+i,(/0,ny+1/),:)-(gg%yd_(nx+i,(/2,ny-1/),:)-gg%yd_(nx+i,(/1,ny/),:))
               gg%zd_(nx+i,(/-1,ny+2/),:)=gg%zd_(nx+i,(/0,ny+1/),:)-(gg%zd_(nx+i,(/2,ny-1/),:)-gg%zd_(nx+i,(/1,ny/),:))
            ENDDO


!        Non-matching case - fine
         ELSEIF(ny.gt.ny_gl(cb)) THEN
            m=ny/ny_gl(cB)
            IF (ALLOCATED(dx)) DEALLOCATE(dx, dy, dz)
            ALLOCATE(dx(1-nGS:nz+nGS),dy(1-nGS:nz+nGS),dz(1-nGS:nz+nGS))
            DO i=1,nGS
               DO j=1-nGS,ny+nGS
                  dx= geo_gl(cB)%xd_(i,(j-1)/m+2,:)
     &               -geo_gl(cB)%xd_(i,(j-1)/m  ,:)
                  dy= geo_gl(cB)%yd_(i,(j-1)/m+2,:)
     &               -geo_gl(cB)%yd_(i,(j-1)/m  ,:)
                  dz= geo_gl(cB)%zd_(i,(j-1)/m+2,:)
     &               -geo_gl(cB)%zd_(i,(j-1)/m  ,:)
                  gg%xd_(nx+i,j,:)= geo_gl(cB)%xd_(i,(j-1)/m+1,:)
     &                             +dx*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%yd_(nx+i,j,:)= geo_gl(cB)%yd_(i,(j-1)/m+1,:)
     &                             +dy*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%zd_(nx+i,j,:)= geo_gl(cB)%zd_(i,(j-1)/m+1,:)
     &                             +dz*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
               ENDDO
            ENDDO
         ENDIF
      ENDIF

      IF (bf(ib,fo_south)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_south)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_south)%connBlock
!        Matching case
         IF    (nx.eq.nx_gl(cb)) THEN
            DO i=1,nGS
               gg%xd_(:,1-i,:)=geo_gl(cB)%xd_(:,ny_gl(cb)-i+1,:)
               gg%yd_(:,1-i,:)=geo_gl(cB)%yd_(:,ny_gl(cb)-i+1,:)
               gg%zd_(:,1-i,:)=geo_gl(cB)%zd_(:,ny_gl(cb)-i+1,:)
            ENDDO
!        Non-matching case - coarse
         ELSEIF(nx.lt.nx_gl(cb)) THEN
            m=nx_gl(cB)/nx
            DO i=1,nGS
               DO j=1,nx
                  gg%xd_(j,1-i,:)=SUM(geo_gl(cB)%xd_(m*j-m+1:m*j,ny_gl(cb)-i+1,:),DIM=1)/m
                  gg%yd_(j,1-i,:)=SUM(geo_gl(cB)%yd_(m*j-m+1:m*j,ny_gl(cb)-i+1,:),DIM=1)/m
                  gg%zd_(j,1-i,:)=SUM(geo_gl(cB)%zd_(m*j-m+1:m*j,ny_gl(cb)-i+1,:),DIM=1)/m
               ENDDO
               gg%xd_((/0,nx+1/),1-i,:)=gg%xd_((/1,nx/),1-i,:)-(gg%xd_((/2,nx-1/),1-i,:)-gg%xd_((/1,nx/),1-i,:))
               gg%yd_((/0,nx+1/),1-i,:)=gg%yd_((/1,nx/),1-i,:)-(gg%yd_((/2,nx-1/),1-i,:)-gg%yd_((/1,nx/),1-i,:))
               gg%zd_((/0,nx+1/),1-i,:)=gg%zd_((/1,nx/),1-i,:)-(gg%zd_((/2,nx-1/),1-i,:)-gg%zd_((/1,nx/),1-i,:))
               gg%xd_((/-1,nx+2/),1-i,:)=gg%xd_((/0,nx+1/),1-i,:)-(gg%xd_((/2,nx-1/),1-i,:)-gg%xd_((/1,nx/),1-i,:))
               gg%yd_((/-1,nx+2/),1-i,:)=gg%yd_((/0,nx+1/),1-i,:)-(gg%yd_((/2,nx-1/),1-i,:)-gg%yd_((/1,nx/),1-i,:))
               gg%zd_((/-1,nx+2/),1-i,:)=gg%zd_((/0,nx+1/),1-i,:)-(gg%zd_((/2,nx-1/),1-i,:)-gg%zd_((/1,nx/),1-i,:))
            ENDDO


!        Non-matching case - fine
         ELSEIF(nx.gt.nx_gl(cb)) THEN
            m=nx/nx_gl(cB)
            IF (ALLOCATED(dx)) DEALLOCATE(dx, dy, dz)
            ALLOCATE(dx(1-nGS:nz+nGS),dy(1-nGS:nz+nGS),dz(1-nGS:nz+nGS))
            DO i=1,nGS
               DO j=1-nGS,nx+nGS
                  dx= geo_gl(cB)%xd_((j-1)/m+2,ny_gl(cb)-i+1,:)
     &               -geo_gl(cB)%xd_((j-1)/m  ,ny_gl(cb)-i+1,:)
                  dy= geo_gl(cB)%yd_((j-1)/m+2,ny_gl(cb)-i+1,:)
     &               -geo_gl(cB)%yd_((j-1)/m  ,ny_gl(cb)-i+1,:)
                  dz= geo_gl(cB)%zd_((j-1)/m+2,ny_gl(cb)-i+1,:)
     &               -geo_gl(cB)%zd_((j-1)/m  ,ny_gl(cb)-i+1,:)
                  gg%xd_(j,1-i,:)= geo_gl(cB)%xd_((j-1)/m+1,ny_gl(cb)-i+1,:)
     &                            +dx*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%yd_(j,1-i,:)= geo_gl(cB)%yd_((j-1)/m+1,ny_gl(cb)-i+1,:)
     &                            +dy*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%zd_(j,1-i,:)= geo_gl(cB)%zd_((j-1)/m+1,ny_gl(cb)-i+1,:)
     &                            +dz*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
               ENDDO
            ENDDO
         ENDIF

      ENDIF

      IF (bf(ib,fo_north)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_north)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_north)%connBlock
!        Matching case
         IF    (nx.eq.nx_gl(cb)) THEN
            DO i=1,nGS
               gg%xd_(:,ny+i,:)=geo_gl(cB)%xd_(:,i,:)
               gg%yd_(:,ny+i,:)=geo_gl(cB)%yd_(:,i,:)
               gg%zd_(:,ny+i,:)=geo_gl(cB)%zd_(:,i,:)
            ENDDO
!        Non-matching case - coarse
         ELSEIF(nx.lt.nx_gl(cb)) THEN
            m=nx_gl(cB)/nx
            DO i=1,nGS
               DO j=1,nx
                  gg%xd_(j,ny+i,:)=SUM(geo_gl(cB)%xd_(m*j-m+1:m*j,i,:),DIM=1)/m
                  gg%yd_(j,ny+i,:)=SUM(geo_gl(cB)%yd_(m*j-m+1:m*j,i,:),DIM=1)/m
                  gg%zd_(j,ny+i,:)=SUM(geo_gl(cB)%zd_(m*j-m+1:m*j,i,:),DIM=1)/m
               ENDDO
               gg%xd_((/0,nx+1/),ny+i,:)=gg%xd_((/1,nx/),ny+i,:)-(gg%xd_((/2,nx-1/),ny+i,:)-gg%xd_((/1,nx/),ny+i,:))
               gg%yd_((/0,nx+1/),ny+i,:)=gg%yd_((/1,nx/),ny+i,:)-(gg%yd_((/2,nx-1/),ny+i,:)-gg%yd_((/1,nx/),ny+i,:))
               gg%zd_((/0,nx+1/),ny+i,:)=gg%zd_((/1,nx/),ny+i,:)-(gg%zd_((/2,nx-1/),ny+i,:)-gg%zd_((/1,nx/),ny+i,:))
               gg%xd_((/-1,nx+2/),ny+i,:)=gg%xd_((/0,nx+1/),ny+i,:)-(gg%xd_((/2,nx-1/),ny+i,:)-gg%xd_((/1,nx/),ny+i,:))
               gg%yd_((/-1,nx+2/),ny+i,:)=gg%yd_((/0,nx+1/),ny+i,:)-(gg%yd_((/2,nx-1/),ny+i,:)-gg%yd_((/1,nx/),ny+i,:))
               gg%zd_((/-1,nx+2/),ny+i,:)=gg%zd_((/0,nx+1/),ny+i,:)-(gg%zd_((/2,nx-1/),ny+i,:)-gg%zd_((/1,nx/),ny+i,:))
            ENDDO
!        Non-matching case - fine
         ELSEIF(nx.gt.nx_gl(cb)) THEN
            m=nx/nx_gl(cB)
            IF (ALLOCATED(dx)) DEALLOCATE(dx, dy, dz)
            ALLOCATE(dx(1-nGS:nz+nGS),dy(1-nGS:nz+nGS),dz(1-nGS:nz+nGS))
            DO i=1,nGS
               DO j=1-nGS,nx+nGS
                  dx= geo_gl(cB)%xd_((j-1)/m+2,i,:)
     &               -geo_gl(cB)%xd_((j-1)/m  ,i,:)
                  dy= geo_gl(cB)%yd_((j-1)/m+2,i,:)
     &               -geo_gl(cB)%yd_((j-1)/m  ,i,:)
                  dz= geo_gl(cB)%zd_((j-1)/m+2,i,:)
     &               -geo_gl(cB)%zd_((j-1)/m  ,i,:)
                  gg%xd_(j,ny+i,:)= geo_gl(cB)%xd_((j-1)/m+1,i,:)
     &                                +dx*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%yd_(j,ny+i,:)= geo_gl(cB)%yd_((j-1)/m+1,i,:)
     &                                +dy*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
                  gg%zd_(j,ny+i,:)= geo_gl(cB)%zd_((j-1)/m+1,i,:)
     &                                +dz*((MOD(j-1,m)+0.5d0)/m-0.5d0)*0.5d0
               ENDDO
            ENDDO
         ENDIF

      ENDIF

!     no nonmatching interface in zeta(k) direction
      IF (bf(ib,fo_back)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_back)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_back)%connBlock
         DO i=1,nGS
            gg%xd_(:,:,1-i)=geo_gl(cB)%xd_(:,:,nz_gl(cB)-i+1)
            gg%yd_(:,:,1-i)=geo_gl(cB)%yd_(:,:,nz_gl(cB)-i+1)
            gg%zd_(:,:,1-i)=geo_gl(cB)%zd_(:,:,nz_gl(cB)-i+1)
         ENDDO
      ENDIF

      IF (bf(ib,fo_front)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_front)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_front)%connBlock
         DO i=1,nGS
            gg%xd_(:,:,nz+i)=geo_gl(cB)%xd_(:,:,i)
            gg%yd_(:,:,nz+i)=geo_gl(cB)%yd_(:,:,i)
            gg%zd_(:,:,nz+i)=geo_gl(cB)%zd_(:,:,i)
         ENDDO
      ENDIF

      END SUBROUTINE ExpandPointCopy



