!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ChemReversibleEquilibrium(ro, intEnergy, yfs, tInit)
!***********************************************************************
!***CHEMICAL REACTION
!
! Balancing reactions
!	2CH4 +  O2 =  2CO + 4H2
!
!	2H2  +  O2 = 2H2O
!	2CO  +  O2 = 2CO2
!	  O  +   H =   OH
!	        O2 =   2O
!	        H2 =   2H
!
!***********************************************************************
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
      USE thermal_Property,         ONLY:td,wMol,rs
      USE thermocoeff,              ONLY:NASAPolynomial,NASAPolynomial7,
     &                                   NASAPolynomial9
      USE control_Data,             ONLY:ns, nt

      USE lapack95
      USE f95_precision

      IMPLICIT NONE

      INTEGER,PARAMETER::
     &                   H2  = 1,
     &                   O2  = 2,
     &                   H2O = 3,
     &                   CH4 = 4,
     &                   CO  = 5,
     &                   CO2 = 6,
     &                   N2  = 7,
     &                   H   = 8,
     &                   O   = 9,
     &                   OH  = 10

      REAL(8),PARAMETER::
     &      threshold = 1.0d-4

      REAL(8),INTENT(IN)::
     &      ro, intEnergy
      REAL(8),INTENT(INOUT),DIMENSION(ns)::
     &      yfs
      REAL(8),INTENT(IN)::
     &      tInit
      REAL(8)::t, t_old, O_c, C_c, H_c, N2_c, rm, phi, yfsReact, dxScale
      REAL(8),DIMENSION(ns)::
     &      fs, helmFE, yfsNew, dx, f
      REAL(8),DIMENSION(6)::
     &      dHelmFE, K
      INTEGER,DIMENSION(ns)::
     &      IPIV
!              IPIV: not a meaninful variable. Pivoting?
      REAL(8),DIMENSION(ns,ns)::
     &      J
      INTEGER::is, i, iter, info
      CLASS(NASAPolynomial),ALLOCATABLE::tdrm

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     Get concentration of participating atoms
!        which is invarient
      H_c= 2.0d0*yfs( H2) + 2.0d0*yfs(H2O) + 4.0d0*yfs(CH4) +       yfs(  H) + yfs(OH)
      O_c= 2.0d0*yfs( O2) +       yfs(H2O) +       yfs( CO) + 2.0d0*yfs(CO2) + yfs( O) + yfs(OH)
      C_c=       yfs(CH4) +       yfs( CO) +       yfs(CO2)
      N2_c=      yfs( N2)

      IF (O_c.lt.1.0d-12) RETURN
      IF (C_c.lt.1.0d-12) RETURN
      IF (H_c.lt.1.0d-12) RETURN
      IF (N2_c.lt.1.0d-12) RETURN

      IF    (nt.eq.7) THEN
            ALLOCATE(NASAPolynomial7::tdrm)
      ELSEIF(nt.eq.9) THEN
            ALLOCATE(NASAPolynomial9::tdrm)
      ELSE
            CALL ErrorStop(2105)
      ENDIF

!     Get initial temperature
      t=tInit
      fs=yfs*wMol/ro
      rm=SUM(rs*fs)
      CALL tdrm%makeMixture(Td,fs,rs)
      CALL tdrm%putTemp(t,intEnergy,rm)

!     BEGINNING of fuel consumption code
!     BEGINNING of fuel consumption code
!     BEGINNING of fuel consumption code
!        Removing the code below does not affect the result significantly
!        in some test.
!     - DEPRECATED -

!     Proceed chemical reaction sufficiently
!        if temperature is low
      IF (t.lt.1500.0d0 .and. .false.) THEN
!     Consume CH4
!	      2CH4 +  O2 =  2CO + 4H2
         phi=yfs(CH4)/(yfs(O2)*2.0d0)
         IF (yfs(O2).eq.0.0d0) THEN
            yfsReact=0.0d0
         ELSEIF (phi.gt.1.0d0) THEN
            yfsReact=yfs(O2)
         ELSE
            yfsReact=yfs(CH4)/2.0d0
         ENDIF

         yfsReact=yfsReact * 0.9d0
         yfs(CH4)=yfs(CH4)-2.0d0*yfsReact
         yfs( O2)=yfs( O2)-      yfsReact
         yfs( CO)=yfs( CO)+2.0d0*yfsReact
         yfs( H2)=yfs( H2)+4.0d0*yfsReact

!     Consume H2
!	      2H2  +  O2 = 2H2O
         phi=yfs(H2)/(yfs(O2)*2.0d0)
         IF (yfs(O2).eq.0.0d0) THEN
            yfsReact=0.0d0
         ELSEIF (phi.gt.1.0d0) THEN
            yfsReact=yfs(O2)
         ELSE
            yfsReact=yfs(H2)/2.0d0
         ENDIF

         yfsReact=yfsReact * 0.9d0
         yfs( H2)=yfs( H2)-2.0d0*yfsReact
         yfs( O2)=yfs( O2)-      yfsReact
         yfs(H2O)=yfs(H2O)+2.0d0*yfsReact

!     Consume CO
!	      2CO  +  O2 = 2CO2
         phi=yfs(CO)/(yfs(O2)*2.0d0)
         IF (yfs(O2).eq.0.0d0) THEN
            yfsReact=0.0d0
         ELSEIF (phi.gt.1.0d0) THEN
            yfsReact=yfs(O2)
         ELSE
            yfsReact=yfs(CO)/2.0d0
         ENDIF

         yfsReact=yfsReact * 0.9d0
         yfs( CO)=yfs( CO)-2.0d0*yfsReact
         yfs( O2)=yfs( O2)-      yfsReact
         yfs(CO2)=yfs(CO2)+2.0d0*yfsReact
      ENDIF

      IF (t.lt.1500.0d0.and..false.) THEN
!     Consume CH4
!	       CH4 + 2O2 ->  CO2 + 2H2O
         phi=yfs(CH4)*2.0d0/yfs(O2)
         IF (yfs(O2).eq.0.0d0) THEN
            yfsReact=0.0d0
         ELSEIF (phi.gt.1.0d0) THEN
            yfsReact=yfs(O2)/2.0d0
         ELSE
            yfsReact=yfs(CH4)
         ENDIF

         yfsReact=yfsReact*0.9d0
         yfs(CH4)=yfs(CH4)-  yfsReact
         yfs( O2)=yfs( O2)-2*yfsReact
         yfs(CO2)=yfs(CO2)+  yfsReact
         yfs(H2O)=yfs(H2O)+2*yfsReact
      ENDIF
!     END of fuel consumption code
!     END of fuel consumption code
!     END of fuel consumption code


      fs=yfs*wMol/ro
      rm=SUM(rs*fs)
      CALL tdrm%makeMixture(Td,fs,rs)
      CALL tdrm%putTemp(t,intEnergy,rm)

!     When temperature is too(?) low, do not try to run equilibrium code
      IF (t.lt.400.0d0) RETURN

!     Initial guess
      WHERE(yfs.le.0.0d0)
         yfs=1.0d-10
      ENDWHERE

!     Concentration of Nitrogen does not change
      yfs(N2)=N2_c

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      Newton: DO iter=1,50
!     Iteration by Newton method
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     1. initialize Jacobian
      J=0.0d0

!     2. obtain Free energy (Helmholtz function) for equilibrium constants
!        when temperature change is significient (for cost reduction)
      IF (iter.eq.1) t_old=10000.0d0

      IF (ABS(t-t_old)/t.gt.1.0d-4) THEN
         DO is=1,ns
            SELECT CASE (is)
               CASE (H2, O2, H2O, CH4, CO, CO2, H, O, OH)
                  helmFE(is)=Td(is)%helmFE(t)
               CASE DEFAULT
                  CONTINUE
            ENDSELECT
         ENDDO

!                      (          reactant         ) (      product            )
!	                             2CH4 +          O2  =              2CO +             4H2
         dHelmFE(1)=2.0d0*helmFE(CH4)+  helmFE( O2) - 2.0d0*helmFE( CO)-4.0d0*helmFE( H2)

!	                              2H2 +          O2  =             2H2O
         dHelmFE(2)=2.0d0*helmFE( H2)+  helmFE( O2) - 2.0d0*helmFE(H2O)

!                                2CO +          O2  =             2CO2
         dHelmFE(3)=2.0d0*helmFE( CO)+  helmFE( O2) - 2.0d0*helmFE(CO2)

!	                                O +           H  =               OH
         dHelmFE(4)=      helmFE(  O)+  helmFE(  H) -       helmFE( OH)

!	                               2O                =               O2
         dHelmFE(5)=2.0d0*helmFE(  O)               -       helmFE( O2)

!	                               2H                =               H2
         dHelmFE(6)=2.0d0*helmFE(  H)               -       helmFE( H2)

         K = exp(dHelmFE)
!         reactant: +positive;   product: -negative
!           therefore, no - sign for dHelmFE
      ENDIF


!     3. Evaluation of values of the system of equations and its jacobian
      f(1) =yfs(H)+2.0d0*yfs(H2)+yfs(OH)+2.0d0*yfs(H2O)+4.0d0*yfs(CH4)   - H_c
      f(2) =yfs(O)+2.0d0*yfs(O2)+yfs(OH)+yfs(H2O)+yfs(CO)+2.0d0*yfs(CO2) - O_c
      f(3) =yfs(CH4) + yfs(CO) + yfs(CO2)                                - C_c
      f(4) =yfs(N2)                                                      - N2_c

!            (      reactant             ) (      product      )
!               2CH4     +     O2         =     2CO     +    4H2
      f(5) = yfs(CH4)**2 * yfs(O2) * K(1) - yfs( CO)**2 * yfs(H2)**4

!                2H2     +     O2         =    2H2O
      f(6) = yfs( H2)**2 * yfs(O2) * K(2) - yfs(H2O)**2

!                2CO     +     O2         =    2CO2
      f(7) = yfs( CO)**2 * yfs(O2) * K(3) - yfs(CO2)**2

!                  O     +      H         =      OH
      f(8) = yfs(  O)    * yfs( H) * K(4) - yfs( OH)

!	          2O                      =      O2
      f(9) = yfs(  O)**2           * K(5) - yfs( O2)

!	          2H                      =      H2
      f(10)= yfs(  H)**2           * K(6) - yfs( H2)


      J(1, H2) = 2.0d0
      J(1,H2O) = 2.0d0
      J(1,CH4) = 4.0d0
      J(1,  H) = 1.0d0
      J(1, OH) = 1.0d0

      J(2, O2) = 2.0d0
      J(2,H2O) = 1.0d0
      J(2, CO) = 1.0d0
      J(2,CO2) = 2.0d0
      J(2,  O) = 1.0d0
      J(2, OH) = 1.0d0

      J(3,CH4) = 1.0d0
      J(3, CO) = 1.0d0
      J(3,CO2) = 1.0d0

      J(4, N2) = 1.0d0

      J(5,CH4) = 2.0d0*yfs(CH4)    * yfs(O2)    * K(1)
      J(5, O2) =       yfs(CH4)**2              * K(1)
      J(5, CO) =-2.0d0*yfs( CO)    * yfs(H2)**4
      J(5, H2) =-4.0d0*yfs( CO)**2 * yfs(H2)**3

      J(6, H2) = 2.0d0*yfs( H2)    * yfs(O2)    * K(2)
      J(6, O2) =       yfs( H2)**2              * K(2)
      J(6,H2O) =-2.0d0*yfs(H2O)

      J(7, CO) = 2.0d0*yfs( CO)    * yfs(O2)    * K(3)
      J(7, O2) =       yfs( CO)**2              * K(3)
      J(7,CO2) =-2.0d0*yfs(CO2)

      J(8,  O) =       yfs(  H)                 * K(4)
      J(8,  H) =       yfs(  O)                 * K(4)
      J(8, OH) =-1.0d0

      J(9,  O) = 2.0d0*yfs(  O)                 * K(5)
      J(9, O2) =-1.0d0

      J(10,  H) = 2.0d0*yfs(  H)                 * K(6)
      J(10, H2) =-1.0d0

!     get inverse of the Jacobian
      CALL GETRF( J, IPIV, info)
      IF (info.ne.0) THEN
         PRINT *,'singular jacobian 1', info
         print *,"yfs",yfs
         print *,"K",k
         RETURN
      ENDIF
!     J*dx=f => f turns into dx(?) not good explanation...
      CALL GETRS( J, IPIV, f, TRANS='N', INFO=info)
      IF (info.ne.0) THEN
         PRINT *,'singular jacobian 2', info
         RETURN
      ENDIF

!     INFORMATION
!        If info=i, this means that ith "column" is linearly dependent with
!        one of the preceding columns.

!     If dx is too big, modify its magnitude
      dxScale = MAXVAL(ABS(f/yfs))

      IF (dxScale.lt.threshold) EXIT Newton

      IF (dxScale.gt.0.9d0) THEN
         f=f/dxScale*0.9d0
      ENDIF

!     New concentrations
      yfsNew = yfs - f

!     update temperature, mass fraction
      t_old=t
      fs=yfsNew*wMol/ro
      rm=SUM(rs*fs)
      CALL tdrm%makeMixture(Td,fs,rs)
      CALL tdrm%putTemp(t,intEnergy,rm)

!     Warn negative temperature
      IF (t.lt.0.0d0) THEN
         PRINT *, "energy, t, t_old, ro, fssum"
         PRINT *, intEnergy, t, t_old, ro, SUM(fs)
         PRINT *, yfs
         PRINT *, yfsNew
         RETURN
      ENDIF

      yfs=yfsNew

!     when new temperature is below 200 K, stop reaction
!        - helmholtz energy would not be correctly calculated.
      IF (t.lt.200.0d0) RETURN

      ENDDO Newton

      END SUBROUTINE
