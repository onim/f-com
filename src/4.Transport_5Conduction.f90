!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE getConduction
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,ib, &
                                      num_nonRadical, nonRadical, &
                                      conductMethod
   USE M_BlockPointData,         ONLY:bl
   USE thermal_Property,         ONLY:rs, vmol, dmol
   USE fundamental_Constants,    ONLY:nGS
   USE M_Transport,              ONLY:pt,pts
   USE Status_Check,              ONLY:meanConductivity

IMPLICIT NONE
   REAL(8)::conduct(ns),condsum,condcalc

   INTEGER::i,j,k,nr,nrr,is,iss

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   meanConductivity=0.0d0

!$OMP PARALLEL DO PRIVATE(i,j,k,conduct,condsum,condcalc,nr,nrr,is,iss)
   _forAllPoints_
#define c   bl(ib)%p(i,j,k)%c
#define t   bl(ib)%p(i,j,k)%t
      ! THERMAL FLUX
      !  1. Eucken method
      IF (conductMethod.eq.'eucken') THEN
         DO is=1,num_nonRadical
            nr=nonRadical(is)
            conduct(nr)=0.25D0                         &
               *(9.0D0*t%cps(nr)-5.0D0*(t%cps(nr)-rs(nr))) &
               *pts(i,j,k,nr)%VISM
         ENDDO

         condsum=0.0d0

         DO is=1,num_nonRadical
            nr=nonRadical(is)
            condcalc=0.0d0

            DO iss=1,num_nonRadical
               nrr=nonRadical(iss)
               IF (nrr.eq.nr) THEN
                  condcalc=condcalc+c%yfs(nrr)
               ELSE
                  condcalc=condcalc+c%yfs(nrr)                &
                     *(1.0d0+SQRT(conduct(nr)*conduct(nrr)) &
                     *vmol(nr,nrr)**2*dmol(nr,nrr))
               ENDIF
            ENDDO

            condsum=condsum+c%yfs(nr)*conduct(nr)/condcalc
         ENDDO

      ! 2. Chung method
      ELSEIF (conductMethod.eq.'chunGS') THEN
            condsum=0.0d0
            print *,'Under construction'
      ! 3. Other method
      ELSE
            condsum=0.0d0
            print *,'Error!! Check your option for conductivity.'
      ENDIF

      pt(i,j,k)%cond=condsum

#undef c
#undef t
   _endAllPoints_
!$OMP END PARALLEL DO

   meanConductivity=meanConductivity/((nx+2*nGS)*(ny+2*nGS)*(nz+2*nGS))

END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE heatDiffusion(dimen)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ib, &
                                      num_nonRadical, nonRadical, &
                                      conductMethod,ns
   USE M_BlockPointData,         ONLY:bl
   USE fundamental_Constants,    ONLY:Prt,nGS
   USE M_Transport,              ONLY:pt, face, faces

   IMPLICIT NONE
   REAL(8)::cndct,ROTRM,qx,qy,qz

   INTEGER,INTENT(IN)::dimen
   INTEGER::i,j,k,ish,jsh,ksh

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ish=0
   jsh=0
   ksh=0
   SELECT CASE(dimen)
      CASE(1)
         ish=1
      CASE(2)
         jsh=1
      CASE(3)
         ksh=1
   END SELECT

!$OMP PARALLEL DO PRIVATE(i,j,k,cndct,ROTRM,qx,qy,qz)
   DO k=nGS,nz+nGS;DO j=nGS,ny+nGS;DO i=nGS,nx+nGS
#define fs  bl(ib)%p(i,j,k)%c%fs
#define u(i,j,k) bl(ib)%p(i,j,k)%c%u
#define v(i,j,k) bl(ib)%p(i,j,k)%c%v
#define w(i,j,k) bl(ib)%p(i,j,k)%c%w
#define cps bl(ib)%p(i,j,k)%t%cps
#define hf  bl(ib)%p(i,j,k)%t%hf
#define face face(i,j,k)
#define faces  faces(i,j,k,:)
      ! Prt: prandtl number
      ! rodif: conductivity (alpha) * density
      ROTRM=face%VIST/Prt

      CNDCT= SUM(cps*fs)*ROTRM + 0.5d0*(pt(i,j,k)%cond+pt(i+ish,j+jsh,k+ksh)%cond)

      QX=CNDCT*face%DTX+SUM(hf*faces%rsfx)
      QY=CNDCT*face%DTY+SUM(hf*faces%rsfy)
      QZ=CNDCT*face%DTZ+SUM(hf*faces%rsfz)

      ! Including friction work...
      face%refx= &
            ( (u(i,j,k)+u(i+ish,j+jsh,k+ksh))*face%rufx &
             +(v(i,j,k)+v(i+ish,j+jsh,k+ksh))*face%rvfx &
             +(w(i,j,k)+w(i+ish,j+jsh,k+ksh))*face%rwfx)&
            +qx
      face%refy= &
            ( (u(i,j,k)+u(i+ish,j+jsh,k+ksh))*face%rufy &
             +(v(i,j,k)+v(i+ish,j+jsh,k+ksh))*face%rvfy &
             +(w(i,j,k)+w(i+ish,j+jsh,k+ksh))*face%rwfy)&
            +qy
      face%refz= &
            ( (u(i,j,k)+u(i+ish,j+jsh,k+ksh))*face%rufz &
             +(v(i,j,k)+v(i+ish,j+jsh,k+ksh))*face%rvfz &
             +(w(i,j,k)+w(i+ish,j+jsh,k+ksh))*face%rwfz)&
            +qz

      ! Excluding friction work...
      !      face%refx=QX
      !      face%REFY=QY
      !      face%REFZ=QZ

#undef fs
#undef u
#undef v
#undef w
#undef cps
#undef hf
#undef face
#undef faces
   ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE
