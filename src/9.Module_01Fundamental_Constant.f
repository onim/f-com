!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     prefixes
!     fo_         Block face orientation
!     ft_         Block face type
!     ga_         Grid Attribute

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** constants and parameters

!     nf          number of governing equation
!     nGS         number of ghost stencil
!     Runi        universal gas constant, J/(mol-K) SI unit
!     RCal        universal gas constant, cal/(mol-K)
!     r2inv       squareroot of 1/2
!     Cs          LES model parameter
!     Prt         turbulent Prandtl number
!     Sct         turbulent Schmidt number
!     alpha,beta  AUSM parameters for subsonic Mach number calculation
!     ijkPosition A position indicated by ijk indices
!                       with point evaluation function sv

      MODULE Fundamental_Constants
      IMPLICIT NONE

      INTEGER,PARAMETER::Db=KIND(1.0d0)

      INTEGER,PARAMETER::NF =5,
     &                   nGS=2,
     &                   ns=8,
     &                   ne=nf+ns

      INTEGER,PARAMETER::left  =1,
     &                   right =2,
     &                   center=3

      INTEGER,PARAMETER::fo_west =1,
     &                   fo_east =2,
     &                   fo_south=3,
     &                   fo_north=4,
     &                   fo_back =5,
     &                   fo_front=6

      INTEGER,PARAMETER::ft_interface    =1,
     &                   ft_interface_MPI=2,
     &                   ft_inlet        =3,
     &                   ft_outlet       =4,
     &                   ft_wall         =5,
     &                   ft_periodic     =6,
     &                   ft_symmetric    =7

      INTEGER,PARAMETER::ga_boundary=1,
     &                   ga_inside=2

      REAL(8),PARAMETER::RUni=8.3144621D0,
     &                   RCal=1.9872041D0,
     &                   r2Inv=0.7071067811865475D0,
     &                   pi=3.14159265758979323846264d0

      REAL(8),PARAMETER::Prt=0.6d0,
     &                   Sct=0.6d0,
     &                   alpha=0.1875d0,
     &                   beta=0.125d0

      CHARACTER(79),PARAMETER::title=
     &      '  Iter Bl   I  J  K     dqMax    dqNorm      Time'//
     &      '     dtMax    CFLMax MaxResQnt'



      TYPE ijkPosition
            INTEGER::i,j,k

      CONTAINS
            PROCEDURE::sv
      ENDTYPE ijkPosition

      CONTAINS
!$$$$$$$$$$$$$$$
!     From 75statuscheck
!$$$$$$$$$$$$$$$
      FUNCTION sv(mnt,var)
      IMPLICIT NONE

      REAL(8)::sv
      REAL(8),DIMENSION(:,:,:),INTENT(IN)::var
      CLASS(ijkPosition)::mnt

      sv=var(mnt%i,mnt%j,mnt%k)

      ENDFUNCTION sv

      END MODULE

