!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE combustionWrapper
! Wrapper for chemical reaction - combustion
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ib,reactionModel
   USE M_BlockPointData,         ONLY:bl
   USE Thermal_Property,         ONLY:wMol
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

   INTEGER::i,j,k
   INTEGER,PARAMETER::Cold=0, &
                      JonesLindstedt=3, &
                      IrreversibleFast=5, &
                      LiWilliams=6

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   SELECT CASE (reactionModel)
   CASE (Cold)
      CONTINUE

   CASE (JonesLindstedt)
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(GUIDED)
      _forInnerPoints_
#define yfs bl(ib)%p(i,j,k)%c%yfs
#define rfs bl(ib)%p(i,j,k)%c%rfs
#define temp   bl(ib)%p(i,j,k)%c%t
#define hf  bl(ib)%p(i,j,k)%t%hf
#define qChem bl(ib)%p(i,j,k)%c%qChem
         yfs=rfs/wMol
         qChem=SUM(yfs*wMol*hf)
         yfs=yfs*1.0D-3
         CALL ChemJL(yfs,temp)
         yfs=yfs*1.0D3
         rfs=yfs*wMol
         qChem=qChem-sum(yfs*wMol*hf)
#undef yfs
#undef rfs
#undef temp
#undef hf
#undef qChem
      _endInnerPoints_
!$OMP END PARALLEL DO


   CASE (IrreversibleFast)
!$OMP PARALLEL DO PRIVATE(i,j,k)
      _forInnerPoints_
#define yfs bl(ib)%p(i,j,k)%c%yfs
#define rfs bl(ib)%p(i,j,k)%c%rfs
#define hf  bl(ib)%p(i,j,k)%t%hf
#define qChem bl(ib)%p(i,j,k)%c%qChem
         yfs=rfs/wMol
         qChem=SUM(yfs*wMol*hf)
         CALL ChemIrreversibleEquilibrium(yfs)
         rfs=yfs*wMol
         qChem=qChem-sum(yfs*wMol*hf)
#undef yfs
#undef rfs
#undef hf
#undef qChem
      _endInnerPoints_
!$OMP END PARALLEL DO

   CASE (LiWilliams)
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(GUIDED)
      _forInnerPoints_
#define yfs bl(ib)%p(i,j,k)%c%yfs
#define rfs bl(ib)%p(i,j,k)%c%rfs
#define temp   bl(ib)%p(i,j,k)%c%t
#define hf  bl(ib)%p(i,j,k)%t%hf
#define qChem bl(ib)%p(i,j,k)%c%qChem
         yfs=rfs/wMol
         qChem=SUM(yfs*wMol*hf)
         CALL ChemLi(yfs,temp)
         rfs=yfs*wMol
         qChem=qChem-sum(yfs*wMol*hf)
#undef yfs
#undef rfs
#undef temp
#undef hf
#undef qChem
      _endInnerPoints_
!$OMP END PARALLEL DO

   CASE DEFAULT
      PRINT *,"No appropriate combustion model assigned."
      CONTINUE
   END SELECT

END SUBROUTINE
