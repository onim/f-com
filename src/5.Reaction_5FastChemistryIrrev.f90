!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
PURE SUBROUTINE ChemIrreversibleEquilibrium(yfs)
! Combustion reaction
!	 CH4 + 2O2 =  CO2 + 2H2O
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE Control_Data,             ONLY:ns

   IMPLICIT NONE

   INTEGER,PARAMETER::&
      O2  = 1, &
      H2O = 2, &
      CH4 = 3, &
      CO2 = 4, &
      N2  = 5

!      H2  = 1, &
!      O2  = 2, &
!      H2O = 3, &
!      CH4 = 4, &
!      CO  = 5, &
!      CO2 = 6, &
!      N2  = 7, &
!      H   = 8, &
!      O   = 9, &
!      OH  = 10

   REAL(8),INTENT(INOUT),DIMENSION(ns)::yfs
   REAL(8)::phi, yfsReact

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     Consume CH4
!	       CH4 + 2O2 =  CO2 + 2H2O
      phi=yfs(CH4)*2.0d0/yfs(O2)
      IF (yfs(O2).eq.0.0d0) THEN
         yfsReact=0.0d0
      ELSEIF (phi.gt.1.0d0) THEN
         yfsReact=yfs(O2)/2.0d0
      ELSE
         yfsReact=yfs(CH4)
      ENDIF

      yfs(CH4)=yfs(CH4)-  yfsReact
      yfs( O2)=yfs( O2)-2*yfsReact
      yfs(CO2)=yfs(CO2)+  yfsReact
      yfs(H2O)=yfs(H2O)+2*yfsReact

END SUBROUTINE
