!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Calculate coordinate difference for metrices
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE CoordDifference(gg,nx,ny,nz)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data,                ONLY:gridData,geo_gl
      USE control_Data,             ONLY:nx_gl,ny_gl,nz_gl,ib
      USE boundary_Data_OOP,        ONLY:bF
      USE fundamental_Constants


      IMPLICIT NONE

      TYPE(gridData),INTENT(INOUT)::gg
      INTEGER,INTENT(IN)::nx,ny,nz
      REAL(8),PARAMETER::h=0.5d0

!     cB    conBlock
      INTEGER::dimen,cB

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      dimen=1
      gg%gxx_=h*(CSHIFT(gg%xd_,1,dimen)-CSHIFT(gg%xd_,-1,dimen))
      gg%gyx_=h*(CSHIFT(gg%yd_,1,dimen)-CSHIFT(gg%yd_,-1,dimen))
      gg%gzx_=h*(CSHIFT(gg%zd_,1,dimen)-CSHIFT(gg%zd_,-1,dimen))

      dimen=2
      gg%gxy_=h*(CSHIFT(gg%xd_,1,dimen)-CSHIFT(gg%xd_,-1,dimen))
      gg%gyy_=h*(CSHIFT(gg%yd_,1,dimen)-CSHIFT(gg%yd_,-1,dimen))
      gg%gzy_=h*(CSHIFT(gg%zd_,1,dimen)-CSHIFT(gg%zd_,-1,dimen))

      dimen=3
      gg%gxz_=h*(CSHIFT(gg%xd_,1,dimen)-CSHIFT(gg%xd_,-1,dimen))
      gg%gyz_=h*(CSHIFT(gg%yd_,1,dimen)-CSHIFT(gg%yd_,-1,dimen))
      gg%gzz_=h*(CSHIFT(gg%zd_,1,dimen)-CSHIFT(gg%zd_,-1,dimen))


      IF (bf(ib,fo_west)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_west)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_west)%connBlock
         gg%gxx_(1-nGS,:,:)=gg%xd_(2-nGS,:,:)-gg%xd_(1-nGS,:,:)
         gg%gyx_(1-nGS,:,:)=gg%yd_(2-nGS,:,:)-gg%yd_(1-nGS,:,:)
         gg%gzx_(1-nGS,:,:)=gg%zd_(2-nGS,:,:)-gg%zd_(1-nGS,:,:)
      ELSE
         gg%gxx_(1-nGS,:,:)=h*( gg%xd_(2-nGS,:,:)-(gg%xd_(0,:,:)-(gg%xd_(1+nGS,:,:)-gg%xd_(1,:,:))))
         gg%gyx_(1-nGS,:,:)=h*( gg%yd_(2-nGS,:,:)-(gg%yd_(0,:,:)-(gg%yd_(1+nGS,:,:)-gg%yd_(1,:,:))))
         gg%gzx_(1-nGS,:,:)=h*( gg%zd_(2-nGS,:,:)-(gg%zd_(0,:,:)-(gg%zd_(1+nGS,:,:)-gg%zd_(1,:,:))))
      ENDIF

      IF (bf(ib,fo_east)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_east)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_east)%connBlock
         gg%gxx_(nx+nGS,:,:)=gg%xd_(nx+nGS,:,:)-gg%xd_(nx+nGS-1,:,:)
         gg%gyx_(nx+nGS,:,:)=gg%yd_(nx+nGS,:,:)-gg%yd_(nx+nGS-1,:,:)
         gg%gzx_(nx+nGS,:,:)=gg%zd_(nx+nGS,:,:)-gg%zd_(nx+nGS-1,:,:)
      ELSE
         gg%gxx_(nx+nGS,:,:)=h*( gg%xd_(nx+1,:,:)-(gg%xd_(nx-nGS,:,:)-gg%xd_(nx,:,:))-gg%xd_(nx-1+nGS,:,:))
         gg%gyx_(nx+nGS,:,:)=h*( gg%yd_(nx+1,:,:)-(gg%yd_(nx-nGS,:,:)-gg%yd_(nx,:,:))-gg%yd_(nx-1+nGS,:,:))
         gg%gzx_(nx+nGS,:,:)=h*( gg%zd_(nx+1,:,:)-(gg%zd_(nx-nGS,:,:)-gg%zd_(nx,:,:))-gg%zd_(nx-1+nGS,:,:))
      ENDIF

      IF (bf(ib,fo_south)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_south)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_south)%connBlock
         gg%gxy_(:,1-nGS,:)=gg%xd_(:,2-nGS,:)-gg%xd_(:,1-nGS,:)
         gg%gyy_(:,1-nGS,:)=gg%yd_(:,2-nGS,:)-gg%yd_(:,1-nGS,:)
         gg%gzy_(:,1-nGS,:)=gg%zd_(:,2-nGS,:)-gg%zd_(:,1-nGS,:)
      ELSE
         gg%gxy_(:,1-nGS,:)=h*( gg%xd_(:,2-nGS,:)-(gg%xd_(:,0,:)-(gg%xd_(:,1+nGS,:)-gg%xd_(:,1,:))))
         gg%gyy_(:,1-nGS,:)=h*( gg%yd_(:,2-nGS,:)-(gg%yd_(:,0,:)-(gg%yd_(:,1+nGS,:)-gg%yd_(:,1,:))))
         gg%gzy_(:,1-nGS,:)=h*( gg%zd_(:,2-nGS,:)-(gg%zd_(:,0,:)-(gg%zd_(:,1+nGS,:)-gg%zd_(:,1,:))))
      ENDIF

      IF (bf(ib,fo_north)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_north)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_north)%connBlock
         gg%gxy_(:,ny+nGS,:)=gg%xd_(:,ny+nGS,:)-gg%xd_(:,ny+nGS-1,:)
         gg%gyy_(:,ny+nGS,:)=gg%yd_(:,ny+nGS,:)-gg%yd_(:,ny+nGS-1,:)
         gg%gzy_(:,ny+nGS,:)=gg%zd_(:,ny+nGS,:)-gg%zd_(:,ny+nGS-1,:)
      ELSE
         gg%gxy_(:,ny+nGS,:)=h*( gg%xd_(:,ny+1,:)
     &                         -(gg%xd_(:,ny-nGS,:)-gg%xd_(:,ny,:))-gg%xd_(:,ny-1+nGS,:))
         gg%gyy_(:,ny+nGS,:)=h*( gg%yd_(:,ny+1,:)
     &                         -(gg%yd_(:,ny-nGS,:)-gg%yd_(:,ny,:))-gg%yd_(:,ny-1+nGS,:))
         gg%gzy_(:,ny+nGS,:)=h*( gg%zd_(:,ny+1,:)
     &                         -(gg%zd_(:,ny-nGS,:)-gg%zd_(:,ny,:))-gg%zd_(:,ny-1+nGS,:))
      ENDIF

      IF (bf(ib,fo_back)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_back)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_back)%connBlock
         gg%gxz_(:,:,1-nGS)=gg%xd_(:,:,2-nGS)-gg%xd_(:,:,1-nGS)
         gg%gyz_(:,:,1-nGS)=gg%yd_(:,:,2-nGS)-gg%yd_(:,:,1-nGS)
         gg%gzz_(:,:,1-nGS)=gg%zd_(:,:,2-nGS)-gg%zd_(:,:,1-nGS)
      ELSE
         gg%gxz_(:,:,1-nGS)=h*( gg%xd_(:,:,2-nGS)
     &                        -(gg%xd_(:,:,0)-(gg%xd_(:,:,1+nGS)-gg%xd_(:,:,1))))
         gg%gyz_(:,:,1-nGS)=h*( gg%yd_(:,:,2-nGS)
     &                        -(gg%yd_(:,:,0)-(gg%yd_(:,:,1+nGS)-gg%yd_(:,:,1))))
         gg%gzz_(:,:,1-nGS)=h*( gg%zd_(:,:,2-nGS)
     &                        -(gg%zd_(:,:,0)-(gg%zd_(:,:,1+nGS)-gg%zd_(:,:,1))))

      ENDIF

      IF (bf(ib,fo_front)%faceType.eq.ft_interface .or.
     &    bf(ib,fo_front)%faceType.eq.ft_interface_MPI) THEN
         cB=bf(ib,fo_front)%connBlock
         gg%gxz_(:,:,nz+nGS)=gg%xd_(:,:,nz+nGS)-gg%xd_(:,:,nz+nGS-1)
         gg%gyz_(:,:,nz+nGS)=gg%yd_(:,:,nz+nGS)-gg%yd_(:,:,nz+nGS-1)
         gg%gzz_(:,:,nz+nGS)=gg%zd_(:,:,nz+nGS)-gg%zd_(:,:,nz+nGS-1)
      ELSE
         gg%gxz_(:,:,nz+nGS)=h*( gg%xd_(:,:,nz+1)
     &                          -(gg%xd_(:,:,nz-nGS)-gg%xd_(:,:,nz))-gg%xd_(:,:,nz-1+nGS))
         gg%gyz_(:,:,nz+nGS)=h*( gg%yd_(:,:,nz+1)
     &                          -(gg%yd_(:,:,nz-nGS)-gg%yd_(:,:,nz))-gg%yd_(:,:,nz-1+nGS))
         gg%gzz_(:,:,nz+nGS)=h*( gg%zd_(:,:,nz+1)
     &                          -(gg%zd_(:,:,nz-nGS)-gg%zd_(:,:,nz))-gg%zd_(:,:,nz-1+nGS))
      ENDIF


      END SUBROUTINE CoordDifference

