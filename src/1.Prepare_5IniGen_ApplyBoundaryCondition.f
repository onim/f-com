!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             INITIAL DATA SETTING
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE IniGen_ApplyBoundaryCondition
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:iC,pI,ib,nb,ns,nx_gl,ny_gl,nz_gl
      USE conserved_Quantity,       ONLY:initQuant
      USE boundary_Data_OOP,        ONLY:bf
      USE Grid_Data
      USE Message_Passing_Interface
      USE Fundamental_Constants,    ONLY:fo_south,nGS

      USE AuxSubprogram,            ONLY:arraypoint

      IMPLICIT NONE

      INTEGER::ip,facet,bl,fc,fi,i,j,k,nn
      REAL(8)::dist
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     Generic wall temperature
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         DO facet=1,6
            bf(ib,facet)%wallTemperature=iC%WallTemp

            IF (bf(ib,facet)%nFBC.ge.1) THEN
                  ALLOCATE(bf(ib,facet)%fBC(bf(ib,facet)%nFBC))
            ENDIF

            IF (bf(ib,facet)%faceType.eq.3) THEN
                  bf(ib,facet)%fBC(1)%quant=initQuant
            ENDIF


         ENDDO
      ENDIF;ENDDO multiBlock

      IF (iC%isPartialBC) THEN

      DO ip=1,iC%nPBC
         bl=pI(ip)%pBlock
         fc=pI(ip)%pFace
         fi=pI(ip)%pIndFBC
         IF (myBlockIs(bl)) THEN
            SELECT CASE (TRIM(pI(ip)%pShape))
            CASE ('rectangle')
               bf(bl,fc)%fBC(fi)%nnode=pI(ip)%nnode()
               ALLOCATE(bf(bl,fc)%fBC(fi)%nodelist(pI(ip)%nnode()))
               CALL bf(bl,fc)%fBC(fi)%calcNodeList(pI(ip))
               CALL bf(bl,fc)%fbc(fi)%quant%allocScalar(ns)
               bf(bl,fc)%fbc(fi)%uniform=.true.
               CALL pI(ip)%CondToProperty(bf(bl,fc)%fbc(fi)%quant)

            CASE ('circle')
               SELECT CASE (fc)
               CASE(fo_south)
                  nn=0
                  DO k=1,nz_gl(bl); DO j=1-ngs,0; DO i=1,nx_gl(bl)
                     dist=SQRT((geo_gl(bl)%xd_(i,j,k)-pI(ip)%cxyz(1))**2
     &                        +(geo_gl(bl)%zd_(i,j,k)-pI(ip)%cxyz(3))**2)
                     IF (dist.lt.pI(ip)%radius) THEN
                        nn=nn+1
                     ENDIF
                  ENDDO;ENDDO;ENDDO

                  bf(bl,fc)%fBC(fi)%nnode=nn
                  ALLOCATE(bf(bl,fc)%fBC(fi)%nodelist(nn))

                  nn=0
                  DO k=1,nz_gl(bl); DO j=1-ngs,0; DO i=1,nx_gl(bl)
                     dist=SQRT((geo_gl(bl)%xd_(i,j,k)-pI(ip)%cxyz(1))**2
     &                        +(geo_gl(bl)%zd_(i,j,k)-pI(ip)%cxyz(3))**2)
                     IF (dist.lt.pI(ip)%radius) THEN
                        nn=nn+1
                        bf(bl,fc)%fBC(fi)%nodelist(nn)%i=i
                        bf(bl,fc)%fBC(fi)%nodelist(nn)%j=j
                        bf(bl,fc)%fBC(fi)%nodelist(nn)%k=k
                     ENDIF
                  ENDDO;ENDDO;ENDDO

                  CALL bf(bl,fc)%fbc(fi)%quant%allocScalar(ns)
                  bf(bl,fc)%fbc(fi)%uniform=.true.
                  CALL pI(ip)%CondToProperty(bf(bl,fc)%fbc(fi)%quant)
               CASE DEFAULT
                  CALL ErrorStop(1502)
               ENDSELECT


            CASE DEFAULT
                  CALL ErrorStop(1500)
            ENDSELECT
         ENDIF
      ENDDO

      ENDIF

      END SUBROUTINE IniGen_ApplyBoundaryCondition

