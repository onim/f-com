!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE firstfweuler
!     1st order forward Euler time marching
!     subroutine name: dt on right hand side?
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ib
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   INTEGER::i,j,k

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forInnerPoints_
#define f bl(ib)%p(i,j,k)%f
#define gj bl(ib)%p(i,j,k)%gd%gj
      f%dro=f%dro/gj
      f%dru=f%dru/gj
      f%drv=f%drv/gj
      f%drw=f%drw/gj
      f%dre=f%dre/gj
      f%drfs=f%drfs/gj
#undef f
#undef gj
   _endInnerPoints_
!$OMP END PARALLEL DO

END SUBROUTINE
