!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Get conductivity
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE getConduction
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns,
     &                                   num_nonRadical, nonRadical,
     &                                   conductMethod
      USE conserved_Quantity,       ONLY:fs_, yfs_,T_
      USE thermal_Property,         ONLY:cps_, rs, hf_, vmol, dmol
      USE fundamental_Constants,    ONLY:nGS
      USE transportModule,          ONLY:pt,pts
      USE Status_Check,              ONLY:meanConductivity

      IMPLICIT NONE
      REAL(8)::
     &      conduct(ns),
     &      condsum,condcalc

      INTEGER::i,j,k,nr,nrr,is,iss

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      meanConductivity=0.0d0

      do k=1-nGS,nz+nGS
        do j=1-nGS,ny+nGS
          do i=1-nGS,nx+nGS
!   >>>THERMAL FLUX<<<
!     1. Eucken method
      IF (conductMethod.eq.'eucken') THEN

      DO is=1,num_nonRadical
            nr=nonRadical(is)
!     cps=cp??
            conduct(nr)=0.25D0
     &           *(9.0D0*CPS_(i,j,k,nr)-5.0D0*(CPS_(i,j,k,nr)-RS(nr)))
     &           *pts(i,j,k,nr)%VISM
      ENDDO

      condsum=0.0d0

      DO is=1,num_nonRadical
        nr=nonRadical(is)
        condcalc=0.0d0

        DO iss=1,num_nonRadical
          nrr=nonRadical(iss)
          IF (nrr.eq.nr) THEN
            condcalc=condcalc+yfs_(i,j,k,nrr)
          ELSE
            condcalc=condcalc+yfs_(i,j,k,nrr)
     &             *(1.0d0+SQRT(conduct(nr)*conduct(nrr))
     &                         *vmol(nr,nrr)**2*dmol(nr,nrr))
          ENDIF
        ENDDO

        condsum=condsum+yfs_(i,j,k,nr)*conduct(nr)/condcalc
      ENDDO

!     2. Chung method
      ELSEIF (conductMethod.eq.'chunGS') THEN
            condsum=0.0d0
            print *,'Under construction'
!     3. Other method
      ELSE
            condsum=0.0d0
            print *,'Error!! Check your option for conductivity.'
      ENDIF

      pt(i,j,k)%cond=condsum

          ENDDO
        ENDDO
      ENDDO

      meanConductivity=meanConductivity
     &                /((nx+2*nGS)*(ny+2*nGS)*(nz+2*nGS))

      END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Heat transport by conduction and mass diffusion
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE heatDiffusion(dimen)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,
     &                                   num_nonRadical, nonRadical,
     &                                   conductMethod,ns
      USE conserved_Quantity,       ONLY:u_,v_,w_,fs_
      USE thermal_Property,         ONLY:cps_, hf_
      USE fundamental_Constants,    ONLY:Prt,nGS
      USE transportModule,          ONLY:pt, face, faces

      IMPLICIT NONE
      REAL(8)::
     &      cndct,ROTRM,
     &      qx,qy,qz

      INTEGER,INTENT(IN)::dimen
      INTEGER::i,j,k,ish,jsh,ksh

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      ish=0
      jsh=0
      ksh=0
      SELECT CASE(dimen)
      CASE(1)
            ish=1
      CASE(2)
            jsh=1
      CASE(3)
            ksh=1
      END SELECT

      do k=0,nz; do j=0,ny; do i=0,nx
!     Prt: prandtl number
!     rodif: conductivity (alpha) * density
      ROTRM=face(i,j,k)%VIST/Prt

      CNDCT= SUM(cps_(i,j,k,:)*fs_(i,j,k,:))*ROTRM
     &      +0.5d0*(pt(i,j,k)%cond+pt(i+ish,j+jsh,k+ksh)%cond)

      QX=CNDCT*face(i,j,k)%DTX+SUM(hf_(i,j,k,:)*faces(i,j,k,:)%rsfx)
      QY=CNDCT*face(i,j,k)%DTY+SUM(hf_(i,j,k,:)*faces(i,j,k,:)%rsfy)
      QZ=CNDCT*face(i,j,k)%DTZ+SUM(hf_(i,j,k,:)*faces(i,j,k,:)%rsfz)

!     Including friction work...
      face(i,j,k)%refx=
     &      ( (u_(i,j,k)+u_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rufx
     &       +(v_(i,j,k)+v_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rvfx
     &       +(w_(i,j,k)+w_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rwfx)
     &      +qx
      face(i,j,k)%refy=
     &      ( (u_(i,j,k)+u_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rufy
     &       +(v_(i,j,k)+v_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rvfy
     &       +(w_(i,j,k)+w_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rwfy)
     &      +qy
      face(i,j,k)%refz=
     &      ( (u_(i,j,k)+u_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rufz
     &       +(v_(i,j,k)+v_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rvfz
     &       +(w_(i,j,k)+w_(i+ish,j+jsh,k+ksh))*face(i,j,k)%rwfz)
     &      +qz

!     Excluding friction work...
!      face(i,j,k)%refx=QX
!      face(i,j,k)%REFY=QY
!      face(i,j,k)%REFZ=QZ

      ENDDO;ENDDO;ENDDO

      END SUBROUTINE
