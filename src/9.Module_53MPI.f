!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      MODULE Message_Passing_Interface
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      USE mpi
      USE MPI_Container

!$$$$$$$$$$$$$$$
!     Members
!$$$$$$$$$$$$$$$
      IMPLICIT NONE
!     Constants
      INTEGER,PARAMETER::root=0

!     Constitutional variables
      INTEGER::MPISize, myMPINode, tag, ierr, length
      INTEGER::MPIStatus(MPI_STATUS_SIZE)

!     Boundary data container
!        opp: opposite
      TYPE(MPIDataContainer),DIMENSION(:,:),ALLOCATABLE::bdc_send, bdc_recv
      TYPE(MPIProfileContainer),DIMENSION(:),ALLOCATABLE::MPIprofile
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
      SUBROUTINE startMPI
!           Start MPI parallel computing
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      CALL MPI_INIT(ierr)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD, myMPINode, ierr)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD, MPISize, ierr)

      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE endMPI
!           End MPI parallel computing
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      CALL MPI_FINALIZE(ierr)

      END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$  Functions for identification
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
      FUNCTION IamPrimeNode() RESULT(tf)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE
      LOGICAL::tf

      tf=.false.
      IF (myMPINode.eq.0) tf=.true.

      END FUNCTION

!$$$$$$$$$$$$$$$
      FUNCTION myBlockIs(ib) RESULT(tf)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE
      INTEGER::ib,i
      LOGICAL::tf

      tf=.false.

      DO i=1,MPIprofile(myMPINode)%nb
         IF (ib.eq.MPIprofile(myMPINode)%ib(i)) tf=.true.
      ENDDO

      END FUNCTION

!$$$$$$$$$$$$$$$
      FUNCTION blockOwner(ib) RESULT(node)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE
      INTEGER::ib,node
      INTEGER::i,j

      DO i=0,MPISize-1
         DO j=1,MPIprofile(i)%nb
            IF (ib.eq.MPIprofile(i)%ib(j)) node=i
         ENDDO
      ENDDO

      END FUNCTION

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$  Functions for data transaction
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
      SUBROUTINE obs__sendBoundarySize(dest, nx, ny, nz)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      INTEGER,INTENT(IN)::dest, nx, ny, nz
      INTEGER::nxyz(3)

      nxyz=(/nx, ny, nz/)
      CALL MPI_send(nxyz,3,MPI_INTEGER,dest,0,MPI_COMM_WORLD,ierr)
      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE obs__recvBoundarySize(source, bdc, ne, ib, iface)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      INTEGER,INTENT(IN)::source
      CLASS(MPIDataContainer)::bdc
      INTEGER::nn(3),ne,ib,iface

      CALL MPI_recv(nn,3,MPI_INTEGER,source,0,MPI_COMM_WORLD,MPIStatus,ierr)

      CALL bdc%alloc(nn,ne,ib,iface)

      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE sendBoundaryValue(dest, bdc)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      INTEGER,INTENT(IN)::dest
      CLASS(MPIDataContainer),INTENT(IN)::bdc

      CALL MPI_send(bdc%arr1D,bdc%length,MPI_REAL8,dest,
     &              0,MPI_COMM_WORLD,ierr)

      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE recvBoundaryValue(source, bdc)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      INTEGER,INTENT(IN)::source
      CLASS(MPIDataContainer)::bdc

      CALL MPI_recv(bdc%arr1D,bdc%length,MPI_REAL8,source,
     &              0,MPI_COMM_WORLD,MPIStatus,ierr)

      END SUBROUTINE

      END MODULE

