!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!                     CALCULATE CHEMICAL REACTION
!***********************************************************************
!***CHEMICAL REACTION
!
!     Reference: J. Bibrzycki1 and T. Poinsot, Reduced chemical kinetic mechanisms
!           for methane combustion in O2/N2 and O2/CO2 atmosphere
!
!     Participating species
!
!        H2
!        O2
!        H2O
!        CH4
!
!        CO
!        CO2
!        N2
!
! Reaction steps
!	 CH4 +.5O2  =>  CO  + 2H2
!	 CH4 + H2O  =>  CO  + 3H2
!	 H2  +.5O2 <=>  H2O
!	 CO  + H2O <=>  CO2 +  H2
!
!***********************************************************************
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ChemJL(i,j,k)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
      USE grid_Data,                      ONLY:gj
      USE conserved_Quantity,             ONLY:yfs,qChem,rfs,
     &                                         temperature => t
      USE thermal_Property,               ONLY:td,hf,wMol
      USE flux_difference,                ONLY:dre,drfs
      USE fundamental_Constants,          ONLY:RMOL,RCAL
      USE reaction_data,                  ONLY:cf,zetaf,ef,
     &                                         cf0,zetaf0,ef0,
     &                                         acent,ts1,ts2,ts3
      USE control_Data,                   ONLY:ns,nr,nx,ny,nz,
     &                                         dt

      IMPLICIT NONE

      INTEGER,PARAMETER::
     &                   H2  = 1,
     &                   O2  = 2,
     &                   H2O = 3,
     &                   CH4 = 4,
     &                   CO  = 5,
     &                   CO2 = 6,
     &                   N2  = 7

      REAL(8)::
     &      t,dtRe,dtReSum
      REAL(8),PARAMETER::
     &      unitConversionFactor=1.0d-6,
     &      eps                 =0.0d-50
      REAL(8),DIMENSION(nr)::
     &      rateCoeffForward,    rateCoeffBackward,
     &      reactionRateForward, reactionRateBackward,
     &      domg
      REAL(8),DIMENSION(ns)::
     &      dyfs,ddyfs,
     &      helmFE
      REAL(8),DIMENSION(2)::
     &      dHelmFE

      INTEGER,INTENT(IN)::i,j,k
      INTEGER::is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      dtReSum=0.0d0
      ddyfs=0.0d0

      t=temperature(i,j,k)

!***************
!     Free energy (Helmholtz function)
!***************
      DO is=1,ns
      SELECT CASE (is)
            CASE (H2,O2,H2O,CO,CO2)
            helmFE(is)=Td(is)%helmFE(t)

            CASE DEFAULT
                  CONTINUE
      ENDSELECT
      ENDDO
!***************
!     Difference of free energy of some reactions with reverse reaction
!     The sign convention for stoichiometric coefficients
!           is in opposite direction.
!***************
!	                  H2 +         0.5 O2 <=>        H2O
      dHelmFE(1)=helmFE(H2)+0.5d0*helmFE(O2) -  helmFE(H2O)
!	                  CO +             H2O <=>        CO2  +        H2
      dHelmFE(2)=helmFE(CO)+      helmFE(H2O) -  helmFE(CO2) - helmFE(H2)

!***************
!     Rate coefficients
!***************
      rateCoeffForward(1)=cf(1)*exp(-ef(1)/rcal/t)*unitConversionFactor**0.75
      rateCoeffForward(2)=cf(2)*exp(-ef(2)/rcal/t)*unitConversionFactor
      rateCoeffForward(3)=cf(3)*exp(-ef(3)/rcal/t)*unitConversionFactor**0.75
      rateCoeffForward(4)=cf(4)*exp(-ef(4)/rcal/t)*unitConversionFactor

      rateCoeffBackward(3:4)=rateCoeffForward(3:4)*exp(-dHelmFE)

      DO WHILE (dtReSum.ne.dt)
!***************
!     Reaction rates
!***************
      reactionRateForward(1)=rateCoeffForward(1)
     &       *yfs(i,j,k,CH4)**0.5d0  *yfs(i,j,k,O2)**1.25d0
      reactionRateForward(1)=MIN(reactionRateForward(1),
     &                            yfs(i,j,k,CH4)*0.1d0/dt)

      reactionRateForward(2)=rateCoeffForward(2)
     &       *yfs(i,j,k,CH4)         *yfs(i,j,k,H2O)

      reactionRateForward(3)=rateCoeffForward(3)
     &       *yfs(i,j,k,H2)**0.25d0 *yfs(i,j,k,O2)**1.5d0
      reactionRateForward(3)=MIN(reactionRateForward(3),
     &                            yfs(i,j,k,H2)*0.1d0/dt)

      reactionRateForward(4)=rateCoeffForward(4)
     &       *yfs(i,j,k,CO)         *yfs(i,j,k,H2O)

      reactionRateBackward(1)=0.0d0
      reactionRateBackward(2)=0.0d0

      reactionRateBackward(3)=rateCoeffBackward(3)
     &       *yfs(i,j,k,H2)**(-0.75)
     &       *yfs(i,j,k,O2) *yfs(i,j,k,H2O)
      reactionRateBackward(3)=MIN(reactionRateBackward(3),
     &                            yfs(i,j,k,H2O)*0.1d0/dt)

      reactionRateBackward(4)=rateCoeffBackward(4)
     &       *yfs(i,j,k,CO2)         *yfs(i,j,k,H2)
!
      domg=reactionRateForward-reactionRateBackward

      dyfs(H2) = 2.0d0*domg(1) -      domg(3)
     &          +3.0d0*domg(2)
     &          +      domg(4)

      dyfs(O2) =               -0.5d0*domg(1)
     &                         -0.5d0*domg(3)

      dyfs(H2O)=       domg(3) -      domg(2)
     &                         -      domg(4)

      dyfs(CH4)=               -      domg(1)
     &                         -      domg(2)

      dyfs(CO) =       domg(1) -      domg(4)
     &          +      domg(2)

      dyfs(CO2)=       domg(4)

      dyfs(N2) =0.0d0

!	 CH4 +.5O2  =>  CO  + 2H2
!	 CH4 + H2O  =>  CO  + 3H2
!	 H2  +.5O2 <=>  H2O
!	 CO  + H2O <=>  CO2 +  H2

      dtRe=dt
      do is=1,ns
            IF(dyfs(is).lt.0.0d0) THEN
                  dtRe=MIN(dtRe,yfs(i,j,k,is)/-dyfs(is))
            ENDIF
      ENDDO

      dtRe=dtRe*0.5d0

      IF(dtRe.ge.dt-dtReSum) THEN
            dtRe=dt-dtReSum
            dtReSum=dt
            ddyfs=ddyfs+dyfs*dtRe
      ELSE
            dtReSum=dtReSum+dtRe
            ddyfs=ddyfs+dyfs*dtRe
            yfs(i,j,k,:)=yfs(i,j,k,:)+dyfs*dtRe
      ENDIF

      ENDDO

      ddyfs=ddyfs/dtReSum

      drfs(i,j,k,:)=drfs(i,j,k,:)-ddyfs*wMol*gj(i,j,k)
!     if molar mass is not exact, sum(dyfs*wmol) is not equal to zero.
!     try to make molar mass as exact as possible.


      qChem(i,j,k)=-1.0d0*sum(DYFS*wMol*HF(i,j,k,:))



      END SUBROUTINE


