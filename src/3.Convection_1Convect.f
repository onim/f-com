!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     convection terms on right hand side of
!     EULER EQUATION DIFFERENCE
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE CONVECT
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE convectModule
      USE flux_difference,          ONLY:dro,dru,drv,drw,dre,drfs

      IMPLICIT NONE
      INTEGER:: dimen,i,j,k,ish,jsh,ksh

      TYPE(AUSMplusUp_MUSCL)::conv
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!***********************************************************************
!     Initialize
!***********************************************************************
      dro=0.0D0
      dru=0.0D0
      drv=0.0D0
      drw=0.0D0
      dre=0.0D0
      drfs=0.0D0
      CALL conv%alloc

      eachDimension: DO dimen=1,3

      SELECT CASE (dimen)
      CASE (1)
            ish=-1
            jsh=0
            ksh=0
      CASE (2)
            ish=0
            jsh=-1
            ksh=0
      CASE (3)
            ish=0
            jsh=0
            ksh=-1
      ENDSELECT

      CALL conv%getPrimitive

      CALL conv%calcCellValue(dimen)

      CALL conv%calcFluxRate(dimen)

      CALL conv%calcFaceArea(dimen)

      CALL conv%calcFlux


!***********************************************************************
!***INVISCID FLUX DIFFERENCE AT EACH INTERNAL NODE
!     the flux exists on the right boundary of the node.(CONVENTION)
!***********************************************************************
!$OMP PARALLEL DO PRIVATE(i,j,k)
      DO k=1,nz; DO j=1,ny; DO i=1,nx

      dro(i,j,k)=dro(i,j,k)
     &           +conv%flux(i,j,k,1)-conv%flux(i+ish,j+jsh,k+ksh,1)
      dru(i,j,k)=dru(i,j,k)
     &           +conv%flux(i,j,k,2)-conv%flux(i+ish,j+jsh,k+ksh,2)
      drv(i,j,k)=drv(i,j,k)
     &           +conv%flux(i,j,k,3)-conv%flux(i+ish,j+jsh,k+ksh,3)
      drw(i,j,k)=drw(i,j,k)
     &           +conv%flux(i,j,k,4)-conv%flux(i+ish,j+jsh,k+ksh,4)
      dre(i,j,k)=dre(i,j,k)
     &           +conv%flux(i,j,k,5)-conv%flux(i+ish,j+jsh,k+ksh,5)
      drfs(i,j,k,:)=drfs(i,j,k,:)
     &           +conv%flux(i,j,k,6:)-conv%flux(i+ish,j+jsh,k+ksh,6:)

      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      ENDDO eachDimension

      END SUBROUTINE
