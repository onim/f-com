!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!                 U,V,H,FS,cp,h,dt VARIABLES PREPARATION (Expand)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE EXPVAR(i,j,k)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns,nt,
     &                                   dtMax,dtMin,CFLMax,ib,nb
      USE conserved_Quantity,       ONLY:ro_,ru_,rv_,rw_,re_,
     &                                   u_, v_, w_, h_,
     &                                   T_, gamma_,qChem_, c_, p_,
     &                                   rfs_,fs_,yfs_
      USE thermal_Property,         ONLY:wMol,Td,rs,
     &                                   cps_,hf_
      USE grid_Data,                ONLY:gxx_,gxy_,gxz_,
     &                                   gyx_,gyy_,gyz_,
     &                                   gzx_,gzy_,gzz_
      USE thermocoeff,              ONLY:NASAPolynomial,
     &                                   NASAPolynomial7,
     &                                   NASAPolynomial9
      USE fundamental_Constants,    ONLY:nGS
      USE AuxSubprogram,            ONLY:arraypoint

      IMPLICIT NONE
      INTEGER,INTENT(IN):: i,j,k
      INTEGER:: is
      REAL(8):: rm,cvm,intEnergy,uvw,dt
      REAL(8),PARAMETER::tllimit=200.0d0
      CLASS(NASAPolynomial),ALLOCATABLE::tdrm

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
#define ind i,j,k
      IF    (nt.eq.7) THEN
            ALLOCATE(NASAPolynomial7::tdrm)
      ELSEIF(nt.eq.9) THEN
            ALLOCATE(NASAPolynomial9::tdrm)
      ELSE
            CALL ErrorStop(2105)
      ENDIF

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!***PREPARATION - velocity, mass fraction, mole fraction, Tdrm
!     Compute velocities
      u_(ind)=ru_(ind)/ro_(ind)
      v_(ind)=rv_(ind)/ro_(ind)
      w_(ind)=rw_(ind)/ro_(ind)


!     Mass fraction(fs,  =w_i or Y_i) and
!     concentration(yfs, =c_i =x_i*c)
      DO is=1,ns
            fs_(ind,is)=rfs_(ind,is)/ro_(ind)
            yfs_(ind,is)=rfs_(ind,is)/wMol(is)
      ENDDO


!     Temperature
!           For calculation of thermal properties such as specific heat and
!           enthalpy, this code prepares MASS AVERAGED COEFFICIENTS tdrm
!     get mixture gas constant
      rm=SUM(rs*fs_(ind,:))

!     calculate temperature from re
      CALL tdrm%makeMixture(Td,fs_(ind,:),rs)

!     internal energy from re
      intEnergy = re_(ind)/ro_(ind)-0.5d0*( u_(ind)**2
     &                                     +v_(ind)**2
     &                                     +w_(ind)**2)
      CALL tdrm%putTemp(t_(ind),intEnergy,rm,cv=cvm)


!     Calculate C_p and h from NASA polynomial
!     TBnd divides low temperature region and high t region
!     Calculate values of each species at each iteration head
      DO is=1,ns
            cps_(ind,is)=Td(is)%specheat(t_(ind),rs(is))
            hf_(ind,is) =Td(is)%enthalpy(t_(ind),rs(is))
      ENDDO


!     pressure
      p_(ind)=ro_(ind)*rm*t_(ind)
!     specific heat ratio
      gamma_(ind)=1.0d0+rm/cvm
!     total enthalpy
      h_(ind)=(re_(ind)+p_(ind))/ro_(ind)
!     sound speed
      c_(ind)=SQRT(gamma_(ind)*rm*t_(ind))


!***TIME STEP from possible maximun speed uvw***
      uvw=
     &    SQRT( (gxx_(ind)*u_(ind)+gxy_(ind)*v_(ind)+gxz_(ind)*w_(ind))**2
     &         +(gyx_(ind)*u_(ind)+gyy_(ind)*v_(ind)+gyz_(ind)*w_(ind))**2
     &         +(gzx_(ind)*u_(ind)+gzy_(ind)*v_(ind)+gzz_(ind)*w_(ind))**2)
     &   +c_(ind)*SQRT( gxx_(ind)**2+gxy_(ind)**2+gxz_(ind)**2
     &                 +gyx_(ind)**2+gyy_(ind)**2+gyz_(ind)**2
     &                 +gzx_(ind)**2+gzy_(ind)**2+gzz_(ind)**2)
     &

      dt=CFLMax/uvw
      dtMax=MAX(dt,dtMax)
      dtMin=MIN(dt,dtMin)

!     Error check for dt
      IF (dt.le.0.0d0) CALL ErrorStop(2100)

!     detect NaN
      IF (uvw.ne.uvw) THEN
            PRINT *,'  Position: ',ib,i,j,k
            PRINT *,'t, p, c',T_(ind),p_(ind),c_(ind)
            PRINT *,'ro, fs',ro_(ind),fs_(ind,:)
            CALL ErrorStop(2101)
      ENDIF

#undef ind
      END SUBROUTINE
