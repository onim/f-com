!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      MODULE MPI_Container
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      USE mpi
      IMPLICIT NONE

!$$$$$$$$$$$$$$$
      TYPE MPIDataContainer
!$$$$$$$$$$$$$$$
!     Members
!        nx, ny, nz: size of boundary data of source block
!        ne:         number of equation
!        ib:         index of source block
!        iface:      index of source face
!        r:          actual variables (ro, ru, rfs, ...)
         INTEGER::nx, ny, nz, ne, length, ib, iface
         REAL(8),DIMENSION(:),ALLOCATABLE::arr1D

      CONTAINS
         PROCEDURE::alloc
         PROCEDURE::pack
         PROCEDURE::unpack

      END TYPE

!$$$$$$$$$$$$$$$
      TYPE MPIProfileContainer
!$$$$$$$$$$$$$$$
         INTEGER::nb
         INTEGER,DIMENSION(:),ALLOCATABLE::ib
      END TYPE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
      SUBROUTINE alloc(this, nxyz, ne, ib, iface)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      CLASS(MPIDataContainer)::this
      INTEGER::nxyz(3)
      INTEGER::ne, ib, iface

      this%nx=nxyz(1)
      this%ny=nxyz(2)
      this%nz=nxyz(3)
      this%ne=ne
      this%ib=ib
      this%iface=iface
      this%length=nxyz(1)*nxyz(2)*nxyz(3)*ne

      IF (ALLOCATED(this%arr1D)) DEALLOCATE(this%arr1D)
      ALLOCATE(this%arr1D(this%length))

      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE pack(this, consIS)
!$$$$$$$$$$$$$$$
      USE M_BlockPointData,      ONLY:T_ExtendedConservedQuantity
      USE Control_Data,          ONLY:ns
      IMPLICIT NONE

      CLASS(MPIDataContainer)::this
      CLASS(T_ExtendedConservedQuantity),INTENT(IN),DIMENSION(:,:,:)::consIS

      INTEGER::nn,is

      nn=size(consIS)

      this%arr1D(           :       nn)=RESHAPE(consIS%ro,(/nn/))
      this%arr1D(       nn+1:     2*nn)=RESHAPE(consIS%ru,(/nn/))
      this%arr1D(     2*nn+1:     3*nn)=RESHAPE(consIS%rv,(/nn/))
      this%arr1D(     3*nn+1:     4*nn)=RESHAPE(consIS%rw,(/nn/))
      this%arr1D(     4*nn+1:     5*nn)=RESHAPE(consIS%re,(/nn/))
      DO is=1,ns
      this%arr1D((4+is)*nn+1:(5+is)*nn)=RESHAPE(consIS%rfs(is),(/nn/))
      ENDDO
      this%arr1D((5+ns)*nn+1:         )=RESHAPE(consIS%t,(/nn/))

      END SUBROUTINE

!$$$$$$$$$$$$$$$
      SUBROUTINE unpack(this, consOS)
!$$$$$$$$$$$$$$$
      USE M_BlockPointData,      ONLY:T_ExtendedConservedQuantity
      USE Control_Data,          ONLY:ns
      IMPLICIT NONE

      CLASS(MPIDataContainer)::this
      CLASS(T_ExtendedConservedQuantity),INTENT(OUT),DIMENSION(:,:,:)::consOS
      INTEGER::nn, is

      nn=this%nx*this%ny*this%nz

      IF(SIZE(consOS,1).eq.this%nx .and.
     &   SIZE(consOS,2).eq.this%ny .and.
     &   SIZE(consOS,3).eq.this%nz) THEN
         consOS%ro=RESHAPE(this%arr1D(     1:       nn),(/this%nx, this%ny, this%nz/))
         consOS%ru=RESHAPE(this%arr1D(  nn+1:     2*nn),(/this%nx, this%ny, this%nz/))
         consOS%rv=RESHAPE(this%arr1D(2*nn+1:     3*nn),(/this%nx, this%ny, this%nz/))
         consOS%rw=RESHAPE(this%arr1D(3*nn+1:     4*nn),(/this%nx, this%ny, this%nz/))
         consOS%re=RESHAPE(this%arr1D(4*nn+1:     5*nn),(/this%nx, this%ny, this%nz/))
         DO is=1,ns
         consOS%rfs(is)=RESHAPE(this%arr1D((4+is)*nn+1:(5+is)*nn),(/this%nx, this%ny, this%nz/))
         ENDDO
         consOS%t=RESHAPE(this%arr1D((5+ns)*nn+1:),(/this%nx, this%ny, this%nz/))
      ELSE
         CALL ErrorStop(9520)
      ENDIF

      END SUBROUTINE

      END MODULE

