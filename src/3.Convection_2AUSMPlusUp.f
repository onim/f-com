      SUBROUTINE AUSMPlusUp(this, dimen)
#define indc i,j,k
c           index of center (on interface)
#define indl i,j,k
c           index of left cell (with respect to the interface)
#define indr i+ish,j+jsh,k+ksh
c           index of right cell
      USE Grid_Data,                ONLY:gxx_,gxy_,gxz_,
     &                                   gyx_,gyy_,gyz_,
     &                                   gzx_,gzy_,gzz_,gj_,
     &                                   contraNorm_x_,contraNorm_y_,
     &                                   contraNorm_z_
      USE Fundamental_Constants,    ONLY:alpha, beta

      IMPLICIT NONE
      REAL(8),PARAMETER::
     &      mcutoff=0.2d0,
     &      k_p = 0.25d0, k_u=0.75d0
c     +      k_p = 0.00d0, k_u=0.00d0
      INTEGER,PARAMETER::
     &      left=1,
     &      right=2,
     &      center=3

      CLASS(AUSMplusUp_MUSCL)::this
      INTEGER,INTENT(IN)::dimen

      INTEGER::i,j,k,ish,jsh,ksh
      REAL(8)::
     &      mbar, mstar, f, mach


C***********************************************************************
C     Normal velocity with dimension in physical space
c           it should not be in computational space
C***********************************************************************
      SELECT CASE (dimen)
      CASE (1)
            ish=1
            jsh=0
            ksh=0
            DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            this%U(indc,left)=
     &            ( this%processed(indc,2,left)*gxx_(indl)
     &             +this%processed(indc,3,left)*gxy_(indl)
     &             +this%processed(indc,4,left)*gxz_(indl)
     &            ) / this%processed(indc,1,left)
     &              / contraNorm_x_(indl)
            this%U(indc,right)=
     &            ( this%processed(indc,2,right)*gxx_(indr)
     &             +this%processed(indc,3,right)*gxy_(indr)
     &             +this%processed(indc,4,right)*gxz_(indr)
     &            ) / this%processed(indc,1,right)
     &              / contraNorm_x_(indr)
            ENDDO;ENDDO;ENDDO
      CASE (2)
            ish=0
            jsh=1
            ksh=0
            DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            this%U(indc,left)=
     &            ( this%processed(indc,2,left)*gyx_(indl)
     &             +this%processed(indc,3,left)*gyy_(indl)
     &             +this%processed(indc,4,left)*gyz_(indl)
     &            ) / this%processed(indc,1,left)
     &              / contraNorm_y_(indl)
            this%U(indc,right)=
     &            ( this%processed(indc,2,right)*gyx_(indr)
     &             +this%processed(indc,3,right)*gyy_(indr)
     &             +this%processed(indc,4,right)*gyz_(indr)
     &            ) / this%processed(indc,1,right)
     &              / contraNorm_y_(indr)
            ENDDO;ENDDO;ENDDO
      CASE (3)
            ish=0
            jsh=0
            ksh=1
            DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            this%U(indc,left)=
     &            ( this%processed(indc,2,left)*gzx_(indl)
     &             +this%processed(indc,3,left)*gzy_(indl)
     &             +this%processed(indc,4,left)*gzz_(indl)
     &            ) / this%processed(indc,1,left)
     &              / contraNorm_z_(indl)
            this%U(indc,right)=
     &            ( this%processed(indc,2,right)*gzx_(indr)
     &             +this%processed(indc,3,right)*gzy_(indr)
     &             +this%processed(indc,4,right)*gzz_(indr)
     &            ) / this%processed(indc,1,right)
     &              / contraNorm_z_(indr)
            ENDDO;ENDDO;ENDDO
      ENDSELECT

      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
C***********************************************************************
C     Interface speed of sound
C***********************************************************************
      this%c(indc,center)=
     &      MIN(
     &           this%c(indc, left)**2
     &          /MAX(this%c(indc, left), this%U(indc, left)),
     &           this%c(indc,right)**2
     &          /MAX(this%c(indc,right),-this%U(indc,right))
     &         )

C***********************************************************************
C     Low Mach number treatment
C***********************************************************************
      mbar=SQRT(
     &          (this%U(indc,left)**2+this%U(indc,right)**2)
     &           /2.0d0/this%c(indc,center)**2
     &         )

c     mcutoff=0.2d0 as a parameter
c     Cutoff Mach number should be determined based upon representative
c           Mach number. M_co = k*M_inf, K=O(1) is a reliable criterion.
c     For my cases, k corresponds to 0.1

      mstar = MIN(1.0d0,MAX(ABS(mbar),mcutoff))
      f     = mstar*(2.0d0-mstar)

      this%k_p_mod(indc)=
     &      K_p / f
     &     *MAX(1-mbar**2,0.0d0)
     &     *2.0d0*( this%p(indc,2,right)-this%p(indc,2,left))
     &     /(this%processed(indc,1,left)+this%processed(indc,1,right))
     &     / this%c(indc,center)**2
      this%k_u_mod(indc)=
     &      k_u * f
     &     *( this%processed(indc,1,left)
     &       +this%processed(indc,1,right))
     &     *  this%c(indc,center)
     &     *( this%U(indc,right) - this%U(indc,left))

C***********************************************************************
C     Mach number based on left cell speed of sound
C***********************************************************************
      this%M(indc, left) = this%U(indc, left)/this%c(indc,center)
      this%M(indc,right) = this%U(indc,right)/this%c(indc,center)

C***********************************************************************
C     positive/negative Mach number / pressure weight
C***********************************************************************
      mach=this%M(indc,left)
      IF ((ABS(mach)-1.0d0).lt.0.0d0) THEN
c     subsonic case
            this%M(indc,left)=
     &            0.25d0*(mach+1.0d0)**2
     &           +beta*(mach**2-1.0d0)**2
            this%ppm(indc,left)=
     &            0.25d0*(mach+1.0d0)**2*(2.0d0-mach)
     &           +alpha*mach*(mach**2-1.0d0)**2

      ELSE
c     supersonic case
            this%M(indc,left)=
     &           0.50d0*(mach+ABS(mach))
            this%ppm(indc,left)=
     &           0.50d0*(1.0d0+SIGN(1.0d0,mach))
      ENDIF

      mach=this%M(indc,right)
      IF ((ABS(mach)-1.0d0).lt.0.0d0) THEN
            this%M(indc,right)=
     &           -0.25d0*(mach-1.0d0)**2
     &           -beta*(mach**2-1.0d0)**2
            this%ppm(indc,right)=
     &            0.25d0*(mach-1.0d0)**2*(2.0d0+mach)
     &           -alpha*mach*(mach**2-1.0d0)**2

      ELSE
            this%M(indc,right)=
     &            0.50d0*(mach-ABS(mach))
            this%ppm(indc,right)=
     &            0.50d0*(1.0d0-SIGN(1.0d0,mach))
      ENDIF

      ENDDO;ENDDO;ENDDO

#undef indc
#undef indl
#undef indr
      END SUBROUTINE


