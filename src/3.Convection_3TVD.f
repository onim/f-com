      SUBROUTINE TVD(this,dimen)
      USE grid_Data,                ONLY:gxxf_,gxyf_,gxzf_,
     &                                   gyxf_,gyyf_,gyzf_,
     &                                   gzxf_,gzyf_,gzzf_

      IMPLICIT NONE
      INTEGER,PARAMETER::left=1, right=2

      CLASS(AUSMplusUp_MUSCL)::this
      INTEGER,INTENT(IN)::dimen

      INTEGER::i,j,k,ish,jsh,ksh
      REAL(8),DIMENSION(ne)::
     &      sr, psi
c     sr: slope ratio
c     psi: limiter

c--------------
      SELECT CASE (dimen)
      CASE (1)
            ish=1
            jsh=0
            ksh=0
      CASE (2)
            ish=0
            jsh=1
            ksh=0
      CASE (3)
            ish=0
            jsh=0
            ksh=1
      ENDSELECT


#define cnwind i,j,k
#define upwind i-ish,j-jsh,k-ksh
#define dnwind i+ish,j+jsh,k+ksh

C***********************************************************************
C     For left flux
C***********************************************************************
!$OMP PARALLEL PRIVATE(i,j,k,sr,psi)
!$OMP DO
      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
C***********************************************************************
C     Obtain gradient ratio r
c
c     Definition of r:
c           r_f= (phi_center - phi_upstream)
c               /(phi_dnstream - phi_center)
c***********************************************************************
      sr = (this%primitive(cnwind,:)-this%primitive(upwind,:))
     &    /(this%primitive(dnwind,:)-this%primitive(cnwind,:))

C***********************************************************************
C     Obtain flux limiter function
C***********************************************************************
      psi = MUSCL(sr)

C***********************************************************************
C     Apply flux limiter and obtain final vector
C***********************************************************************
      this%processed(cnwind,:,left) = this%primitive(cnwind,:)
     &                            +0.5d0*psi*( this%primitive(dnwind,:)
     &                                        -this%primitive(cnwind,:))

      CALL reconstruct(this%processed(cnwind,:,left),
     &                 this%phi(cnwind,:,left),
     &                 this%c(cnwind,left),
     &                 this%p(cnwind,:,left))

      ENDDO;ENDDO;ENDDO
!$OMP END DO NOWAIT
#undef upwind
#undef cnwind
#undef dnwind
#define upwind i+ish,j+jsh,k+ksh
#define cnwind i,j,k
#define dnwind i-ish,j-jsh,k-ksh
C***********************************************************************
C     For right flux
C***********************************************************************
!$OMP DO
      DO k=1,nz+ksh; DO j=1,ny+jsh; DO i=1,nx+ish
      sr = (this%primitive(cnwind,:)-this%primitive(upwind,:))
     &    /(this%primitive(dnwind,:)-this%primitive(cnwind,:))
      psi = MUSCL(sr)
      this%processed(dnwind,:,right) = this%primitive(cnwind,:)
     &                            +0.5d0*psi*( this%primitive(dnwind,:)
     &                                        -this%primitive(cnwind,:))

      CALL reconstruct(this%processed(dnwind,:,right),
     &                 this%phi(dnwind,:,right),
     &                 this%c(dnwind,right),
     &                 this%p(dnwind,:,right))

      ENDDO;ENDDO;ENDDO
!$OMP END DO
!$OMP END PARALLEL

#undef upwind
#undef cnwind
#undef dnwind

      END SUBROUTINE

