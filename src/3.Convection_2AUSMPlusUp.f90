SUBROUTINE AUSMPlusUp(this)
#define indc i,j,k
#define indl i,j,k
#define indr i+ish,j+jsh,k+ksh
   USE Fundamental_Constants,    ONLY:alpha, beta

IMPLICIT NONE
   REAL(8),PARAMETER:: &
      mcutoff=0.2d0,   &
      k_p = 0.25d0, k_u=0.75d0
!     k_p = 0.00d0, k_u=0.00d0

   CLASS(T_AUSMplusUp_MUSCL)::this
   REAL(8)::mbar, mstar, f, mach

   !***********************************************************************
   !     Normal velocity with dimension in physical space
   !           it should not be in computational space
   !***********************************************************************
   ASSOCIATE(gxx=>bl(ib)%p(:,:,:)%gd%gxx, &
             gxy=>bl(ib)%p(:,:,:)%gd%gxy, &
             gxz=>bl(ib)%p(:,:,:)%gd%gxz, &
             gyx=>bl(ib)%p(:,:,:)%gd%gyx, &
             gyy=>bl(ib)%p(:,:,:)%gd%gyy, &
             gyz=>bl(ib)%p(:,:,:)%gd%gyz, &
             gzx=>bl(ib)%p(:,:,:)%gd%gzx, &
             gzy=>bl(ib)%p(:,:,:)%gd%gzy, &
             gzz=>bl(ib)%p(:,:,:)%gd%gzz, &
             contraNorm_x=>bl(ib)%p(:,:,:)%gd%contraNorm_x, &
             contraNorm_y=>bl(ib)%p(:,:,:)%gd%contraNorm_y, &
             contraNorm_z=>bl(ib)%p(:,:,:)%gd%contraNorm_z)
   SELECT CASE (dimen)
      CASE (1)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
            this%U(indc,left)=                         &
               ( this%processed(indc,2,left)*gxx(indl) &
                +this%processed(indc,3,left)*gxy(indl) &
                +this%processed(indc,4,left)*gxz(indl) &
               ) / this%processed(indc,1,left)         &
                 / contraNorm_x(indl)
            this%U(indc,right)=                         &
               ( this%processed(indc,2,right)*gxx(indr) &
                +this%processed(indc,3,right)*gxy(indr) &
                +this%processed(indc,4,right)*gxz(indr) &
               ) / this%processed(indc,1,right)         &
                 / contraNorm_x(indr)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
      CASE (2)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
         this%U(indc,left)=                            &
               ( this%processed(indc,2,left)*gyx(indl) &
                +this%processed(indc,3,left)*gyy(indl) &
                +this%processed(indc,4,left)*gyz(indl) &
               ) / this%processed(indc,1,left)         &
                 / contraNorm_y(indl)
         this%U(indc,right)=                            &
               ( this%processed(indc,2,right)*gyx(indr) &
                +this%processed(indc,3,right)*gyy(indr) &
                +this%processed(indc,4,right)*gyz(indr) &
               ) / this%processed(indc,1,right)         &
                 / contraNorm_y(indr)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
      CASE (3)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
         this%U(indc,left)=                            &
               ( this%processed(indc,2,left)*gzx(indl) &
                +this%processed(indc,3,left)*gzy(indl) &
                +this%processed(indc,4,left)*gzz(indl) &
               ) / this%processed(indc,1,left)         &
                 / contraNorm_z(indl)
         this%U(indc,right)=                            &
               ( this%processed(indc,2,right)*gzx(indr) &
                +this%processed(indc,3,right)*gzy(indr) &
                +this%processed(indc,4,right)*gzz(indr) &
               ) / this%processed(indc,1,right)         &
                 / contraNorm_z(indr)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
   ENDSELECT

!$OMP PARALLEL DO PRIVATE(i,j,k,mbar,mstar,f,mach)
   DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
      !***********************************************************************
      !     Interface speed of sound
      !***********************************************************************
      this%c(indc,center)=                                     &
            MIN(                                               &
                 this%c(indc, left)**2                         &
                /MAX(this%c(indc, left), this%U(indc, left)),  &
                 this%c(indc,right)**2                         &
                /MAX(this%c(indc,right),-this%U(indc,right))   &
               )

      !***********************************************************************
      !     Low Mach number treatment
      !***********************************************************************
      mbar=SQRT(                                               &
                (this%U(indc,left)**2+this%U(indc,right)**2)   &
                 /2.0d0/this%c(indc,center)**2                 &
               )

      !     mcutoff=0.2d0 as a parameter
      !     Cutoff Mach number should be determined based upon representative
      !           Mach number. M_co = k*M_inf, K=O(1) is a reliable criterion.
      !     For my cases, k corresponds to 0.1

      mstar = MIN(1.0d0,MAX(ABS(mbar),mcutoff))
      f     = mstar*(2.0d0-mstar)

      this%k_p_mod(indc)=                                            &
         K_p / f                                                     &
         *MAX(1-mbar**2,0.0d0)                                       &
         *2.0d0*( this%p(indc,2,right)-this%p(indc,2,left))          &
         /(this%processed(indc,1,left)+this%processed(indc,1,right)) &
         / this%c(indc,center)**2
      this%k_u_mod(indc)=                                            &
         k_u * f                                                     &
         *( this%processed(indc,1,left)                              &
           +this%processed(indc,1,right))                            &
         *  this%c(indc,center)                                      &
         *( this%U(indc,right) - this%U(indc,left))

      !***********************************************************************
      !     Mach number based on left cell speed of sound
      !***********************************************************************
      this%M(indc, left) = this%U(indc, left)/this%c(indc,center)
      this%M(indc,right) = this%U(indc,right)/this%c(indc,center)

      !***********************************************************************
      !     positive/negative Mach number / pressure weight
      !***********************************************************************
      mach=this%M(indc,left)
      IF ((ABS(mach)-1.0d0).lt.0.0d0) THEN ! subsonic case
         this%M(indc,left)=                     &
            0.25d0*(mach+1.0d0)**2              &
           +beta*(mach**2-1.0d0)**2
         this%ppm(indc,left)=                   &
            0.25d0*(mach+1.0d0)**2*(2.0d0-mach) &
           +alpha*mach*(mach**2-1.0d0)**2

      ELSE ! supersonic case
         this%M(indc,left)=                     &
           0.50d0*(mach+ABS(mach))
         this%ppm(indc,left)=                   &
           0.50d0*(1.0d0+SIGN(1.0d0,mach))
      ENDIF

      mach=this%M(indc,right)
      IF ((ABS(mach)-1.0d0).lt.0.0d0) THEN
         this%M(indc,right)=                    &
           -0.25d0*(mach-1.0d0)**2              &
           -beta*(mach**2-1.0d0)**2
         this%ppm(indc,right)=                  &
            0.25d0*(mach-1.0d0)**2*(2.0d0+mach) &
           -alpha*mach*(mach**2-1.0d0)**2

      ELSE
         this%M(indc,right)=                    &
            0.50d0*(mach-ABS(mach))
         this%ppm(indc,right)=                  &
            0.50d0*(1.0d0-SIGN(1.0d0,mach))
      ENDIF

   ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

   END ASSOCIATE
#undef indc
#undef indl
#undef indr
END SUBROUTINE


