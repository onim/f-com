!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE EXPVAR(pt,i,j,k)
! U,V,H,FS,cp,h,dt VARIABLES PREPARATION (Expand)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,nt,&
                                      dtMax,dtMin,CFLMax,ib,nb
   USE M_BlockPointData,         ONLY:T_PointData
   USE thermal_Property,         ONLY:wMol,Td,rs
   USE thermocoeff,              ONLY:NASAPolynomial,&
                                      NASAPolynomial7,&
                                      NASAPolynomial9
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   TYPE(T_PointData),INTENT(INOUT)::pt
   INTEGER,INTENT(IN)::i,j,k
   INTEGER:: is
   REAL(8):: rm,cvm,intEnergy,uvw,dt
   REAL(8),PARAMETER::tllimit=200.0d0
   CLASS(NASAPolynomial),ALLOCATABLE::tdrm

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ASSOCIATE(gxx=>pt%gd%gxx, &
             gxy=>pt%gd%gxy, &
             gxz=>pt%gd%gxz, &
             gyx=>pt%gd%gyx, &
             gyy=>pt%gd%gyy, &
             gyz=>pt%gd%gyz, &
             gzx=>pt%gd%gzx, &
             gzy=>pt%gd%gzy, &
             gzz=>pt%gd%gzz, &
             ro=>pt%c%ro, &
             ru=>pt%c%ru, &
             rv=>pt%c%rv, &
             rw=>pt%c%rw, &
             re=>pt%c%re, &
             rfs=>pt%c%rfs, &
             t =>pt%c%t, &
             u =>pt%c%u, &
             v =>pt%c%v, &
             w =>pt%c%w, &
             h =>pt%c%h, &
             c =>pt%c%c, &
             p =>pt%c%p, &
             gamma=>pt%c%gamma, &
             fs=>pt%c%fs, &
             yfs=>pt%c%yfs, &
             cps=>pt%t%cps, &
             hf=>pt%t%hf)

   IF    (nt.eq.7) THEN
         ALLOCATE(NASAPolynomial7::tdrm)
   ELSEIF(nt.eq.9) THEN
         ALLOCATE(NASAPolynomial9::tdrm)
   ELSE
         CALL ErrorStop(2105)
   ENDIF

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!***PREPARATION - velocity, mass fraction, mole fraction, Tdrm
   ! Compute velocities
   u=ru/ro
   v=rv/ro
   w=rw/ro

   ! Mass fraction(fs,  =w_i or Y_i) and
   !  concentration(yfs, =c_i =x_i*c)
   fs=rfs/ro
   yfs=rfs/wMol

   ! Temperature
   !  For calculation of thermal properties such as specific heat and
   !  enthalpy, this code prepares MASS AVERAGED COEFFICIENTS tdrm
   !  get mixture gas constant
   rm=SUM(rs*fs(:))

   ! calculate temperature from re
   CALL tdrm%makeMixture(Td,fs,rs)

   ! internal energy from re
   intEnergy = re/ro-0.5d0*(u**2+v**2+w**2)
   CALL tdrm%putTemp(t,intEnergy,rm,cv=cvm)

   ! Calculate C_p and h from NASA polynomial
   ! TBnd divides low temperature region and high t region
   ! Calculate values of each species at each iteration head
   DO is=1,ns
         cps(is)=Td(is)%specheat(t,rs(is))
         hf(is) =Td(is)%enthalpy(t,rs(is))
   ENDDO


   ! pressure, specific heat ratio, total enthalpy, sound speed
   p=ro*rm*t
   gamma=1.0d0+rm/cvm
   h=(re+p)/ro
   c=SQRT(gamma*rm*t)


   ! TIME STEP from possible maximun speed uvw
   uvw= &
       SQRT( (gxx*u+gxy*v+gxz*w)**2 &
            +(gyx*u+gyy*v+gyz*w)**2 &
            +(gzx*u+gzy*v+gzz*w)**2)&
      +c*SQRT( gxx**2+gxy**2+gxz**2 &
              +gyx**2+gyy**2+gyz**2 &
              +gzx**2+gzy**2+gzz**2)


   dt=CFLMax/uvw
   dtMax=MAX(dt,dtMax)
   dtMin=MIN(dt,dtMin)

   ! Error check for dt
   IF (dt.le.0.0d0) CALL ErrorStop(2100)

   ! detect NaN
   IF (uvw.ne.uvw) THEN
         PRINT *,'  Position: ',ib,i,j,k
         PRINT *,'t, p, c',T,p,c
         PRINT *,'ro, fs',ro,fs
         CALL ErrorStop(2101)
   ENDIF

   END ASSOCIATE
END SUBROUTINE
