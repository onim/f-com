!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     transport phenomena
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE VISCOUS
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data
      USE control_Data,             ONLY:nx,ny,nz,ns,ib,
     &                                   avoid_Radical,
     &                                   nonRadical,num_nonRadical
      USE flux_difference,          ONLY:dru,drv,drw,dre,drfs
      USE transportModule,          ONLY:fluxsum,
     &                                   pt,pts,face,faces
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE
      INTEGER::is,dimen

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     variables defined in module transport::face
!     face:  interface             data for transport phenomena
#define a1 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS
      ALLOCATE(pt(a1),pts(a1,ns),
     &         face(a1),faces(a1,ns))

#undef a1

!     Use radical species or not
      IF (.not.avoid_Radical) THEN
            num_nonRadical=ns
            DO is=1,ns
                  nonRadical(is)=is
            ENDDO
      ENDIF

!**********************************************************************
!***Transport terms
!**********************************************************************
!   >>>viscosity TERM<<<
      CALL getViscosity

!   >>>DIFFUSIVITY TERM<<<
      CALL getDiffusion

!   >>>THERMAL FLUX<<<
      CALL getConduction

!**********************************************************************
!***calculation on boundary of each direction
!**********************************************************************
      DO dimen=1,3
!   >>>Physical quantities difference i.e. grad(w_i, u, T)<<<
      CALL qnt_gradient(dimen)

!   >>>viscosity TERM<<<
      CALL momentumDiffusion(dimen)

!   >>>DIFFUSIVITY TERM<<<
      CALL massDiffusion(dimen)

!   >>>THERMAL FLUX<<<
      CALL heatDiffusion(dimen)


!**********************************************************************
!***Add transport fluxes into increment terms
!**********************************************************************
      dru=dru-fluxsum(face%rufx,face%rufy,face%rufz,dimen)
      drv=drv-fluxsum(face%rvfx,face%rvfy,face%rvfz,dimen)
      drw=drw-fluxsum(face%rwfx,face%rwfy,face%rwfz,dimen)

      dre=dre-fluxsum(face%refx,face%refy,face%refz,dimen)

      DO is=1,ns
      drfs(:,:,:,is)=drfs(:,:,:,is)-
     &          fluxsum(faces(:,:,:,is)%rsfx,
     &                  faces(:,:,:,is)%rsfy,
     &                  faces(:,:,:,is)%rsfz,dimen)
      ENDDO

      ENDDO
!**********************************************************************
!***Free the memory
!**********************************************************************
      DEALLOCATE(pt,pts,face,faces)

      END SUBROUTINE
