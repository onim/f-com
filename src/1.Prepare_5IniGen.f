!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             INITIAL DATA SETTING
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE IniGen
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:roRef,ruRef,reRef,
     &                                   chInit,iC,pI,iniCnd
      USE Message_Passing_Interface

      IMPLICIT NONE

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      CALL IniGen_ReadCondition

!***INITIAL CONDITION DATA GENERATION
!   iniCnd=.true. : READ INTERMEDIATE DATA AND CONTINUE CALCULATION
!   OTHERWISE: START CALCULATION FROM IT=1
      IF(iniCnd) THEN
         CALL IniGen_Generation
         CALL IniGen_ReadPrevResult
         IF (primeNode()) PRINT *,'Initial condition loading from ',
     &                            TRIM(chInit),' successful.'
      ELSE
         CALL IniGen_Generation
         IF (primeNode()) PRINT *,'Initial condition generation successful.'
      ENDIF

      END SUBROUTINE IniGen

