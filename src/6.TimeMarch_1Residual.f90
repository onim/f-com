!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE RSZERO
!     MAKE BOUNDARY RESIDUAL ZERO
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl,zeroFluxDifference
   USE control_Data,             ONLY:nx, ny, nz, ns, ib
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   INTEGER::i,j,k
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ASSOCIATE(f=>bl(ib)%p(:,:,:)%f)
      DO k=1,nGS; DO j=1,ny+2*nGS; DO i=1,nx+2*nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO
      DO k=1,nz+2*nGS; DO j=1,nGS; DO i=1,nx+2*nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO
      DO k=1,nz+2*nGS; DO j=1,ny+2*nGS; DO i=1,nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO

      DO k=nz+nGS+1,nz+2*nGS; DO j=1,ny+2*nGS; DO i=1,nx+2*nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO
      DO k=1,nz+2*nGS; DO j=ny+nGS+1,ny+2*nGS; DO i=1,nx+2*nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO
      DO k=1,nz+2*nGS; DO j=1,ny+2*nGS; DO i=nx+nGS+1,nx+2*nGS
         f(i,j,k)=zeroFluxDifference
      ENDDO;ENDDO;ENDDO
   END ASSOCIATE

END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE DTRHS
!     CALCULATION OF THE RESIDUA IN THE GENERAL COORDINATES
!     subroutine name: dt on right hand side?
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,dt,ib
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   INTEGER::i,j,k

!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forInnerPoints_
#define f bl(ib)%p(i,j,k)%f
      f%dro=-1.0d0*f%dro*dt
      f%dru=-1.0d0*f%dru*dt
      f%drv=-1.0d0*f%drv*dt
      f%drw=-1.0d0*f%drw*dt
      f%dre=-1.0d0*f%dre*dt
      f%drfs=-1.0d0*f%drfs*dt
#undef f
   _endInnerPoints_
!$OMP END PARALLEL DO

END SUBROUTINE
