!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Error processing subprograms
!
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ErrorStop(errCode)
      USE control_Data
      USE M_BlockPointData

      IMPLICIT NONE

      INTEGER,INTENT(IN)::errCode
!     Meaning of errCode
!
!     1st, 2nd digit: numbering of the file from which this error process is called.
!           e.g. 7200 from 7.Finishing_2boundCond.f
!     3rd, 4th digit: index in that file from 00 to 99


!$$$$$$$$$$$$$$$$$$$$$
!     Monitor Output
!$$$$$$$$$$$$$$$$$$$$$
      PRINT *,'Error ',errCode

      SELECT CASE(errCode)

      CASE(1401)
         PRINT *,'Negative volume on block ',ib,', node ',MINLOC(bl(ib)%p%gd%gj)

      CASE(2100)
         PRINT *,'uvw error: zero uvw'

      CASE(2101)
         PRINT *,'NaN in uvw'

      CASE(2102)
         PRINT *,'Too much high or low temperature'

      CASE(5401)
         PRINT *, 'ib:', ib

      CASE(7200)
         PRINT *,'Boundary type assignment is wrong'

      CASE DEFAULT
         PRINT *,'Set appropriate error program'
      ENDSELECT

!$$$$$$$$$$$$$$$$$$$$$
!     Treatment
!$$$$$$$$$$$$$$$$$$$$$
      SELECT CASE(errCode)

      CASE(1401,2102)
         CONTINUE

      CASE DEFAULT
         CALL OUTRSL
         CALL statout
      ENDSELECT

!$$$$$$$$$$$$$$$$$$$$$
!     Stopping the main program
!$$$$$$$$$$$$$$$$$$$$$
      SELECT CASE(errCode)
      CASE(1401,2102)
         CONTINUE

      CASE DEFAULT
         STOP

      ENDSELECT

      ENDSUBROUTINE
