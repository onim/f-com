!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE IniGen_ReadCondition
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:iC,pI,roRef,ruRef,reRef, &
                                      chCond,ns
   USE boundary_Data_OOP,        ONLY:bf
   USE M_BlockPointData,         ONLY:initQuant
   USE readCommentedFile,        ONLY:readc

IMPLICIT NONE

   INTEGER::i,is
   INTEGER,ALLOCATABLE,DIMENSION(:)::dummy
   REAL(8),PARAMETER::eps=1.0d-10
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ! Read initial condition
   OPEN(55,file=chCond,form='formatted')
   CALL readc(55,r=iC%WallTemp)
   CALL readc(55,iC%FlowTemp)
   CALL readc(55,iC%Mach)
   CALL readc(55,iC%uvwDirection)
   CALL readc(55,iC%Pressure)
   ALLOCATE(iC%ssin(ns))
   DO is=1,ns
      CALL readc(55,iC%ssin(is))
   ENDDO


   ! Read boundary condition
   CALL readc(55,iC%isPartialBC)

   IF (iC%isPartialBC) THEN
      CALL readc(55,iC%nPBC)
      ALLOCATE(pI(iC%nPBC))
      ALLOCATE(dummy(2))
      DO i=1,iC%nPBC
         CALL readc(55,dummy)
         pI(i)%pBlock=dummy(1)
         pI(i)%pFace=dummy(2)
         bf(pI(i)%pBlock,pI(i)%pFace)%nFBC= &
            bf(pI(i)%pBlock,pI(i)%pFace)%nFBC+1
         pI(i)%pIndFBC=bf(dummy(1),dummy(2))%nFBC
         CALL readc(55,pI(i)%pshape)
         SELECT CASE (TRIM(pI(i)%pshape))
         CASE ('rectangle')
            CALL readc(55,pI(i)%pxyz1)
            CALL readc(55,pI(i)%pxyz2)
         CASE ('circle')
            CALL readc(55,pI(i)%cxyz)
            CALL readc(55,pI(i)%radius)
         ENDSELECT
         CALL readc(55,pI(i)%uvwDirection)
         CALL readc(55,pI(i)%flowTemp)
         CALL readc(55,pI(i)%Mach)
         CALL readc(55,pI(i)%Pressure)
         ALLOCATE(pI(i)%ssin(ns))
         DO is=1,ns
            CALL readc(55,pI(i)%ssin(is))
         ENDDO
      ENDDO
   ENDIF

   CLOSE(55)

   CALL iC%CondToProperty(initQuant)

   !***REFFERENCE FOR RESIDUAL
   roRef=initQuant%ro
   ruRef=initQuant%ru
   reRef=initQuant%re

   IF (roRef.lt.eps .or. &
       roRef.lt.eps .or. &
       roRef.lt.eps) THEN
         CALL ErrorStop(1503)
   ENDIF

   CALL IniGen_ApplyBoundaryCondition

END SUBROUTINE
