!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE OUTRSL
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data
   USE M_BlockPointData,         ONLY:bl
   USE Message_Passing_Interface


IMPLICIT NONE

   INTEGER::i,j,k,is
   CHARACTER*40 chrslt
   CHARACTER*6 itrchr

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   gathering: _forAllBlocks_
   ASSOCIATE(c=>bl(ib)%p%c)
      IF(IamPrimeNode().and.(.not. myBlockIs(ib))) THEN
         CALL MPI_RECV(c%ro,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%ru,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%rv,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%rw,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%re,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%t, SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%p, SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%c, SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         CALL MPI_RECV(c%qchem,SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         DO is=1,ns
         CALL MPI_RECV(c%rfs(is),SIZE(c),MPI_REAL8,blockOwner(ib),0,MPI_COMM_WORLD,MPIStatus,ierr)
         ENDDO
      ELSEIF((.not.IamPrimeNode()).and.myBlockIs(ib)) THEN
         CALL MPI_SEND(c%ro,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%ru,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%rv,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%rw,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%re,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%t, SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%p, SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%c, SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         CALL MPI_SEND(c%qchem,SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         DO is=1,ns
         CALL MPI_SEND(c%rfs(is),SIZE(c),MPI_REAL8,root,0,MPI_COMM_WORLD,ierr)
         ENDDO
      ENDIF
   END ASSOCIATE
   _endAllBlocks_ gathering

   IF(IamPrimeNode()) THEN
      ! PROCESS RESULTS OUTPUT
      !  determine result output filenames by iteration number.
      IF(it.gt.999999) THEN
         WRITE(6,*) 'Too much iteration number, change source code'
      ENDIF

      ! convert iteration number into character type
      WRITE(itrchr,'(I6)') it

      ! replace spaces with 0
      itrchr=repeat('0',scan(itrchr,' ',BACK=.TRUE.))//TRIM(ADJUSTL(itrchr))

      ! output filename
      chrslt='rsl'//itrchr//'.dat'
      OPEN(60,FILE=chrslt,FORM='UNFORMATTED')

      ! output some parameters used for making vtk format file.
      WRITE(60) IT,dqMax,dqNorm,TIME

      _forAllBlocks_
         _forInnerPoints_
         ASSOCIATE(c=>bl(ib)%p(i,j,k)%c)
            WRITE(60) c%RO,c%RU,c%RV,c%RW,c%RE, c%T, &
                      c%p, SQRT(c%ru**2+c%rv**2+c%rw**2)/c%c/c%ro, &
                      c%qChem, (c%RFS(is),is=1,ns)
         END ASSOCIATE
         _endInnerPoints_
      _endAllBlocks_

      CLOSE(60)
   ENDIF

END SUBROUTINE

