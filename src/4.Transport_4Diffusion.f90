!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE getDiffusion
!  REFERENCE: Theoretical and numerical combustion, 2nd ed., T. Poinsot
!              Ch. 1, Diffusion
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,ib
   USE M_BlockPointData,         ONLY:bl
   USE thermal_Property,         ONLY:rs,sigmm,vdif,teki,wMol
   USE fundamental_Constants,    ONLY:nGS,RUni
   USE M_Transport,            ONLY:rci11,bidiffuse,pts

   IMPLICIT NONE
   REAL(8)::rm,mWMol,Patm ! rm: mixture gas constant mWMol: mixture molecular weight Patm: pressure in atm
   REAL(8),DIMENSION(ns,ns)::difi
   REAL(8),DIMENSION(ns)::dif

   INTEGER::i,j,k,l,m

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   !   >>>DIFFUSIVITY TERM<<<

!$OMP PARALLEL DO PRIVATE(i,j,k,rm,mWMol,Patm,difi,dif)
   _forAllPoints_
#define c bl(ib)%p(i,j,k)%c
      RM=SUM(RS*c%fs)
      mWMol=RUni/rm
      Patm=c%p*1.0d-5 ! pressure in bar

      DO l=1,ns-1
         DO m=l+1,ns
            DIFI(l,m)=bidiffuse(Patm,c%t,sigmm(l,m),vdif(l,m),rci11(c%t,TEKI(l,m)))
        ENDDO
      ENDDO

      dif(1)=(1.0d0-c%fs(1)) &
         /( SUM(c%yfs(  2:ns)/difi(1,2:ns)))
      DO l=2,ns-1
      dif(l)=(1.0d0-c%fs(l)) &
         /( SUM(c%yfs(l+1:ns)/difi(l,l+1:ns)) &
           +SUM(c%yfs( 1:l-1)/difi( 1:l-1,l)))
      ENDDO
      dif(ns)=(1.0d0-c%fs(ns)) &
         /( SUM(c%yfs(1:ns-1)/difi(1:ns-1,ns)))


      IF(c%yfs(1).gt.0.99d0) THEN
         dif(1)= (ns-1) &
                /( SUM(mWMol/wMol(2:)/difi(1,2:ns)))
      ENDIF

      DO l=2,ns-1
         IF(c%yfs(l).gt.0.99d0) THEN
            dif(l)= (ns-1) &
               /( SUM(mWMol/wMol(l+1:)/difi(l,l+1:ns)) &
                 +SUM(mWMol/wMol(:l-1)/difi( 1:l-1,l)))
         ENDIF
      ENDDO

      IF(c%yfs(ns).gt.0.99d0) THEN
         dif(ns)=(ns-1) &
            /( SUM(mWMol/wMol(:ns-1)/difi(1:ns-1,ns)))
      ENDIF

      dif=dif*SUM(c%yfs)
!     dif -> dif*rho
      dif=dif*c%ro

      pts(i,j,k,:)%dif=dif

#undef c
   _endAllPoints_
!$OMP END PARALLEL DO

END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE massDiffusion(dimen)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,ib
   USE M_BlockPointData,         ONLY:bl
   USE fundamental_Constants,    ONLY:SCT,nGS
   USE M_Transport,            ONLY:pt, pts, face, faces

IMPLICIT NONE

   INTEGER,INTENT(IN)::dimen
   INTEGER::i,j,k,ish,jsh,ksh,is
   REAL(8)::meanDif
   REAL(8),DIMENSION(nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS):: &
      rsfxSum, rsfySum, rsfzSum

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ish=0
   jsh=0
   ksh=0
   SELECT CASE(dimen)
      CASE(1)
         ish=1
      CASE(2)
         jsh=1
      CASE(3)
         ksh=1
   END SELECT

#define FACE  face(i,j,k)
#define FACES faces(i,j,k,is)
#define FS    bl(ib)%p(i,j,k)%c%fs(is)

!$OMP PARALLEL DO PRIVATE(i,j,k,is,meanDif)
   DO is=1,ns
   DO k=nGS,nz+nGS;DO j=nGS,ny+nGS;DO i=nGS,nx+nGS
      ! sct: schmidt number
      ! rodif: turbulent diffusion coefficient * density
      ! RODIF= 0.5d0*(pts(i,j,k,:)%dif+pts(i+ish,j+ish,k+ish,:)%dif)
      ! &      +face(i,j,k)%vist/sct

      ! rsfx: rho * D * dw_kdx_i = rho * v_i,k * w_k
      meanDif=0.5d0*(pts(i,j,k,is)%dif+pts(i+ish,j+ish,k+ish,is)%dif)
      FACES%RSFX=(meanDif+FACE%vist/sct)*FACES%dsx
      FACES%RSFY=(meanDif+FACE%vist/sct)*FACES%dsy
      FACES%RSFZ=(meanDif+FACE%vist/sct)*FACES%dsz
   ENDDO;ENDDO;ENDDO
   ENDDO
!$OMP END PARALLEL DO

   rsfxSum=sum(faces%rsfx,DIM=4)
   rsfySum=sum(faces%rsfy,DIM=4)
   rsfzSum=sum(faces%rsfz,DIM=4)

!$OMP PARALLEL DO PRIVATE(i,j,k,is)
   DO is=1,ns
   DO k=nGS,nz+nGS;DO j=nGS,ny+nGS;DO i=nGS,nx+nGS
      ! Satisfy mass conservation
      FACES%rsfx=FACES%rsfx-rsfxSum(i,j,k)*FS
      FACES%rsfy=FACES%rsfy-rsfySum(i,j,k)*FS
      FACES%rsfz=FACES%rsfz-rsfzSum(i,j,k)*FS

   ENDDO;ENDDO;ENDDO
   ENDDO
!$OMP END PARALLEL DO

#undef face
#undef FACES
#undef fs

END SUBROUTINE
