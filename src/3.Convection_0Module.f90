!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE M_ConvectModule
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,ne,nb,ib
   USE M_BlockPointData,         ONLY:bl
   USE Status_Check,             ONLY:AUSMFlag
   USE fundamental_Constants,    ONLY:nGS,left,right,center

IMPLICIT NONE
   PRIVATE
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! Intrinsic data type definitions
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   INTEGER,PUBLIC::i,j,k,ish,jsh,ksh,dimen

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! Derived data type definitions
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!<<Abstract Class
   TYPE,ABSTRACT:: T_ConvectionClass
   ! flux variable
      REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::flux
   CONTAINS
      PROCEDURE(template),DEFERRED::alloc
      PROCEDURE(template),DEFERRED::getPrimitive
      PROCEDURE(template),DEFERRED::calcCellValue
      PROCEDURE(template),DEFERRED::calcFaceArea
      PROCEDURE(template),DEFERRED::calcFluxRate
      PROCEDURE(template),DEFERRED::calcFlux
   ENDTYPE

   ABSTRACT INTERFACE
      SUBROUTINE template(this)
         IMPORT T_ConvectionClass
         CLASS(T_ConvectionClass)::this
      END SUBROUTINE
   END INTERFACE
!  Abstract Class>>


   !-------------------------------------
   !     A class for AUSM+-up and MUSCL
   !-------------------------------------
   TYPE,EXTENDS(T_ConvectionClass):: T_AUSMplusUp_MUSCL
      ! Primitive variables
      REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::primitive
      ! Derived variables
      ! (xindex, yindex, zindex, equation, left/right/center)
      REAL(8),DIMENSION(:,:,:,:,:),ALLOCATABLE::processed, phi, p
      ! (xindex, yindex, zindex, left/right/center)
      REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::M, c, ppm, U
      ! (xindex, yindex, zindex)
      REAL(8),DIMENSION(:,:,:),ALLOCATABLE::k_p_mod, k_u_mod

   CONTAINS
      PROCEDURE::alloc         => allocAM
      PROCEDURE::getPrimitive  => getPrimitiveAM
      PROCEDURE::calcCellValue => TVD
      PROCEDURE::calcFaceArea
      PROCEDURE::calcFluxRate  => AUSMPlusUp
      PROCEDURE::calcFlux      => calcFluxAM
   END TYPE

   TYPE(T_AUSMplusUp_MUSCL)::conv
   PUBLIC::conv

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
SUBROUTINE allocAM(this)
IMPLICIT NONE

   CLASS(T_AUSMplusUp_MUSCL)::this

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ! indexing convention
   !  An interface on the right(i.e. bigger index direction)
   !  has the same index with the given cell.
   IF (ALLOCATED(this%flux)) THEN
      DEALLOCATE( &
         this%flux     , &
         this%primitive, &
         this%processed, &
         this%phi,       &
         this%p  ,       &
         this%M  ,       &
         this%c  ,       &
         this%ppm,       &
         this%U  ,       &
         this%k_p_mod  , &
         this%k_u_mod)
   ENDIF

#define cellindex nx+2*nGS,ny+2*nGS,nz+2*nGS
#define interfaceindex nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS
   ALLOCATE( &
      this%flux     (interfaceindex,ne), &
      this%primitive(     cellindex,ne), &
      this%processed(interfaceindex,ne,2), &
      this%phi      (interfaceindex,ne,2), &
      this%p        (interfaceindex,2:4,2), &
      this%M        (interfaceindex,3), &
      this%c        (interfaceindex,3), &
      this%ppm      (interfaceindex,2), &
      this%U        (interfaceindex,2), &
      this%k_p_mod  (interfaceindex), &
      this%k_u_mod  (interfaceindex) )
#undef cellindex
#undef interfaceindex

END SUBROUTINE

!$$$$$$$$$$$$$$$
SUBROUTINE getPrimitiveAM(this)
IMPLICIT NONE
   CLASS(T_AUSMplusUp_MUSCL)::this

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ASSOCIATE(c=>bl(ib)%p%c)
!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forAllPoints_
      this%primitive(i,j,k,1)   =c(i,j,k)%ro
      this%primitive(i,j,k,2)   =c(i,j,k)%ru
      this%primitive(i,j,k,3)   =c(i,j,k)%rv
      this%primitive(i,j,k,4)   =c(i,j,k)%rw
      this%primitive(i,j,k,5)   =c(i,j,k)%t
      this%primitive(i,j,k,6:ne)=c(i,j,k)%rfs
   _endAllPoints_
!$OMP END PARALLEL DO
   END ASSOCIATE

END SUBROUTINE


!$$$$$$$$$$$$$$$
!  SUBROUTINE TVD(this)
#include '3.Convection_3TVD.f90'



!$$$$$$$$$$$$$$$
!  SUBROUTINE AUSMPlusUp(this)
#include '3.Convection_2AUSMPlusUp.f90'


!$$$$$$$$$$$$$$$
SUBROUTINE calcFaceArea(this)
#define indc i,j,k
#define indl i,j,k
#define indr i+ish,j+jsh,k+ksh
IMPLICIT NONE
   CLASS(T_AUSMplusUp_MUSCL)::this
   REAL(8)::area

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ASSOCIATE(gxxf=>bl(ib)%p(:,:,:)%gd%gxxf, &
             gxyf=>bl(ib)%p(:,:,:)%gd%gxyf, &
             gxzf=>bl(ib)%p(:,:,:)%gd%gxzf, &
             gyxf=>bl(ib)%p(:,:,:)%gd%gyxf, &
             gyyf=>bl(ib)%p(:,:,:)%gd%gyyf, &
             gyzf=>bl(ib)%p(:,:,:)%gd%gyzf, &
             gzxf=>bl(ib)%p(:,:,:)%gd%gzxf, &
             gzyf=>bl(ib)%p(:,:,:)%gd%gzyf, &
             gzzf=>bl(ib)%p(:,:,:)%gd%gzzf, &
             contraNorm_x=>bl(ib)%p(:,:,:)%gd%contraNorm_x, &
             contraNorm_y=>bl(ib)%p(:,:,:)%gd%contraNorm_y, &
             contraNorm_z=>bl(ib)%p(:,:,:)%gd%contraNorm_z, &
             gj=>bl(ib)%p(:,:,:)%gd%gj)

   SELECT CASE(dimen)
      CASE (1)
!$OMP PARALLEL DO PRIVATE(i,j,k,area)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
            area=(                             &
                  contraNorm_x(indl)*gj(indl) &
                 +contraNorm_x(indr)*gj(indr) &
                 )*0.5d0

            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gxxf(indc)
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gxyf(indc)
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gxzf(indc)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (2)
!$OMP PARALLEL DO PRIVATE(i,j,k,area)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
            area=(                            &
                  contraNorm_y(indl)*gj(indl) &
                 +contraNorm_y(indr)*gj(indr) &
                 )*0.5d0

            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gyxf(indc)
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gyyf(indc)
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gyzf(indc)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (3)
!$OMP PARALLEL DO PRIVATE(i,j,k,area)
         DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
            area=(                            &
                  contraNorm_z(indl)*gj(indl) &
                 +contraNorm_z(indr)*gj(indr) &
                 )*0.5d0

            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gzxf(indc)
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gzyf(indc)
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gzzf(indc)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
   ENDSELECT
   END ASSOCIATE
#undef indc
#undef indl
#undef indr
END SUBROUTINE


!$$$$$$$$$$$$$$$
SUBROUTINE calcFluxAM(this)

IMPLICIT NONE
   CLASS(T_AUSMplusUp_MUSCL)::this

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!$OMP PARALLEL DO PRIVATE(i,j,k)
   DO k=1-ksh+nGS,nz+nGS;DO j=1-jsh+nGS,ny+nGS;DO i=1-ish+nGS,nx+nGS
      this%flux(i,j,k,:)=0.0d0
      this%M(i,j,k,center)= &
         this%M(i,j,k,left)+this%M(i,j,k,right) &
        -this%k_p_mod(i,j,k)

      ! Add Advection terms
      IF (this%M(i,j,k,center).ge.0.0d0) THEN
         this%flux(i,j,k,:)=        &
           ( this%M(i,j,k,center)   &
            *this%c(i,j,k,center)   &
            *this%phi(i,j,k,:,left) &
           )

      ELSE
         this%flux(i,j,k,:)=        &
           ( this%M(i,j,k,center)   &
            *this%c(i,j,k,center)   &
            *this%phi(i,j,k,:,right)&
           )
      ENDIF

      !     Add pressure terms
      this%flux(i,j,k,2:4)=this%flux(i,j,k,2:4) &
          +( ( this%ppm(i,j,k,left)             &
              *this%p(i,j,k,2:4,left)           &
             )                                  &
            +( this%ppm(i,j,k,right)            &
              *this%p(i,j,k,2:4,right)          &
             )                                  &
            -( this%k_u_mod(i,j,k)              &
              *this%ppm(i,j,k,left)             &
              *this%ppm(i,j,k,right)            &
             )                                  &
           )

   ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE



!$$$$$$$$$$$$$$$
PURE SUBROUTINE reconstruct(processed, phi, c, press)
   USE thermal_Property,      ONLY:Td, rs
   USE thermocoeff,           ONLY:NASAPolynomial9

IMPLICIT NONE
   REAL(8),INTENT(IN),DIMENSION(ne)::processed
   REAL(8),INTENT(OUT),DIMENSION(ne)::phi
   REAL(8),INTENT(OUT),DIMENSION(3)::press
   REAL(8),INTENT(OUT)::c
   TYPE(NASAPolynomial9)::TdMix

   REAL(8)::rm, p, uSq, rht, gamma

   !<<Reconstruct other variables
#define frac processed(6:ne)
#define rho processed(1)
#define temp processed(5)
   rm=SUM(rs*frac/rho)
   p=rho*rm*temp
   CALL TdMix%makeMixture(td, frac/rho, rs)
   uSq=( processed(2)**2 &
        +processed(3)**2 &
        +processed(4)**2)/rho

   rht=rho*TdMix%enthalpy(temp)+0.5d0*uSq

   ! critical speed of sound
   gamma =  TdMix%specheat(temp) &
          /(TdMix%specheat(temp)-rm)
   c=SQRT(gamma*rm*temp)

   c=SQRT((2.0d0*c**2+(gamma-1.0d0)*uSq/rho) &
          /(gamma+1.0d0))
#undef frac
#undef rho
#undef temp
   !  Reconstruct other variables>>

   ! Output reconstructed phi and pressure
   phi=processed
   phi(5)=rht
   press=p

END SUBROUTINE


!$$$$$$$$$$$$$$$
PURE FUNCTION MUSCL(var) RESULT(fl)
   REAL(8),DIMENSION(ne),INTENT(in)::var
   REAL(8),DIMENSION(ne)::fl

   fl =MAX(0.0d0, MIN(2.0d0*var,0.5d0*(var+1.0d0),2.0d0))

ENDFUNCTION

END MODULE
