!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             GRID METRIC CALCULATION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE arrayalloc
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
!     all variables in these two modules are allocated in this subroutine.
      USE grid_Data,                ONLY:geo_gl
      USE conserved_Quantity

      USE control_Data,             ONLY:nx,ny,nz,ns,nt,nb,
     &                                   nx_gl,ny_gl,nz_gl,
     &                                   chGrid
      USE boundary_Data_OOP,        ONLY:bf
      USE readCommentedFile
      USE thermal_Property,         ONLY:ther_gl
      USE flux_difference,          ONLY:fdif_gl
      USE Status_Check,             ONLY:mm
      USE Message_Passing_Interface
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

      INTEGER::i
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!#######################################################################
!**** Open geometry data file
      OPEN(54,file=chGrid,FORM='unformatted')

      READ(54) nb
      ALLOCATE(  nx_gl(nb),  ny_gl(nb),  nz_gl(nb),   bf(nb,6),
     &          geo_gl(nb),cons_gl(nb),fdif_gl(nb),ther_gl(nb),
     &          consIS(nb,6),consOS(nb,6),  mm(nb))

!***********************************************************************
!     read the grid number
!***********************************************************************
      READ(54) nx_gl
      READ(54) ny_gl
      READ(54) nz_gl
      CLOSE(54)

!***********************************************************************
!     allocate spatial arrays
!***********************************************************************
      DO i=1,nb
      nx=nx_gl(i)
      ny=ny_gl(i)
      nz=nz_gl(i)
#define a1 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS
#define a2 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS,ns
#define g_g geo_gl(i)
#define c_g cons_gl(i)
#define f_g fdif_gl(i)
#define t_g ther_gl(i)

      ALLOCATE(  g_g%xd_(a1),  g_g%yd_(a1),  g_g%zd_(a1),
     &          g_g%gxx_(a1), g_g%gxy_(a1), g_g%gxz_(a1),
     &          g_g%gyx_(a1), g_g%gyy_(a1), g_g%gyz_(a1),
     &          g_g%gzx_(a1), g_g%gzy_(a1), g_g%gzz_(a1),
     &         g_g%gxxf_(a1),g_g%gxyf_(a1),g_g%gxzf_(a1),
     &         g_g%gyxf_(a1),g_g%gyyf_(a1),g_g%gyzf_(a1),
     &         g_g%gzxf_(a1),g_g%gzyf_(a1),g_g%gzzf_(a1),
     &         g_g%gxxb_(a1),g_g%gxyb_(a1),g_g%gxzb_(a1),
     &         g_g%gyxb_(a1),g_g%gyyb_(a1),g_g%gyzb_(a1),
     &         g_g%gzxb_(a1),g_g%gzyb_(a1),g_g%gzzb_(a1),
     &         g_g%contraNorm_x_(a1),
     &         g_g%contraNorm_y_(a1),
     &         g_g%contraNorm_z_(a1),
     &         g_g%gj_(a1),      g_g%sgsDlt_(a1))

      ALLOCATE(g_g%gridAttr_(a1))

      IF(myBlockIs(i)) THEN
         ALLOCATE(c_g%ro_(a1),c_g%ru_(a1),c_g%rv_(a1),c_g%rw_(a1),c_g%re_(a1),
     &                        c_g%u_(a1), c_g%v_(a1), c_g%w_(a1), c_g%h_(a1),
     &            c_g%c_(a1), c_g%p_(a1), c_g%t_(a1),
     &            c_g%gamma_(a1),         c_g%qChem_(a1))

         ALLOCATE(c_g%rfs_(a2),c_g%fs_(a2),c_g%yfs_(a2))

         ALLOCATE(f_g%dro_(a1),f_g%dru_(a1),f_g%drv_(a1),f_g%drw_(a1),f_g%dre_(a1))

         ALLOCATE(f_g%drfs_(a2))

         ALLOCATE(t_g%cps_(a2),t_g%hf_(a2))

!***********************************************************************
!     Point array slice
!           for data exchange between blocks
!***********************************************************************
         CALL arrayAllocSlice(i)

!***********************************************************************
      ELSEIF(primeNode()) THEN
!     array on node 00 for data output
!***********************************************************************
         ALLOCATE(c_g%ro_(a1),c_g%ru_(a1),c_g%rv_(a1),c_g%rw_(a1),c_g%re_(a1),
     &            c_g%c_(a1), c_g%p_(a1), c_g%t_(a1),
     &            c_g%qChem_(a1))

         ALLOCATE(c_g%rfs_(a2))
      ENDIF

      ENDDO
#undef a1
#undef a2
#undef g_g
#undef c_g
#undef f_g
#undef t_g



      END SUBROUTINE arrayalloc
