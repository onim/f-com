
      SELECT CASE (dimen)
      CASE (1)
!$OMP PARALLEL DO PRIVATE(i,j,k)
      DO k=1,nz;DO j=1,ny;DO i=0,nx
      diff(i,j,k)=
     &      ( (gx(i,j,k)+gx(i+1,j,k))
     &                 *( phi(i+1  ,j,k)
     &                   -phi(i    ,j,k))*2.0d0
     &       +(gy(i,j,k)+gy(i+1,j,k))
     &                 *(+phi(i  ,j+1,k)
     &                   +phi(i+1,j+1,k)
     &                   -phi(i  ,j-1,k)
     &                   -phi(i+1,j-1,k))
     &       +(gz(i,j,k)+gz(i+1,j,k))
     &                 *(+phi(i  ,j,k+1)
     &                   +phi(i+1,j,k+1)
     &                   -phi(i  ,j,k-1)
     &                   -phi(i+1,j,k-1))) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (2)
!$OMP PARALLEL DO PRIVATE(i,j,k)
      DO k=1,nz;DO j=0,ny;DO i=1,nx
      diff(i,j,k)=
     &      ( (gx(i,j,k)+gx(i,j+1,k))
     &                 *(+phi(i+1,j  ,k)
     &                   +phi(i+1,j+1,k)
     &                   -phi(i-1,j  ,k)
     &                   -phi(i-1,j+1,k))
     &       +(gy(i,j,k)+gy(i,j+1,k))
     &                 *(+phi(i,j+1  ,k)
     &                   -phi(i,j    ,k))*2.0d0
     &       +(gz(i,j,k)+gz(i,j+1,k))
     &                 *(+phi(i,j  ,k+1)
     &                   +phi(i,j+1,k+1)
     &                   -phi(i,j  ,k-1)
     &                   -phi(i,j+1,k-1))) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (3)
!$OMP PARALLEL DO PRIVATE(i,j,k)
      DO k=0,nz;DO j=1,ny;DO i=1,nx
      diff(i,j,k)=
     &      ( (gx(i,j,k)+gx(i,j,k+1))
     &                 *(+phi(i+1,j,k  )
     &                   +phi(i+1,j,k+1)
     &                   -phi(i-1,j,k  )
     &                   -phi(i-1,j,k+1))
     &       +(gy(i,j,k)+gy(i,j,k+1))
     &                 *( phi(i,j+1,k  )
     &                   +phi(i,j+1,k+1)
     &                   -phi(i,j-1,k  )
     &                   -phi(i,j-1,k+1))
     &       +(gz(i,j,k)+gz(i,j,k+1))
     &                 *(+phi(i,j,k+1  )
     &                   -phi(i,j,k    ))*2.0d0) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      ENDSELECT
