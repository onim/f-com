!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** variables for thermodynamic data and species

!     wMol        molecular weight (kg/mol)
!     sig         collision diameter (nm)
!     tek         effective temperature (K)
!     rs          species gas constant (J/(kg K))
!
!     sigmm       ...
!     vmol        ...
!     dmol        ...
!     teki        ...
!     vdif        ...
!                 dual quantities used for transport coefficient calculation
!
!     Td          NASA polynomial coefficients
!     Tdrm        NASA coeff of mixture * gas constant
!     TBnd        temperature boundary of NASA polynomials (K)
!     cps         specific heat at constant pressure of each species
!     hf          enthalpy of each species
!     tlimit      upper bound of temperature range of NASA polynomial

      MODULE Thermal_Property
      USE thermocoeff

      IMPLICIT NONE

      SAVE

      REAL(8),ALLOCATABLE::wMol(:),sig(:),tek(:),rs(:),
     &       sigmm(:,:),vmol(:,:),dmol(:,:),teki(:,:),vdif(:,:)
      REAL(8)::tlimit

      CLASS(NASAPolynomial),ALLOCATABLE::Td(:)
      CLASS(NASAPolynomial),ALLOCATABLE::TdMixture

      END MODULE


