!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE qnt_gradient(dimen)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns, ib
   USE M_BlockPointData,         ONLY:bl
   USE M_Transport,              ONLY:diffx,diffy,diffz, &
                                      face,faces
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

   INTEGER,INTENT(IN)::dimen

   INTEGER::is

   REAL(8),DIMENSION(nx+2*nGS,ny+2*nGS,nz+2*nGS)::valArray

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   !**********************************************************************
   !***Physical quantities difference i.e. grad(c_i, v, T) (INNER REGION)
   !**********************************************************************
   !     CENTRAL DIFFERENCE - 1st derivatives
   !     VELOCITIES, TEMPERATURE, MASS FRACTION
#define c bl(ib)%p(:,:,:)%c
   valArray=c%u
   face%dux=diffx(valArray,dimen)
   face%duy=diffy(valArray,dimen)
   face%duz=diffz(valArray,dimen)

   valArray=c%v
   face%dvx=diffx(valArray,dimen)
   face%dvy=diffy(valArray,dimen)
   face%dvz=diffz(valArray,dimen)

   valArray=c%w
   face%dwx=diffx(valArray,dimen)
   face%dwy=diffy(valArray,dimen)
   face%dwz=diffz(valArray,dimen)

   valArray=c%t
   face%dtx=diffx(valArray,dimen)
   face%dty=diffy(valArray,dimen)
   face%dtz=diffz(valArray,dimen)

   DO is=1,ns
      valArray=c%fs(is)
      faces(:,:,:,is)%dsx=diffx(valArray,dimen)
      faces(:,:,:,is)%dsy=diffy(valArray,dimen)
      faces(:,:,:,is)%dsz=diffz(valArray,dimen)
   ENDDO
#undef c
END SUBROUTINE

