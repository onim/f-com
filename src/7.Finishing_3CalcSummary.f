!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     AVERAGE RESIDUAL IN ALL VARIABLES TYPE 1
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE RESID1
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE conserved_Quantity
      USE control_Data
      USE flux_difference
      USE Message_Passing_Interface
      USE fundamental_Constants
      USE AuxSubprogram,            ONLY:arraypoint

      IMPLICIT NONE

      REAL(8)::DQDMY,dqMaxLocal
      INTEGER::i,j,k,IdqMax,JdqMax,KdqMax,IBdqMax,maxNode
      CHARACTER(10)::dqMaxquant
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      IdqMax=0
      JdqMax=0
      KdqMax=0
      IBdqMax=0
      dqMax=0.0D0
      dqMaxLocal=0.0d0
      dqNorm=0.0d0
      dqMaxquant='     error'

      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         CALL arraypoint

         DO k=1,nz; DO j=1,ny; DO i=1,nx

            DQDMY=MAX( ABS(DRO(I,J,K)/roRef)
     &                ,ABS(DRU(I,J,K)/ruRef)
     &                ,ABS(DRV(I,J,K)/ruRef)
     &                ,ABS(DRW(I,J,K)/ruRef)
     &                ,ABS(DRE(I,J,K)/reRef) )

            IF(dqMaxLocal.LE.DQDMY)THEN
               IdqMax=I
               JdqMax=J
               KdqMax=K
               IBdqMax=ib
               dqMaxLocal=DQDMY
               IF      (ABS(dro(i,j,k)/roRef).eq.DQDMY) THEN
                  dqMaxquant='   density'
               ELSEIF ((ABS(dru(i,j,k)/ruRef).eq.DQDMY).or.
     &                 (ABS(drv(i,j,k)/ruRef).eq.DQDMY).or.
     &                 (ABS(drw(i,j,k)/ruRef).eq.DQDMY)) THEN
                  dqMaxquant='  velocity'
               ELSEIF  (ABS(dre(i,j,k)/reRef).eq.DQDMY) THEN
                  dqMaxquant='    energy'
               ELSE
                  PRINT *,'error in residual calculation'
               ENDIF
            END IF
         ENDDO;ENDDO;ENDDO

         dqNorm=dqNorm
     &      +SQRT(SUM((DRO/roRef)**2+(DRU/ruRef)**2
     &               +(DRV/ruRef)**2+(DRW/ruRef)**2
     &               +(DRE/reRef)**2)
     &            /(nb*nf*nx*ny*nz))

      ENDIF; ENDDO multiBlock

      CALL MPI_ALLREDUCE(dqMaxLocal,dqMax,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,dqNorm,1,MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
      maxNode=0
      IF(dqMaxLocal.eq.dqMax) maxNode=myMPINode
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,maxNode,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)

      CALL MPI_BCAST(IdqMax,1,MPI_INTEGER,maxNode,MPI_COMM_WORLD,ierr)
      CALL MPI_BCAST(JdqMax,1,MPI_INTEGER,maxNode,MPI_COMM_WORLD,ierr)
      CALL MPI_BCAST(KdqMax,1,MPI_INTEGER,maxNode,MPI_COMM_WORLD,ierr)
      CALL MPI_BCAST(IBdqMax,1,MPI_INTEGER,maxNode,MPI_COMM_WORLD,ierr)
      CALL MPI_BCAST(dqMaxquant,10,MPI_CHARACTER,maxNode,MPI_COMM_WORLD,ierr)

!     CFLMax is determined by both dqMaxLim and CFLLim.
!           (1) if dqMax is too much large, CFLMax is decreased.
!           (2) CFLMax cannot exceed CFLLim
      CFLMax=MIN(dqMaxLim/dqMax*CFLMax,CFLLim)

      IF(primeNode()) THEN

!     Writing title on the log file / monitor
      IF(IT.EQ.itrBgn) THEN
            OPEN(90,FILE=chConv,FORM='FORMATTED',POSITION='append')
            WRITE(90,'(A)') title
            CLOSE(90)
      END IF

      IF(MOD(IT,itvcnv*15).EQ.0) THEN
            WRITE( *,'(A,A)') ACHAR(13),title
      END IF

!     values same with that on the monitor
      OPEN(90,FILE=chConv,FORM='formatted',POSITION='append')
      WRITE(90,9010)IT,IBdqMax,IdqMax,JdqMax,KdqMax,dqMax,dqNorm,TIME,
     &              DT,CFLMax,dqMaxquant


!     current iteration printout code for gfortran
!#######################################################################
!      IF(MOD(it,itvcnv).EQ.0) THEN
!            WRITE( *,'(a)',ADVANCE='no') ACHAR(13)
!            WRITE( *,9010)IT,IdqMax,JdqMax,KdqMax,dqMax,dqNorm,TIME,
!     +                    DT,CFLMax,dqMaxquant
!      END IF

!     Monitor title/current iteration output
!      WRITE(*,'(A,I6,A)',ADVANCE='no')
!     +       ACHAR(13), it+1, ': current step'
!#######################################################################



!     current iteration printout code for ifort
!#######################################################################
      IF(MOD(it,itvcnv).EQ.0) THEN
            WRITE( *,200) ACHAR(13)
            WRITE( *,9010)IT,IBdqMax,IdqMax,JdqMax,KdqMax,dqMax,dqNorm,TIME,
     &                    DT,CFLMax,dqMaxquant
      END IF
  200 FORMAT(' ',a,$)
  300 FORMAT(' ',A,I6,A,$)
!     Monitor title/current iteration output
      WRITE(*,300)
     &       ACHAR(13), it+1, ': current step'
!#######################################################################

 9010 FORMAT(' ',I5,I3,I4,2I3,5D10.3,A10)
      CLOSE(90)

      ENDIF

      END SUBROUTINE
