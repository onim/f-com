!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE PRESPC
!  PREPARATION of SPECIES DATA
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!  Header
!$$$$$$$$$$$$$$$
   USE control_Data,             ONLY:nr,ns,nt
   USE reaction_data
   USE thermal_Property
   USE Message_Passing_Interface
   USE fundamental_Constants,    ONLY:runi,r2inv
   USE readCommentedFile,        ONLY:readc

IMPLICIT NONE
   INTEGER::i,j,is,l
   REAL(8),ALLOCATABLE::readdummy(:),readdummy2(:,:)

!$$$$$$$$$$$$$$$
!  Main
!$$$$$$$$$$$$$$$
!***********************************************************************
!  allocate the array
!***********************************************************************
   ALLOCATE(cf(nr) ,zetaf(nr) ,ef(nr) ,  &
            cf0(nr),zetaf0(nr),ef0(nr),  &
            acent(nr),ts1(nr),ts2(nr),ts3(nr))

   ! For cps/hf, they're allocated after the grid number is obtained
   ALLOCATE(wMol(ns),rs(ns),sig(ns),tek(ns),sigmm(ns,ns),  &
            vmol(ns,ns),dmol(ns,ns),teki(ns,ns),vdif(ns,ns))

!***********************************************************************
!  Input of thermodynamic data
!  READ TABLE.txt (FILE OF POLYNOMIAL COEFF. OF NASA)
!***********************************************************************
   OPEN(51,FILE='table.txt')

   ALLOCATE(readdummy(1),readdummy2(nt,2))

   IF    (nt.eq.7) THEN
      ALLOCATE(NASAPolynomial7::td(ns))
      DO is=1,ns
         CALL readc(51,readdummy)
         CALL readc(51,readdummy2(:,1),'(7e15.8)')
         CALL readc(51,readdummy2(:,2),'(7e15.8)')
         CALL Td(is)%getCoeff(readdummy2,readdummy(1))
      END DO
      ALLOCATE(NASAPolynomial7::TdMixture)

   ELSEIF(nt.eq.9) THEN
      ALLOCATE(NASAPolynomial9::td(ns))
      DO is=1,ns
         CALL readc(51,readdummy)
         CALL readc(51,readdummy2(:,1),'(9e16.9)')
         CALL readc(51,readdummy2(:,2),'(9e16.9)')
         CALL Td(is)%getCoeff(readdummy2,readdummy(1))
      END DO
      ALLOCATE(NASAPolynomial9::TdMixture)
   ELSE
      CALL ErrorStop(1200)
   ENDIF

   CLOSE(51)
   DEALLOCATE(readdummy2,readdummy)

   IF(IamPrimeNode()) PRINT *,'Thermodynamic data load done.'

!***********************************************************************
!  Input of parameters for chemical reactions
!  READ FREQUENCY FACTOR, TEMPERATURE EXPONENTS, AND ACTIVATION ENERGY
!        FOR EACH REACTIONS
!***********************************************************************
   OPEN(52,FILE='react.dat')
   ALLOCATE(readdummy(11))
   DO l=1,nr
      CALL readc(52,readdummy)
   ! Unit for frequency factor: depends
        cf(l)    =readdummy(1)
        zetaf(l) =readdummy(2)
   ! Unit for activation energy: cal/mol
        ef(l)    =readdummy(3)

        cf0(l)   =readdummy(4)
        zetaf0(l)=readdummy(5)
        ef0(l)   =readdummy(6)

        acent(l) =readdummy(7)
        ts1(l)   =readdummy(8)
        ts2(l)   =readdummy(9)
        ts3(l)   =readdummy(10)

      END DO
      DEALLOCATE(readdummy)

      CLOSE(52)

      IF(IamPrimeNode()) PRINT *,'Chemical reaction data load done.'

   !***** input of parameters for chemical species
   !     Read molecular weight(kg/mol), collision diameter and effective temperature
   !     of each species used for obtaining laminar viscosity and conductivity
   OPEN(53,FILE='viscous.dat')

   !3--->
   ALLOCATE(readdummy(3))
   DO is=1,ns
      CALL readc(53,readdummy,'(3e10.3)')
      wMol(is)=readdummy(1)
      sig(is) =readdummy(2)
      tek(is) =readdummy(3)
   END DO
   DEALLOCATE(readdummy)
   !3<---

   CLOSE(53)

   IF(IamPrimeNode()) PRINT *,'Species parameters load successful.'

   !     --- GAS CONSTANT OF EACH SPECIES ---
   !4--->
   rs=runi/wMol
   !4<---

   ! PREPARATION FOR DIFFUSIVE TERMS
   !  teki: T*_12
   !  sigmm:[sigma_12]                  squared
   !  vdif: [(M_1+M_2)/2(M_1*M_2)]*1000 unit conversion
   !  vmol:
   !  dmol:
   DO i=1,ns; DO j=i,ns
      teki(i,j)=SQRT(tek(i)*tek(j))
   ENDDO;ENDDO

   ! THESE CALCULATIONS ARE PREPARATION FOR GETTING
   ! LAMINAR VISCOSITY AND LAMINAR CONDUCTIVITY
   DO i=1,ns; DO j=1,ns
       sigmm(i,j)=(0.5d0*(sig(i)+sig(j)))**2
       vdif(i,j)=1.0d-3*(wMol(i)+wMol(j))/(wMol(i)*wMol(j))
       vmol(i,j)=(wMol(i)/wMol(j))**0.25d0
       dmol(i,j)=0.5d0*r2inv/sqrt(1.0d0+wMol(i)/wMol(j))
   ENDDO; ENDDO

END SUBROUTINE PRESPC

