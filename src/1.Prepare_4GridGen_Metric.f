!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Subgrid scale and reverse jacobian matrix
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE Metric
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data
      USE control_Data,             ONLY:ib,nb,
     &                                   nx,ny,nz
      USE Message_Passing_Interface
      USE fundamental_Constants,    ONLY:nGS
      USE AuxSubprogram,            ONLY:arraypoint,copyMetric


      IMPLICIT NONE

      REAL(8)::minvolume
      real(8),ALLOCATABLE,DIMENSION(:,:,:)::
     &      gdxx,gdxy,gdxz,
     &      gdyx,gdyy,gdyz,
     &      gdzx,gdzy,gdzz
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

      multiBlock: DO ib=1,nb; IF (myBlockIs(ib).or..true.) THEN

!     point specific block
      CALL arraypoint

!***********************************************************************
!   SUBGRID SCALE for LES
!***********************************************************************
      sgsdlt=((gxx**2+gyx**2+gzx**2)*
     &        (gxy**2+gyy**2+gzy**2)*
     &        (gxz**2+gyz**2+gzz**2))**(1.0d0/6.0d0)

!***********************************************************************
!***REVERSE JACOBIAN
!***********************************************************************
!   Here, GJ is reciprocal of Jacobian. i.e. 1/J
      gj_=gxx_*gyy_*gzz_+gxz_*gyx_*gzy_+gxy_*gyz_*gzx_
     &  -(gxx_*gyz_*gzy_+gxy_*gyx_*gzz_+gxz_*gyy_*gzx_)


#define a1 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS

      ALLOCATE(gdxx(a1),gdxy(a1),gdxz(a1),
     &         gdyx(a1),gdyy(a1),gdyz(a1),
     &         gdzx(a1),gdzy(a1),gdzz(a1))

#undef a1


!      GD: Inversed matrix
      gdxx=gyy_*gzz_-gyz_*gzy_
      gdxy=gzy_*gxz_-gxy_*gzz_
      gdxz=gxy_*gyz_-gyy_*gxz_
      gdyx=gzx_*gyz_-gyx_*gzz_
      gdyy=gxx_*gzz_-gxz_*gzx_
      gdyz=gyx_*gxz_-gxx_*gyz_
      gdzx=gyx_*gzy_-gzx_*gyy_
      gdzy=gxy_*gzx_-gxx_*gzy_
      gdzz=gxx_*gyy_-gyx_*gxy_

      gxx_=gdxx/gj_
      gxy_=gdxy/gj_
      gxz_=gdxz/gj_
      gyx_=gdyx/gj_
      gyy_=gdyy/gj_
      gyz_=gdyz/gj_
      gzx_=gdzx/gj_
      gzy_=gdzy/gj_
      gzz_=gdzz/gj_

      DEALLOCATE(gdxx,gdxy,gdxz,gdyx,gdyy,gdyz,gdzx,gdzy,gdzz)
!     d(xi)/d(x) equals to gxx
!     d(xi)/d(y) equals to gxy

!     probably copyMetric is redundant.
      CALL copyMetric(gxx_)
      CALL copyMetric(gxy_)
      CALL copyMetric(gxz_)
      CALL copyMetric(gyx_)
      CALL copyMetric(gyy_)
      CALL copyMetric(gyz_)
      CALL copyMetric(gzx_)
      CALL copyMetric(gzy_)
      CALL copyMetric(gzz_)
      CALL copyMetric(gj_)

      contraNorm_x_=SQRT(gxx_**2+gxy_**2+gxz_**2)
      contraNorm_y_=SQRT(gyx_**2+gyy_**2+gyz_**2)
      contraNorm_z_=SQRT(gzx_**2+gzy_**2+gzz_**2)

      GXXF_=0.5D0*(CSHIFT(GXX_*GJ_,1,1)+GXX_*GJ_)
      GXYF_=0.5D0*(CSHIFT(GXY_*GJ_,1,1)+GXY_*GJ_)
      GXZF_=0.5D0*(CSHIFT(GXZ_*GJ_,1,1)+GXZ_*GJ_)
      GYXF_=0.5D0*(CSHIFT(GYX_*GJ_,1,2)+GYX_*GJ_)
      GYYF_=0.5D0*(CSHIFT(GYY_*GJ_,1,2)+GYY_*GJ_)
      GYZF_=0.5D0*(CSHIFT(GYZ_*GJ_,1,2)+GYZ_*GJ_)
      GZXF_=0.5D0*(CSHIFT(GZX_*GJ_,1,3)+GZX_*GJ_)
      GZYF_=0.5D0*(CSHIFT(GZY_*GJ_,1,3)+GZY_*GJ_)
      GZZF_=0.5D0*(CSHIFT(GZZ_*GJ_,1,3)+GZZ_*GJ_)

!     probably g??b is redundant.
      GXXB_=0.5D0*(CSHIFT(GXX_*GJ_,-1,1)+GXX_*GJ_)
      GXYB_=0.5D0*(CSHIFT(GXY_*GJ_,-1,1)+GXY_*GJ_)
      GXZB_=0.5D0*(CSHIFT(GXZ_*GJ_,-1,1)+GXZ_*GJ_)
      GYXB_=0.5D0*(CSHIFT(GYX_*GJ_,-1,2)+GYX_*GJ_)
      GYYB_=0.5D0*(CSHIFT(GYY_*GJ_,-1,2)+GYY_*GJ_)
      GYZB_=0.5D0*(CSHIFT(GYZ_*GJ_,-1,2)+GYZ_*GJ_)
      GZXB_=0.5D0*(CSHIFT(GZX_*GJ_,-1,3)+GZX_*GJ_)
      GZYB_=0.5D0*(CSHIFT(GZY_*GJ_,-1,3)+GZY_*GJ_)
      GZZB_=0.5D0*(CSHIFT(GZZ_*GJ_,-1,3)+GZZ_*GJ_)

      minvolume=MIN(MINVAL(gj_(:,1:ny,1:nz)),
     &              MINVAL(gj_(1:nx,:,1:nz)),
     &              MINVAL(gj_(1:nx,1:ny,:)))
      IF (minvolume.eq.0.0d0) THEN
            CALL ErrorStop(1401)
      ENDIF

      ENDIF;ENDDO multiBlock

      END SUBROUTINE Metric

