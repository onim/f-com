!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE Metric
! Subgrid scale and reverse jacobian matrix
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData
   USE control_Data,             ONLY:ib,nb,nx,ny,nz,nx_gl,ny_gl,nz_gl
   USE Message_Passing_Interface
   USE fundamental_Constants,    ONLY:nGS
   USE AuxSubprogram,            ONLY:copyMetric

IMPLICIT NONE

   REAL(8)::minvolume
   REAL(8),ALLOCATABLE,DIMENSION(:,:,:):: &
      gdxx,gdxy,gdxz, &
      gdyx,gdyy,gdyz, &
      gdzx,gdzy,gdzz

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   _forAllBlocks_
   ASSOCIATE(gxx=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gxx, &
             gxy=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gxy, &
             gxz=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gxz, &
             gyx=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gyx, &
             gyy=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gyy, &
             gyz=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gyz, &
             gzx=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gzx, &
             gzy=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gzy, &
             gzz=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gzz, &
             sgsdlt=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%sgsdlt)
      !***********************************************************************
      !   SUBGRID SCALE for LES
      !***********************************************************************
      sgsdlt=((gxx**2+gyx**2+gzx**2)* &
              (gxy**2+gyy**2+gzy**2)* &
              (gxz**2+gyz**2+gzz**2))**(1.0d0/6.0d0)
   END ASSOCIATE

   ASSOCIATE(gxx=>bl(ib)%p(:,:,:)%gd%gxx, &
             gxy=>bl(ib)%p(:,:,:)%gd%gxy, &
             gxz=>bl(ib)%p(:,:,:)%gd%gxz, &
             gyx=>bl(ib)%p(:,:,:)%gd%gyx, &
             gyy=>bl(ib)%p(:,:,:)%gd%gyy, &
             gyz=>bl(ib)%p(:,:,:)%gd%gyz, &
             gzx=>bl(ib)%p(:,:,:)%gd%gzx, &
             gzy=>bl(ib)%p(:,:,:)%gd%gzy, &
             gzz=>bl(ib)%p(:,:,:)%gd%gzz, &
             gxxf=>bl(ib)%p(:,:,:)%gd%gxxf, &
             gxyf=>bl(ib)%p(:,:,:)%gd%gxyf, &
             gxzf=>bl(ib)%p(:,:,:)%gd%gxzf, &
             gyxf=>bl(ib)%p(:,:,:)%gd%gyxf, &
             gyyf=>bl(ib)%p(:,:,:)%gd%gyyf, &
             gyzf=>bl(ib)%p(:,:,:)%gd%gyzf, &
             gzxf=>bl(ib)%p(:,:,:)%gd%gzxf, &
             gzyf=>bl(ib)%p(:,:,:)%gd%gzyf, &
             gzzf=>bl(ib)%p(:,:,:)%gd%gzzf, &
             contraNorm_x=>bl(ib)%p(:,:,:)%gd%contraNorm_x, &
             contraNorm_y=>bl(ib)%p(:,:,:)%gd%contraNorm_y, &
             contraNorm_z=>bl(ib)%p(:,:,:)%gd%contraNorm_z, &
             gj =>bl(ib)%p(:,:,:)%gd%gj)

      !***********************************************************************
      !***REVERSE JACOBIAN
      !***********************************************************************
      !   Here, GJ is reciprocal of Jacobian. i.e. 1/J
      gj=  gxx*gyy*gzz+gxz*gyx*gzy+gxy*gyz*gzx &
         -(gxx*gyz*gzy+gxy*gyx*gzz+gxz*gyy*gzx)


#define a1 nx+2*nGS,ny+2*nGS,nz+2*nGS
      ALLOCATE(gdxx(a1),gdxy(a1),gdxz(a1), &
               gdyx(a1),gdyy(a1),gdyz(a1), &
               gdzx(a1),gdzy(a1),gdzz(a1))
#undef a1


      ! GD: Inversed matrix
      gdxx=gyy*gzz-gyz*gzy
      gdxy=gzy*gxz-gxy*gzz
      gdxz=gxy*gyz-gyy*gxz
      gdyx=gzx*gyz-gyx*gzz
      gdyy=gxx*gzz-gxz*gzx
      gdyz=gyx*gxz-gxx*gyz
      gdzx=gyx*gzy-gzx*gyy
      gdzy=gxy*gzx-gxx*gzy
      gdzz=gxx*gyy-gyx*gxy

      gxx=gdxx/gj
      gxy=gdxy/gj
      gxz=gdxz/gj
      gyx=gdyx/gj
      gyy=gdyy/gj
      gyz=gdyz/gj
      gzx=gdzx/gj
      gzy=gdzy/gj
      gzz=gdzz/gj

      DEALLOCATE(gdxx,gdxy,gdxz,gdyx,gdyy,gdyz,gdzx,gdzy,gdzz)
      !     d(xi)/d(x) equals to gxx
      !     d(xi)/d(y) equals to gxy
      CALL copyMetric(gxx)
      CALL copyMetric(gxy)
      CALL copyMetric(gxz)
      CALL copyMetric(gyx)
      CALL copyMetric(gyy)
      CALL copyMetric(gyz)
      CALL copyMetric(gzx)
      CALL copyMetric(gzy)
      CALL copyMetric(gzz)
      CALL copyMetric(gj)

      contraNorm_x=SQRT(gxx**2+gxy**2+gxz**2)
      contraNorm_y=SQRT(gyx**2+gyy**2+gyz**2)
      contraNorm_z=SQRT(gzx**2+gzy**2+gzz**2)

      GXXF=0.5D0*(CSHIFT(GXX*GJ,1,1)+GXX*GJ)
      GXYF=0.5D0*(CSHIFT(GXY*GJ,1,1)+GXY*GJ)
      GXZF=0.5D0*(CSHIFT(GXZ*GJ,1,1)+GXZ*GJ)
      GYXF=0.5D0*(CSHIFT(GYX*GJ,1,2)+GYX*GJ)
      GYYF=0.5D0*(CSHIFT(GYY*GJ,1,2)+GYY*GJ)
      GYZF=0.5D0*(CSHIFT(GYZ*GJ,1,2)+GYZ*GJ)
      GZXF=0.5D0*(CSHIFT(GZX*GJ,1,3)+GZX*GJ)
      GZYF=0.5D0*(CSHIFT(GZY*GJ,1,3)+GZY*GJ)
      GZZF=0.5D0*(CSHIFT(GZZ*GJ,1,3)+GZZ*GJ)

      minvolume=MIN(MINVAL(gj(:,1+nGS:ny+nGS,1+nGS:nz+nGS)), &
                    MINVAL(gj(1+nGS:nx+nGS,:,1+nGS:nz+nGS)), &
                    MINVAL(gj(1+nGS:nx+nGS,1+nGS:ny+nGS,:)))
      IF (minvolume.eq.0.0d0) THEN
            CALL ErrorStop(1401)
      ENDIF

   END ASSOCIATE
   _endAllBlocks_

END SUBROUTINE Metric

