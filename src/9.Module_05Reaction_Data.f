!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** variables for reaction data

!     cf          frequency factor: mole-mm-sec-K
!     zetaf       temperature exponent
!     ef          activation energy: cal/mole
      MODULE Reaction_Data
      IMPLICIT NONE
      REAL(8),ALLOCATABLE,SAVE,DIMENSION(:)::
     &      cf ,zetaf ,ef ,
     &      cf0,zetaf0,ef0,
     &      acent,ts1,ts2,ts3

      END MODULE

