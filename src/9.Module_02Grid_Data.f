!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** grid related variables
      MODULE Grid_Data
      IMPLICIT NONE
      SAVE

!$$$$$$$$$$$$$$$$$$$$$
!     Intrinsic data type declaration
!$$$$$$$$$$$$$$$$$$$$$
      TYPE gridData
      REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::
     &                    xd_,               yd_,               zd_,
     &       gxx_, gxy_, gxz_,  gyx_, gyy_, gyz_,  gzx_, gzy_, gzz_,
     &        gj_,    sgsDlt_,
     &      gxxf_,gxyf_,gxzf_, gyxf_,gyyf_,gyzf_, gzxf_,gzyf_,gzzf_,
     &      gxxb_,gxyb_,gxzb_, gyxb_,gyyb_,gyzb_, gzxb_,gzyb_,gzzb_,
     &      contraNorm_x_,contraNorm_y_,contraNorm_z_
      INTEGER,ALLOCATABLE,DIMENSION(:,:,:)::
     &      gridAttr_
      ENDTYPE

      TYPE(gridData),ALLOCATABLE,TARGET,DIMENSION(:)::geo_gl
!$$$$$$$$$$$$$$$$$$$$$
!     Pointers
!$$$$$$$$$$$$$$$$$$$$$

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &                    xd_,               yd_,               zd_,
     &       gxx_, gxy_, gxz_,  gyx_, gyy_, gyz_,  gzx_, gzy_, gzz_,
     &        gj_,    sgsDlt_,
     &      gxxf_,gxyf_,gxzf_, gyxf_,gyyf_,gyzf_, gzxf_,gzyf_,gzzf_,
     &      gxxb_,gxyb_,gxzb_, gyxb_,gyyb_,gyzb_, gzxb_,gzyb_,gzzb_,
     &      contraNorm_x_,contraNorm_y_,contraNorm_z_
      INTEGER,POINTER,DIMENSION(:,:,:)::
     &      gridAttr_

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      gxx,gxy,gxz, gyx,gyy,gyz, gzx,gzy,gzz,
     &      gj,sgsdlt,
     &      gxxf,gxyf,gxzf, gyxf,gyyf,gyzf, gzxf,gzyf,gzzf,
     &      gxxb,gxyb,gxzb, gyxb,gyyb,gyzb, gzxb,gzyb,gzzb,
     &      contraNorm_x,contraNorm_y,contraNorm_z

      INTEGER,POINTER,DIMENSION(:,:,:)::
     &      gridAttr

      END MODULE

