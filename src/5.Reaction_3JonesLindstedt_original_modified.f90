!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
SUBROUTINE ChemJL(yfs, t)
!
!     Reference: Global reaction schemes for hydrocarbon combustion,
!                 WP Jones and RP Lindstedt, Comb. Flame, 1988
!
!     Participating species
!
!        H2
!        O2
!        H2O
!        CH4
!
!        CO
!        CO2
!        N2
!
! Reaction steps (paper => code)
!	 ( 7 => 1) H2  +.5O2 <=>  H2O
!	 ( 8 => 2) CO  + H2O <=>  CO2 +  H2
!	 ( 9 => 3) CH4 + H2O  =>  CO  + 3H2
!	 (10 => 4) CH4 +.5O2  =>  CO  + 2H2
!
! Variables and their units
!
! variable name   meaning                    unit
!--------------------------------------------------------------------------------------
! yfs             Concentration              converted into kmol/m3 from mol/m3
! dyfs            Concentration difference   kmol/(m3 s)
! cf              Frequency factor           (kmol/m3)^(1−(m+n))/s (i.e.mks with kmol)
! ef              Activation energy          cal/mol
! RCal            (universal) gas constant   cal/(mol K)
! reactionRate    Reaction rate              kmol/(m3 s)
!
!***********************************************************************
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE Fundamental_Constants,    ONLY:ns,RCal
   USE Control_Data,             ONLY:nr, dt
   USE Reaction_Data
   USE Thermal_Property,         ONLY:wMol,rs, Td


   IMPLICIT NONE

   INTEGER,PARAMETER::H2  = 1, &
                      O2  = 2, &
                      H2O = 3, &
                      CH4 = 4, &
                      CO  = 5, &
                      CO2 = 6, &
                      N2  = 7
   REAL(8),PARAMETER::eps = 1.0d-30

   REAL(8),INTENT(INOUT),DIMENSION(ns)::yfs
   REAL(8),INTENT(IN)::t

   REAL(8)::dtRe,dtReSum
   REAL(8),DIMENSION(nr)::rateCoeffForward, &
                          rateCoeffBackward, &
                          reactionRateForward, &
                          reactionRateBackward, &
                          reactionRate, &
                          dHelmFE
   REAL(8),DIMENSION(ns)::helmFE,dyfs

   INTEGER::is

   !$$$$$$$$$$$$$$$
   !     Main
   !$$$$$$$$$$$$$$$
   dtReSum=0.0d0 ! time interval used already in reaction progress

   !***************
   !     Free energy (Helmholtz function)
   !***************
   DO is=1,ns
   SELECT CASE (is)
         CASE (H2,O2,H2O,CO,CO2)
         helmFE(is)=Td(is)%helmFE(t)

         CASE DEFAULT
               CONTINUE
   ENDSELECT
   ENDDO
   !***************
   !     Difference of free energy of some reactions with reverse reaction
   !     The sign convention for stoichiometric coefficients
   !           is in opposite direction.
   !***************
   !	               H2 +         0.5 O2  <=>        H2O
   dHelmFE(1)=helmFE(H2)+0.5d0*helmFE(O2)  -  helmFE(H2O)
   !	               CO +             H2O <=>        CO2  +        H2
   dHelmFE(2)=helmFE(CO)+      helmFE(H2O) -  helmFE(CO2) - helmFE(H2)

   !***************
   !     Rate coefficients
   !***************
   rateCoeffForward(1)=cf(1)/t*exp(-ef(1)/RCal/t)
   rateCoeffForward(2)=cf(2)  *exp(-ef(2)/RCal/t)
   rateCoeffForward(3)=cf(3)  *exp(-ef(3)/RCal/t)
   rateCoeffForward(4)=cf(4)  *exp(-ef(4)/RCal/t)

   rateCoeffBackward(1:2)=rateCoeffForward(1:2)*exp(-dHelmFE(1:2))

   rateCoeffBackward(3:4)=0.0d0

   DO WHILE (dtReSum.ne.dt)
      ! Assume no (temperature & reaction rate coefficient) change
      !   during reaction in one time step
      !   but only concentration, and consequently reaction rate change

      !***************
      !     Reaction rates
      !***************
      !	 (1) H2  +.5O2 <=>  H2O
      reactionRateForward( 1)=MIN( yfs( H2)/dt, 0.5d0*yfs( O2)/dt, &
                              rateCoeffForward( 1)*yfs( H2)**0.25d0*yfs( O2)**1.50d0)
      ! In order to avoid stiffness problem due to fractional index of concentration of H2,
      !  infinitely fast chemistry was applied.
      !  What if (yfs + dyfs*dt) is less than zero because of reaction (2)?
      reactionRateBackward(1)=rateCoeffBackward(1)*yfs(H2O)**1.75d0

      !	 (2) CO  + H2O <=>  CO2 +  H2
      reactionRateForward( 2)=rateCoeffForward( 2)*yfs( CO)        *yfs(H2O)
      reactionRateBackward(2)=rateCoeffBackward(2)*yfs(CO2)        *yfs( H2)

      !	 (3) CH4 + H2O  =>  CO  + 3H2
      reactionRateForward( 3)=rateCoeffForward( 3)*yfs(CH4)        *yfs(H2O)
      reactionRateBackward(3)=rateCoeffBackward(3)

      !	 (4) CH4 +.5O2  =>  CO  + 2H2
      reactionRateForward( 4)=MIN( yfs(CH4)/dt, 0.5d0*yfs( O2)/dt, &
                              rateCoeffForward( 4)*yfs(CH4)**0.5d0 *yfs( O2)**1.25D0)
      reactionRateBackward(4)=rateCoeffBackward(4)

      reactionRate=reactionRateForward-reactionRateBackward

#define rr reactionRate
      dyfs( H2) =       -rr(1)      +rr(2)+3.0d0*rr(3)+2.0d0*rr(4)
      dyfs( O2) = -0.5d0*rr(1)-0.5d0*rr(4)
      dyfs(H2O) =        rr(1)      -rr(2)      -rr(3)
      dyfs(CH4) =       -rr(3)      -rr(4)
      dyfs( CO) =       -rr(2)      +rr(3)      +rr(4)
      dyfs(CO2) =        rr(2)
      dyfs( N2:) = 0.0d0

#undef rr

      dtRe=dt*1.0d0 ! reaction time interval guess

      DO is=1,ns
         IF(dyfs(is).lt.0.0d0) THEN
            dtRe=MIN(dtRe,yfs(is)/dyfs(is)*(-0.99d0))
            ! if dyfs decrease is more than 99% of yfs,
            ! reduce time interval
         ENDIF
      ENDDO

      IF(dtRe.ge.dt-dtReSum) THEN
         dtRe=dt-dtReSum
         dtReSum=dt
      ELSE
         dtReSum=dtReSum+dtRe
      ENDIF
      yfs=yfs+dyfs*dtRe
   ENDDO

   END SUBROUTINE
