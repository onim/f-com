!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     list of subroutines in transport module
!           SUBROUTINE gradient
!
!     list of functions in transport module
!           FUNCTION rci11(t, tek)
!           FUNCTION rci22(t, tek)
!           FUNCTION viscosity(t, moleculardiameter, rci, molarmass)
!           FUNCTION corrfactor(visc1, visc2, molarmass1, molarmass2)
!           FUNCTION bidiffuse(p,t,moledia1,moledia2,molarm1,molarm2,rci)
!
!           FUNCTION fluxsum
!
!
!
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      MODULE transportModule
      IMPLICIT NONE
!$$$$$$$$$$$$$$$$$$$$$
!     Parameters
!$$$$$$$$$$$$$$$$$$$$$
      INTEGER,PARAMETER::moddim=3

!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type definitions
!$$$$$$$$$$$$$$$$$$$$$
      TYPE transpoint
      REAL(8)::
     &      visc,cond
      ENDTYPE

      TYPE transpointsp
      REAL(8)::
     &      vism,dif
      ENDTYPE

      TYPE transface
      REAL(8)::
     &      dux,duy,duz,
     &      dvx,dvy,dvz,
     &      dwx,dwy,dwz,
     &      dtx,dty,dtz,
     &      vist,
     &      rufx,rvfx,rwfx,refx,
     &      rufy,rvfy,rwfy,refy,
     &      rufz,rvfz,rwfz,refz

      REAL(8)::
     &      Sxx,Sxy,Sxz,
     &          Syy,Syz,
     &              Szz, Snorm

      CONTAINS
            PROCEDURE::strainRate
      ENDTYPE

      TYPE transfacesp
      REAL(8)::
     &      dsx,dsy,dsz,
     &      rsfx,rsfy,rsfz
      ENDTYPE


!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type declaration
!$$$$$$$$$$$$$$$$$$$$$
!     ptt : pointwise data set for transport phenomena
      TYPE(transpoint),ALLOCATABLE :: pt(:,:,:)
      TYPE(transface), ALLOCATABLE :: face(:,:,:)
      TYPE(transpointsp),ALLOCATABLE :: pts(:,:,:,:)
      TYPE(transfacesp), ALLOCATABLE :: faces(:,:,:,:)

      CONTAINS

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


      FUNCTION diffx(phi,dimen) RESULT(diff)
      USE grid_Data,                ONLY:gx => gxx_,
     &                                   gy => gyx_,
     &                                   gz => gzx_
      USE control_Data,             ONLY:nx,ny,nz
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS),INTENT(in)::phi
      INTEGER,INTENT(IN)::dimen
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS)::diff
      INTEGER::i,j,k

#include '4.Transport_0Module_diffCode.f'

      END FUNCTION diffx

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION diffy(phi,dimen) RESULT(diff)

      USE grid_Data,          ONLY:gx => gxy_,
     &                             gy => gyy_,
     &                             gz => gzy_
      USE control_Data,             ONLY:nx,ny,nz
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS),INTENT(in)::phi
      INTEGER,INTENT(IN)::dimen
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS)::diff
      INTEGER::i,j,k

#include '4.Transport_0Module_diffCode.f'

      ENDFUNCTION diffy
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION diffz(phi,dimen) RESULT(diff)

      USE grid_Data,          ONLY:gx => gxz_,
     &                             gy => gyz_,
     &                             gz => gzz_
      USE control_Data,             ONLY:nx,ny,nz
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS),INTENT(in)::phi
      INTEGER,INTENT(IN)::dimen
      REAL(8),DIMENSION(1-nGS:nx+nGS,
     &                  1-nGS:ny+nGS,
     &                  1-nGS:nz+nGS)::diff
      INTEGER::i,j,k

#include '4.Transport_0Module_diffCode.f'

      END FUNCTION diffz

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$





!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 1-1: reduced collision integral
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION rci11(t, tek)

      IMPLICIT NONE
      REAL(8)::rci11
      REAL(8),INTENT(in)::t, tek
      REAL(8)::ts

      ts=t/tek
!     reference: web
!           thesis.library.caltech.edu/2290/12/11_appendix.pdf
      rci11=  1.06036d0/(ts**0.15610d0)
     &       +0.19300d0/exp(0.47635d0*ts)
     &       +1.03587d0/exp(1.52996d0*ts)
     &       +1.76474d0/exp(3.89411d0*ts)
      END FUNCTION rci11

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 1-2: reduced collision integral
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION rci22(t, tek)

      IMPLICIT NONE
      REAL(8)::rci22
      REAL(8),INTENT(in)::t, tek
      REAL(8)::ts

      ts=t/tek
!     reference:  the properties of gases and liquids, 5th ed.,
!           Poling et al., McGrawhill
      rci22=  1.16145d0/(ts**0.14874d0)
     &       +0.52487d0/exp(0.77320d0*ts)
     &       +2.16178d0/exp(2.43787d0*ts)

!      rci22=1.147D0*TS**(-0.145D0)+1.0D0/(TS+0.5D0)**2

      END FUNCTION rci22

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 2: species viscosity
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION getViscSpecies(t, moleculardiameter, rci, molarmass)
!     unit
!     temperature:            Kelvin
!     moleculardiameter:      nanometer
!     rci:                    dimensionless
!     molar mass:             kg/mol
      IMPLICIT NONE
      REAL(8)::getViscSpecies
      REAL(8),INTENT(in)::t,moleculardiameter,rci,molarmass
!     reference: Combustion, Warnatz
!           Eq. 5.16
      getViscSpecies = 2.6693d-8*
     &      SQRT(molarmass*1.0D3*t)
     &     /((moleculardiameter**2)*rci)

      END FUNCTION getViscSpecies



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 3: correction factor phi_ik
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION corrfactor(visc1, visc2, molarmass1, molarmass2)

      USE fundamental_Constants,    ONLY:r2inv

      IMPLICIT NONE

      REAL(8)::corrfactor
      REAL(8),INTENT(in)::visc1,visc2,molarmass1,molarmass2
!     reference: Combustion, Warnatz
!           Eq. 5.11
      corrfactor=0.5d0*r2inv*
     &            (1.0d0+molarmass1/molarmass2)**(-0.5d0)*
     &            (1.0d0+(visc1/visc2)**(0.5d0)
     &                  *(molarmass1/molarmass2)**(0.25d0))**2.0d0

      END FUNCTION corrfactor

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 4: binary diffusion coefficient
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION bidiffuse(p,t,moledia12,molarmass12,rci)
!     unit
!     diffusion coefficient:  m^2/s
!     pressure:               bar
!     temperature:            Kelvin
!     radius:                 nanometer
!     molar mass:             g/mol
      IMPLICIT NONE

      REAL(8)::bidiffuse
      REAL(8),INTENT(in)::p,t,moledia12,molarmass12,rci
!     reference: Combustion, Warnatz
!           Eq. 5.24

!      molamass12 =0.5d0*(molarm1+molarm2)/(molarm1*molarm2)
!      moledia12=moledia12**2


      bidiffuse = 2.662d-9*
     &      SQRT(molarmass12*t**3)
     &     /(p*moledia12*rci)

      END FUNCTION bidiffuse


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 5: Summation of diffusive flux on a node
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      FUNCTION fluxsum(rx, ry, rz, dimen)
      USE control_Data,             ONLY:nx, ny, nz
      USE grid_Data,                ONLY:gxxf_,gxyf_,gxzf_,
     &                                   gyxf_,gyyf_,gyzf_,
     &                                   gzxf_,gzyf_,gzzf_
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

      REAL(8),DIMENSION(1-ngs: ,1-nGS: ,1-nGS: ),INTENT(IN) ::rx,ry,rz
      INTEGER,INTENT(IN)::dimen
      REAL(8),DIMENSION(nx, ny, nz)::fluxsum
      INTEGER::i,j,k

      SELECT CASE(dimen)

      CASE(1)
      DO k=1,nz; DO j=1,ny; DO i=1,nx
      fluxsum(i,j,k) = (
     &        gxxf_(i,j,k)  *rx(i,j,k)
     &       +gxyf_(i,j,k)  *ry(i,j,k)
     &       +gxzf_(i,j,k)  *rz(i,j,k)

     &       -gxxf_(i-1,j,k)*rx(i-1,j,k)
     &       -gxyf_(i-1,j,k)*ry(i-1,j,k)
     &       -gxzf_(i-1,j,k)*rz(i-1,j,k))
      ENDDO;ENDDO;ENDDO

      CASE(2)
      DO k=1,nz; DO j=1,ny; DO i=1,nx
      fluxsum(i,j,k) = (
     &       +gyxf_(i,j,k)  *rx(i,j,k)
     &       +gyyf_(i,j,k)  *ry(i,j,k)
     &       +gyzf_(i,j,k)  *rz(i,j,k)

     &       -gyxf_(i,j-1,k)*rx(i,j-1,k)
     &       -gyyf_(i,j-1,k)*ry(i,j-1,k)
     &       -gyzf_(i,j-1,k)*rz(i,j-1,k))
      ENDDO;ENDDO;ENDDO

      CASE(3)
      DO k=1,nz; DO j=1,ny; DO i=1,nx
      fluxsum(i,j,k) = (
     &       +gzxf_(i,j,k)  *rx(i,j,k)
     &       +gzyf_(i,j,k)  *ry(i,j,k)
     &       +gzzf_(i,j,k)  *rz(i,j,k)

     &       -gzxf_(i,j,k-1)*rx(i,j,k-1)
     &       -gzyf_(i,j,k-1)*ry(i,j,k-1)
     &       -gzzf_(i,j,k-1)*rz(i,j,k-1))
      ENDDO;ENDDO;ENDDO
      END SELECT

!     e.g. tau_xx|(1/2)=tau_xx|0 + tau_xx|1
      END FUNCTION

!$$$$$$$$$$$$$$$
!     Obtain strain rate tensor from velocity gradient tensor
      SUBROUTINE strainRate(this)
!$$$$$$$$$$$$$$$

      IMPLICIT NONE
      CLASS(transface)::this

      this%Sxx= this%dux
      this%Sxy=(this%duy+this%dvx)*0.5d0
      this%Sxz=(this%duz+this%dwx)*0.5d0
      this%Syy= this%dvy
      this%Syz=(this%dvz+this%dwy)*0.5d0
      this%Szz= this%dwz
!     Syx, Szx, Szy are isotropic and only conterparts should be used

      this%Snorm=SQRT(( this%Sxx**2 +this%Sxy**2 +this%Sxz**2
     &                 +this%Sxy**2 +this%Syy**2 +this%Syz**2
     &                 +this%Sxz**2 +this%Syz**2 +this%Szz**2)*2.0d0)

      END SUBROUTINE



      END MODULE transportModule

