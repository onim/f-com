!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE arrayalloc
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
# include '0.Script_02Definitions.h'

   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,nb, &
                                      nx_gl,ny_gl,nz_gl, &
                                      chGrid, ib
   USE boundary_Data_OOP,        ONLY:bf
   USE Status_Check,             ONLY:mm
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!#######################################################################
!**** Open geometry data file
   OPEN(54,file=chGrid,FORM='unformatted')

   READ(54) nb
   ALLOCATE(nx_gl(nb),ny_gl(nb),nz_gl(nb),bf(nb,6),bl(nb),mm(nb))
   ! global extent, block face, block data, monitoring point
!***********************************************************************
!     read the grid number
!***********************************************************************
   READ(54) nx_gl
   READ(54) ny_gl
   READ(54) nz_gl
   CLOSE(54)

!***********************************************************************
!     allocate spatial arrays
!***********************************************************************
   _forAllBlocks_
      ! Allocate points with ghost stencil
      ALLOCATE(bl(ib)%p(nx+2*nGS,ny+2*nGS,nz+2*nGS))
   _endAllBlocks_

END SUBROUTINE arrayalloc
