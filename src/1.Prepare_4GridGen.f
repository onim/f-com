!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             GRID METRIC CALCULATION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE GRDGEN
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
!     all variables in these two modules are allocated in this subroutine.
      USE grid_Data,                ONLY:geo_gl

      use control_Data,             ONLY:nx,ny,nz,ns,nt,nb,nn,
     &                                   nx_gl,ny_gl,nz_gl,
     &                                   chGrid,
     &                                   ib
      USE boundary_Data_OOP,        ONLY:bf
      USE readCommentedFile
      USE Message_Passing_Interface
      USE fundamental_Constants

      IMPLICIT NONE
      INTEGER::i,j,k,facet
      REAL(8)::volume, overallVolume
      REAL(8),DIMENSION(:,:,:),ALLOCATABLE::normLength

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!***********************************************************************
!*****Position data read from a grid file
!***********************************************************************
      open(55,file=chGrid,form='unformatted',POSITION='rewind')

!     # of blocks
      READ(55) nb
      READ(55) nx_gl
      READ(55) ny_gl
      READ(55) nz_gl

      nn=SUM(nx_gl*ny_gl*nz_gl)

      DO ib=1,nb
!        # of nodes along each axis
         READ(55) nx
         READ(55) ny
         READ(55) nz

!        Type of block boundary face(6)
         READ(55) bf(ib,:)%faceType

!        Parameters according to the type
         DO facet=1,6

!           ignore MPI interface & slip wall
            IF(bf(ib,facet)%faceType.eq.ft_interface_MPI) bf(ib,facet)%faceType=ft_interface
            IF(bf(ib,facet)%faceType.eq.ft_symmetric) bf(ib,facet)%faceType=ft_wall

            SELECT CASE (bf(ib,facet)%faceType)
            CASE(ft_interface)
               READ(55) bf(ib,facet)%connBlock,
     &                  bf(ib,facet)%connFace

            CASE(ft_interface_MPI)
               READ(55) bf(ib,facet)%connBlock,
     &                  bf(ib,facet)%connFace
               bf(ib,facet)%myNode=blockOwner(ib)
               bf(ib,facet)%connNode=blockOwner(bf(ib,facet)%connBlock)

            CASE(ft_inlet)
               bf(ib,facet)%nFBC=1

            ENDSELECT
         ENDDO

         DO k=1,nz; DO j=1,ny; DO i=1,nx
            READ(55) geo_gl(ib)%xd_(i,j,k),
     &               geo_gl(ib)%yd_(i,j,k),
     &               geo_gl(ib)%zd_(i,j,k)
         ENDDO;ENDDO;ENDDO

         DO facet=1,6
            IF(ALLOCATED(normLength)) DEALLOCATE(normLength)
            SELECT CASE (bf(ib,facet)%faceType)
            CASE(ft_symmetric)
               SELECT CASE (facet)
               CASE(fo_west)
                  ALLOCATE(
     &               normLength(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normx(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normy(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normz(2,1-nGS:ny+nGS,1-nGS:nz+nGS))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_((/1,1/),:,:)-geo_gl(ib)%xd_((/2,2/),:,:)
                  bf(ib,facet)%normy=geo_gl(ib)%yd_((/1,1/),:,:)-geo_gl(ib)%yd_((/2,2/),:,:)
                  bf(ib,facet)%normz=geo_gl(ib)%zd_((/1,1/),:,:)-geo_gl(ib)%zd_((/2,2/),:,:)
               CASE(fo_east)
                  ALLOCATE(
     &               normLength(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normx(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normy(2,1-nGS:ny+nGS,1-nGS:nz+nGS),
     &               bf(ib,facet)%normz(2,1-nGS:ny+nGS,1-nGS:nz+nGS))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_((/nx,nx/),:,:)-geo_gl(ib)%xd_((/nx-1,nx-1/),:,:)
                  bf(ib,facet)%normy=geo_gl(ib)%yd_((/nx,nx/),:,:)-geo_gl(ib)%yd_((/nx-1,nx-1/),:,:)
                  bf(ib,facet)%normz=geo_gl(ib)%zd_((/nx,nx/),:,:)-geo_gl(ib)%zd_((/nx-1,nx-1/),:,:)
               CASE(fo_south)
                  ALLOCATE(
     &               normLength(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normx(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normy(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normz(1-nGS:nx+nGS,2,1-nGS:nz+nGS))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_(:,(/1,1/),:)-geo_gl(ib)%xd_(:,(/2,2/),:)
                  bf(ib,facet)%normy=geo_gl(ib)%yd_(:,(/1,1/),:)-geo_gl(ib)%yd_(:,(/2,2/),:)
                  bf(ib,facet)%normz=geo_gl(ib)%zd_(:,(/1,1/),:)-geo_gl(ib)%zd_(:,(/2,2/),:)
               CASE(fo_north)
                  ALLOCATE(
     &               normLength(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normx(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normy(1-nGS:nx+nGS,2,1-nGS:nz+nGS),
     &               bf(ib,facet)%normz(1-nGS:nx+nGS,2,1-nGS:nz+nGS))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_(:,(/ny,ny/),:)-geo_gl(ib)%xd_(:,(/ny-1,ny-1/),:)
                  bf(ib,facet)%normy=geo_gl(ib)%yd_(:,(/ny,ny/),:)-geo_gl(ib)%yd_(:,(/ny-1,ny-1/),:)
                  bf(ib,facet)%normz=geo_gl(ib)%zd_(:,(/ny,ny/),:)-geo_gl(ib)%zd_(:,(/ny-1,ny-1/),:)
               CASE(fo_back)
                  ALLOCATE(
     &               normLength(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normx(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normy(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normz(1-nGS:nx+nGS,1-nGS:ny+nGS,2))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_(:,:,(/1,1/))-geo_gl(ib)%xd_(:,:,(/2,2/))
                  bf(ib,facet)%normy=geo_gl(ib)%yd_(:,:,(/1,1/))-geo_gl(ib)%yd_(:,:,(/2,2/))
                  bf(ib,facet)%normz=geo_gl(ib)%zd_(:,:,(/1,1/))-geo_gl(ib)%zd_(:,:,(/2,2/))
               CASE(fo_front)
                  ALLOCATE(
     &               normLength(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normx(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normy(1-nGS:nx+nGS,1-nGS:ny+nGS,2),
     &               bf(ib,facet)%normz(1-nGS:nx+nGS,1-nGS:ny+nGS,2))
                  bf(ib,facet)%normx=geo_gl(ib)%xd_(:,:,(/nz,nz/))-geo_gl(ib)%xd_(:,:,(/nz-1,nz-1/))
                  bf(ib,facet)%normy=geo_gl(ib)%yd_(:,:,(/nz,nz/))-geo_gl(ib)%yd_(:,:,(/nz-1,nz-1/))
                  bf(ib,facet)%normz=geo_gl(ib)%zd_(:,:,(/nz,nz/))-geo_gl(ib)%zd_(:,:,(/nz-1,nz-1/))
               ENDSELECT
               normLength=SQRT(bf(ib,facet)%normx**2+bf(ib,facet)%normy**2+bf(ib,facet)%normz**2)
               WHERE(normLength.eq.0.0d0)
                  normLength=1.0d0
               ENDWHERE
               bf(ib,facet)%normx=bf(ib,facet)%normx/normLength
               bf(ib,facet)%normy=bf(ib,facet)%normy/normLength
               bf(ib,facet)%normz=bf(ib,facet)%normz/normLength
            ENDSELECT
         ENDDO
      ENDDO

      CLOSE(55)

      IF(primeNode()) PRINT *, 'Position data load successful.'

!***********************************************************************
!*****Grant grid attribute
!***********************************************************************
      CALL GridAttribute

!***********************************************************************
!*****POSITION DATA Expansion
!***********************************************************************
      DO ib=1,nb
            CALL ExpandPointSimple(geo_gl(ib),
     &                              nx_gl(ib),ny_gl(ib),nz_gl(ib))
      ENDDO

      DO ib=1,nb
            CALL ExpandPointCopy(geo_gl(ib),
     &                            nx_gl(ib),ny_gl(ib),nz_gl(ib))
      ENDDO

!***********************************************************************
!     COORDINATE DIFFERENCE ALONG EACH CARTESIAN AXIS
!***********************************************************************
      DO ib=1,nb
            CALL CoordDifference(geo_gl(ib),
     &                            nx_gl(ib),ny_gl(ib),nz_gl(ib))
      ENDDO

!***********************************************************************
!***REVERSE JACOBIAN
!***********************************************************************
      CALL Metric

      volume=0.0d0
      overallVolume=0.0d0
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         volume= volume
     &          +SUM(geo_gl(ib)%gj_(1:nx_gl(ib),1:ny_gl(ib),1:nz_gl(ib)))
      ENDIF; ENDDO multiBlock

      CALL MPI_REDUCE(volume, overallVolume, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

      IF(primeNode()) WRITE(*,'(A,E14.7)')'  The domain volume is ',overallVolume

      END SUBROUTINE grdgen
