!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE AddRHS
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,dt,it,ib
   USE M_BlockPointData,         ONLY:bl
   USE Status_Check,             ONLY:wrongConcentr,voidNode, &
                                      rocons,recons,rfscons
   USE AuxSubprogram,            ONLY:volinteg,fluxinteg
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

   INTEGER::i,j,k
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   wrongConcentr=.false.

   IF (.not.ALLOCATED(rfscons)) ALLOCATE(rfscons(ns))

!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forInnerPoints_
#define f bl(ib)%p(i,j,k)%f
#define c bl(ib)%p(i,j,k)%c
      c%ro =c%ro +f%dro
      c%ru =c%ru +f%dru
      c%rv =c%rv +f%drv
      c%rw =c%rw +f%drw
      c%re =c%re +f%dre
      c%rfs=c%rfs+f%drfs
      ! detect negative concentration
      IF (MINVAL(c%rfs).lt.0.0d0) wrongConcentr=.true.
      WHERE(c%rfs.lt.0.0d0)
         c%rfs=0.0d0
      ENDWHERE
#undef c
#undef f
   _endInnerPoints_
!$OMP END PARALLEL DO

   rocons=0.0d0
   recons=0.0d0
   rfscons=0.0d0

   !   rocons=volinteg(dro)+fluxinteg(ro,dt)
   !   recons=volinteg(ro)
   !   DO is=1,ns
   !         rfscons(is)=volinteg(rfs(:,:,:,is))/volinteg(ro)
   !   ENDDO
   !      rucons=volsum(dru,dt)+surfsum(ru)+presssum()
   !      rvcons=volsum(drv,dt)+surfsum(rv)+presssum()
   !      rwcons=volsum(drw,dt)+surfsum(rw)+presssum()

   !      recons=volsum(dre,dt)+surfsum(re)+heatcond()+reac()+source()

!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forInnerPoints_
#define ro bl(ib)%p(i,j,k)%c%ro
#define rfs bl(ib)%p(i,j,k)%c%rfs
   !   >>>SPECIES CORRECTION<<<
   !        Make overall concentration which is deviated from unity
   !   because of removal of negative concentration.
   !        HOWEVER, the deviation of concentration is not only
   !   produced by eliminating negative values.
   !   Originally the sum of concentration is not equal to
   !   unity from convection term manipulation
   !   and chemical reaction calculation.
   !        For diffusion, sum of species(i.e. mass) is conserved now.
      IF(MAXVAL(rfs).EQ.0.0D0) THEN
         rfs(1)=0.778D0*ro
         rfs(3)=0.222D0*ro
         voidNode=.true.
      ELSE
         rfs=ro*rfs/SUM(rfs)
      END IF
#undef ro
#undef rfs
   _endInnerPoints_
!$OMP END PARALLEL DO

END SUBROUTINE

