C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
C
C      AUSM+ (advection upstream splitting method plus)
c
c     indexing convention
c           An interface on the right(i.e. bigger index direction)
c           has the same index with the given cell.
C
C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE AUSMplus(Mp,Mn,cm,pp,pn,dimen)
C$$$$$$$$$$$$$$$
C     Header
C$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns
      USE conserved_Quantity,       ONLY: u_, v_, w_, gamma_, c_,
     +                                    t_
      USE thermal_Property,         ONLY:rs
      USE grid_Data,                ONLY:gxx_,gxy_,gxz_,
     +                                   gyx_,gyy_,gyz_,
     +                                   gzx_,gzy_,gzz_,gj_,
     +                                   contraNorm_x_,contraNorm_y_,
     +                                   contraNorm_z_
      USE statuscheck,              ONLY:AUSMFlag
      USE fundamental_Constants,    ONLY:alpha, beta, nGS

      IMPLICIT NONE

#define a1 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS

      INTEGER,INTENT(IN):: dimen

      INTEGER::is,i,j,k,ish,jsh,ksh

      REAL(8),DIMENSION(a1)::
     +      contraNorm,ug,ccri,
     +      ml,mr
c           contraNorm: velocity conversion factor from phy to com
c           ccri:    critical speed of sound; sonic condition
c           ug:      velocity normal to the boundary
c           ml,mr:   Mach number in left/right cell

      REAL(8),DIMENSION(a1),INTENT(OUT)::
     +      Mp,Mn,cm,pp,pn
c           convective Mach positive, convective Mach negative,
c           boundary speed of sound,
c           pressure Mach positive,   pressure Mach negative

C$$$$$$$$$$$$$$$
C     Main
C$$$$$$$$$$$$$$$
C***********************************************************************
C     Initialisation
C***********************************************************************
      Mp=1.0d0
      Mn=1.0d0
      cm=1.0d0
      pp=1.0d0
      pn=1.0d0

C***********************************************************************
C     Normal velocity in physical space
c           it should not be in computational space
C***********************************************************************
      SELECT CASE (dimen)
      CASE (1)
            ish=1
            jsh=0
            ksh=0
            contraNorm=contraNorm_x_
            ug=(u_*gxx_+v_*gxy_+w_*gxz_) / contraNorm
      CASE (2)
            ish=0
            jsh=1
            ksh=0
            contraNorm=contraNorm_y_
            ug=(u_*gyx_+v_*gyy_+w_*gyz_) / contraNorm
      CASE (3)
            ish=0
            jsh=0
            ksh=1
            contraNorm=contraNorm_z_
            ug=(u_*gzx_+v_*gzy_+w_*gzz_) / contraNorm
      ENDSELECT

C***********************************************************************
C     critical(sonic) speed of sound, different with c
C***********************************************************************
      ccri=SQRT(
     +          (2.0d0*c_**2 + (gamma_-1.0d0)*(u_**2+v_**2+w_**2))
     +         /(gamma_+1.0d0)
     +         )


#define indc i,j,k
c           index of center (on interface)
#define indl i,j,k
c           index of left cell (with respect to the interface)
#define indr i+ish,j+jsh,k+ksh
c           index of right cell

      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
C***********************************************************************
C     Interface speed of sound
C***********************************************************************
      cm(indc)=MIN(
     +             ccri(indl)**2/MAX(ccri(indl), ug(indl)),
     +             ccri(indr)**2/MAX(ccri(indr),-ug(indr))
     +            )
C***********************************************************************
C     Mach number based on left cell speed of sound
C***********************************************************************
      ml(indc)=ug(indl)/cm(indc)
      mr(indc)=ug(indr)/cm(indc)

      cm(indc)=cm(indc)*(contraNorm(indl)*gj_(indl)
     +                  +contraNorm(indr)*gj_(indr))*0.5d0
C***********************************************************************
C     positive/negative Mach number / pressure weight
C***********************************************************************
      IF ((ABS(ml(indc))-1.0d0).lt.0.0d0) THEN
c     subsonic case
          mp(indc)= 0.25d0*(ml(indc)+1.0d0)**2
     +               +beta*(ml(indc)**2-1.0d0)**2
          pp(indc)= 0.25d0*(ml(indc)+1.0d0)**2*(2.0d0-ml(indc))
     +               +alpha*ml(indc)*(ml(indc)**2-1.0d0)**2

      ELSE
c     supersonic case
          mp(indc)= 0.50d0*(ml(indc)+ABS(ml(indc)))
          pp(indc)= 0.50d0*(1.0d0+SIGN(1.0d0,ml(indc)))
      ENDIF


      IF ((ABS(mr(indc))-1.0d0).lt.0.0d0) THEN
          mn(indc)=-0.25d0*(mr(indc)-1.0d0)**2
     +               -beta*(mr(indc)**2-1.0d0)**2
          pn(indc)= 0.25d0*(mr(indc)-1.0d0)**2*(2.0d0+mr(indc))
     +               -alpha*mr(indc)*(mr(indc)**2-1.0d0)**2

      ELSE
          mn(indc)= 0.50d0*(mr(indc)-ABS(mr(indc)))
          pn(indc)= 0.50d0*(1.0d0-SIGN(1.0d0,mr(indc)))
      ENDIF

      ENDDO;ENDDO;ENDDO

#undef indc
#undef indl
#undef indr

      END SUBROUTINE AUSMplus
