!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE TurbulenceModeling
!     Turbulence MODULE
!          containing species diffusion / combustion
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
   USE LES
   USE RANS

IMPLICIT NONE

   INTEGER::TurbModel=1

   INTEGER,PARAMETER::isLaminar=1,  &
                      isLES=2,      &
                      isRANS=3

   REAL(8),EXTERNAL,POINTER::getVisT=>null()

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Methods
CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
SUBROUTINE assignTurbModel
!     Assign turbulence model function
!     Should be run right after the program starts.
!$$$$$$$$$$$$$$$
IMPLICIT NONE

   SELECT CASE (TurbModel)
      CASE (isLES)
         CALL assignLESModel
         getVisT=>getLESVisT
      CASE DEFAULT
         getVisT=>getZeroVisT
   END SELECT

END SUBROUTINE


!$$$$$$$$$$$$$$$
PURE FUNCTION getZeroVisT() RESULT(vist)
!     A generic function of a turbulent eddy-viscosity
!       for NO turbulence modeling
!$$$$$$$$$$$$$$$
IMPLICIT NONE
   REAL(8)::vist
   vist=0.0d0
END FUNCTION


END MODULE
