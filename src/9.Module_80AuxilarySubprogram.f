!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Auxilary subprogram set
!
!           These subprograms have chance to be used at any place in the
!     main program, and are not dependent upon or owned by specific
!     subroutines. A programmer can use these subprograms freely in any
!     part of the program.
!           When you need a slightly different subprogram, tend to make a
!     new subprogram rather than modify and reuse existing one. Do not
!     attempt to change the objective of them.
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      MODULE AuxSubprogram

      IMPLICIT NONE

!$$$$$$$$$$$$$$$$$$$$$
!     Interface
!$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$
!     Parameters
!$$$$$$$$$$$$$$$$$$$$$


!$$$$$$$$$$$$$$$$$$$$$
!     Variables
!$$$$$$$$$$$$$$$$$$$$$
!     For cSA functions
      INTEGER,DIMENSION(:),ALLOCATABLE,TARGET,SAVE::
     & constSideArray1,constSideArray2,constSideArray3,
     & constSideArray4,constSideArray5,constSideArray6


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Subroutines
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
!     From 1.Prepare_4GridGen_Metric.f
!$$$$$$$$$$$$$$$

      SUBROUTINE copyMetric(g)
      USE boundary_Data_OOP,        ONLY:bf
      USE control_Data,             ONLY:ib

      IMPLICIT NONE

      REAL(8),DIMENSION(:,:,:),INTENT(INOUT)::g

      INTEGER::x1,x2,y1,y2,z1,z2
      INTEGER::ft(6)

      x1=LBOUND(g,1)
      x2=UBOUND(g,1)
      y1=LBOUND(g,2)
      y2=UBOUND(g,2)
      z1=LBOUND(g,3)
      z2=UBOUND(g,3)

      ft=bf(ib,:)%faceType

      IF(ft(1).eq.5) g(x1  ,   :,   :)=g(x1+3,   :,   :)
      IF(ft(1).eq.5) g(x1+1,   :,   :)=g(x1+2,   :,   :)
      IF(ft(3).eq.5) g(   :,y1  ,   :)=g(   :,y1+3,   :)
      IF(ft(3).eq.5) g(   :,y1+1,   :)=g(   :,y1+2,   :)
      IF(ft(5).eq.5) g(   :,   :,z1  )=g(   :,   :,z1+3)
      IF(ft(5).eq.5) g(   :,   :,z1+1)=g(   :,   :,z1+2)

      IF(ft(2).eq.5) g(x2  ,   :,   :)=g(x2-3,   :,   :)
      IF(ft(2).eq.5) g(x2-1,   :,   :)=g(x2-2,   :,   :)
      IF(ft(4).eq.5) g(   :,y2  ,   :)=g(   :,y2-3,   :)
      IF(ft(4).eq.5) g(   :,y2-1,   :)=g(   :,y2-2,   :)
      IF(ft(6).eq.5) g(   :,   :,z2  )=g(   :,   :,z2-3)
      IF(ft(6).eq.5) g(   :,   :,z2-1)=g(   :,   :,z2-2)

      END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Functions
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
!     From 31AUSM
!$$$$$$$$$$$$$$$

      FUNCTION h_ave(var,dimen)
      REAL(8),DIMENSION(:,:,:),INTENT(in)::var
      INTEGER,INTENT(in)::dimen
      REAL(8),DIMENSION(SIZE(var,1),SIZE(var,2),SIZE(var,3))::
     &                                           h_ave

      h_ave =0.5d0*(var+EOSHIFT(var,1,0.0d0,dimen))

      ENDFUNCTION


!$$$$$$$$$$$$$$$
!     From 70addrhs
!$$$$$$$$$$$$$$$
      FUNCTION volinteg(dphi)

      IMPLICIT NONE

      REAL(8),DIMENSION(:,:,:),INTENT(in)::dphi
      REAL(8)::volinteg

      volinteg =0.0d0

      ENDFUNCTION


      FUNCTION fluxinteg(phi,dt)
      IMPLICIT NONE

      REAL(8),DIMENSION(:,:,:),INTENT(in)::phi
      REAL(8),INTENT(IN)::dt
      REAL(8)::fluxinteg

      fluxinteg=0.0d0*dt

      ENDFUNCTION


      END MODULE AuxSubprogram

