!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE M_BlockPointData
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** fluid related variables
!     later, this module is to be merged with grid_Data module.

!     ro          density (kg/m3)
!     ru, rv, rw  density by velocity (kg/(m2*s))
!     re          density by internal total energy (kg/m3*J/kg = J/m3)
!     u, v, w     velocity (m/s)
!     h           enthalpy (J/m3)
!     T           temperature (K)
!     gamma       specific heat ratio
!     qChem       heat release
!     c           speed of sound (m/s)
!     fs          mass fraction (ND)
!     yfs         concentration (mol/m3)
!     rfs         density by mass fraction
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   USE Fundamental_Constants,       ONLY:ns
IMPLICIT NONE
   PRIVATE
   SAVE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! Derived data type definitions
!     bl(ib)%p(i,j,k)%g%xd
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_GeometryData
   ! Cartesian coordinates of a grid point
   !$$$$$$$$$$$$$$$$$$$$$
      REAL(8):: xd, yd, zd
   ENDTYPE
   PUBLIC::T_GeometryData

   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_GeometryDiffData
   !$$$$$$$$$$$$$$$$$$$$$
   REAL(8):: &
       gxx, gxy, gxz,  gyx, gyy, gyz,  gzx, gzy, gzz, &
        gj,   sgsDlt, &
      gxxf,gxyf,gxzf, gyxf,gyyf,gyzf, gzxf,gzyf,gzzf, &
      contraNorm_x,contraNorm_y,contraNorm_z
   ENDTYPE

   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_ConservedQuantity
   !$$$$$$$$$$$$$$$$$$$$$
      REAL(8)::ro,ru,rv,rw,re,T
      ! Here, temperature is not a conserved quantity,
      !  but included as an exception for boundary condition exchange
      REAL(8),DIMENSION(ns)::rfs
   CONTAINS
      PROCEDURE::AssignConservedQuantity
      GENERIC::ASSIGNMENT(=) => AssignConservedQuantity
   ENDTYPE

   !$$$$$$$$$$$$$$$$$$$$$
   TYPE,EXTENDS(T_ConservedQuantity)::T_ExtendedConservedQuantity
   !$$$$$$$$$$$$$$$$$$$$$
      REAL(8)::u, v, w, h, gamma, qChem, c, p
      REAL(8),DIMENSION(ns)::fs,yfs
   ENDTYPE



   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_ThermalProperty
   !$$$$$$$$$$$$$$$$$$$$$
      REAL(8),DIMENSION(ns)::cps,hf
   ENDTYPE


   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_Fluxdifference
   !$$$$$$$$$$$$$$$$$$$$$
      REAL(8)::dro, dru, drv, drw, dre
      REAL(8),DIMENSION(ns)::drfs
   ENDTYPE

   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_PointData
   !$$$$$$$$$$$$$$$$$$$$$
      TYPE(T_GeometryData)::g
      TYPE(T_GeometryDiffData)::gd
      TYPE(T_ExtendedConservedQuantity)::c
      TYPE(T_ThermalProperty)::t
      TYPE(T_Fluxdifference)::f
   END TYPE

   !$$$$$$$$$$$$$$$$$$$$$
   TYPE T_BlockData
   !$$$$$$$$$$$$$$$$$$$$$
      INTEGER::xlb, xub, ylb, yub, zlb, zub, nSpecies
      TYPE(T_PointData),ALLOCATABLE,DIMENSION(:,:,:)::p
   END TYPE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! Derived data type variable declaration
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   TYPE(T_BlockData),ALLOCATABLE,DIMENSION(:)::bl
   TYPE(T_ConservedQuantity)::initQuant
   TYPE(T_Fluxdifference),PARAMETER::zeroFluxDifference &
      = T_Fluxdifference(0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0)

   PUBLIC::bl, initQuant, zeroFluxDifference
   PUBLIC::T_ConservedQuantity,T_ExtendedConservedQuantity,T_PointData

CONTAINS

PURE SUBROUTINE AssignConservedQuantity(a, b)

IMPLICIT NONE

   CLASS(T_ConservedQuantity),INTENT(INOUT)::a
   CLASS(T_ConservedQuantity),INTENT(IN)::b

   a%ro=b%ro
   a%ru=b%ru
   a%rv=b%rv
   a%rw=b%rw
   a%re=b%re
   a%rfs=b%rfs
   a%t=b%t

END SUBROUTINE

END MODULE

