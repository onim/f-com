!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE ExpVarWrapper
!     Prepare expanded variables
!           e.g. U,V,H,FS,t,cp,h,dt
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,nx_gl,ny_gl,nz_gl, &
                                      dt,dtMax,dtMin,time, &
                                      CFLMax,CFLMin,ib,nb
   USE M_BlockPointData,         ONLY:bl
   USE fundamental_Constants,    ONLY:nGS
   USE Message_Passing_Interface

   IMPLICIT NONE
   INTEGER:: i,j,k

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   !initilization of dt
   dt=0.0d0
   dtMax=0.0d0
   dtMin=1.0d10

   _forMyBlocks_
!$OMP PARALLEL DO PRIVATE(i,j,k)
      _forAllPoints_
         CALL EXPVAR(bl(ib)%p(i,j,k),i,j,k)
      _endAllPoints_
!$OMP END PARALLEL DO
   _endMyBlocks_

   !     dt is determined by CFLMax.
   CALL MPI_ALLREDUCE(MPI_IN_PLACE,dtmin,1,MPI_REAL8,MPI_MIN,MPI_COMM_WORLD,ierr)
   CALL MPI_ALLREDUCE(MPI_IN_PLACE,dtmax,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)
   dt=dtMin
   CFLMin=CFLMax*dtMin/dtMax
   time=time+dt

END SUBROUTINE
