!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             GRID METRIC CALCULATION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE arrayAllocSlice(i)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
!     all variables in these two modules are allocated in this subroutine.
      USE conserved_Quantity

      USE control_Data,             ONLY:nx,ny,nz
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

      INTEGER,INTENT(IN)::i
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!***********************************************************************
!     Point array slice
!           for data exchange between blocks
!***********************************************************************
      consIS(i,1)%ro_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%ro_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%ro_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%ro_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%ro_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%ro_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%ro_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%ro_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%ro_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%ro_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%ro_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%ro_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%ro_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%ro_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%ro_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%ro_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ro_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%ro_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%ro_(        :      ,        :      ,    nz+1:nz+nGS)




      consIS(i,1)%ru_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%ru_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%ru_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%ru_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%ru_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%ru_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%ru_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%ru_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%ru_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%ru_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%ru_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%ru_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%ru_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%ru_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%ru_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%ru_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%ru_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%ru_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%ru_(        :      ,        :      ,    nz+1:nz+nGS)


      consIS(i,1)%rv_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%rv_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%rv_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%rv_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%rv_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%rv_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%rv_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%rv_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%rv_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%rv_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%rv_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%rv_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%rv_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%rv_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%rv_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%rv_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rv_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%rv_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%rv_(        :      ,        :      ,    nz+1:nz+nGS)


      consIS(i,1)%rw_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%rw_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%rw_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%rw_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%rw_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%rw_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%rw_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%rw_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%rw_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%rw_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%rw_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%rw_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%rw_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%rw_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%rw_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%rw_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%rw_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%rw_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%rw_(        :      ,        :      ,    nz+1:nz+nGS)


      consIS(i,1)%re_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%re_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%re_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%re_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%re_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%re_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%re_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%re_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%re_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%re_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%re_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%re_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%re_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%re_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%re_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%re_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%re_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%re_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%re_(        :      ,        :      ,    nz+1:nz+nGS)


      consIS(i,1)%rfs_(       1:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(       1:   nGS,        :      ,        :      ,:)
      consIS(i,2)%rfs_(nx-nGS+1:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(nx-nGS+1:    nx,        :      ,        :      ,:)
      consIS(i,3)%rfs_(1-nGS:,       1:,1-nGS:,1:)=>cons_gl(i)%rfs_(        :      ,       1:   nGS,        :      ,:)
      consIS(i,4)%rfs_(1-nGS:,ny-nGS+1:,1-nGS:,1:)=>cons_gl(i)%rfs_(        :      ,ny-nGS+1:    ny,        :      ,:)
      consIS(i,5)%rfs_(1-nGS:,1-nGS:,       1:,1:)=>cons_gl(i)%rfs_(        :      ,        :      ,       1:   nGS,:)
      consIS(i,6)%rfs_(1-nGS:,1-nGS:,nz-nGS+1:,1:)=>cons_gl(i)%rfs_(        :      ,        :      ,nz-nGS+1:    nz,:)

      consOS(i,1)%rfs_(1-nGS:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(   1-nGS:     0,        :      ,        :      ,:)
      consOS(i,2)%rfs_( nx+1:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(    nx+1:nx+nGS,        :      ,        :      ,:)
      consOS(i,3)%rfs_(1-nGS:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(        :      ,   1-nGS:     0,        :      ,:)
      consOS(i,4)%rfs_(1-nGS:, ny+1:,1-nGS:,1:)=>cons_gl(i)%rfs_(        :      ,    ny+1:ny+nGS,        :      ,:)
      consOS(i,5)%rfs_(1-nGS:,1-nGS:,1-nGS:,1:)=>cons_gl(i)%rfs_(        :      ,        :      ,   1-nGS:     0,:)
      consOS(i,6)%rfs_(1-nGS:,1-nGS:, nz+1:,1:)=>cons_gl(i)%rfs_(        :      ,        :      ,    nz+1:nz+nGS,:)


      consIS(i,1)%t_(       1:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(       1:   nGS,        :      ,        :      )
      consIS(i,2)%t_(nx-nGS+1:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(nx-nGS+1:    nx,        :      ,        :      )
      consIS(i,3)%t_(1-nGS:,       1:,1-nGS:)=>cons_gl(i)%t_(        :      ,       1:   nGS,        :      )
      consIS(i,4)%t_(1-nGS:,ny-nGS+1:,1-nGS:)=>cons_gl(i)%t_(        :      ,ny-nGS+1:    ny,        :      )
      consIS(i,5)%t_(1-nGS:,1-nGS:,       1:)=>cons_gl(i)%t_(        :      ,        :      ,       1:   nGS)
      consIS(i,6)%t_(1-nGS:,1-nGS:,nz-nGS+1:)=>cons_gl(i)%t_(        :      ,        :      ,nz-nGS+1:    nz)

      consOS(i,1)%t_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(   1-nGS:     0,        :      ,        :      )
      consOS(i,2)%t_( nx+1:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(    nx+1:nx+nGS,        :      ,        :      )
      consOS(i,3)%t_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(        :      ,   1-nGS:     0,        :      )
      consOS(i,4)%t_(1-nGS:, ny+1:,1-nGS:)=>cons_gl(i)%t_(        :      ,    ny+1:ny+nGS,        :      )
      consOS(i,5)%t_(1-nGS:,1-nGS:,1-nGS:)=>cons_gl(i)%t_(        :      ,        :      ,   1-nGS:     0)
      consOS(i,6)%t_(1-nGS:,1-nGS:, nz+1:)=>cons_gl(i)%t_(        :      ,        :      ,    nz+1:nz+nGS)

      END SUBROUTINE arrayAllocSlice

