!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE getViscosity
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_Transport,            ONLY:rci22,getViscSpecies,pt,pts

   USE thermal_Property,         ONLY:sig,tek,wMol,vmol,dmol
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ns,ib, &
  &                                   num_nonRadical, &
  &                                   nonRadical
   USE Status_Check,             ONLY:meanViscosity
   USE fundamental_Constants,    ONLY:nGS

   IMPLICIT NONE

   INTEGER::i,j,k,is,iss,nr,nrr
   REAL(8)::phi,visc
   REAL(8),DIMENSION(ns)::vism

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!$OMP PARALLEL DO PRIVATE(i,j,k,is,iss,vism,visc,nr,nrr,phi) REDUCTION(+:meanViscosity)
   _forAllPoints_
#define c bl(ib)%p(i,j,k)%c
      visc=0.0d0
      vism=0.0d0 ! <- may be meaningless

      ! LAMINAR VISCOSITY from non-radical species
      !  viscosities of each non-radicals
      DO is=1,num_nonRadical
         nr=nonRadical(is)
         vism(nr)=getViscSpecies(c%T, sig(nr), rci22(c%t,tek(nr)), wMol(nr))
      ENDDO

      ! viscosity of mixture
      ! Combustion, Eq. 5.18
      DO is=1,num_nonRadical
         phi=0.0d0
         nr=nonRadical(is)

         DO iss=1,num_nonRadical
            nrr=nonRadical(iss)
            IF (nrr.ne.nr) THEN
               phi=phi+c%YFS(nrr)*(1.0D0+SQRT(vism(nr)/vism(nrr))*VMOL(nr,nrr)**2*DMOL(nr,nrr))
            ELSE
               phi=phi+c%yfs(nrr)
            ENDIF
         ENDDO

         visc=visc+c%YFS(nr)*vism(nr)/phi

      ENDDO

      pts(i,j,k,:)%vism=vism
      pt(i,j,k)%visc=visc

      meanViscosity=meanViscosity+visc

#undef c
   _endAllPoints_
!$OMP END PARALLEL DO

END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE momentumDiffusion(dimen)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_Transport,            ONLY:pt, face

   USE control_Data,             ONLY:nx,ny,nz,ib
   USE M_BlockPointData,         ONLY:bl
   USE Status_Check,             ONLY:visc_Ratio
   USE fundamental_Constants,    ONLY:nGS
   USE TurbulenceModeling

IMPLICIT NONE

   INTEGER,INTENT(IN)::dimen
   INTEGER::i,j,k,ish,jsh,ksh
   REAL(8)::sxx,syy,szz,sxy,syz,sxz, &
            txx,tyy,tzz,txy,tyz,txz, &
            visc,vist

   REAL(8)::turbulentPressure=0.0d0
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

   ish=0
   jsh=0
   ksh=0
   SELECT CASE(dimen)
      CASE(1)
         ish=1
      CASE(2)
         jsh=1
      CASE(3)
         ksh=1
   END SELECT

   visc_Ratio=0.0d0

   IF( TurbModel.eq.isLES .and. &
       (LESModel.eq.isDynamic .or. &
        LESModel.eq.isCompressibleDynamic)) THEN
      CONTINUE
!      CALL Filter(face, bl(ib)%p%gd%sgsdlt)
   ENDIF

!$OMP PARALLEL DO PRIVATE(i,j,k,vist,visc,sxx,syy,szz,sxy,sxz,syz,txx,tyy,tzz,txy,txz,tyz)
   DO k=nGS,nz+nGS;DO j=nGS,ny+nGS;DO i=nGS,nx+nGS
#define face face(i,j,k)
      CALL face%strainRate

      vist=getVisT(face,bl(ib)%p(i,j,k)%gd%sgsdlt)*bl(ib)%p(i,j,k)%c%ro
      !Strange. sgsdlt/ro on point and snorm on face

      visc=0.5d0*(pt(i,j,k)%visc+pt(i+ish,j+jsh,k+ksh)%visc)

      !     deviatoric strain rate on the boundary
      SXX=2.D0*(2.D0*face%DUX-face%DVY-face%DWZ)/3.D0
      SYY=2.D0*(2.D0*face%DVY-face%DWZ-face%DUX)/3.D0
      SZZ=2.D0*(2.D0*face%DWZ-face%DVY-face%DUX)/3.D0
      SXY=(face%DUY+face%DVX)
      SXZ=(face%DWX+face%DUZ)
      SYZ=(face%DVZ+face%DWY)


      !   >>>STRESS TENSOR<<<
      TXX=(visc+vist)*sxx + turbulentPressure
      TYY=(visc+vist)*syy + turbulentPressure
      TZZ=(visc+vist)*szz + turbulentPressure
      TXY=(visc+vist)*SXY
      TXZ=(visc+vist)*SXZ
      TYZ=(visc+vist)*SYZ

      visc_Ratio=MAX(visc_Ratio, vist/visc*1.0d2)
      face%vist=vist

      !     Input stress result into flux terms
      face%RUFX=TXX
      face%RVFX=TXY
      face%RWFX=TXZ

      face%RUFY=TXY
      face%RVFY=TYY
      face%RWFY=TYZ

      face%RUFZ=TXZ
      face%RVFZ=TYZ
      face%RWFZ=TZZ

#undef face
   ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
END SUBROUTINE
