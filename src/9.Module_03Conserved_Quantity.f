!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** fluid related variables
!     later, this module is to be merged with grid_Data module.

!     ro          density (kg/m3)
!     ru, rv, rw  density by velocity (kg/(m2*s))
!     re          density by internal total energy (kg/m3*J/kg = J/m3)
!     u, v, w     velocity (m/s)
!     h           enthalpy (J/m3)
!     T           temperature (K)
!     gamma       specific heat ratio
!     qChem       heat release
!     c           speed of sound (m/s)
!     fs          mass fraction (ND)
!     yfs         concentration (mol/m3)
!     rfs         density by mass fraction

!     cons_gl     Derived type variable which contains all conserved
!                 quantity variables
!     consIS      Inner Slice of conservative quantity variables
!     consIS      Outer Slice of conservative quantity variables

      MODULE Conserved_Quantity
      IMPLICIT NONE
      SAVE

      TYPE conservedQuantity
            REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::
     &            ro_,ru_,rv_,rw_,re_,
     &            T_
!                 Here, temperature is an exception for boundary condition
            REAL(8),ALLOCATABLE,DIMENSION(:,:,:,:)::
     &            rfs_
      CONTAINS
            PROCEDURE::allocScalar
      ENDTYPE conservedQuantity

      TYPE,EXTENDS(conservedQuantity)::inducedConservedQuantity
            REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::
     &            u_, v_, w_, h_,
     &            gamma_,qChem_, c_, p_
            REAL(8),ALLOCATABLE,DIMENSION(:,:,:,:)::
     &            fs_,yfs_
      ENDTYPE inducedConservedQuantity

      TYPE conservedQuantitySlice
            REAL(8),POINTER,DIMENSION(:,:,:)::
     &            ro_,ru_,rv_,rw_,re_,
     &            T_
            REAL(8),POINTER,DIMENSION(:,:,:,:)::
     &            rfs_
      ENDTYPE

      TYPE(conservedQuantity)::initQuant

      TYPE(inducedConservedQuantity),ALLOCATABLE,TARGET,DIMENSION(:)::cons_gl

      TYPE(conservedQuantitySlice),ALLOCATABLE,DIMENSION(:,:)::consIS
      TYPE(conservedQuantitySlice),ALLOCATABLE,DIMENSION(:,:)::consOS

!$$$$$$$$$$$$$$$$$$$$$
!     Pointers
!$$$$$$$$$$$$$$$$$$$$$
      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      ro_,ru_,rv_,rw_,re_,
     &           u_, v_, w_, h_,
     &      T_, gamma_,qChem_, c_, p_
      REAL(8),POINTER,DIMENSION(:,:,:,:)::
     &      rfs_,fs_,yfs_

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      ro,ru,rv,rw,re,
     &          u, v, w, h,
     &      T, gamma,qChem, c, p
      REAL(8),POINTER,DIMENSION(:,:,:,:)::
     &      rfs,fs,yfs

      CONTAINS

      SUBROUTINE allocScalar(this,ns)

      IMPLICIT NONE

      CLASS(conservedQuantity)::this
      INTEGER::ns

      ALLOCATE(this%ro_(1,1,1),
     &         this%ru_(1,1,1),this%rv_(1,1,1),this%rw_(1,1,1),
     &         this%re_(1,1,1),this%rfs_(1,1,1,ns),this%t_(1,1,1))

      ENDSUBROUTINE

      END MODULE

