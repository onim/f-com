!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE BCSLT
!       Wrapper for multiblock interface data exchange
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
IMPLICIT NONE

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

   CALL BCSLT1

   CALL BCForInterface
   CALL BCForInterfaceMPI

   CALL ForcedBC

END SUBROUTINE



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE BCSLT1
!       HOLE SONIC&SUBSONIC INJECTION BOUNDARY CONDITION
!       ALL WALLS : NON-SLIP CONDITION
!                   Adiabatic
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nb,ns,nx,ny,nz,nx_gl,ny_gl,nz_gl,ib
   USE boundary_Data_OOP,        ONLY:bf
   USE Message_Passing_Interface
   USE Fundamental_Constants

IMPLICIT NONE

   INTEGER::facet,is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   _forMyBlocks_
      DO facet=1,6
      ASSOCIATE(bf=>bf(ib,facet))
      ASSOCIATE(consOS=>bl(ib)%p(bf%xol:bf%xou,bf%yol:bf%you,bf%zol:bf%zou)%c, &
                consIS=>bl(ib)%p(bf%xil:bf%xiu,bf%yil:bf%yiu,bf%zil:bf%ziu)%c)
         SELECT CASE (bf%faceType)
         CASE(ft_interface,ft_interface_MPI)
            CONTINUE
         CASE(ft_inlet)
            ! get values from preset
            ! blockface forcedboundarycondition quantities
            consOS%ro=bf%fBC(1)%quant%ro
            consOS%ru=bf%fBC(1)%quant%ru
            consOS%rv=bf%fBC(1)%quant%rv
            consOS%rw=bf%fBC(1)%quant%rw
            consOS%re=bf%fBC(1)%quant%re
            DO is=1,ns
            consOS%rfs(is)=bf%fBC(1)%quant%rfs(is)
            ENDDO
            consOS%t=bf%fBC(1)%quant%t
         CASE(ft_outlet)
            ! zero gradient condition
            SELECT CASE (facet)
            CASE(fo_west)
               consOS=consIS((/1,1/),:,:)
            CASE(fo_east)
               consOS=consIS((/nGS,nGS/),:,:)
            CASE(fo_south)
               consOS=consIS(:,(/1,1/),:)
            CASE(fo_north)
               consOS=consIS(:,(/nGS,nGS/),:)
            CASE(fo_back)
               consOS=consIS(:,:,(/1,1/))
            CASE(fo_front)
               consOS=consIS(:,:,(/nGS,nGS/))
            ENDSELECT

         CASE(ft_wall)
            SELECT CASE (facet)
            CASE(fo_west, fo_east)
               consOS=consIS(nGS:1:-1,:,:)
            CASE(fo_south, fo_north)
               consOS=consIS(:,nGS:1:-1,:)
            CASE(fo_back, fo_front)
               consOS=consIS(:,:,nGS:1:-1)
            ENDSELECT

            consOS%ru=consOS%ru*(-1.0d0)
            consOS%rv=consOS%rv*(-1.0d0)
            consOS%rw=consOS%rw*(-1.0d0)

         CASE(ft_symmetric)
            SELECT CASE (facet)
            CASE(fo_west, fo_east)
               consOS=consIS(nGS:1:-1,:,:)
            CASE(fo_south, fo_north)
               consOS=consIS(:,nGS:1:-1,:)
            CASE(fo_back, fo_front)
               consOS=consIS(:,:,nGS:1:-1)
            ENDSELECT

            consOS%ru=consOS%ru*(1.0d0-bf%normx*2.0d0)
            consOS%rv=consOS%rv*(1.0d0-bf%normy*2.0d0)
            consOS%rw=consOS%rw*(1.0d0-bf%normz*2.0d0)

         CASE DEFAULT
            CALL ErrorStop(7200)
         ENDSELECT
      END ASSOCIATE;END ASSOCIATE
      ENDDO

   _endMyBlocks_

END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE BCForInterface
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nb,nx,ny,nz,nx_gl,ny_gl,nz_gl,ib
   USE boundary_Data_OOP,        ONLY:bf
   USE Message_Passing_Interface
   USE Fundamental_Constants

IMPLICIT NONE

   INTEGER::f,tB,tF,m,j,is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   _forMyBlocks_
      DO f=fo_west,fo_east
      ASSOCIATE(consOS=>bl(ib)%p(bf(ib,f)%xol:bf(ib,f)%xou, &
                                 bf(ib,f)%yol:bf(ib,f)%you, &
                                 bf(ib,f)%zol:bf(ib,f)%zou)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface) THEN

            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace
            ASSOCIATE(consIS=>bl(tB)%p(bf(tB,tF)%xil:bf(tB,tF)%xiu, &
                                       bf(tB,tF)%yil:bf(tB,tF)%yiu, &
                                       bf(tB,tF)%zil:bf(tB,tF)%ziu)%c)

            ! Matching case
            IF    (ny.eq.ny_gl(tB)) THEN
               consOS=consIS

            ! Non-matching case - coarse
            ELSEIF(ny.lt.ny_gl(tB)) THEN
               m=ny_gl(tB)/ny
               IF (m*ny.ne.ny_gl(tB)) CALL ErrorStop(7200)
               DO j=1+nGS,ny+nGS
                  consOS(:,j,:)%ro=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%ro,DIM=2)/m
                  consOS(:,j,:)%ru=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%ru,DIM=2)/m
                  consOS(:,j,:)%rv=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rv,DIM=2)/m
                  consOS(:,j,:)%rw=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rw,DIM=2)/m
                  consOS(:,j,:)%re=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%re,DIM=2)/m
                  DO is=1,ns
                  consOS(:,j,:)%rfs(is)=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rfs(is),DIM=2)/m
                  ENDDO
                  consOS(:,j,:)%t=SUM(consIS(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%t,DIM=2)/m
               ENDDO

            ! Non-matching case - fine
            ELSEIF(ny.gt.ny_gl(tB)) THEN
               m=ny/ny_gl(tB)
               IF (m*ny_gl(tB).ne.ny) CALL ErrorStop(7200)
               DO j=1+nGS,ny+nGS
                  consOS(:,j,:)=consIS(:,(j-1-nGS)/m+nGS+1,:)
               ENDDO
            ENDIF
            END ASSOCIATE
         ENDIF
      END ASSOCIATE
      ENDDO


      DO f=fo_south,fo_north
      ASSOCIATE(consOS=>bl(ib)%p(bf(ib,f)%xol:bf(ib,f)%xou, &
                                 bf(ib,f)%yol:bf(ib,f)%you, &
                                 bf(ib,f)%zol:bf(ib,f)%zou)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface) THEN

            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace
            ASSOCIATE(consIS=>bl(tB)%p(bf(tB,tF)%xil:bf(tB,tF)%xiu, &
                                       bf(tB,tF)%yil:bf(tB,tF)%yiu, &
                                       bf(tB,tF)%zil:bf(tB,tF)%ziu)%c)

            ! Matching case
            IF    (nx.eq.nx_gl(tB)) THEN
               consOS=consIS

            ! Non-matching case - coarse
            ELSEIF(nx.lt.nx_gl(tB)) THEN
               m=nx_gl(tB)/nx
               IF (m*nx.ne.nx_gl(tB)) CALL ErrorStop(7200)
               DO j=1+nGS,nx+nGS
                  consOS(j,:,:)%ro=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%ro,DIM=1)/m
                  consOS(j,:,:)%ru=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%ru,DIM=1)/m
                  consOS(j,:,:)%rv=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rv,DIM=1)/m
                  consOS(j,:,:)%rw=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rw,DIM=1)/m
                  consOS(j,:,:)%re=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%re,DIM=1)/m
                  DO is=1,ns
                  consOS(j,:,:)%rfs(is)=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rfs(is),DIM=1)/m
                  ENDDO
                  consOS(j,:,:)%t=SUM(consIS(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%t,DIM=1)/m
               ENDDO

            ! Non-matching case - fine
            ELSEIF(nx.gt.nx_gl(tB)) THEN
               m=nx/nx_gl(tB)
               IF (m*nx_gl(tB).ne.nx) CALL ErrorStop(7200)
               DO j=1+nGS,nx+nGS
                  consOS(j,:,:)=consIS((j-1-nGS)/m+nGS+1,:,:)
               ENDDO
            ENDIF
            END ASSOCIATE
         ENDIF
      END ASSOCIATE
      ENDDO

      DO f=fo_back,fo_front
      ASSOCIATE(consOS=>bl(ib)%p(bf(ib,f)%xol:bf(ib,f)%xou, &
                                 bf(ib,f)%yol:bf(ib,f)%you, &
                                 bf(ib,f)%zol:bf(ib,f)%zou)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface) THEN

            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace
            ASSOCIATE(consIS=>bl(tB)%p(bf(tB,tF)%xil:bf(tB,tF)%xiu, &
                                       bf(tB,tF)%yil:bf(tB,tF)%yiu, &
                                       bf(tB,tF)%zil:bf(tB,tF)%ziu)%c)

            ! Matching case
            consOS=consIS
            ! No non-matching case
            END ASSOCIATE
         ENDIF
      END ASSOCIATE
      ENDDO

   _endMyBlocks_

END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE BCForInterfaceMPI
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl,T_ExtendedConservedQuantity
   USE control_Data,             ONLY:ne,nb,nx,ny,nz,nx_gl,ny_gl,nz_gl,ib
   USE boundary_Data_OOP,        ONLY:bf
   USE Message_Passing_Interface
   USE Fundamental_Constants

IMPLICIT NONE

   INTEGER::f,sN,tN,tB,tF,m,j,x,y,z, is
   TYPE(T_ExtendedConservedQuantity),ALLOCATABLE,DIMENSION(:,:,:)::dummy
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ! Prepare data transaction (at the first time)
   IF(.not.ALLOCATED(bdc_send)) THEN
      ALLOCATE(bdc_send(nb,6),bdc_recv(nb,6))
      _forAllBlocks_
         DO f=fo_west,fo_east
            IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
               tB=bf(ib,f)%connBlock
               tF=bf(ib,f)%connFace

               CALL bdc_send(ib,f)%alloc((/2,ny_gl(ib)+2*nGS,nz_gl(ib)+2*nGS/),ne+1,tB,tF)
               CALL bdc_recv(ib,f)%alloc((/2,ny_gl(tB)+2*nGS,nz_gl(tB)+2*nGS/),ne+1,tB,tF)
            ENDIF
         ENDDO

         DO f=fo_south,fo_north
            IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
               tB=bf(ib,f)%connBlock
               tF=bf(ib,f)%connFace

               CALL bdc_send(ib,f)%alloc((/nx_gl(ib)+2*nGS,2,nz_gl(ib)+2*nGS/),ne+1,tB,tF)
               CALL bdc_recv(ib,f)%alloc((/nx_gl(tB)+2*nGS,2,nz_gl(tB)+2*nGS/),ne+1,tB,tF)
            ENDIF
         ENDDO
      _endAllBlocks_
   ENDIF

   ! make transaction
   _forAllBlocks_
      DO f=fo_west,fo_front
      ASSOCIATE(consIS=>bl(ib)%p(bf(ib,f)%xil:bf(ib,f)%xiu, &
                                 bf(ib,f)%yil:bf(ib,f)%yiu, &
                                 bf(ib,f)%zil:bf(ib,f)%ziu)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            sN=bf(ib,f)%myNode
            tN=bf(ib,f)%connNode
            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace

            IF (myBlockIs(ib)) THEN
               CALL bdc_send(ib,f)%pack(consIS)
               CALL sendBoundaryValue(tN, bdc_send(ib,f))
            ENDIF

            IF (myBlockIs(tB)) THEN
               CALL recvBoundaryValue(sN, bdc_recv(tb,tf))
            ENDIF
         ENDIF
      END ASSOCIATE
      ENDDO
   _endAllBlocks_

   ! unpack transacted data
   _forMyBlocks_
      DO f=fo_west,fo_east
      ASSOCIATE(consOS=>bl(ib)%p(bf(ib,f)%xol:bf(ib,f)%xou, &
                                 bf(ib,f)%yol:bf(ib,f)%you, &
                                 bf(ib,f)%zol:bf(ib,f)%zou)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock


            x=bdc_recv(ib,f)%nx
            y=bdc_recv(ib,f)%ny
            z=bdc_recv(ib,f)%nz
            ! cf) x==nx+2*nGS

            IF (ALLOCATED(dummy)) DEALLOCATE(dummy)
            ALLOCATE(dummy(nGS,y,z))
            CALL bdc_recv(ib,f)%unpack(dummy)

            ! Matching case
            IF    (ny.eq.ny_gl(tB)) THEN
               consOS=dummy

            ! Non-matching case - coarse
            ELSEIF(ny.lt.ny_gl(tB)) THEN
               m=ny_gl(tB)/ny
               IF (m*ny.ne.ny_gl(tB)) CALL ErrorStop(7200)
               DO j=1+nGS,ny+nGS
                  consOS(:,j,:)%ro=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%ro,DIM=2)/m
                  consOS(:,j,:)%ru=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%ru,DIM=2)/m
                  consOS(:,j,:)%rv=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rv,DIM=2)/m
                  consOS(:,j,:)%rw=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rw,DIM=2)/m
                  consOS(:,j,:)%re=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%re,DIM=2)/m
                  DO is=1,ns
                  consOS(:,j,:)%rfs(is)=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%rfs(is),DIM=2)/m
                  ENDDO
                  consOS(:,j,:)%t=SUM(dummy(:,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%t,DIM=2)/m
               ENDDO

            ! Non-matching case - fine
            ELSEIF(ny.gt.ny_gl(tB)) THEN
               m=ny/ny_gl(tB)
               IF (m*ny_gl(tB).ne.ny) CALL ErrorStop(7200)
               DO j=1+nGS,ny+nGS
                  consOS(:,j,:)=dummy(:,(j-1-nGS)/m+nGS+1,:)
               ENDDO
            ENDIF
         ENDIF
      END ASSOCIATE
      ENDDO

      DO f=fo_south,fo_north
      ASSOCIATE(consOS=>bl(ib)%p(bf(ib,f)%xol:bf(ib,f)%xou, &
                                 bf(ib,f)%yol:bf(ib,f)%you, &
                                 bf(ib,f)%zol:bf(ib,f)%zou)%c)
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock

            x=bdc_recv(ib,f)%nx
            y=bdc_recv(ib,f)%ny
            z=bdc_recv(ib,f)%nz

            IF (ALLOCATED(dummy)) DEALLOCATE(dummy)
            ALLOCATE(dummy(x,nGS,z))
            CALL bdc_recv(ib,f)%unpack(dummy)

            ! Matching case
            IF    (nx.eq.nx_gl(tB)) THEN
               consOS=dummy

            ! Non-matching case - coarse
            ELSEIF(nx.lt.nx_gl(tB)) THEN
               m=nx_gl(tB)/nx
               IF (m*nx.ne.nx_gl(tB)) CALL ErrorStop(7200)
               DO j=1+nGS,nx+nGS
                  consOS(j,:,:)%ro=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%ro,DIM=1)/m
                  consOS(j,:,:)%ru=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%ru,DIM=1)/m
                  consOS(j,:,:)%rv=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rv,DIM=1)/m
                  consOS(j,:,:)%rw=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rw,DIM=1)/m
                  consOS(j,:,:)%re=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%re,DIM=1)/m
                  DO is=1,ns
                  consOS(j,:,:)%rfs(is)=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%rfs(is),DIM=1)/m
                  ENDDO
                  consOS(j,:,:)%t=SUM(dummy(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:,:)%t,DIM=1)/m
               ENDDO

            ! Non-matching case - fine
            ELSEIF(nx.gt.nx_gl(tB)) THEN
               m=nx/nx_gl(tB)
               IF (m*nx_gl(tB).ne.nx) CALL ErrorStop(7200)
               DO j=1+nGS,nx+nGS
                  consOS(j,:,:)=dummy((j-1-nGS)/m+nGS+1,:,:)
               ENDDO
            ENDIF
         ENDIF
      END ASSOCIATE
      ENDDO
   _endMyBlocks_

END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      SUBROUTINE ForcedBC
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,nx_gl,ny_gl,nz_gl,nb,ib
   USE boundary_Data_OOP,        ONLY:bf
   USE Message_Passing_Interface

IMPLICIT NONE

   INTEGER::facet,iFBC,iNL,i,j,k
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ! For each surface
   _forMyBlocks_
      DO facet=1,6
      ASSOCIATE(bf=>bf(ib,facet))
      ASSOCIATE(consOS=>bl(ib)%p(bf%xol:bf%xou, &
                                 bf%yol:bf%you, &
                                 bf%zol:bf%zou)%c)
         ! If there exist forced boundary condition(s)
         IF (bf%nFBC.ne.0) THEN

            ! For each forced boundary condition and point
            DO iFBC=1,bf%nFBC
               DO iNL=1,bf%fBC(iFBC)%nnode
                     i=bf%fBC(iFBC)%nodelist(iNL)%i
                     j=bf%fBC(iFBC)%nodelist(iNL)%j
                     k=bf%fBC(iFBC)%nodelist(iNL)%k
                     consOS(i,j,k)=bf%fBC(iFBC)%quant
               ENDDO
            ENDDO
         ENDIF
      END ASSOCIATE;END ASSOCIATE
      ENDDO
   _endMyBlocks_

      END SUBROUTINE

