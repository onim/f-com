!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Large Eddy Simulation MODULE
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      MODULE LES
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE transportModule,          ONLY:transface
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Variables
      IMPLICIT NONE

!     Basic variables
      REAL(8),PARAMETER::C_Smagorinsky=0.1d0

      INTEGER::LESModel
      INTEGER,PARAMETER::isSmagorinsky=1,
     &                   isDynamic=2,
     &                   isCompressibleDynamic=3


!     Filtered variables (have prefix f on their name)
      REAL(8),ALLOCATABLE,DIMENSION(:,:,:),TARGET::
     &      fro,
     &      fru, frv, frw,
     &      fruu,frvv,frww,
     &      fruv,fruw,frvw,
     &      fSuu,fSvv,fSww,
     &      fSuv,fSuw,fSvw

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      frvu,frwu,frwv,
     &      fSvu,fSwu,fSwv

      REAL(8)::C_Dynamic_ave

!     Function pointer
      REAL(8),EXTERNAL,POINTER::getLESVisT=>null()

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Methods
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
!     Assign turbulence model function
      SUBROUTINE assignLESModel
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      SELECT CASE (LESModel)
            CASE (isSmagorinsky)
                  getLESVisT=>getLESVisTSmagorinsky
            CASE (isCompressibleDynamic)
                  getLESVisT=>getLESVisTCompDynamic
            CASE DEFAULT
                  getLESVisT=>getLESVisTSmagorinsky
      END SELECT

      END SUBROUTINE


!$$$$$$$$$$$$$$$
!     Smagorinsky model
!           one of the simplest and the first model
      FUNCTION getLESVisTSmagorinsky(face,sgsDlt) RESULT(vist)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      TYPE(transface),INTENT(IN)::face
      REAL(8),INTENT(IN)::sgsDlt
      REAL(8)::vist

      vist=(C_Smagorinsky*sgsDlt)**2*Snorm(face)

      END FUNCTION

!$$$$$$$$$$$$$$$
!     Compressible dynamic model
!           Original model by Germano(1991)
!           For compressible by Moin?
      FUNCTION getLESVisTCompDynamic(face,sgsDlt) RESULT(vist)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      TYPE(transface),INTENT(IN)::face
      REAL(8),INTENT(IN)::sgsDlt
      REAL(8)::vist

      vist=(C_Dynamic_ave*sgsDlt)**2*Snorm(face)

      END FUNCTION


!$$$$$$$$$$$$$$$
!     Magnitude of velocity gradient tensor
      FUNCTION Snorm(face)
!$$$$$$$$$$$$$$$
      IMPLICIT NONE

      TYPE(transface),INTENT(IN)::face
      REAL(8)::Snorm

      Snorm=SQRT(( face%dux**2 +face%dvx**2 +face%dwx**2
     &            +face%duy**2 +face%dvy**2 +face%dwy**2
     &            +face%duz**2 +face%dvz**2 +face%dwz**2)*2.0d0)

      END FUNCTION

!$$$$$$$$$$$$$$$
!     Magnitude of velocity gradient tensor
      SUBROUTINE Filter(face,sgsDlt)
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz
      USE conserved_Quantity
      IMPLICIT NONE

      TYPE(transface),INTENT(IN),DIMENSION(:,:,:)::face
      REAL(8),INTENT(in),DIMENSION(:,:,:)::sgsDlt
      REAL(8)::denom,nom

      IF(ALLOCATED(fro)) THEN
      DEALLOCATE(
     &      fro,
     &      fru, frv, frw,
     &      fruu,frvv,frww,
     &      fruv,fruw,frvw)
      ENDIF

      ALLOCATE(fro(nx,ny,nz))
      ALLOCATE(
     &      fru, frv, frw,
     &      fruu,frvv,frww,
     &      fruv,fruw,frvw,mold=fro)

      fro =testFiltering(ro_)

      fru =testFiltering(ru_)
      frv =testFiltering(rv_)
      frw =testFiltering(rw_)

      fruu=testFiltering(ru_*u_)
      fruv=testFiltering(ru_*v_)
      fruw=testFiltering(ru_*w_)
      frvu=>fruv
      frvv=testFiltering(rv_*v_)
      frvw=testFiltering(rv_*w_)
      frwu=>fruw
      frwv=>frvw

      fSww=testFiltering(ro_*w_)
      fSuu=testFiltering(ru_*u_)
      fSuv=testFiltering(ru_*v_)
      fSuw=testFiltering(ru_*w_)
      fSvu=>fSuv
      fSvv=testFiltering(rv_*v_)
      fSvw=testFiltering(rv_*w_)
      fSwu=>fSuw
      fSwv=>fSvw
      fSww=testFiltering(rw_*w_)

      denom=SUM(
     &       (fruu-fru*fru/fro)*face%dux*2.0d0/3.0d0
     &      +(fruv-fru*frv/fro)*face%duy
     &      +(fruw-fru*frw/fro)*face%duz
     &      +(frvu-frv*fru/fro)*face%dvx
     &      +(frvv-frv*frv/fro)*face%dvy*2.0d0/3.0d0
     &      +(frvw-frv*frw/fro)*face%dvz
     &      +(frwu-frw*fru/fro)*face%dwx
     &      +(frwv-frw*frv/fro)*face%dwy
     &      +(frww-frw*frw/fro)*face%dwz*2.0d0/3.0d0
     &      )/SIZE(fro)



      END SUBROUTINE

!$$$$$$$$$$$$$$$
!     Test(bigger) filter denoted by caret
!     Box filtering...not sure whether this is correct
      FUNCTION testFiltering(q) RESULT(fq)
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ngs
      IMPLICIT NONE

      REAL(8),DIMENSION(1-ngs:nx+ngs,1-ngs:ny+ngs,1-ngs:nz+ngs),INTENT(in)::q
      REAL(8),DIMENSION(nx,ny,nz)::fq

      INTEGER::i,j,k

      DO k=1,nz; DO j=1,ny; DO i=1,nx

      fq(i,j,k)=
     &       8.0d0*  q(i,j,k)
     &      +4.0d0*( q(i+1,j,k)+q(i,j+1,k)+q(i,j,k+1)
     &              +q(i-1,j,k)+q(i,j-1,k)+q(i,j,k-1))
     &      +2.0d0*( q(i+1,j+1,k)+q(i+1,j,k+1)+q(i,j+1,k+1)
     &              +q(i+1,j-1,k)+q(i+1,j,k-1)+q(i,j+1,k-1)
     &              +q(i-1,j+1,k)+q(i-1,j,k+1)+q(i,j-1,k+1)
     &              +q(i-1,j-1,k)+q(i-1,j,k-1)+q(i,j-1,k-1))
     &      +1.0d0*( q(i+1,j+1,k+1)+q(i-1,j-1,k-1)
     &              +q(i+1,j-1,k-1)+q(i-1,j+1,k-1)+q(i-1,j-1,k+1)
     &              +q(i-1,j+1,k+1)+q(i+1,j-1,k+1)+q(i+1,j+1,k-1))
      fq(i,j,k)=fq(i,j,k)/80.0d0

      ENDDO;ENDDO;ENDDO

      END FUNCTION


      END MODULE
