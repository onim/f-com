!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE CoordDifference
! Calculate coordinate difference for metrices
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:ib,nx,ny,nz
   USE boundary_Data_OOP,        ONLY:bf
   USE fundamental_Constants


IMPLICIT NONE

   REAL(8),PARAMETER::h=0.5d0
   INTEGER::dimen

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   ASSOCIATE(gxx=>bl(ib)%p(:,:,:)%gd%gxx, &
             gxy=>bl(ib)%p(:,:,:)%gd%gxy, &
             gxz=>bl(ib)%p(:,:,:)%gd%gxz, &
             gyx=>bl(ib)%p(:,:,:)%gd%gyx, &
             gyy=>bl(ib)%p(:,:,:)%gd%gyy, &
             gyz=>bl(ib)%p(:,:,:)%gd%gyz, &
             gzx=>bl(ib)%p(:,:,:)%gd%gzx, &
             gzy=>bl(ib)%p(:,:,:)%gd%gzy, &
             gzz=>bl(ib)%p(:,:,:)%gd%gzz, &
             xd =>bl(ib)%p(:,:,:)%g%xd, &
             yd =>bl(ib)%p(:,:,:)%g%yd, &
             zd =>bl(ib)%p(:,:,:)%g%zd)
      dimen=1
      gxx=h*(CSHIFT(xd,1,dimen)-CSHIFT(xd,-1,dimen))
      gyx=h*(CSHIFT(yd,1,dimen)-CSHIFT(yd,-1,dimen))
      gzx=h*(CSHIFT(zd,1,dimen)-CSHIFT(zd,-1,dimen))

      dimen=2
      gxy=h*(CSHIFT(xd,1,dimen)-CSHIFT(xd,-1,dimen))
      gyy=h*(CSHIFT(yd,1,dimen)-CSHIFT(yd,-1,dimen))
      gzy=h*(CSHIFT(zd,1,dimen)-CSHIFT(zd,-1,dimen))

      dimen=3
      gxz=h*(CSHIFT(xd,1,dimen)-CSHIFT(xd,-1,dimen))
      gyz=h*(CSHIFT(yd,1,dimen)-CSHIFT(yd,-1,dimen))
      gzz=h*(CSHIFT(zd,1,dimen)-CSHIFT(zd,-1,dimen))


      IF (bf(ib,fo_west)%faceType.eq.ft_interface .or. &
          bf(ib,fo_west)%faceType.eq.ft_interface_MPI) THEN
         gxx(1,:,:)=xd(2,:,:)-xd(1,:,:)
         gyx(1,:,:)=yd(2,:,:)-yd(1,:,:)
         gzx(1,:,:)=zd(2,:,:)-zd(1,:,:)
      ELSE
         gxx(1,:,:)=h*(xd(2,:,:)-(xd(nGS,:,:)-(xd(1+2*nGS,:,:)-xd(1+nGS,:,:))))
         gyx(1,:,:)=h*(yd(2,:,:)-(yd(nGS,:,:)-(yd(1+2*nGS,:,:)-yd(1+nGS,:,:))))
         gzx(1,:,:)=h*(zd(2,:,:)-(zd(nGS,:,:)-(zd(1+2*nGS,:,:)-zd(1+nGS,:,:))))
      ENDIF

      IF (bf(ib,fo_east)%faceType.eq.ft_interface .or. &
          bf(ib,fo_east)%faceType.eq.ft_interface_MPI) THEN
         gxx(nx+2*nGS,:,:)=xd(nx+2*nGS,:,:)-xd(nx+2*nGS-1,:,:)
         gyx(nx+2*nGS,:,:)=yd(nx+2*nGS,:,:)-yd(nx+2*nGS-1,:,:)
         gzx(nx+2*nGS,:,:)=zd(nx+2*nGS,:,:)-zd(nx+2*nGS-1,:,:)
      ELSE
         gxx(nx+2*nGS,:,:)=h*(xd(nx+1+nGS,:,:)-(xd(nx,:,:)-xd(nx+nGS,:,:))-xd(nx-1+2*nGS,:,:))
         gyx(nx+2*nGS,:,:)=h*(yd(nx+1+nGS,:,:)-(yd(nx,:,:)-yd(nx+nGS,:,:))-yd(nx-1+2*nGS,:,:))
         gzx(nx+2*nGS,:,:)=h*(zd(nx+1+nGS,:,:)-(zd(nx,:,:)-zd(nx+nGS,:,:))-zd(nx-1+2*nGS,:,:))
      ENDIF

      IF (bf(ib,fo_south)%faceType.eq.ft_interface .or. &
          bf(ib,fo_south)%faceType.eq.ft_interface_MPI) THEN
         gxy(:,1,:)=xd(:,2,:)-xd(:,1,:)
         gyy(:,1,:)=yd(:,2,:)-yd(:,1,:)
         gzy(:,1,:)=zd(:,2,:)-zd(:,1,:)
      ELSE
         gxy(:,1,:)=h*(xd(:,2,:)-(xd(:,nGS,:)-(xd(:,1+2*nGS,:)-xd(:,1+nGS,:))))
         gyy(:,1,:)=h*(yd(:,2,:)-(yd(:,nGS,:)-(yd(:,1+2*nGS,:)-yd(:,1+nGS,:))))
         gzy(:,1,:)=h*(zd(:,2,:)-(zd(:,nGS,:)-(zd(:,1+2*nGS,:)-zd(:,1+nGS,:))))
      ENDIF

      IF (bf(ib,fo_north)%faceType.eq.ft_interface .or. &
          bf(ib,fo_north)%faceType.eq.ft_interface_MPI) THEN
         gxy(:,ny+2*nGS,:)=xd(:,ny+2*nGS,:)-xd(:,ny+2*nGS-1,:)
         gyy(:,ny+2*nGS,:)=yd(:,ny+2*nGS,:)-yd(:,ny+2*nGS-1,:)
         gzy(:,ny+2*nGS,:)=zd(:,ny+2*nGS,:)-zd(:,ny+2*nGS-1,:)
      ELSE
         gxy(:,ny+2*nGS,:)=h*(xd(:,ny+1+nGS,:)-(xd(:,ny,:)-xd(:,ny+nGS,:))-xd(:,ny-1+2*nGS,:))
         gyy(:,ny+2*nGS,:)=h*(yd(:,ny+1+nGS,:)-(yd(:,ny,:)-yd(:,ny+nGS,:))-yd(:,ny-1+2*nGS,:))
         gzy(:,ny+2*nGS,:)=h*(zd(:,ny+1+nGS,:)-(zd(:,ny,:)-zd(:,ny+nGS,:))-zd(:,ny-1+2*nGS,:))
      ENDIF

      IF (bf(ib,fo_back)%faceType.eq.ft_interface .or. &
          bf(ib,fo_back)%faceType.eq.ft_interface_MPI) THEN
         gxz(:,:,1)=xd(:,:,2)-xd(:,:,1)
         gyz(:,:,1)=yd(:,:,2)-yd(:,:,1)
         gzz(:,:,1)=zd(:,:,2)-zd(:,:,1)
      ELSE
         gxz(:,:,1)=h*(xd(:,:,2)-(xd(:,:,nGS)-(xd(:,:,1+2*nGS)-xd(:,:,1+nGS))))
         gyz(:,:,1)=h*(yd(:,:,2)-(yd(:,:,nGS)-(yd(:,:,1+2*nGS)-yd(:,:,1+nGS))))
         gzz(:,:,1)=h*(zd(:,:,2)-(zd(:,:,nGS)-(zd(:,:,1+2*nGS)-zd(:,:,1+nGS))))
      ENDIF

      IF (bf(ib,fo_front)%faceType.eq.ft_interface .or. &
          bf(ib,fo_front)%faceType.eq.ft_interface_MPI) THEN
         gxz(:,:,nz+2*nGS)=xd(:,:,nz+2*nGS)-xd(:,:,nz+2*nGS-1)
         gyz(:,:,nz+2*nGS)=yd(:,:,nz+2*nGS)-yd(:,:,nz+2*nGS-1)
         gzz(:,:,nz+2*nGS)=zd(:,:,nz+2*nGS)-zd(:,:,nz+2*nGS-1)
      ELSE
         gxz(:,:,nz+2*nGS)=h*(xd(:,:,nz+1+nGS)-(xd(:,:,nz)-xd(:,:,nz+nGS))-xd(:,:,nz-1+2*nGS))
         gyz(:,:,nz+2*nGS)=h*(yd(:,:,nz+1+nGS)-(yd(:,:,nz)-yd(:,:,nz+nGS))-yd(:,:,nz-1+2*nGS))
         gzz(:,:,nz+2*nGS)=h*(zd(:,:,nz+1+nGS)-(zd(:,:,nz)-zd(:,:,nz+nGS))-zd(:,:,nz-1+2*nGS))
      ENDIF





   END ASSOCIATE

END SUBROUTINE CoordDifference

