!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!      IMPLICIT SOLUTION METHOD
!      LU-SGS SOLUTION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE LUSGS
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data,                ONLY:gxx,gxy,gxz,
     &                                   gyx,gyy,gyz,
     &                                   gzx,gzy,gzz,
     &                                   gj
!      USE grid_Data,                ONLY:gxxo=>gxx,gxyo=>gxy,gxzo=>gxz,
!     +                                   gyxo=>gyx,gyyo=>gyy,gyzo=>gyz,
!     +                                   gzxo=>gzx,gzyo=>gzy,gzzo=>gzz,
!     +                                   gj
      USE conserved_Quantity
      USE thermal_Property
      USE flux_difference
      USE fundamental_Constants
      USE control_Data

      IMPLICIT NONE

      REAL(8)::PSIMAX,A,B,D
      REAL(8),DIMENSION(nx,ny,nz)::
     &         CM,AA,BB,GAMM,
     &         UM,VM,WM,HM,
     &         ROR,ROD,
     &         CM1X,CM2X,CM3X,
     &         CM1Y,CM2Y,CM3Y,
     &         CM1Z,CM2Z,CM3Z,
     &         PSIX,PSIY,PSIZ,
     &         GK,GKX,GKY,GKZ,
     &         ULU,VLU,WLU,
     &         CL
!     +         ,gxx,gxy,gxz,gyx,gyy,gyz,gzx,gzy,gzz
      REAL(8),DIMENSION(nx+ny-1,ne)::DQNEW
      INTEGER::i,j,k,is,l,IBGN,IEND,DIMEN

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!      gxx=gxxo*gj
!      gxy=gxyo*gj
!      gxz=gxzo*gj
!
!      gyx=gyxo*gj
!      gyy=gyyo*gj
!      gyz=gyzo*gj
!
!      gzx=gzxo*gj
!      gzy=gzyo*gj
!      gzz=gzzo*gj
!
!***********************************************************************
!***TRANSFORMATION OF DQ TO DIAGINAL ELEMENT
!***********************************************************************
!---PREPARATION FOR IMPLICIT---
!    >>>SOUND SPEED<<<
      CM=SQRT((gamma-1.0d0)*(h-0.5d0*(u**2+v**2+w**2)))
      AA=(GAMMA-1.0D0)*(0.5D0*(U**2+V**2+W**2)*DRO
     &                  -(U*DRU+V*DRV+W*DRW)
     &                  +DRE)
      BB=CM*(U*DRO-DRU)
!---CHARACRERISTIC VARIABLES---
      DRU=(-W*DRO+DRW)/RO
!     Isn't this code strange? DRU from W and DRW?
      DRV=( V*DRO-DRV)/RO
      DO is=1,ns
      DRFS(:,:,:,is)=(DRFS(:,:,:,is)-FS(:,:,:,is)*DRO)/RO
      ENDDO
      DRO=DRO-AA/(CM**2)

      DRW=( AA-BB )*R2INV/(RO*CM)
      DRE=( AA+BB )*R2INV/(RO*CM)

!***********************************************************************
!***IMPLICIT GRID ELEMENT
!***********************************************************************

!     SIGN(A,B): Returns the absolute value of A times the sign of B.
!                If A is non-zero, you can use the result to determine
!                whether B is negative or non-negative,
!                as the sign of the result is the same as the sign of B


!   >>>GZI ROE'S AVERAGE<<<
      DIMEN=1
      ROR=SQRT(CSHIFT(RO,1,DIMEN)/RO)
      ROD=1.0D0/(ROR+1.0D0)
      UM=(ROR*CSHIFT(U,1,DIMEN)+U)*ROD
      VM=(ROR*CSHIFT(V,1,DIMEN)+V)*ROD
      WM=(ROR*CSHIFT(W,1,DIMEN)+W)*ROD
      HM=(ROR*CSHIFT(H,1,DIMEN)+H)*ROD
      GAMM=(ROR*CSHIFT(GAMMA,1,DIMEN)+GAMMA)*ROD
!   >>>GZI IMPLICIT TERM<<<
      GKX=0.5D0*(GXX+CSHIFT(GXX,1,DIMEN))
      GKY=0.5D0*(GXY+CSHIFT(GXY,1,DIMEN))
      GKZ=0.5D0*(GXZ+CSHIFT(GXZ,1,DIMEN))
      ULU=(GKX*UM+GKY*VM+GKZ*WM)*DT
      GK =SQRT(GKX**2+GKY**2+GKZ**2)
      CL =CM*GK*DT
      GKX=GKX/GK
      GKY=GKY/GK
      GKZ=GKZ/GK
      CM1X=GKX*CL
      CM2X=-R2INV*GKY*CL
      CM3X=-R2INV*GKZ*CL
      PSIX=SIGN(ABS(ULU)+ABS(CL),GJ)

!   >>>ETA<<<
      DIMEN=2
      ROR=SQRT(CSHIFT(RO,1,DIMEN)/RO)
      ROD=1.0D0/(ROR+1.0D0)
      UM=(ROR*CSHIFT(U,1,DIMEN)+U)*ROD
      VM=(ROR*CSHIFT(V,1,DIMEN)+V)*ROD
      WM=(ROR*CSHIFT(W,1,DIMEN)+W)*ROD
      HM=(ROR*CSHIFT(H,1,DIMEN)+H)*ROD
      GAMM=(ROR*CSHIFT(GAMMA,1,DIMEN)+GAMMA)*ROD
!   >>>ETA IMPLICIT TERM<<<
      GKX=0.5D0*(GYX+CSHIFT(GYX,1,DIMEN))
      GKY=0.5D0*(GYY+CSHIFT(GYY,1,DIMEN))
      GKZ=0.5D0*(GYZ+CSHIFT(GYZ,1,DIMEN))
      VLU=(GKX*UM+GKY*VM+GKZ*WM)*DT
      GK =SQRT(GKX**2+GKY**2+GKZ**2)
      CL =CM*GK*DT
      GKX=GKX/GK
      GKY=GKY/GK
      GKZ=GKZ/GK
      CM1Y=GKX*CL
      CM2Y=-R2INV*GKY*CL
      CM3Y=-R2INV*GKZ*CL
      PSIY=SIGN(ABS(VLU)+ABS(CL),GJ)

!   >>>ZETA<<<
      DIMEN=3
      ROR=SQRT(CSHIFT(RO,1,DIMEN)/RO)
      ROD=1.0D0/(ROR+1.0D0)
      UM=(ROR*CSHIFT(U,1,DIMEN)+U)*ROD
      VM=(ROR*CSHIFT(V,1,DIMEN)+V)*ROD
      WM=(ROR*CSHIFT(W,1,DIMEN)+W)*ROD
      HM=(ROR*CSHIFT(H,1,DIMEN)+H)*ROD
      GAMM=(ROR*CSHIFT(GAMMA,1,DIMEN)+GAMMA)*ROD
!   >>>ZETA IMPLICIT TERM<<<
      GKX=0.5D0*(GZX+CSHIFT(GZX,1,DIMEN))
      GKY=0.5D0*(GZY+CSHIFT(GZY,1,DIMEN))
      GKZ=0.5D0*(GZZ+CSHIFT(GZZ,1,DIMEN))
      WLU=(GKX*UM+GKY*VM+GKZ*WM)*DT
      GK =SQRT(GKX**2+GKY**2+GKZ**2)
      CL =CM*GK*DT
      GKX=GKX/GK
      GKY=GKY/GK
      GKZ=GKZ/GK
      CM1Z=GKX*CL
      CM2Z=-R2INV*GKY*CL
      CM3Z=-R2INV*GKZ*CL
      PSIZ=SIGN(ABS(WLU)+ABS(CL),GJ)

!***********************************************************************
!***LOWER STEP
!***********************************************************************

      DO K=2,NZ-1
      DO L=4,NX+NY-2
      IBGN=MAX(L-NY+1,2)
      IEND=MIN(L-2,NX-1)
      DO I=IBGN,IEND
       DQNEW(I,1)=DRO(I,L-I,K)
     &  +(PSIX(I-1,L-I,K) +ULU(I-1,L-I,K))*DRO(I-1,L-I,K)
     &  +(PSIY(I,L-I-1,K) +VLU(I,L-I-1,K))*DRO(I,L-I-1,K)
     &  +(PSIZ(I,L-I,K-1) +WLU(I,L-I,K-1))*DRO(I,L-I,K-1)
       DQNEW(I,2)=DRU(I,L-I,K)
     &  +(PSIX(I-1,L-I,K)+ ULU(I-1,L-I,K))*DRU(I-1,L-I,K)
     &                   -CM3X(I-1,L-I,K) *DRW(I-1,L-I,K)
     &                   -CM3X(I-1,L-I,K) *DRE(I-1,L-I,K)
     &  +(PSIY(I,L-I-1,K) +VLU(I,L-I-1,K))*DRU(I,L-I-1,K)
     &                   -CM3Y(I,L-I-1,K) *DRW(I,L-I-1,K)
     &                   -CM3Y(I,L-I-1,K) *DRE(I,L-I-1,K)
     &  +(PSIZ(I,L-I,K-1)+WLU(I,L-I,K-1))*DRU(I,L-I,K-1)
     &                   -CM3Z(I,L-I,K-1)*DRW(I,L-I,K-1)
     &                   -CM3Z(I,L-I,K-1)*DRE(I,L-I,K-1)
       DQNEW(I,3)=DRV(I,L-I,K)
     &  +(PSIX(I-1,L-I,K)+ULU(I-1,L-I,K))*DRV(I-1,L-I,K)
     &                   +CM2X(I-1,L-I,K)*DRW(I-1,L-I,K)
     &                   +CM2X(I-1,L-I,K)*DRE(I-1,L-I,K)
     &  +(PSIY(I,L-I-1,K)+VLU(I,L-I-1,K))*DRV(I,L-I-1,K)
     &                   +CM2Y(I,L-I-1,K)*DRW(I,L-I-1,K)
     &                   +CM2Y(I,L-I-1,K)*DRE(I,L-I-1,K)
     &  +(PSIZ(I,L-I,K-1)+WLU(I,L-I,K-1))*DRV(I,L-I,K-1)
     &                   +CM2Z(I,L-I,K-1)*DRW(I,L-I,K-1)
     &                   +CM2Z(I,L-I,K-1)*DRE(I,L-I,K-1)
       DQNEW(I,4)=DRW(I,L-I,K)
     & +(PSIX(I-1,L-I,K)+ULU(I-1,L-I,K)+CM1X(I-1,L-I,K))*DRW(I-1,L-I,K)
     &                                 -CM3X(I-1,L-I,K) *DRU(I-1,L-I,K)
     &                                 +CM2X(I-1,L-I,K) *DRV(I-1,L-I,K)
     & +(PSIY(I,L-I-1,K)+VLU(I,L-I-1,K)+CM1Y(I,L-I-1,K))*DRW(I,L-I-1,K)
     &                                 -CM3Y(I,L-I-1,K) *DRU(I,L-I-1,K)
     &                                 +CM2Y(I,L-I-1,K) *DRV(I,L-I-1,K)
     & +(PSIZ(I,L-I,K-1)+WLU(I,L-I,K-1)+CM1Z(I,L-I,K-1))*DRW(I,L-I,K-1)
     &                                 -CM3Z(I,L-I,K-1) *DRU(I,L-I,K-1)
     &                                 +CM2Z(I,L-I,K-1) *DRV(I,L-I,K-1)
       DQNEW(I,5)=DRE(I,L-I,K)
     & +(PSIX(I-1,L-I,K)+ULU(I-1,L-I,K)-CM1X(I-1,L-I,K))*DRE(I-1,L-I,K)
     &                                 -CM3X(I-1,L-I,K) *DRU(I-1,L-I,K)
     &                                 +CM2X(I-1,L-I,K) *DRV(I-1,L-I,K)
     & +(PSIY(I,L-I-1,K)+VLU(I,L-I-1,K)-CM1Y(I,L-I-1,K))*DRE(I,L-I-1,K)
     &                                 -CM3Y(I,L-I-1,K) *DRU(I,L-I-1,K)
     &                                 +CM2Y(I,L-I-1,K) *DRV(I,L-I-1,K)
     & +(PSIZ(I,L-I,K-1)+WLU(I,L-I,K-1)-CM1Z(I,L-I,K-1))*DRE(I,L-I,K-1)
     &                                 -CM3Z(I,L-I,K-1) *DRU(I,L-I,K-1)
     &                                 +CM2Z(I,L-I,K-1) *DRV(I,L-I,K-1)
       DQNEW(I,6:)=DRFS(I,L-I,K,:)
     &   +(PSIX(I-1,L-I,K)+ULU(I-1,L-I,K))*DRFS(I-1,L-I,K,:)
     &   +(PSIY(I,L-I-1,K)+VLU(I,L-I-1,K))*DRFS(I,L-I-1,K,:)
     &   +(PSIZ(I,L-I,K-1)+WLU(I,L-I,K-1))*DRFS(I,L-I,K-1,:)
!
       PSIMAX=GJ(I,L-I,K)
     &       +PSIX(I,L-I,K)+PSIY(I,L-I,K)+PSIZ(I,L-I,K)
     &       +PSIX(I-1,L-I,K)+PSIY(I,L-I-1,K)+PSIZ(I,L-I,K-1)

       DQNEW(I,:)=DQNEW(I,:)/PSIMAX
      ENDDO

          DO I=IBGN,IEND
       DRO(I,L-I,K)   =DQNEW(I,1)
       DRU(I,L-I,K)   =DQNEW(I,2)
       DRV(I,L-I,K)   =DQNEW(I,3)
       DRW(I,L-I,K)   =DQNEW(I,4)
       DRE(I,L-I,K)   =DQNEW(I,5)
       DRFS(I,L-I,K,:)=DQNEW(I,6:)
          ENDDO
        ENDDO
      ENDDO

!***********************************************************************
!*** DIAGOANL MATRIX
!***********************************************************************

      DO k=2,nz-1
        DO J=2,ny-1
          DO I=2,nx-1
       PSIMAX=GJ(i,j,k)
     &       +PSIX(i,j,k)+PSIY(i,j,k)+PSIZ(i,j,k)
     &       +PSIX(i-1,j,k)+PSIY(i,j-1,k)+PSIZ(i,j,k-1)

       DRO(i,j,k)=DRO(i,j,k)*PSIMAX
       DRU(i,j,k)=DRU(i,j,k)*PSIMAX
       DRV(i,j,k)=DRV(i,j,k)*PSIMAX
       DRW(i,j,k)=DRW(i,j,k)*PSIMAX
       DRE(i,j,k)=DRE(i,j,k)*PSIMAX
       DRFS(i,j,k,:)=DRFS(i,j,k,:)*PSIMAX
          ENDDO
        ENDDO
      ENDDO
!***********************************************************************
!***UPPER STEP
!***********************************************************************

      DO K=NZ-1,2,-1
      DO L=NX+NY-2,4,-1
      IBGN=MAX(L-NY+1,2)
      IEND=MIN(L-2,NX-1)
      DO I=IBGN,IEND
       DQNEW(I,1)=DRO(I,L-I,K)
     &  +(PSIX(I,L-I,K)-ULU(I,L-I,K))*DRO(I+1,L-I,K)
     &  +(PSIY(I,L-I,K)-VLU(I,L-I,K))*DRO(I,L-I+1,K)
     &  +(PSIZ(I,L-I,K)-WLU(I,L-I,K))*DRO(I,L-I,K+1)
       DQNEW(I,2)=DRU(I,L-I,K)
     &  +(PSIX(I,L-I,K)-ULU(I,L-I,K))*DRU(I+1,L-I,K)
     &                 +CM3X(I,L-I,K)*DRW(I+1,L-I,K)
     &                 +CM3X(I,L-I,K)*DRE(I+1,L-I,K)
     &  +(PSIY(I,L-I,K)-VLU(I,L-I,K))*DRU(I,L-I+1,K)
     &                 +CM3Y(I,L-I,K)*DRW(I,L-I+1,K)
     &                 +CM3Y(I,L-I,K)*DRE(I,L-I+1,K)
     &  +(PSIZ(I,L-I,K)-WLU(I,L-I,K))*DRU(I,L-I,K+1)
     &                 +CM3Z(I,L-I,K)*DRW(I,L-I,K+1)
     &                 +CM3Z(I,L-I,K)*DRE(I,L-I,K+1)
       DQNEW(I,3)=DRV(I,L-I,K)
     &  +(PSIX(I,L-I,K)-ULU(I,L-I,K))*DRV(I+1,L-I,K)
     &                 -CM2X(I,L-I,K)*DRW(I+1,L-I,K)
     &                 -CM2X(I,L-I,K)*DRE(I+1,L-I,K)
     &  +(PSIY(I,L-I,K)-VLU(I,L-I,K))*DRV(I,L-I+1,K)
     &                 -CM2Y(I,L-I,K)*DRW(I,L-I+1,K)
     &                 -CM2Y(I,L-I,K)*DRE(I,L-I+1,K)
     &  +(PSIZ(I,L-I,K)-WLU(I,L-I,K))*DRV(I,L-I,K+1)
     &                 -CM2Z(I,L-I,K)*DRW(I,L-I,K+1)
     &                 -CM2Z(I,L-I,K)*DRE(I,L-I,K+1)
       DQNEW(I,4)=DRW(I,L-I,K)
     & +(PSIX(I,L-I,K)-ULU(I,L-I,K)-CM1X(I,L-I,K))*DRW(I+1,L-I,K)
     &                             +CM3X(I,L-I,K) *DRU(I+1,L-I,K)
     &                             -CM2X(I,L-I,K) *DRV(I+1,L-I,K)
     & +(PSIY(I,L-I,K)-VLU(I,L-I,K)-CM1Y(I,L-I,K))*DRW(I,L-I+1,K)
     &                             +CM3Y(I,L-I,K) *DRU(I,L-I+1,K)
     &                             -CM2Y(I,L-I,K) *DRV(I,L-I+1,K)
     & +(PSIZ(I,L-I,K)-WLU(I,L-I,K)-CM1Z(I,L-I,K))*DRW(I,L-I,K+1)
     &                             +CM3Z(I,L-I,K) *DRU(I,L-I,K+1)
     &                             -CM2Z(I,L-I,K) *DRV(I,L-I,K+1)
       DQNEW(I,5)=DRE(I,L-I,K)
     & +(PSIX(I,L-I,K)-ULU(I,L-I,K)+CM1X(I,L-I,K))*DRE(I+1,L-I,K)
     &                             +CM3X(I,L-I,K) *DRU(I+1,L-I,K)
     &                             -CM2X(I,L-I,K) *DRV(I+1,L-I,K)
     & +(PSIY(I,L-I,K)-VLU(I,L-I,K)+CM1Y(I,L-I,K))*DRE(I,L-I+1,K)
     &                             +CM3Y(I,L-I,K) *DRU(I,L-I+1,K)
     &                             -CM2Y(I,L-I,K) *DRV(I,L-I+1,K)
     & +(PSIZ(I,L-I,K)-WLU(I,L-I,K)+CM1Z(I,L-I,K))*DRE(I,L-I,K+1)
     &                             +CM3Z(I,L-I,K) *DRU(I,L-I,K+1)
     &                             -CM2Z(I,L-I,K) *DRV(I,L-I,K+1)
       DQNEW(I,6:)=DRFS(I,L-I,K,:)
     &   +(PSIX(I,L-I,K)-ULU(I,L-I,K))*DRFS(I,L-I+1,K,:)
     &   +(PSIY(I,L-I,K)-VLU(I,L-I,K))*DRFS(I,L-I+1,K,:)
     &   +(PSIZ(I,L-I,K)-WLU(I,L-I,K))*DRFS(I,L-I,K+1,:)

       PSIMAX=GJ(I,L-I,K)
     &       +PSIX(I,L-I,K)+PSIY(I,L-I,K)+PSIZ(I,L-I,K)
     &       +PSIX(I-1,L-I,K)+PSIY(I,L-I-1,K)+PSIZ(I,L-I,K-1)

       DQNEW(I,:)=DQNEW(I,:)/PSIMAX
      ENDDO
      DO I=IBGN,IEND
       DRO(I,L-I,K)   =DQNEW(I,1)
       DRU(I,L-I,K)   =DQNEW(I,2)
       DRV(I,L-I,K)   =DQNEW(I,3)
       DRW(I,L-I,K)   =DQNEW(I,4)
       DRE(I,L-I,K)   =DQNEW(I,5)
       DRFS(I,L-I,K,:)=DQNEW(I,6:)
      ENDDO

        ENDDO
      ENDDO
!***********************************************************************
!***TRANSFORMATION OF DIAGINAL ELEMENT TO DQ
!***********************************************************************
      DO k=2,nz-1
        DO j=2,ny-1
          DO i=2,nx-1
!    >>>PRESSURE DERIVATIVES<<<
       A=R2INV*RO(i,j,k)/CM(i,j,k)*(DRW(i,j,k)+DRE(i,j,k))
       B=R2INV*RO(i,j,k)      *(DRW(i,j,k)-DRE(i,j,k))
       D=DRO(i,j,k)+A
!
!   >>>PHISYCAL VARIABLES<<<
       DRO(i,j,k)=D
       DRU(i,j,k)=D*U(i,j,k)+B
       DRV(i,j,k)=D*V(i,j,k)-RO(i,j,k)*DRV(i,j,k)
       DRW(i,j,k)=D*W(i,j,k)+RO(i,j,k)*DRU(i,j,k)
       DRE(i,j,k)=U(i,j,k)*DRU(i,j,k)+V(i,j,k)*DRV(i,j,k)
     &           +W(i,j,k)*DRW(i,j,k)
     &    -0.5D0*(U(i,j,k)**2+V(i,j,k)**2+W(i,j,k)**2)*DRO(i,j,k)
     &    +(H(i,j,k)-0.5D0*(U(i,j,k)**2+V(i,j,k)**2+W(i,j,k)**2))*A
       DRFS(i,j,k,:)=D*FS(i,j,k,:)+RO(i,j,k)*DRFS(i,j,k,:)
          ENDDO
        ENDDO
      ENDDO

      END SUBROUTINE
