!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             ADD OF RESIDUAL(NEW TIME STEP VALUABLES)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ADDRHS
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns,dt,it,ib
      USE conserved_Quantity,       ONLY:ro,ru,rv,rw,re,rfs,re_
      USE flux_difference,          ONLY:dro,dru,drv,drw,dre,drfs
      USE Status_Check,              ONLY:wrongConcentr,voidNode,
     &                                   rocons,recons,rfscons
      USE AuxSubprogram,            ONLY:volinteg,fluxinteg

      IMPLICIT NONE

      INTEGER::is,i,j,k
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      wrongConcentr=.false.

      IF (.not.ALLOCATED(rfscons)) THEN
            ALLOCATE(rfscons(ns))
      ENDIF

      ro=ro+DRO
      ru=ru+DRU
      rv=rv+DRV
      rw=rw+DRW
      re=re+DRE
      rfs=rfs+DRFS

!     detect negative concentration
      IF (MINVAL(rfs).lt.0.0d0) THEN
            wrongConcentr=.true.
!            print *,minloc(rfs)
      ENDIF

      WHERE(rfs.lt.0.0d0)
            rfs=0.0d0
      ENDWHERE

      rocons=volinteg(dro)+fluxinteg(ro,dt)
      recons=volinteg(ro)

      DO is=1,ns
            rfscons(is)=volinteg(rfs(:,:,:,is))/volinteg(ro)
      ENDDO
!      rucons=volsum(dru,dt)+surfsum(ru)+presssum()
!      rvcons=volsum(drv,dt)+surfsum(rv)+presssum()
!      rwcons=volsum(drw,dt)+surfsum(rw)+presssum()

!      recons=volsum(dre,dt)+surfsum(re)+heatcond()+reac()+source()


!   >>>SPECIES CORRECTION<<<
!           Make overall concentration which is deviated from unity
!     because of removal of negative concentration.
!           HOWEVER, the deviation of concentration is not only
!     produced by eliminating negative values.
!     Originally the sum of concentration is not equal to
!     unity from convection term manipulation
!     and chemical reaction calculation.
!           For diffusion, sum of species(i.e. mass) is conserved now.

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx

       IF(MAXVAL(rfs(i,j,k,:)).EQ.0.0D0) THEN
         RFS(i,j,k,1)=0.778D0*RO(i,j,k)
         RFS(i,j,k,3)=0.222D0*RO(i,j,k)
         voidNode=.true.
       ELSE
         RFS(i,j,k,:)=RO(i,j,k)*RFS(i,j,k,:)/SUM(rfs(i,j,k,:))
       END IF

          ENDDO
        ENDDO
      ENDDO

      END SUBROUTINE

