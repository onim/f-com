!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             INITIAL DATA GENERATION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE IniGen_Generation
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:ib,nb,ns,itrBgn,itrEnd,maxItr
      USE conserved_Quantity,       ONLY:ro_,ru_,rv_,rw_,re_,rfs_,t_,
     &                                   initQuant
      USE AuxSubprogram,            ONLY:arraypoint
      USE Message_Passing_Interface

      IMPLICIT NONE

      INTEGER::is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         CALL arraypoint
         SELECT CASE (ib)
         CASE(9,10,19,20,29,30)
            ro_=initQuant%ro_(1,1,1)
            ru_=0.0d0
            rv_=0.0d0
            rw_=0.0d0
            re_=initQuant%re_(1,1,1)
     &           -0.5d0*(initQuant%ru_(1,1,1)**2
     &                  +initQuant%rv_(1,1,1)**2
     &                  +initQuant%rw_(1,1,1)**2)/initQuant%ro_(1,1,1)
            t_ =initQuant%t_(1,1,1)
               DO is=1,ns
            rfs_(:,:,:,is)=initQuant%rfs_(1,1,1,is)
               ENDDO

         CASE DEFAULT
            ro_=initQuant%ro_(1,1,1)
            ru_=initQuant%ru_(1,1,1)
            rv_=initQuant%rv_(1,1,1)
            rw_=initQuant%rw_(1,1,1)
            re_=initQuant%re_(1,1,1)
            t_ =initQuant%t_(1,1,1)
               DO is=1,ns
            rfs_(:,:,:,is)=initQuant%rfs_(1,1,1,is)
               ENDDO
         END SELECT
      ENDIF;ENDDO multiBlock

      itrBgn=1
      itrEnd=maxItr

      END SUBROUTINE
