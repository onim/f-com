!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE configload
!     Configuration file Reading
!     Default configuration file is 'c1.dat'
!     You can adjust many configurations by editing 'c1.dat'
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:ns,nr,nt,                      &
                                      chGrid,chCond,chInit,chConv,   &
                                      chStat,                        &
                                      CFLLim,CFLMax,iniCnd,          &
                                      avoid_Radical,num_nonRadical,  &
                                      nonRadical,sort_of_BC,         &
                                      reactionModel,                 &
                                      enable_Diffusion,              &
                                      spatialDifference
   USE thermal_Property,         ONLY:tlimit
   USE readCommentedFile,        ONLY:readc
   USE TurbulenceModeling
   USE Message_Passing_Interface

   IMPLICIT NONE
   CHARACTER(512)::buf
   INTEGER::i
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   tlimit=6000.0d0

   IF(IamPrimeNode()) PRINT *,'Calculation started.'
   OPEN(50,FILE='c1.dat',FORM='FORMATTED')

   CALL readc(50,i)
   IF(i.ne.ns) THEN
      PRINT *,"Number of species is not consistent. Check your program."
      STOP
   ENDIF
   CALL readc(50,nr)
   CALL readc(50,chGrid)
   CALL readc(50,chCond)
   CALL readc(50,CFLLim)
   CALL readc(50,iniCnd)
   CALL readc(50,chInit)

   CALL readc(50,avoid_Radical)
   CALL readc(50,num_nonRadical)
   ALLOCATE(nonRadical(num_nonRadical))
   CALL readc(50,nonRadical)

   CALL readc(50,enable_Diffusion)
   CALL readc(50,reactionModel)
   CALL readc(50,sort_of_BC)
   CALL readc(50,spatialDifference)

   CFLMax=CFLLim

!     Turbulence Modeling
   CALL readc(50,buf)

   IF(TRIM(buf).eq.'ImplicitLES') TurbModel=isLaminar
   IF(TRIM(buf).eq.'LES') THEN
      TurbModel=isLES
      CALL readc(50,buf)
      IF(TRIM(buf).eq.'Smagorinsky') LESModel=isSmagorinsky
      IF(TRIM(buf).eq.'CompressibleDynamic') LESModel=isCompressibleDynamic
   ENDIF
   CALL assignTurbModel

   ALLOCATE(MPIprofile(0:MPISize-1))

   CALL readc(50,MPIprofile(:)%nb)
   DO i=0,MPISize-1
      ALLOCATE(MPIprofile(i)%ib(MPIprofile(i)%nb))
      CALL readc(50,MPIprofile(i)%ib)
   ENDDO
   CLOSE(50)

!     read variable profile
   CALL parameterload

!     renew chStat/chConv file
   OPEN(97,FILE=chConv,STATUS='REPLACE')
   CLOSE(97)
   OPEN(98,FILE=chStat,STATUS='REPLACE')
   CLOSE(98)


END SUBROUTINE configload

