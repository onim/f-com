!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE GRDGEN
! GRID METRIC CALCULATION
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ns,nt,nb,nn, &
                                      nx_gl,ny_gl,nz_gl, &
                                      chGrid, ib
   USE boundary_Data_OOP,        ONLY:bf
   USE readCommentedFile
   USE Message_Passing_Interface
   USE fundamental_Constants

IMPLICIT NONE
   INTEGER::i,j,k,facet
   REAL(8)::volume, overallVolume
   REAL(8),DIMENSION(:,:,:),ALLOCATABLE::normLength

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!***********************************************************************
!*****Position data read from a grid file
!***********************************************************************
   open(55,file=chGrid,form='unformatted',POSITION='rewind')

   !     # of blocks
   READ(55) nb
   READ(55) nx_gl
   READ(55) ny_gl
   READ(55) nz_gl

   nn=SUM(nx_gl*ny_gl*nz_gl)

   _forAllBlocks_
      globalGeometry: ASSOCIATE( &
         xd=>bl(ib)%p(:,:,:)%g%xd, &
         yd=>bl(ib)%p(:,:,:)%g%yd, &
         zd=>bl(ib)%p(:,:,:)%g%zd)

         ! # of nodes along each axis
         READ(55) nx
         READ(55) ny
         READ(55) nz

         ! Type of block boundary face(6)
         READ(55) bf(ib,:)%faceType

         ! Parameters according to the type
         DO facet=1,6
            blockFace: ASSOCIATE(b => bf(ib,facet))
            ! ignore MPI interface
             IF(b%faceType.eq.ft_interface_MPI) b%faceType=ft_interface
            ! ignore slip wall
            IF(b%faceType.eq.ft_symmetric) b%faceType=ft_wall

            SELECT CASE (b%faceType)
               CASE(ft_interface)
               READ(55) b%connBlock,&
                        b%connFace

               CASE(ft_interface_MPI)
               READ(55) b%connBlock,&
                        b%connFace
               b%myNode=blockOwner(ib)
               b%connNode=blockOwner(b%connBlock)

               CASE(ft_inlet)
               b%nFBC=1

            ENDSELECT
            CALL b%setBoundaryExtent(nx,ny,nz,nGS,facet)

            END ASSOCIATE blockFace
         ENDDO

         _forInnerPoints_
            READ(55) xd(i,j,k),&
                     yd(i,j,k),&
                     zd(i,j,k)
         _endInnerPoints_

      END ASSOCIATE globalGeometry
   _endAllBlocks_

   CLOSE(55)

   IF(IamPrimeNode()) PRINT *, 'Position data load successful.'

   ! Position data expansion
   _forAllBlocks_
      CALL ExpandPointSimple
   _endAllBlocks_

   _forAllBlocks_
      CALL ExpandPointCopy
   _endAllBlocks_

   ! Coord. difference along each axis
   _forAllBlocks_
      CALL CoordDifference
   _endAllBlocks_

   ! Reciprocal Jacobian
   CALL Metric

   volume=0.0d0
   overallVolume=0.0d0

   _forMyBlocks_
      volume= volume &
             +SUM(bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%gd%gj)
   _endMyBlocks_

   CALL MPI_REDUCE(volume, overallVolume, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

   IF(IamPrimeNode()) WRITE(*,'(A,E14.7)')'  The domain volume is ',overallVolume

!   print *,bl(5)%p(90,3,3)%gd%gxxf,bl(5)%p(90,2,2)%gd%gxxf,bl(5)%p(90,1,1)%gd%gxxf
!   print *,bl(5)%p(89,4,4)%gd%gxxf,bl(5)%p(91,3,3)%gd%gxxf,bl(5)%p(92,3,3)%gd%gxxf

END SUBROUTINE grdgen
