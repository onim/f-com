      MODULE readcommentedfile

      IMPLICIT NONE

      INTERFACE readc
            MODULE PROCEDURE readcc
            MODULE PROCEDURE readcl
            MODULE PROCEDURE readcr
            MODULE PROCEDURE readci
            MODULE PROCEDURE readcra
            MODULE PROCEDURE readcia
      END INTERFACE

      CONTAINS


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Character
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readcc (UnitNum, cha, form)

      IMPLICIT NONE

      INTEGER, INTENT (in) :: UnitNum
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      CHARACTER(LEN=*), INTENT(out) :: cha

      CHARACTER (LEN=300) :: line

      ReadComments: do
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      ENDDO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) cha
      ELSE
            READ (UnitNum, *) cha
      ENDIF

      END SUBROUTINE readcc


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Logical
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readcl (UnitNum, lgc, form)

      IMPLICIT NONE

      INTEGER, INTENT (in) :: UnitNum
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      LOGICAL, INTENT(out) :: lgc

      CHARACTER (LEN=300) :: line

      ReadComments: do
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      ENDDO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) lgc
      ELSE
            READ (UnitNum, *) lgc
      ENDIF

      END SUBROUTINE readcl



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Real(Double)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readcr (UnitNum, r, form)
      IMPLICIT NONE

      INTEGER, INTENT (in) :: UnitNum
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form
      REAL(8), INTENT (out) :: r

      CHARACTER(LEN=300) :: line

      ReadComments: DO
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      ENDDO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) r
      ELSE
            READ (UnitNum, *) r
      ENDIF

      END SUBROUTINE readcr




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Integer
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readci (UnitNum, integ, form)
      IMPLICIT NONE

      INTEGER, INTENT (in) :: UnitNum
      INTEGER, INTENT (out) :: integ
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form

      CHARACTER(LEN=300) :: line

      ReadComments: DO
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      ENDDO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) integ
      ELSE
            READ (UnitNum, *) integ
      ENDIF

      END SUBROUTINE readci




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Real(double) array
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readcra (UnitNum, Array, form)
      IMPLICIT NONE

      INTEGER, INTENT (IN) :: UnitNum
      REAL(8), DIMENSION(:), INTENT (OUT) :: Array
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form

      CHARACTER (LEN=300) :: line

      ReadComments: DO
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      END DO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) array
      ELSE
            READ (UnitNum, *) array
      ENDIF

      END SUBROUTINE readcra

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Integer array
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE readcia (UnitNum, Array, form)
      IMPLICIT NONE

      INTEGER, INTENT (IN) :: UnitNum
      INTEGER, DIMENSION(:), INTENT (OUT) :: Array
      CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: form

      CHARACTER (LEN=300) :: line

      ReadComments: DO
         READ (UnitNum, '(A)') line
         IF (line (1:1).ne.cf()) EXIT ReadComments
      END DO ReadComments

      BACKSPACE (UnitNum)

      IF(present(form)) THEN
            READ (UnitNum, form) array
      ELSE
            READ (UnitNum, *) array
      ENDIF

      END SUBROUTINE readcia


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Comment character
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      CHARACTER FUNCTION cf()
      IMPLICIT NONE
      cf='!'
      RETURN
      ENDFUNCTION cf




      END MODULE readcommentedfile

