!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** boundary data object oriented programming type

      MODULE Boundary_Data_OOP
      USE fundamental_Constants,    ONLY:ijkPosition
      USE conserved_Quantity,       ONLY:conservedQuantity

      IMPLICIT NONE
      SAVE
!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type declaration
!$$$$$$$$$$$$$$$$$$$$$
!$$$$$
      TYPE forcedBoundaryCondition
!           Set a specific part of boundary value as a fixed one
!           Can be used for inlet / partial injection
!$$$$$
         TYPE(conservedQuantity)::
     &      quant
         INTEGER::
     &      nnode
         TYPE(ijkPosition),DIMENSION(:),ALLOCATABLE::
     &      nodelist
         LOGICAL::
     &      uniform
      CONTAINS
         PROCEDURE::calcNodeList
      ENDTYPE

!$$$$$
      TYPE blockFace
!$$$$$
!     interfaceBlockFace     -|
!         inletBlockFace      |
!        outletBlockFace      | => blockFace
!          wallBlockFace      |
!      periodicBlockFace     -|
         INTEGER::
     &      faceType=0,
     &      myNode=0,
     &      connNode=0,
     &      connBlock=0,
     &      connFace=0,
     &      nFBC=0,
     &      inletType=0,
     &      outletType=0
         LOGICAL::
     &      isSlipwall=.false.
         REAL(8)::
     &      wallTemperature=0
         REAL(8),DIMENSION(:,:,:),ALLOCATABLE::
     &      normx, normy, normz
         TYPE(forcedBoundaryCondition),DIMENSION(:),ALLOCATABLE::
     &      fBC
      CONTAINS
      ENDTYPE

!     faceType numbering
!     1 for connected (interface, periodic)
!     2 for interface_MPI
!     3 for inlet
!     4 for outlet
!     5 for wall

!     dimension of blockFace
!     1 for west  -- x =  1
!     2 for east  -- x = nx
!     3 for south -- y =  1
!     4 for north -- y = ny
!     5 for back  -- z =  1
!     6 for front -- z = nz

      TYPE(blockFace),ALLOCATABLE::bf(:,:)

!$$$$$$$$$$$$$$$
      CONTAINS
!     Class subprograms
!           1. Calculate node list from vertex
!$$$$$$$$$$$$$$$
      SUBROUTINE calcNodeList(this,pBC)
      USE control_Data,       ONLY:partialBoundaryCondition

      IMPLICIT NONE

      CLASS(forcedBoundaryCondition)::this
      TYPE(partialBoundaryCondition)::pBC

      INTEGER::i,j,k,inode

      inode=0

      DO k=pBC%pxyz1(3),pBC%pxyz2(3)
      DO j=pBC%pxyz1(2),pBC%pxyz2(2)
      DO i=pBC%pxyz1(1),pBC%pxyz2(1);
         inode=inode+1
         this%nodelist(inode)%i=i
         this%nodelist(inode)%j=j
         this%nodelist(inode)%k=k
      ENDDO;ENDDO;ENDDO

      END SUBROUTINE

      END MODULE





