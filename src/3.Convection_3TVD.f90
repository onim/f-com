SUBROUTINE TVD(this)

IMPLICIT NONE
   CLASS(T_AUSMplusUp_MUSCL)::this

   INTEGER,PARAMETER::nDirection=3,  nDimen=3
   INTEGER,PARAMETER::dnwind=1, cnwind=2, upwind=3

   REAL(8),DIMENSION(nDirection,ne):: primitive
   REAL(8),DIMENSION(ne):: sr, psi, processed, phi ! slope ratio, limiter
   REAL(8),DIMENSION(nDimen):: p
   REAL(8)::c

   !***********************************************************************
   !     For left flux
   !***********************************************************************
#define CNWIND i,j,k
#define UPWIND i-ish,j-jsh,k-ksh
#define DNWIND i+ish,j+jsh,k+ksh

!$OMP PARALLEL PRIVATE(i,j,k,sr,psi,primitive,processed,phi,p,c)
!$OMP DO SCHEDULE(GUIDED)
   DO k=1-ksh+nGS,nz+nGS; DO j=1-jsh+nGS,ny+nGS; DO i=1-ish+nGS,nx+nGS
      primitive(cnwind,:)=this%primitive(CNWIND,:)
      primitive(upwind,:)=this%primitive(UPWIND,:)
      primitive(dnwind,:)=this%primitive(DNWIND,:)

      !***********************************************************************
      !     Obtain gradient ratio r
      !
      !     Definition of r:
      !           r_f= (phi_center - phi_upstream)
      !               /(phi_dnstream - phi_center)
      !***********************************************************************
      sr = (primitive(cnwind,:)-primitive(upwind,:)) &
          /(primitive(dnwind,:)-primitive(cnwind,:))

      !***********************************************************************
      !     Obtain flux limiter function
      !***********************************************************************
      psi = MUSCL(sr)

      !***********************************************************************
      !     Apply flux limiter and obtain final vector
      !***********************************************************************
      processed = primitive(cnwind,:) &
                 +0.5d0*psi*(primitive(dnwind,:)-primitive(cnwind,:))

      CALL reconstruct(processed,phi,c,p)

      this%processed(CNWIND,:,left) = processed
      this%phi(CNWIND,:,left)       = phi
      this%c(CNWIND,left)           = c
      this%p(CNWIND,:,left)         = p

   ENDDO;ENDDO;ENDDO
!$OMP END DO NOWAIT
#undef CNWIND
#undef UPWIND
#undef DNWIND

   !***********************************************************************
   !     For right flux
   !***********************************************************************
#define UPWIND i+ish,j+jsh,k+ksh
#define CNWIND i,j,k
#define DNWIND i-ish,j-jsh,k-ksh
!$OMP DO SCHEDULE(GUIDED)
   DO k=1+nGS,nz+ksh+nGS; DO j=1+nGS,ny+jsh+nGS; DO i=1+nGS,nx+ish+nGS
      primitive(cnwind,:)=this%primitive(CNWIND,:)
      primitive(upwind,:)=this%primitive(UPWIND,:)
      primitive(dnwind,:)=this%primitive(DNWIND,:)

      sr = (primitive(cnwind,:)-primitive(upwind,:)) &
          /(primitive(dnwind,:)-primitive(cnwind,:))
      psi = MUSCL(sr)
      processed = primitive(cnwind,:) &
                  +0.5d0*psi*(primitive(dnwind,:)-primitive(cnwind,:))

      CALL reconstruct(processed,phi,c,p)

      this%processed(DNWIND,:,right) = processed
      this%phi(DNWIND,:,right)       = phi
      this%c(DNWIND,right)           = c
      this%p(DNWIND,:,right)         = p

   ENDDO;ENDDO;ENDDO
!$OMP END DO
!$OMP END PARALLEL

#undef UPWIND
#undef CNWIND
#undef DNWIND

END SUBROUTINE

