!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE Reaction_Data
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** variables for reaction data

!     cf          frequency factor: mole-cm-sec-K
!     zetaf       temperature exponent
!     ef          activation energy: cal/mole or J/mol
!                    BE CAREFUL ON THE UNITS OF THESE PARAMETERS!!!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

   USE Fundamental_Constants, ONLY:ns,Db

IMPLICIT NONE
   REAL(Db),ALLOCATABLE,DIMENSION(:):: &
      cf ,zetaf ,ef , &
      cf0,zetaf0,ef0, &
      acent,ts1,ts2,ts3

END MODULE

