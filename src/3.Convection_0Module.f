!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     convection module
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      MODULE convectModule
      USE control_Data,             ONLY:nx,ny,nz,ns,ne
      USE conserved_Quantity,       ONLY:ro_, ru_, rv_, rw_, re_,rfs_,
     &                                         u_,  v_,  w_,  h_,  p_, t_
      USE Status_Check,             ONLY:AUSMFlag
      USE fundamental_Constants,    ONLY:nGS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Derived types and variables
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      IMPLICIT NONE

!     <<Abstract Class
      TYPE,ABSTRACT:: convectionClass
!           flux variable
            REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::
     &            flux
      CONTAINS
            PROCEDURE(template),DEFERRED::alloc
            PROCEDURE(template),DEFERRED::getPrimitive
            PROCEDURE(template2),DEFERRED::calcCellValue
            PROCEDURE(template2),DEFERRED::calcFaceArea
            PROCEDURE(template2),DEFERRED::calcFluxRate
            PROCEDURE(template),DEFERRED::calcFlux
!            PROCEDURE::putResult
      ENDTYPE

      ABSTRACT INTERFACE
            SUBROUTINE template(this)
                  IMPORT convectionClass
                  CLASS(convectionClass)::this
            END SUBROUTINE
            SUBROUTINE template2(this,dimen)
                  IMPORT convectionClass
                  CLASS(convectionClass)::this
                  INTEGER,INTENT(IN)::dimen
            END SUBROUTINE
      END INTERFACE
!     Abstract Class>>


!-------------------------------------
!     A class for AUSM+-up and MUSCL
!-------------------------------------
      TYPE,EXTENDS(convectionClass):: AUSMplusUp_MUSCL
!           Primitive variables
            REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::
     &            primitive
!           Derived variables
            REAL(8),DIMENSION(:,:,:,:,:),ALLOCATABLE::
     &            processed, phi, p
            REAL(8),DIMENSION(:,:,:,:),ALLOCATABLE::
     &            M, c, ppm, U
            REAL(8),DIMENSION(:,:,:),ALLOCATABLE::
     &            k_p_mod, k_u_mod

      CONTAINS
            PROCEDURE::alloc         => allocAM
            PROCEDURE::getPrimitive  => getPrimitiveAM
            PROCEDURE::calcCellValue => TVD
            PROCEDURE::calcFaceArea
            PROCEDURE::calcFluxRate  => AUSMPlusUp
            PROCEDURE::calcFlux      => calcFluxAM
!            PROCEDURE::putResult=>putResultAM
      END TYPE



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Methods
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
      SUBROUTINE allocAM(this)
      USE control_Data,             ONLY:nx,ny,nz,ne
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

      CLASS(AUSMplusUp_MUSCL)::this

#define cellindex 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS
#define interfaceindex 0:nx,0:ny,0:nz
!     indexing convention
!           An interface on the right(i.e. bigger index direction)
!           has the same index with the given cell.

      IF (ALLOCATED(this%flux)) THEN
      DEALLOCATE(
     &         this%flux     ,
     &         this%primitive,
     &         this%processed,
     &         this%phi      ,
     &         this%p        ,
     &         this%M        ,
     &         this%c        ,
     &         this%ppm      ,
     &         this%U        ,
     &         this%k_p_mod  ,
     &         this%k_u_mod
     &        )
      ENDIF

      ALLOCATE(
     &         this%flux     (interfaceindex,ne),
     &         this%primitive(     cellindex,ne),
     &         this%processed(interfaceindex,ne,2),
     &         this%phi      (interfaceindex,ne,2),
     &         this%p        (interfaceindex,2:4,2),
     &         this%M        (interfaceindex,3),
     &         this%c        (interfaceindex,3),
     &         this%ppm      (interfaceindex,2),
     &         this%U        (interfaceindex,2),
     &         this%k_p_mod  (interfaceindex),
     &         this%k_u_mod  (interfaceindex)
     &        )


#undef cellindex
#undef interfaceindex


      END SUBROUTINE




!$$$$$$$$$$$$$$$
      SUBROUTINE getPrimitiveAM(this)
      IMPLICIT NONE

      CLASS(AUSMplusUp_MUSCL)::this

      this%primitive(:,:,:,1)   =ro_
      this%primitive(:,:,:,2)   =ru_
      this%primitive(:,:,:,3)   =rv_
      this%primitive(:,:,:,4)   =rw_
      this%primitive(:,:,:,5)   = t_
      this%primitive(:,:,:,6:ne)=rfs_

      END SUBROUTINE





!$$$$$$$$$$$$$$$
!$$$$ SUBROUTINE TVD(this,dimen)
#include '3.Convection_3TVD.f'



!$$$$$$$$$$$$$$$
!$$$$ SUBROUTINE AUSMPlusUp(this, dimen)
#include '3.Convection_2AUSMPlusUp.f'


!$$$$$$$$$$$$$$$
!$$$$ SUBROUTINE calcFaceArea(this,dimen)
      SUBROUTINE calcFaceArea(this,dimen)
#define indc i,j,k
#define indl i,j,k
#define indr i+ish,j+jsh,k+ksh
      USE grid_Data

      IMPLICIT NONE

      INTEGER,PARAMETER::left=1, right=2, center=3

      CLASS(AUSMplusUp_MUSCL)::this
      INTEGER,INTENT(IN)::dimen

      INTEGER::i,j,k,ish,jsh,ksh
      REAL(8)::gx, gy, gz, area

      SELECT CASE(dimen)
      CASE (1)

      ish=1
      jsh=0
      ksh=0

      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            area=(
     &            contraNorm_x_(indl)*gj_(indl)
     &           +contraNorm_x_(indr)*gj_(indr)
     &           )*0.5d0

            gx=gxxf_(i,j,k)
            gy=gxyf_(i,j,k)
            gz=gxzf_(i,j,k)

            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gx
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gy
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gz
      ENDDO;ENDDO;ENDDO

      CASE (2)

      ish=0
      jsh=1
      ksh=0

      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            area=(
     &            contraNorm_y_(indl)*gj_(indl)
     &           +contraNorm_y_(indr)*gj_(indr)
     &           )*0.5d0

            gx=gyxf_(i,j,k)
            gy=gyyf_(i,j,k)
            gz=gyzf_(i,j,k)

            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gx
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gy
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gz
      ENDDO;ENDDO;ENDDO

      CASE (3)

      ish=0
      jsh=0
      ksh=1

      DO k=1-ksh,nz; DO j=1-jsh,ny; DO i=1-ish,nx
            area=(
     &            contraNorm_z_(indl)*gj_(indl)
     &           +contraNorm_z_(indr)*gj_(indr)
     &           )*0.5d0

            gx=gzxf_(i,j,k)
            gy=gzyf_(i,j,k)
            gz=gzzf_(i,j,k)
            this%c(indc,center) = this%c(indc,center)*area

            this%k_u_mod(indc) = this%k_u_mod(indc)*area

            this%p(indc,2,left:right) = this%p(indc,2,left:right) * gx
            this%p(indc,3,left:right) = this%p(indc,3,left:right) * gy
            this%p(indc,4,left:right) = this%p(indc,4,left:right) * gz
      ENDDO;ENDDO;ENDDO

      ENDSELECT


#undef indc
#undef indl
#undef indr

      END SUBROUTINE


!$$$$$$$$$$$$$$$
!$$$$ SUBROUTINE calcFluxAM(this)
      SUBROUTINE calcFluxAM(this)

      IMPLICIT NONE
      INTEGER,PARAMETER::left=1, right=2, center=3

      CLASS(AUSMplusUp_MUSCL)::this

      INTEGER::i,j,k,ie

      this%flux=0.0d0
      this%M(:,:,:,center)=
     &      this%M(:,:,:,left)+this%M(:,:,:,right)
     &     -this%k_p_mod

      DO k=0,nz; DO j=0,ny; DO i=0,nx

!     Add Advection terms
      DO ie=1,ne
      IF (this%M(i,j,k,center).ge.0.0d0) THEN
      this%flux(i,j,k,ie)=
     &      ( this%M(i,j,k,center)
     &       *this%c(i,j,k,center)
     &       *this%phi(i,j,k,ie,left)
     &      )

      ELSE
      this%flux(i,j,k,ie)=
     &      ( this%M(i,j,k,center)
     &       *this%c(i,j,k,center)
     &       *this%phi(i,j,k,ie,right)
     &      )
      ENDIF
      ENDDO

!     Add pressure terms
      DO ie=2,4
      this%flux(i,j,k,ie)=this%flux(i,j,k,ie)
     &     +( ( this%ppm(i,j,k,left)
     &         *this%p(i,j,k,ie,left)
     &        )
     &       +( this%ppm(i,j,k,right)
     &         *this%p(i,j,k,ie,right)
     &        )
     &       -( this%k_u_mod(i,j,k)
     &         *this%ppm(i,j,k,left)
     &         *this%ppm(i,j,k,right)
     &        )
     &      )
      ENDDO

      ENDDO;ENDDO;ENDDO

      END SUBROUTINE



!$$$$$$$$$$$$$$$
      SUBROUTINE reconstruct(processed, phi, c, press)
      USE thermal_Property,               ONLY:Td, rs
      USE thermocoeff,                    ONLY:NASAPolynomial,
     &                                         NASAPolynomial7,
     &                                         NASAPolynomial9,nt

      IMPLICIT NONE
      REAL(8),INTENT(IN),DIMENSION(:)::processed
      REAL(8),INTENT(OUT),DIMENSION(:)::phi, press
      REAL(8),INTENT(OUT)::c

      REAL(8)::rm, p, uSq, rht, gamma
      CLASS(NASAPolynomial),ALLOCATABLE::tdrm

      IF    (nt.eq.7) THEN
            ALLOCATE(NASAPolynomial7::tdrm)
      ELSEIF(nt.eq.9) THEN
            ALLOCATE(NASAPolynomial9::tdrm)
      ENDIF

!     <<Reconstruct other variables
#define frac processed(6:ne)
#define rho processed(1)
#define temp processed(5)
      rm=SUM(rs*frac/rho)
      p=rho*rm*temp
      CALL tdrm%makeMixture(td, frac/rho, rs)
      uSq=( processed(2)**2
     &     +processed(3)**2
     &     +processed(4)**2)/rho

      rht=rho*tdrm%enthalpy(temp)+0.5d0*uSq

!     critical speed of sound
      gamma =  tdrm%specheat(temp)
     &       /(tdrm%specheat(temp)-rm)
      c=SQRT(gamma*rm*temp)

      c=SQRT((2.0d0*c**2+(gamma-1.0d0)*uSq/rho)
     &       /(gamma+1.0d0))
#undef frac
#undef rho
#undef temp
!     Reconstruct other variables>>

!     Output reconstructed phi and pressure
      phi=processed
      phi(5)=rht
      press=p
      END SUBROUTINE


!$$$$$$$$$$$$$$$
      FUNCTION MUSCL(var) RESULT(fl)
      REAL(8),DIMENSION(ne),INTENT(in)::var
      REAL(8),DIMENSION(ne)::fl

      fl =MAX(0.0d0, MIN(2.0d0*var,0.5d0*(var+1.0d0),2.0d0))

      ENDFUNCTION

      END MODULE
