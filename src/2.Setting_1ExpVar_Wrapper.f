!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Prepare expanded variables
!           e.g. U,V,H,FS,t,cp,h,dt
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ExpVarWrapper
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns,
     &                                   dt,dtMax,dtMin,time,
     &                                   CFLMax,CFLMin,ib,nb
      USE fundamental_Constants,    ONLY:nGS
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:arraypoint

      IMPLICIT NONE
      INTEGER:: i,j,k

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     initilization of dt
      dt=0.0d0
      dtMax=0.0d0
      dtMin=1.0d10

      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         CALL arraypoint
!$OMP PARALLEL DO PRIVATE(i,j,k)
         do k=1-nGS,nz+nGS;do j=1-nGS,ny+nGS;do i=1-nGS,nx+nGS
            CALL EXPVAR(i,j,k)
         ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO
      ENDIF;ENDDO multiblock

!     dt is determined by CFLMax.
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,dtmin,1,MPI_REAL8,MPI_MIN,MPI_COMM_WORLD,ierr)
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,dtmax,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)
      dt=dtMin
      CFLMin=CFLMax*dtMin/dtMax
      time=time+dt

      END SUBROUTINE
