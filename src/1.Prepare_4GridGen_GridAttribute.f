!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      SUBROUTINE GridAttribute
!        Grant Grid Attribute
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE grid_Data
      USE control_Data,             ONLY:nx,ny,nz,ib,nb
      USE boundary_Data_OOP,        ONLY:bf
      USE Message_Passing_Interface
      USE fundamental_Constants
      USE AuxSubprogram,            ONLY:arraypoint

      IMPLICIT NONE

      INTEGER::facet
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
      CALL arraypoint

      geo_gl(ib)%gridAttr_=ga_boundary

      geo_gl(ib)%gridAttr_(1:nx,1:ny,1:nz)=ga_inside

      ENDIF; ENDDO multiBlock

      END SUBROUTINE GridAttribute
