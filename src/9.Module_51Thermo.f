!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!             Thermodynamic properties
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      MODULE thermocoeff
      USE Fundamental_Constants,    ONLY:ns
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Derived types and variables
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      IMPLICIT NONE

      INTEGER,PARAMETER::nt=9
      REAL(8),PARAMETER::tULimit=6.0d3,
     &                   tLLimit=2.0d2

!     <<Abstract Class
      TYPE,ABSTRACT:: NASAPolynomial
            REAL(8),DIMENSION(nt)::coeffLow,coeffHigh
            REAL(8)::TBnd
      CONTAINS
            PROCEDURE::getCoeff
            PROCEDURE::makeMixture
            PROCEDURE::putTemp
            PROCEDURE(template),DEFERRED::specheat
            PROCEDURE(template),DEFERRED::enthalpy
            PROCEDURE(template2),DEFERRED::HelmFE
      ENDTYPE

      ABSTRACT INTERFACE
            PURE FUNCTION template(this, t, r)
                  IMPORT NASAPolynomial
                  REAL(8)::template
                  REAL(8),INTENT(in)::t
                  CLASS(NASAPolynomial),INTENT(IN)::this
                  REAL(8),OPTIONAL,INTENT(IN)::r
            END FUNCTION
            PURE FUNCTION template2(this, t, r) RESULT(freeEnergy)
                  IMPORT NASAPolynomial
                  REAL(8)::freeEnergy
                  CLASS(NASAPolynomial),INTENT(IN)::this
                  REAL(8),INTENT(IN)::t
                  REAL(8),OPTIONAL,INTENT(IN)::r
            END FUNCTION
      END INTERFACE
!     Abstract Class>>


      TYPE,EXTENDS(NASAPolynomial)::NASAPolynomial7

      CONTAINS
            PROCEDURE::specheat=>specheatr7
            PROCEDURE::enthalpy=>enthalpyr7
            PROCEDURE::HelmFE  =>HelmFEr7
      ENDTYPE

      TYPE,EXTENDS(NASAPolynomial)::NASAPolynomial9

      CONTAINS
            PROCEDURE::specheat=>specheatr9
            PROCEDURE::enthalpy=>enthalpyr9
            PROCEDURE::HelmFE  =>HelmFEr9
      ENDTYPE



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Methods
      CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
      PURE SUBROUTINE getCoeff(this, vector, bound)

      IMPLICIT NONE

      CLASS(NASAPolynomial),INTENT(INOUT)::this
      REAL(8),DIMENSION(nt,2),INTENT(IN)::vector
      REAL(8),INTENT(IN)::bound

!--------------
      this%coeffLow=vector(:,1)
      this%coeffHigh=vector(:,2)

      this%TBnd=bound

      END SUBROUTINE



!$$$$$$$$$$$$$$$
      PURE SUBROUTINE makeMixture(this, td, ssin, rs)

      IMPLICIT NONE

      CLASS(NASAPolynomial),INTENT(INOUT)::this

      CLASS(NASAPolynomial),DIMENSION(ns),INTENT(IN)::td
      REAL(8),DIMENSION(ns),INTENT(IN)::ssin,rs

      REAL(8),DIMENSION(nt,2)::container
      REAL(8)::mul
      INTEGER::is

!--------------
      container=0.0d0

      DO is=1,ns
         mul=ssin(is)*rs(is)
         container(:,1) = container(:,1)+td(is)%coeffLow*mul
         container(:,2) = container(:,2)+td(is)%coeffHigh*mul
      ENDDO

      CALL this%getCoeff(container,td(1)%TBnd)

      END SUBROUTINE


!$$$$$$$$$$$$$$$
      PURE SUBROUTINE putTemp(this, t, intEnergy, rm, cv)
!      USE statuscheck,        ONLY:tempExcess

      IMPLICIT NONE

      CLASS(NASAPolynomial),INTENT(IN)::this

      REAL(8),INTENT(INOUT)::t
      REAL(8),INTENT(IN)::intEnergy, rm
      REAL(8),INTENT(OUT),OPTIONAL::cv
      REAL(8)::cvm, rutm

      INTEGER::iit

!--------------
      iit=1
      rutm=1.0d1

      DO WHILE((iit.le.100).and.(abs(rutm).gt.1.0d-3))

!           normal temperature range case

!           compute specific heat of constant volume from T
!           Cv = Cp - R
            cvm  = this%specheat(t)-rm
!           deviation of internal energy each from T and re
            rutm = this%enthalpy(t)-rm*t
!             from assumed T
     &            -intEnergy
!             from re

      t=t-rutm/cvm
      iit=iit+1

      ENDDO

      IF(PRESENT(cv)) cv=cvm
      t=max(t,10.0d0)

      END SUBROUTINE




!$$$$$$$$$$$$$$$
      PURE FUNCTION specheatr7(this, t, r)

      IMPLICIT NONE

      REAL(8)::specheatr7
      REAL(8),INTENT(in)::t
      CLASS(NASAPolynomial7),INTENT(IN)::this
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt,rr

!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1.0d0
      ELSE
            rr=r
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
      ELSE
            tt=t
      ENDIF

      IF(tt.lt.this%TBnd) THEN
      specheatr7 =
     &      this%coeffLow(1)
     &    + this%coeffLow(2)*tt
     &    + this%coeffLow(3)*tt**2
     &    + this%coeffLow(4)*tt**3
     &    + this%coeffLow(5)*tt**4
      ELSE
      specheatr7 =
     &      this%coeffHigh(1)
     &    + this%coeffHigh(2)*tt
     &    + this%coeffHigh(3)*tt**2
     &    + this%coeffHigh(4)*tt**3
     &    + this%coeffHigh(5)*tt**4
      ENDIF
      specheatr7 = specheatr7*rr

      END FUNCTION specheatr7





!$$$$$$$$$$$$$$$
      PURE FUNCTION enthalpyr7(this, t, r)
      IMPLICIT NONE

      REAL(8)::enthalpyr7
      REAL(8),INTENT(IN)::t
      CLASS(NASAPolynomial7),INTENT(IN)::this
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt,rr,prodEne


!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1
      ELSE
            rr=r
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
            prodEne = specheatr7(this, tt, r)*(t-tt)
      ELSE
            tt=t
            prodEne=0.0d0
      ENDIF

      IF(tt.lt.this%TBnd) THEN
      enthalpyr7 =
     &      this%coeffLow(1)
     &   +  this%coeffLow(2)/2.0d0          *tt
     &   +  this%coeffLow(3)/3.0d0          *tt**2
     &   +  this%coeffLow(4)/4.0d0          *tt**3
     &   +  this%coeffLow(5)/5.0d0          *tt**4
     &   +  this%coeffLow(6)/tt
      ELSE
      enthalpyr7 =
     &      this%coeffHigh(1)
     &   +  this%coeffHigh(2)/2.0d0          *tt
     &   +  this%coeffHigh(3)/3.0d0          *tt**2
     &   +  this%coeffHigh(4)/4.0d0          *tt**3
     &   +  this%coeffHigh(5)/5.0d0          *tt**4
     &   +  this%coeffHigh(6)/tt
      ENDIF

      enthalpyr7 = enthalpyr7*rr*tt+prodEne

      END FUNCTION enthalpyr7


!$$$$$$$$$$$$$$$
      PURE FUNCTION HelmFEr7(this, t, r) RESULT(freeEnergy)
      IMPLICIT NONE

      REAL(8)::freeEnergy
      CLASS(NASAPolynomial7),INTENT(IN)::this
      REAL(8),INTENT(IN)::t
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt, rr


!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1
      ELSE
            rr=r*t
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
      ELSE
            tt=t
      ENDIF

      IF (tt.lt.this%TBnd) Then
                  freeEnergy=
     &             this%coeffLow(1)         *(1.0d0-LOG(tt))
     &            -this%coeffLow(2)/(2.0d0) *tt
     &            -this%coeffLow(3)/(6.0d0) *tt**2
     &            -this%coeffLow(4)/(12.0d0)*tt**3
     &            -this%coeffLow(5)/(20.0d0)*tt**4
     &            +this%coeffLow(6)         /tt
     &            -this%coeffLow(7)
     &            -1.0d0
            ELSE
                  freeEnergy=
     &             this%coeffHigh(1)         *(1.0d0-LOG(tt))
     &            -this%coeffHigh(2)/(2.0d0) *tt
     &            -this%coeffHigh(3)/(6.0d0) *tt**2
     &            -this%coeffHigh(4)/(12.0d0)*tt**3
     &            -this%coeffHigh(5)/(20.0d0)*tt**4
     &            +this%coeffHigh(6)         /tt
     &            -this%coeffHigh(7)
     &            -1.0d0
            ENDIF

      freeEnergy = freeEnergy*rr

      END FUNCTION HelmFEr7


!$$$$$$$$$$$$$$$
      PURE FUNCTION specheatr9(this, t, r)

      IMPLICIT NONE

      REAL(8)::specheatr9
      REAL(8),INTENT(in)::t
      CLASS(NASAPolynomial9),INTENT(IN)::this
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt, rr


!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1
      ELSE
            rr=r
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
      ELSE
            tt=t
      ENDIF

      IF(tt.lt.this%TBnd) THEN
      specheatr9 =
     &      this%coeffLow(1)/tt**2
     &    + this%coeffLow(2)/tt
     &    + this%coeffLow(3)
     &    + this%coeffLow(4)*tt
     &    + this%coeffLow(5)*tt**2
     &    + this%coeffLow(6)*tt**3
     &    + this%coeffLow(7)*tt**4
      ELSE
      specheatr9 =
     &      this%coeffHigh(1)/tt**2
     &    + this%coeffHigh(2)/tt
     &    + this%coeffHigh(3)
     &    + this%coeffHigh(4)*tt
     &    + this%coeffHigh(5)*tt**2
     &    + this%coeffHigh(6)*tt**3
     &    + this%coeffHigh(7)*tt**4
      ENDIF
      specheatr9 = specheatr9*rr

      END FUNCTION specheatr9





!$$$$$$$$$$$$$$$
      PURE FUNCTION enthalpyr9(this, t, r)
      IMPLICIT NONE

      REAL(8)::enthalpyr9
      REAL(8),INTENT(IN)::t
      CLASS(NASAPolynomial9),INTENT(IN)::this
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt, rr, prodEne


!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1
      ELSE
            rr=r
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
            prodEne = specheatr9(this, tt, rr)*(t-tt)
      ELSE
            tt=t
            prodEne=0.0d0
      ENDIF

      IF(tt.lt.this%TBnd) THEN
      enthalpyr9 =
     &      this%coeffLow(1)/tt**2*(-1.0d0)
     &   +  this%coeffLow(2)/tt*LOG(tt)
     &   +  this%coeffLow(3)
     &   +  this%coeffLow(4)/2.0d0          *tt
     &   +  this%coeffLow(5)/3.0d0          *tt**2
     &   +  this%coeffLow(6)/4.0d0          *tt**3
     &   +  this%coeffLow(7)/5.0d0          *tt**4
     &   +  this%coeffLow(8)/tt
      ELSE
      enthalpyr9 =
     &      this%coeffHigh(1)/tt**2*(-1.0d0)
     &   +  this%coeffHigh(2)/tt*LOG(tt)
     &   +  this%coeffHigh(3)
     &   +  this%coeffHigh(4)/2.0d0          *tt
     &   +  this%coeffHigh(5)/3.0d0          *tt**2
     &   +  this%coeffHigh(6)/4.0d0          *tt**3
     &   +  this%coeffHigh(7)/5.0d0          *tt**4
     &   +  this%coeffHigh(8)/tt
      ENDIF

      enthalpyr9 = enthalpyr9*rr*tt+prodEne
      END FUNCTION enthalpyr9


!$$$$$$$$$$$$$$$
      PURE FUNCTION HelmFEr9(this, t, r) RESULT(freeEnergy)
      IMPLICIT NONE

      REAL(8)::freeEnergy
      CLASS(NASAPolynomial9),INTENT(IN)::this
      REAL(8),INTENT(IN)::t
      REAL(8),OPTIONAL,INTENT(IN)::r
      REAL(8)::tt, rr


!--------------
      IF(.NOT.PRESENT(r)) THEN
            rr=1
      ELSE
            rr=r*t
      ENDIF

      IF(t.le.tLLimit) THEN
            tt=tLLimit
      ELSE
            tt=t
      ENDIF

      IF (tt.lt.this%TBnd) Then
                  freeEnergy=
     &             this%coeffLow(1)/(-2.0d0) /tt**2
     &            +this%coeffLow(2)          *(1.0d0+LOG(tt))/tt
     &            +this%coeffLow(3)          *(1.0d0-LOG(tt))
     &            +this%coeffLow(4)/(-2.0d0) *tt
     &            +this%coeffLow(5)/(-6.0d0) *tt**2
     &            +this%coeffLow(6)/(-12.0d0)*tt**3
     &            +this%coeffLow(7)/(-20.0d0)*tt**4
     &            +this%coeffLow(8)          /tt
     &            -this%coeffLow(9)
     &            -1.0d0
            ELSE
                  freeEnergy=
     &             this%coeffHigh(1)/(-2.0d0) /tt**2
     &            +this%coeffHigh(2)          *(1.0d0+LOG(tt))/tt
     &            +this%coeffHigh(3)          *(1.0d0-LOG(tt))
     &            +this%coeffHigh(4)/(-2.0d0) *tt
     &            +this%coeffHigh(5)/(-6.0d0) *tt**2
     &            +this%coeffHigh(6)/(-12.0d0)*tt**3
     &            +this%coeffHigh(7)/(-20.0d0)*tt**4
     &            +this%coeffHigh(8)          /tt
     &            -this%coeffHigh(9)
     &            -1.0d0
            ENDIF

      freeEnergy = freeEnergy*rr

      END FUNCTION HelmFEr9


      END MODULE thermocoeff

