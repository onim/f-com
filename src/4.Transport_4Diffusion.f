!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     transport phenomena
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE getDiffusion
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns
      USE conserved_Quantity,       ONLY:t_, p_, fs_, yfs_, ro_
      USE thermal_Property,         ONLY:rs,sigmm,vdif,teki,wMol
      USE fundamental_Constants,    ONLY:nGS,RUni
      USE transportModule,          ONLY:rci11,bidiffuse,
     &                                   pts

      IMPLICIT NONE
      REAL(8)::
     &      rm,mWMol,Patm
      REAL(8),DIMENSION(ns,ns)::
     &      difi
      REAL(8),DIMENSION(ns)::
     &      dif

      INTEGER::i,j,k,l,m

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!   >>>DIFFUSIVITY TERM<<<

!$OMP PARALLEL DO PRIVATE(i,j,k,rm,mWMol,Patm,difi,dif)
      do k=1-nGS,nz+nGS
        do j=1-nGS,ny+nGS
          do i=1-nGS,nx+nGS

      RM=SUM(RS*FS_(i,j,k,:))
      mWMol=RUni/rm
!     pressure in bar
      Patm=p_(i,j,k)*1.0d-5

      DO l=1,ns-1
        DO m=l+1,ns
      DIFI(l,m)=bidiffuse(Patm,t_(i,j,k),sigmm(l,m),vdif(l,m),
     &                   rci11(T_(i,j,k),TEKI(l,m)))
        ENDDO
      ENDDO

      dif(1)=(1.0d0-FS_(i,j,k,1))
     &    /( SUM(yfs_(i,j,k, 2:ns )/difi(1,2:ns)))
      DO l=2,ns-1
      dif(l)=(1.0d0-FS_(i,j,k,l))
     &    /( SUM(yfs_(i,j,k,l+1:ns)/difi(l,l+1:ns))
     &      +SUM(yfs_(i,j,k, 1:l-1)/difi( 1:l-1,l)))
      ENDDO
      dif(ns)=(1.0d0-FS_(i,j,k,ns))
     &    /( SUM(yfs_(i,j,k,1:ns-1)/difi(1:ns-1,ns)))


      IF(yfs_(i,j,k,1).gt.0.99d0) THEN
            dif(1)= (ns-1)
     &             /( SUM(mWMol/wMol(2:)/difi(1,2:ns)))
      ENDIF

      DO l=2,ns-1
      IF(yfs_(i,j,k,l).gt.0.99d0) THEN
            dif(l)= (ns-1)
     &             /( SUM(mWMol/wMol(l+1:)/difi(l,l+1:ns))
     &               +SUM(mWMol/wMol(:l-1)/difi( 1:l-1,l)))
      ENDIF
      ENDDO
      IF(yfs_(i,j,k,ns).gt.0.99d0) THEN
            dif(ns)=(ns-1)
     &             /( SUM(mWMol/wMol(:ns-1)/difi(1:ns-1,ns)))
      ENDIF

      dif=dif*SUM(yfs_(i,j,k,:))
!     dif -> dif*rho
      dif=dif*ro_(i,j,k)

      pts(i,j,k,:)%dif=dif

          ENDDO
        ENDDO
      ENDDO
!$OMP END PARALLEL DO

      END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     transport phenomena
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE massDiffusion(dimen)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns
      USE conserved_Quantity,       ONLY:fs_
      USE fundamental_Constants,    ONLY:SCT,nGS
      USE transportModule,          ONLY:pt, pts, face, faces

      IMPLICIT NONE

      INTEGER,INTENT(IN)::dimen
      INTEGER::i,j,k,ish,jsh,ksh

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      ish=0
      jsh=0
      ksh=0
      SELECT CASE(dimen)
      CASE(1)
            ish=1
      CASE(2)
            jsh=1
      CASE(3)
            ksh=1
      END SELECT

!   >>>DIFFUSIVITY TERM<<<
!$OMP PARALLEL DO PRIVATE(i,j,k)
      do k=0,nz; do j=0,ny; do i=0,nx

!     sct: schmidt number
!     rodif: turbulent diffusion coefficient * density
!      RODIF= 0.5d0*(pts(i,j,k,:)%dif+pts(i+ish,j+ish,k+ish,:)%dif)
!     &      +face(i,j,k)%VIST/SCT

!     rsfx: rho * D * dw_kdx_i = rho * v_i,k * w_k
      faces(i,j,k,:)%RSFX=
     &   (0.5d0*(pts(i,j,k,:)%dif+pts(i+ish,j+ish,k+ish,:)%dif)+face(i,j,k)%VIST/SCT)*faces(i,j,k,:)%DSX
      faces(i,j,k,:)%RSFY=
     &   (0.5d0*(pts(i,j,k,:)%dif+pts(i+ish,j+ish,k+ish,:)%dif)+face(i,j,k)%VIST/SCT)*faces(i,j,k,:)%DSY
      faces(i,j,k,:)%RSFZ=
     &   (0.5d0*(pts(i,j,k,:)%dif+pts(i+ish,j+ish,k+ish,:)%dif)+face(i,j,k)%VIST/SCT)*faces(i,j,k,:)%DSZ

!     vc: rho * vc_k,i * w_k
!      vcx=SUM(faces(i,j,k,:)%rsfx)
!      vcy=SUM(faces(i,j,k,:)%rsfy)
!      vcz=SUM(faces(i,j,k,:)%rsfz)

!      faces(i,j,k,:)%RSFX=faces(i,j,k,:)%rsfx-vcx*fs_(i,j,k,:)
!      faces(i,j,k,:)%RSFY=faces(i,j,k,:)%rsfy-vcy*fs_(i,j,k,:)
!      faces(i,j,k,:)%RSFZ=faces(i,j,k,:)%rsfz-vcz*fs_(i,j,k,:)

      faces(i,j,k,:)%RSFX=faces(i,j,k,:)%rsfx-SUM(faces(i,j,k,:)%rsfx)*fs_(i,j,k,:)
      faces(i,j,k,:)%RSFY=faces(i,j,k,:)%rsfy-SUM(faces(i,j,k,:)%rsfy)*fs_(i,j,k,:)
      faces(i,j,k,:)%RSFZ=faces(i,j,k,:)%rsfz-SUM(faces(i,j,k,:)%rsfz)*fs_(i,j,k,:)

      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      END SUBROUTINE
