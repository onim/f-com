!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     output result of calculation status check
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE configcheck
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data
      USE thermal_Property,         ONLY:Td,rs,sig,wMol,tek
      USE transportModule,          ONLY:getViscSpecies,rci22


      IMPLICIT NONE

      INTEGER::i,ss

      CHARACTER(66),PARAMETER::
     & dashs=  '---------------------------------'//
     &         '---------------------------------',
     & dollars='$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'//
     &         '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!#######################################################################
!     Configurations
      OPEN(98,FILE=chStat,FORM='formatted',POSITION='append')
      WRITE(98,*)
      WRITE(98,'(A)') dollars
      WRITE(98,'(A)')
     & '---  Configuration report  ------'//
     & '---------------------------------'

      WRITE(98,'(A)') dollars
      WRITE(98,'(A,I6)')'Number of species :',ns
      WRITE(98,*)

      IF (enable_Diffusion) THEN

            WRITE(98,'(A)')'Transport phenomena simulated.'
            WRITE(98,*)

            IF (avoid_Radical) THEN
                  WRITE(98,'(A)')'Only major species are used for transport coefficients'
                  WRITE(98,*)'The major species :',nonRadical
            ELSE
                  WRITE(98,'(A)')'Every species is used for transport coefficients'
            ENDIF
            WRITE(98,*)

            WRITE(98,'(A)')'Specific heats and enthalpy at 298.15K and 2000K of some species are'
            WRITE(98,'(A)')'   Specific heats (J/kg K)     |   Enthalpy (J/kg)'
            DO i=1,num_nonRadical
                  ss=nonRadical(i)
                  WRITE(98,1983)
     &                        Td(ss)%specheat(298.15d0,rs(ss)),
     &                        Td(ss)%specheat(2000.0d0,rs(ss)),' | ',
     &                        Td(ss)%enthalpy(298.15d0,rs(ss)),
     &                        Td(ss)%enthalpy(2000.0d0,rs(ss))
            ENDDO
            WRITE(98,*)

            WRITE(98,'(A)')'Viscosities at 298.15K and 2000K of some species are'
            WRITE(98,'(A)')'   Viscosity (kg/m s)'
            DO i=1,num_nonRadical
                  ss=nonRadical(i)
                  WRITE(98,1989)
     &            getViscSpecies(298.15d0, sig(ss), rci22(298.15d0,tek(ss)), wMol(ss)),
     &            getViscSpecies(2000.0d0, sig(ss), rci22(2000.0d0,tek(ss)), wMol(ss))
            ENDDO
            WRITE(98,*)


            WRITE(98,'(A,A)')'Conductivity calculation method is ',conductMethod

      ELSE
            WRITE(98,'(A)')'Transport phenomena suppressed.'
      ENDIF
      WRITE(98,*)


      IF (reactionModel.ne.0) THEN
            WRITE(98,'(A)')'Chemical reaction simulated.'
            WRITE(98,'(A,I6)')'Number of reactions :',nr
      ELSE
            WRITE(98,'(A)')'Chemical reaction suppressed.'
      ENDIF
      WRITE(98,*)

      WRITE(98,'(A)') 'The simulation ends up when;'
      WRITE(98,'(A,E12.5)') '  residual reaches ',dqCri
      WRITE(98,'(A,I8)') '  or max iteration number reaches ',maxItr

      WRITE(98,'(A)') dollars
      WRITE(98,*)






      CLOSE(98)




      WRITE(*,*)
      WRITE(*,'(A)') dollars
      WRITE(*,'(A)')
     & '---  Configuration report  ------'//
     & '---------------------------------'

      WRITE(*,'(A)') dollars
      WRITE(*,'(A,I6)')'Number of species :',ns
      WRITE(*,*)

      IF (enable_Diffusion) THEN

            WRITE(*,'(A)')'Transport phenomena simulated.'
            WRITE(*,*)

            IF (avoid_Radical) THEN
                  WRITE(*,'(A)')'Only major species are used for transport coefficients'
                  WRITE(*,*)'The major species :',nonRadical
            ELSE
                  WRITE(*,'(A)')'Every species is used for transport coefficients'
            ENDIF
            WRITE(*,*)

            WRITE(*,'(A)')'Specific heats and enthalpy at 298.15K and 2000K of some species are'
            WRITE(*,'(A)')'   Specific heats (J/kg K)     |   Enthalpy (J/kg)'
            DO i=1,num_nonRadical
                  ss=nonRadical(i)
                  WRITE(*,1983)
     +                        Td(ss)%specheat(298.15d0,rs(ss)),
     +                        Td(ss)%specheat(2000.0d0,rs(ss)),' | ',
     +                        Td(ss)%enthalpy(298.15d0,rs(ss)),
     +                        Td(ss)%enthalpy(2000.0d0,rs(ss))
            ENDDO
            WRITE(*,*)

            WRITE(*,'(A)')'Viscosities at 298.15K and 2000K of some species are'
            WRITE(*,'(A)')'   Viscosity (kg/m s)'
            DO i=1,num_nonRadical
                  ss=nonRadical(i)
                  WRITE(*,1989)
     &            getViscSpecies(298.15d0, sig(ss), rci22(298.15d0,tek(ss)), wMol(ss)),
     &            getViscSpecies(2000.0d0, sig(ss), rci22(2000.0d0,tek(ss)), wMol(ss))
            ENDDO
            WRITE(*,*)


            WRITE(*,'(A,A)')'Conductivity calculation method is ',conductMethod

      ELSE
            WRITE(*,'(A)')'Transport phenomena suppressed.'
      ENDIF
      WRITE(*,*)


      IF (reactionModel.ne.0) THEN
            WRITE(*,'(A)')'Chemical reaction simulated.'
            WRITE(*,'(A,I6)')'Number of reactions :',nr
      ELSE
            WRITE(*,'(A)')'Chemical reaction suppressed.'
      ENDIF
      WRITE(*,*)

      WRITE(*,'(A)') 'The simulation ends up when;'
      WRITE(*,'(A,E12.5)') '  residual reaches ',dqCri
      WRITE(*,'(A,I8)') '  or max iteration number reaches ',maxItr

      WRITE(*,'(A)') dollars
      WRITE(*,*)

 1983 FORMAT(2F15.3,A,2F15.3)
 1989 FORMAT(2E15.8)
      END SUBROUTINE

