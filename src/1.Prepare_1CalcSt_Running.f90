!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE parameterload
!     Parameters file Loading
!     Parameters loaded by this subroutine will be updated at each
!     iteration.
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:chConv,                        &
                                      chStat,                        &
                                      CFLMin,maxItr,itrBgn,itrEnd,   &
                                      itvcnv,itvrsl,dqCri,dqMaxLim,  &
                                      conductMethod,                 &
                                      num_Mnt,mnt,                   &
                                      forcedStop
   USE readCommentedFile,        ONLY:readc

   IMPLICIT NONE

   INTEGER::i
   INTEGER,DIMENSION(4)::readdummy

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   OPEN(49,FILE='c1_running.dat',FORM='FORMATTED')

   CALL readc(49,dqMaxLim)
   CALL readc(49,maxItr)

   itrEnd=maxItr

   CALL readc(49,ITVCNV)
   CALL readc(49,chConv)
   CALL readc(49,ITVRSL)

   CALL readc(49,num_Mnt)
   IF (num_Mnt.ne.0) THEN
      IF (.not.ALLOCATED(mnt)) ALLOCATE(mnt(num_Mnt))
      DO i=1,num_Mnt
         CALL readc(49,readdummy)
         mnt(i)%i=readdummy(1)
         mnt(i)%j=readdummy(2)
         mnt(i)%k=readdummy(3)
         mnt(i)%blockIndex=readdummy(4)
      ENDDO
   ENDIF

   CALL readc(49,conductMethod)

   CALL readc(49,chStat)
   CALL readc(49,dqCri)

   CALL readc(49,forcedStop)

   CLOSE(49)

END SUBROUTINE parameterload

