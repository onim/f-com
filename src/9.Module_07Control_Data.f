!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** control parameter

!     ns          # of species
!     nr          # of reactions
!     nt          # of coefficients in NASA specific heat polynomial
!     nx, ny, nz  # of grid in each direction
!     ne          # of equations for species/reactions i.e. ne=ns+1+3+1
!     nb          # of blocks in domain
!     nn          total # of nodes

!     The prefix 'ch' typically means character (variable)
!     chGrid      grid information file
!     chCond      calculation conditions file
!     chInit      initial condition(former result) file
!     chConv      convergency result file
!     chStat      calculation status output file

!     cfl         CFL number (?not in use?)
!     CFLMin      mininum CFL number defined by usercontrol. Time marching
!                 interval is determined by this variable.
!     CFLMax      maximum CFL number calculated from EXPVAR subroutine
!     CFLLim      CFL number limitation used for adjusting CFLMin in RESID subroutine
!     dqMax       maximum residual
!     dqNorm      root-mean-square residual
!     dqCri       residual criterion for judging calculation converge
!     time        current virtually elapsed time in calculation
!     dt          time interval for temporal integral
!     roRef, ruRef, reRef
!                 reference values for calculating residuals of
!                 mass, momentum, energy equation.
!     walltemp    wall temperature from usercontrol

!     it          iteration number
!     itvcnv
!     itvrsl
!     maxItr      maximum iteration limit
!     itrBgn      beginning of the iteration
!     itrEnd      ending of the iteration
!     num_nonRadical
!                 # of nonRadical species
!     num_Mnt     # of monitoring points
!     sort_of_BC  kind of boundary condition

!     iniCnd      existence of initial condition
!     inChem      existence of chemical reaction
!     avoid_Radical
!                 radicals included or not when calculating transport coefficients
!     forcedStop  force the calculation save current result and stop
!     enable_Diffusion
!                 include transport phenomena(viscosity, diffusion, conduction)

!     nonRadical  index of nonRadical species
!     conductMethod
!                 ??
!     spatialDifference
!                 ??
      MODULE Control_Data
      USE fundamental_Constants,    ONLY:ijkPosition,nGS,ns,ne
      USE thermocoeff,              ONLY:nt
      USE M_BlockPointData,         ONLY:T_ConservedQuantity

      IMPLICIT NONE
      SAVE

      INTEGER::nr,nx,ny,nz,nb,nn
      INTEGER,ALLOCATABLE,DIMENSION(:)::nx_gl,ny_gl,nz_gl

      CHARACTER(40)::
     &      chGrid,chCond,chInit,chConv,chStat

      REAL(8)::
     &      cfl,CFLMin,CFLMax,CFLLim,
     &      dqMax,dqNorm,dqCri,dqMaxLim,
     &      time,dt,dtMax,dtMin,
     &      roRef,ruRef,reRef

      INTEGER::it,itvcnv,itvrsl,maxItr,itrBgn,itrEnd,
     &      num_nonRadical,
     &      num_Mnt,
     &      sort_of_BC,
     &      ib,reactionModel

      LOGICAL::iniCnd,avoid_Radical,forcedStop,
     &      enable_Diffusion

      INTEGER,ALLOCATABLE,DIMENSION(:)::
     &      nonRadical

      CHARACTER(6)::conductMethod,spatialDifference


!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type declaration
!$$$$$$$$$$$$$$$$$$$$$
!     Monitoring points
      TYPE,EXTENDS(ijkPosition):: monitoringPoints
            INTEGER::blockIndex
      ENDTYPE

      TYPE(monitoringPoints),ALLOCATABLE::mnt(:)

!     Initial / boundary condition, partial boundary condition
      TYPE initialBoundaryCondition
            REAL(8)::WallTemp, FlowTemp,Mach,Pressure
            REAL(8),DIMENSION(:),ALLOCATABLE::ssin
            REAL(8),DIMENSION(3)::uvwDirection
            LOGICAL::isPartialBC
            INTEGER::nPBC
      CONTAINS
            PROCEDURE::CondToProperty
      ENDTYPE

      TYPE,EXTENDS(initialBoundaryCondition)::partialBoundaryCondition
            INTEGER::pBlock, pFace, pIndFBC=0
            INTEGER,DIMENSION(3)::pxyz1, pxyz2
            REAL(8),DIMENSION(3)::cxyz
            REAL(8)::radius
            CHARACTER(20)::pShape
      CONTAINS
            PROCEDURE::nnode
      ENDTYPE

      TYPE(initialBoundaryCondition)::iC
      TYPE(partialBoundaryCondition),DIMENSION(:),ALLOCATABLE::pI

!$$$$$$$$$$$$$$$
      CONTAINS
!     Class subprograms
!           1. (Initial/boundary) condition to flow property
!           2. Get number of nodes
!$$$$$$$$$$$$$$$

      SUBROUTINE CondToProperty(iBC,cQ)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE thermal_Property,         ONLY:wMol,Td,rs
      USE thermocoeff,              ONLY:NASAPolynomial,
     &                                   NASAPolynomial7,
     &                                   NASAPolynomial9

      IMPLICIT NONE

      CLASS(initialBoundaryCondition)::iBC
      TYPE(T_ConservedQuantity)::cQ

      REAL(8)::press,tmp,dm,rm,cvm,utm,gam,ccc,uvw
      REAL(8),DIMENSION(:),ALLOCATABLE::ssin
      CLASS(NASAPolynomial),ALLOCATABLE::tdrm

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      ALLOCATE(ssin(ns))
      IF    (nt.eq.7) THEN
            ALLOCATE(NASAPolynomial7::tdrm)
      ELSEIF(nt.eq.9) THEN
            ALLOCATE(NASAPolynomial9::tdrm)
      ELSE
            CALL ErrorStop(2105)
      ENDIF

!     Get default pressure, temperature, Mach number and mole fraction
      press=iBC%Pressure
      tmp=iBC%FlowTemp
      dm=iBC%Mach
      ssin=iBC%ssin/SUM(iBC%ssin)

!     normalization for the non-unity summation of fraction
      ssin=ssin*wMol/SUM(ssin*wMol)
!     ssin becomes mass fraction

!     Combine gas constant and NASA coefficient
      rm=sum(rs*ssin)
      CALL tdrm%makeMixture(td, ssin, rs)


!     Get specific heat, internal energy,
!           specific heat ratio, speed of sound and velocity
      cvm=tdrm%specheat(tmp)-rm
      utm=tdrm%enthalpy(tmp)-rm*tmp
      gam=(cvm+rm)/cvm
      ccc=sqrt(gam*rm*tmp)
      uvw=dm*ccc

!     normalise direction of velocity
      iBC%uvwDirection=iBC%uvwDirection/SUM(iBC%uvwDirection)


!     Set density, velocity, energy, temperature and mass fraction
      cQ%ro=press/(rm*tmp)
      cQ%ru=iBC%uvwDirection(1)*cQ%ro*uvw
      cQ%rv=iBC%uvwDirection(2)*cQ%ro*uvw
      cQ%rw=iBC%uvwDirection(3)*cQ%ro*uvw
      cQ%re=cQ%ro*( utm+0.5d0*uvw**2 )
      cQ%t =tmp
      cQ%rfs=cQ%ro*ssin

      END SUBROUTINE



      FUNCTION nnode(this)

      IMPLICIT NONE

      CLASS(partialBoundaryCondition)::this
      INTEGER::nnode

      nnode=  (this%pxyz2(1)-this%pxyz1(1)+1)
     &       *(this%pxyz2(2)-this%pxyz1(2)+1)
     &       *(this%pxyz2(3)-this%pxyz1(3)+1)

      ENDFUNCTION


      END MODULE

