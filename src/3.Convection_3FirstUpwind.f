C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
C
C      MUSCL TVD
C
C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE FirstUpwind(phil,phir,pl,pr,di)
C$$$$$$$$$$$$$$$
C     Header
C$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:nx,ny,nz,ns,ne
      USE conserved_Quantity,       ONLY: ro_, ru_, rv_, rw_, re_,rfs_,
     +                                     h_, p_
      USE grid_Data,                ONLY:gxx_,gxy_,gxz_,
     +                                   gyx_,gyy_,gyz_,
     +                                   gzx_,gzy_,gzz_,gj_,
     +                                   contraNorm_x_,
     +                                   contraNorm_y_,
     +                                   contraNorm_z_
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

#define a1 1-nGS:nx+nGS,1-nGS:ny+nGS,1-nGS:nz+nGS

      REAL(8),PARAMETER::eps=1.0d-20
c           Put small number if divided by 0 occurs
c           when calculating r

      REAL(8),DIMENSION(a1)::
     +                                 cn,   gx,   gy,   gz,
     +        cro,  cru,  crv,  crw,  cre,  cpx,  cpy,  cpz
      REAL(8),DIMENSION(a1,ns)::
     +        crfs
c           c series:   quantities in computational space

      INTEGER,INTENT(IN)::di
c           di: dimension
      REAL(8),DIMENSION(a1,ne),INTENT(OUT)::
     +      phil,phir,pl,pr
c           convective vector left/right,
c           pressure vector left/right
#undef a1

C$$$$$$$$$$$$$$$
C     Main
C$$$$$$$$$$$$$$$
C***********************************************************************
C     Initialization
C***********************************************************************
      phil  = 0.0d0
      phir  = 0.0d0
      pl    = 0.0d0
      pr    = 0.0d0



C***********************************************************************
C     Quantities in computational space
c     checked by 7.12. in Fujii
C***********************************************************************
      cro=ro_
      cru=ru_
      crv=rv_
      crw=rw_
      cre=ro_*h_
      crfs=rfs_

      cpx=p_
      cpy=p_
      cpz=p_
C***********************************************************************
C     For left flux
C***********************************************************************
C***********************************************************************
C     Apply flux limiter and obtain final vector
C***********************************************************************
      phil(:,:,:,1)   =cro
      phil(:,:,:,2)   =cru
      phil(:,:,:,3)   =crv
      phil(:,:,:,4)   =crw
      phil(:,:,:,5)   =cre
      phil(:,:,:,6:ne)=crfs

      pl(:,:,:,2)     =cpx
      pl(:,:,:,3)     =cpy
      pl(:,:,:,4)     =cpz

c     r(nxyz) meaningless

C***********************************************************************
C     For right flux
C***********************************************************************

C***********************************************************************
C     Apply flux limiter and obtain final vector
C***********************************************************************
      phir(:,:,:,1)   =EOSHIFT(cro,1,0.0d0,di)
      phir(:,:,:,2)   =EOSHIFT(cru,1,0.0d0,di)
      phir(:,:,:,3)   =EOSHIFT(crv,1,0.0d0,di)
      phir(:,:,:,4)   =EOSHIFT(crw,1,0.0d0,di)
      phir(:,:,:,5)   =EOSHIFT(cre,1,0.0d0,di)
      phir(:,:,:,6:ne)=EOSHIFT(crfs,1,0.0d0,di)

      pr(:,:,:,2)     =EOSHIFT(cpx,1,0.0d0,di)
      pr(:,:,:,3)     =EOSHIFT(cpy,1,0.0d0,di)
      pr(:,:,:,4)     =EOSHIFT(cpz,1,0.0d0,di)

c     r(nxyz) meaningless

      END SUBROUTINE
