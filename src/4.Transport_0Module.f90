!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE M_Transport
!     list of subroutines in transport module
!           SUBROUTINE gradient
!
!     list of functions in transport module
!           FUNCTION rci11(t, tek)
!           FUNCTION rci22(t, tek)
!           FUNCTION viscosity(t, moleculardiameter, rci, molarmass)
!           FUNCTION corrfactor(visc1, visc2, molarmass1, molarmass2)
!           FUNCTION bidiffuse(p,t,moledia1,moledia2,molarm1,molarm2,rci)
!
!           FUNCTION fluxsum
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
IMPLICIT NONE
!$$$$$$$$$$$$$$$$$$$$$
!     Parameters
!$$$$$$$$$$$$$$$$$$$$$
   INTEGER,PARAMETER::moddim=3

!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type definitions
!$$$$$$$$$$$$$$$$$$$$$
   TYPE transpoint
      REAL(8)::visc,cond
   ENDTYPE

   TYPE transpointsp
      REAL(8)::vism,dif
   ENDTYPE

   TYPE transface
      REAL(8)::               &
         dux,duy,duz,         &
         dvx,dvy,dvz,         &
         dwx,dwy,dwz,         &
         dtx,dty,dtz,         &
         vist,                &
         rufx,rvfx,rwfx,refx, &
         rufy,rvfy,rwfy,refy, &
         rufz,rvfz,rwfz,refz

      REAL(8)::               &
         Sxx,Sxy,Sxz,         &
             Syy,Syz,         &
                 Szz, Snorm

   CONTAINS
      PROCEDURE::strainRate
   ENDTYPE

   TYPE transfacesp
      REAL(8)::               &
         dsx,dsy,dsz,         &
         rsfx,rsfy,rsfz
   ENDTYPE

!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type declaration
!$$$$$$$$$$$$$$$$$$$$$
!     ptt : pointwise data set for transport phenomena
   TYPE(transpoint),ALLOCATABLE :: pt(:,:,:)
   TYPE(transface), ALLOCATABLE :: face(:,:,:)
   TYPE(transpointsp),ALLOCATABLE :: pts(:,:,:,:)
   TYPE(transfacesp), ALLOCATABLE :: faces(:,:,:,:)


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
CONTAINS
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION diffx(phi,dimen) RESULT(diff)
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ib
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   REAL(8),DIMENSION(nx+2*nGS,ny+2*nGS,nz+2*nGS),INTENT(IN)::phi
   INTEGER,INTENT(IN)::dimen
   REAL(8),DIMENSION(nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS)::diff
   INTEGER::i,j,k

#define DIFFX
#include '4.Transport_0Module_diffCode.f90'
#undef DIFFX
END FUNCTION diffx
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION diffy(phi,dimen) RESULT(diff)
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ib
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   REAL(8),DIMENSION(nx+2*nGS,ny+2*nGS,nz+2*nGS),INTENT(in)::phi
   INTEGER,INTENT(IN)::dimen
   REAL(8),DIMENSION(nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS)::diff
   INTEGER::i,j,k

#define DIFFY
#include '4.Transport_0Module_diffCode.f90'
#undef DIFFY

ENDFUNCTION diffy
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION diffz(phi,dimen) RESULT(diff)
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:nx,ny,nz,ib
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   REAL(8),DIMENSION(nx+2*nGS,ny+2*nGS,nz+2*nGS),INTENT(in)::phi
   INTEGER,INTENT(IN)::dimen
   REAL(8),DIMENSION(nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS)::diff
   INTEGER::i,j,k

#define DIFFZ
#include '4.Transport_0Module_diffCode.f90'
#undef DIFFZ

END FUNCTION diffz
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$





!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 1-1: reduced collision integral
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION rci11(t, tekBinary)

IMPLICIT NONE
   REAL(8)::rci11
   REAL(8),INTENT(in)::t, tekBinary
   REAL(8)::tReduced

   tReduced=t/tekBinary
!   IF (tReduced.lt.0.01d0 .or. tReduced.gt.100.0d0) THEN
      ! reference: web
      !  thesis.library.caltech.edu/2290/12/11_appendix.pdf
      rci11= 1.06036d0/(tReduced**0.15610d0)    &
            +0.19300d0/exp(0.47635d0*tReduced)  &
            +1.03587d0/exp(1.52996d0*tReduced)  &
            +1.76474d0/exp(3.89411d0*tReduced)
!   ELSE
!      rci11=  7.985271361d-14*tReduced**10 &
!             -1.492274621d-12*tReduced**9  &
!             -2.415444946d-9 *tReduced**8  &
!             +2.499512918d-7 *tReduced**7  &
!             -6.997033214d-6 *tReduced**6  &
!             -1.069798746d-4 *tReduced**5  &
!             +8.836874508d-3 *tReduced**4  &
!             -1.625274097d-1 *tReduced**3  &
!             +1.237063549d0  *tReduced**2  &
!             -3.790144440d0  *tReduced     &
!             +4.313478910d0
!   ENDIF
END FUNCTION rci11

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 1-2: reduced collision integral
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION rci22(t, tek)

IMPLICIT NONE
   REAL(8)::rci22
   REAL(8),INTENT(in)::t, tek
   REAL(8)::tReduced

   tReduced=t/tek
   ! reference:  the properties of gases and liquids, 5th ed.,
   !  Poling et al., McGrawhill
   rci22= 1.16145d0/(tReduced**0.14874d0)    &
         +0.52487d0/exp(0.77320d0*tReduced)  &
         +2.16178d0/exp(2.43787d0*tReduced)

   ! Simplified
   !rci22=1.147D0*tReduced**(-0.145D0)+1.0D0/(tReduced+0.5D0)**2
   ! using simplified version causes big error!!

END FUNCTION rci22

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 2: species viscosity
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION getViscSpecies(t, moleculardiameter, rci, molarmass)
!     unit
!     temperature:            Kelvin
!     moleculardiameter:      nanometer
!     rci:                    dimensionless
!     molar mass:             kg/mol
IMPLICIT NONE
   REAL(8)::getViscSpecies
   REAL(8),INTENT(in)::t,moleculardiameter,rci,molarmass
   ! reference: Combustion, Warnatz
   !  Eq. 5.16
   getViscSpecies = 2.6693d-8*SQRT(molarmass*1.0D3*t) &
                   /((moleculardiameter**2)*rci)

END FUNCTION getViscSpecies



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 3: correction factor phi_ik
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION corrfactor(visc1, visc2, molarmass1, molarmass2)
   USE fundamental_Constants,    ONLY:r2inv

IMPLICIT NONE

   REAL(8)::corrfactor
   REAL(8),INTENT(in)::visc1,visc2,molarmass1,molarmass2
   ! reference: Combustion, Warnatz
   !  Eq. 5.11
   corrfactor=0.5d0*r2inv* &
              (1.0d0+molarmass1/molarmass2)**(-0.5d0)* &
              (1.0d0+(visc1/visc2)**(0.5d0) &
                    *(molarmass1/molarmass2)**(0.25d0))**2.0d0

END FUNCTION corrfactor

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 4: binary diffusion coefficient
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION bidiffuse(p,t,moledia12,molarmass12,rci)
!     unit
!     diffusion coefficient:  m^2/s
!     pressure:               bar
!     temperature:            Kelvin
!     radius:                 nanometer
!     molar mass:             g/mol
IMPLICIT NONE

   REAL(8)::bidiffuse
   REAL(8),INTENT(in)::p,t,moledia12,molarmass12,rci
   ! reference: Combustion, Warnatz
   !   Eq. 5.24

   !      molamass12 =0.5d0*(molarm1+molarm2)/(molarm1*molarm2)
   !      moledia12=moledia12**2
   bidiffuse = 2.662d-9 &
      *SQRT(molarmass12*t**3) &
      /(p*moledia12*rci)

END FUNCTION bidiffuse


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!     Function 5: Summation of diffusive flux on a node
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE FUNCTION fluxsum(rx, ry, rz, dimen)
   USE control_Data,             ONLY:nx, ny, nz, ib
   USE M_BlockPointData,         ONLY:bl
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

   REAL(8),DIMENSION(nGS:,nGS:,nGS:),INTENT(IN) ::rx,ry,rz
   INTEGER,INTENT(IN)::dimen
   REAL(8),DIMENSION(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)::fluxsum
   INTEGER::i,j,k

   ASSOCIATE(gxxf=>bl(ib)%p(:,:,:)%gd%gxxf, &
             gxyf=>bl(ib)%p(:,:,:)%gd%gxyf, &
             gxzf=>bl(ib)%p(:,:,:)%gd%gxzf, &
             gyxf=>bl(ib)%p(:,:,:)%gd%gyxf, &
             gyyf=>bl(ib)%p(:,:,:)%gd%gyyf, &
             gyzf=>bl(ib)%p(:,:,:)%gd%gyzf, &
             gzxf=>bl(ib)%p(:,:,:)%gd%gzxf, &
             gzyf=>bl(ib)%p(:,:,:)%gd%gzyf, &
             gzzf=>bl(ib)%p(:,:,:)%gd%gzzf)

   SELECT CASE(dimen)
      CASE(1)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         _forInnerPoints_
            fluxsum(i,j,k) = ( &
              gxxf(i,j,k)  *rx(i,j,k) &
             +gxyf(i,j,k)  *ry(i,j,k) &
             +gxzf(i,j,k)  *rz(i,j,k) &
             -gxxf(i-1,j,k)*rx(i-1,j,k) &
             -gxyf(i-1,j,k)*ry(i-1,j,k) &
             -gxzf(i-1,j,k)*rz(i-1,j,k))
         _endInnerPoints_
!$OMP END PARALLEL DO

      CASE(2)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         _forInnerPoints_
            fluxsum(i,j,k) = ( &
             +gyxf(i,j,k)  *rx(i,j,k) &
             +gyyf(i,j,k)  *ry(i,j,k) &
             +gyzf(i,j,k)  *rz(i,j,k) &
             -gyxf(i,j-1,k)*rx(i,j-1,k) &
             -gyyf(i,j-1,k)*ry(i,j-1,k) &
             -gyzf(i,j-1,k)*rz(i,j-1,k))
         _endInnerPoints_
!$OMP END PARALLEL DO

      CASE(3)
!$OMP PARALLEL DO PRIVATE(i,j,k)
         _forInnerPoints_
            fluxsum(i,j,k) = ( &
             +gzxf(i,j,k)  *rx(i,j,k) &
             +gzyf(i,j,k)  *ry(i,j,k) &
             +gzzf(i,j,k)  *rz(i,j,k) &
             -gzxf(i,j,k-1)*rx(i,j,k-1) &
             -gzyf(i,j,k-1)*ry(i,j,k-1) &
             -gzzf(i,j,k-1)*rz(i,j,k-1))
         _endInnerPoints_
!$OMP END PARALLEL DO
   END SELECT

   END ASSOCIATE
! e.g. tau_xx|(1/2)=tau_xx|0 + tau_xx|1
END FUNCTION

!$$$$$$$$$$$$$$$
! Obtain strain rate tensor from velocity gradient tensor
PURE SUBROUTINE strainRate(this)
!$$$$$$$$$$$$$$$
IMPLICIT NONE
   CLASS(transface),INTENT(INOUT)::this

   this%Sxx= this%dux
   this%Sxy=(this%duy+this%dvx)*0.5d0
   this%Sxz=(this%duz+this%dwx)*0.5d0
   this%Syy= this%dvy
   this%Syz=(this%dvz+this%dwy)*0.5d0
   this%Szz= this%dwz
   ! Syx, Szx, Szy are isotropic and only conterparts should be used

   this%Snorm=SQRT(( this%Sxx**2 +this%Sxy**2 +this%Sxz**2 &
                    +this%Sxy**2 +this%Syy**2 +this%Syz**2 &
                    +this%Sxz**2 +this%Syz**2 +this%Szz**2)*2.0d0)

END SUBROUTINE

END MODULE M_Transport

