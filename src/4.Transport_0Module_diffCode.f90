#ifdef DIFFX
#define gx(i,j,k) bl(ib)%p(i,j,k)%gd%gxx
#define gy(i,j,k) bl(ib)%p(i,j,k)%gd%gyx
#define gz(i,j,k) bl(ib)%p(i,j,k)%gd%gzx
#endif

#ifdef DIFFY
#define gx(i,j,k) bl(ib)%p(i,j,k)%gd%gxy
#define gy(i,j,k) bl(ib)%p(i,j,k)%gd%gyy
#define gz(i,j,k) bl(ib)%p(i,j,k)%gd%gzy
#endif

#ifdef DIFFZ
#define gx(i,j,k) bl(ib)%p(i,j,k)%gd%gxz
#define gy(i,j,k) bl(ib)%p(i,j,k)%gd%gyz
#define gz(i,j,k) bl(ib)%p(i,j,k)%gd%gzz
#endif

SELECT CASE (dimen)
   CASE (1)
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(GUIDED)
      DO k=1+nGS,nz+nGS;DO j=1+nGS,ny+nGS;DO i=nGS,nx+nGS
         diff(i,j,k)=                              &
           ( (gx(i,j,k)+gx(i+1,j,k))               &
                      *( phi(i+1  ,j,k)            &
                        -phi(i    ,j,k))*2.0d0     &
            +(gy(i,j,k)+gy(i+1,j,k))               &
                      *(+phi(i  ,j+1,k)            &
                        +phi(i+1,j+1,k)            &
                        -phi(i  ,j-1,k)            &
                        -phi(i+1,j-1,k))           &
            +(gz(i,j,k)+gz(i+1,j,k))               &
                      *(+phi(i  ,j,k+1)            &
                        +phi(i+1,j,k+1)            &
                        -phi(i  ,j,k-1)            &
                        -phi(i+1,j,k-1))) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (2)
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(GUIDED)
      DO k=1+nGS,nz+nGS;DO j=nGS,ny+nGS;DO i=1+nGS,nx+nGS
      diff(i,j,k)=                                 &
           ( (gx(i,j,k)+gx(i,j+1,k))               &
                      *(+phi(i+1,j  ,k)            &
                        +phi(i+1,j+1,k)            &
                        -phi(i-1,j  ,k)            &
                        -phi(i-1,j+1,k))           &
            +(gy(i,j,k)+gy(i,j+1,k))               &
                      *(+phi(i,j+1  ,k)            &
                        -phi(i,j    ,k))*2.0d0     &
            +(gz(i,j,k)+gz(i,j+1,k))               &
                      *(+phi(i,j  ,k+1)            &
                        +phi(i,j+1,k+1)            &
                        -phi(i,j  ,k-1)            &
                        -phi(i,j+1,k-1))) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      CASE (3)
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(GUIDED)
      DO k=nGS,nz+nGS;DO j=1+nGS,ny+nGS;DO i=1+nGS,nx+nGS
      diff(i,j,k)=                                 &
           ( (gx(i,j,k)+gx(i,j,k+1))               &
                      *(+phi(i+1,j,k  )            &
                        +phi(i+1,j,k+1)            &
                        -phi(i-1,j,k  )            &
                        -phi(i-1,j,k+1))           &
            +(gy(i,j,k)+gy(i,j,k+1))               &
                      *( phi(i,j+1,k  )            &
                        +phi(i,j+1,k+1)            &
                        -phi(i,j-1,k  )            &
                        -phi(i,j-1,k+1))           &
            +(gz(i,j,k)+gz(i,j,k+1))               &
                      *(+phi(i,j,k+1  )            &
                        -phi(i,j,k    ))*2.0d0) * 0.25d0
      ENDDO;ENDDO;ENDDO
!$OMP END PARALLEL DO

      ENDSELECT

#undef gx
#undef gy
#undef gz
