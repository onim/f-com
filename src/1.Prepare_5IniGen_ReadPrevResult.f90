!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE IniGen_ReadPrevResult
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:itrEnd,dqMax,dqNorm,time, &
                                      itrBgn,maxItr,chInit,     &
                                      ib,nb,nx,ny,nz,nx_gl,ny_gl,nz_gl,nsp => ns
   USE M_BlockPointData,         ONLY:bl
   USE Fundamental_Constants,    ONLY:nGS
   USE Message_Passing_Interface

IMPLICIT NONE

   INTEGER,PARAMETER::ns=nsp
   INTEGER::i,j,k,is
   REAL(8)::dummy(9), dummyfs(ns)
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   OPEN(53,file=chInit,form='unformatted')
   READ(53) itrEnd,dqMax,dqNorm,time

   _forMyBlocks_
   ASSOCIATE(ro=>bl(ib)%p(:,:,:)%c%ro, &
             ru=>bl(ib)%p(:,:,:)%c%ru, &
             rv=>bl(ib)%p(:,:,:)%c%rv, &
             rw=>bl(ib)%p(:,:,:)%c%rw, &
             re=>bl(ib)%p(:,:,:)%c%re, &
             t =>bl(ib)%p(:,:,:)%c%t, &
             p =>bl(ib)%p(:,:,:)%c%p, &
             qChem=>bl(ib)%p(:,:,:)%c%qChem, &
             c=>bl(ib)%p(:,:,:)%c%c &
             )
      _forInnerPoints_
         READ(53) ro(i,j,k),ru(i,j,k),rv(i,j,k),rw(i,j,k),           &
                  re(i,j,k),t(i,j,k),p(i,j,k),c(i,j,k),qChem(i,j,k), &
                 (bl(ib)%p(i,j,k)%c%rfs(is),is=1,ns)
         ! Here, c is not speed of sound, but Mach number.
      _endInnerPoints_
   END ASSOCIATE
   ELSE ! if NOT my block
      _forInnerPoints_
         READ(53) dummy(1), dummy(2), dummy(3), dummy(4), dummy(5), &
                  dummy(6), dummy(7), dummy(8), dummy(9), (dummyfs(is),is=1,ns)
      _endInnerPoints_
   _endMyBlocks_

   CLOSE(53)

   itrBgn=itrEnd+1
   itrEnd=maxItr

END SUBROUTINE IniGen_ReadPrevResult

