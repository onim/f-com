!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE VISCOUS
! transport phenomena
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:nx,ny,nz,ns,ib, &
                                      avoid_Radical,  &
                                      nonRadical,num_nonRadical
   USE M_BlockPointData,         ONLY:bl
   USE M_Transport,              ONLY:fluxsum,pt,pts,face,faces
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE
   INTEGER::is,dimen
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     variables defined in module transport::face
!     face:  interface             data for transport phenomena
#define AllPoints nx+2*nGS,ny+2*nGS,nz+2*nGS
#define InnerInterface nGS:nx+nGS,nGS:ny+nGS,nGS:nz+nGS
   ALLOCATE(pt(AllPoints),pts(AllPoints,ns))
   ALLOCATE(face(InnerInterface),faces(InnerInterface,ns))
#undef AllPoints
#undef InnerInterface

   ! Use radical species or not
   IF (.not.avoid_Radical) THEN
      num_nonRadical=ns
      DO is=1,ns
         nonRadical(is)=is
      ENDDO
   ENDIF

   !**********************************************************************
   !***Transport terms
   !**********************************************************************
   CALL getViscosity
   CALL getDiffusion
   CALL getConduction

   !**********************************************************************
   !***calculation on boundary of each direction
   !**********************************************************************
   DO dimen=1,3
      CALL qnt_gradient(dimen)
      CALL momentumDiffusion(dimen)
      CALL massDiffusion(dimen)
      CALL heatDiffusion(dimen)

      !**********************************************************************
      !***Add transport fluxes into increment terms
      !**********************************************************************
      ASSOCIATE(f=>bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%f)
      f%dru=f%dru-fluxsum(face%rufx,face%rufy,face%rufz,dimen)
      f%drv=f%drv-fluxsum(face%rvfx,face%rvfy,face%rvfz,dimen)
      f%drw=f%drw-fluxsum(face%rwfx,face%rwfy,face%rwfz,dimen)
      f%dre=f%dre-fluxsum(face%refx,face%refy,face%refz,dimen)
      DO is=1,ns
          bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%f%drfs(is) &
         =bl(ib)%p(1+nGS:nx+nGS,1+nGS:ny+nGS,1+nGS:nz+nGS)%f%drfs(is) &
            -fluxsum(faces(:,:,:,is)%rsfx, &
                     faces(:,:,:,is)%rsfy, &
                     faces(:,:,:,is)%rsfz,dimen)
      ENDDO
      END ASSOCIATE

   ENDDO

!**********************************************************************
!***Free the memory
!**********************************************************************
   DEALLOCATE(pt,pts,face,faces)

END SUBROUTINE
