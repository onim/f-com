!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     output result of calculation status check
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE statout
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:it,itvcnv,ib,nb,
     &                                   chStat,
     &                                   num_Mnt,mnt,
     &                                   nx,ny,nz,ns,nn
      USE conserved_Quantity,       ONLY:ro,ru,rv,rw,re,
     &                                   u,v,w,h,t,rfs,c,p,qChem
      USE Status_Check
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:arraypoint

      INTEGER::cautionflag, i, extent
      CHARACTER(66),PARAMETER::
     & dashs=  '---------------------------------'//
     &         '---------------------------------',
     & dollars='$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'//
     &         '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
      TYPE(minmaxQuant)::mergemm

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$
!     Calculation part
!$$$$$$$$$$$$$$$
      CALL MPI_TYPE_EXTENT(MPI_INTEGER, extent, ierr)
      CALL MPI_TYPE_STRUCT(2,
     &                     (/40,13/),
     &                     (/0,40*extent/),
     &                     (/MPI_INTEGER,MPI_REAL8/),
     &                     MPI_mmq, ierr)
      CALL MPI_TYPE_COMMIT(MPI_mmq, ierr)

      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         CALL arraypoint
         CALL findminmax(mm(ib))
      ENDIF
         CALL MPI_BCAST(mm(ib),1,MPI_mmq,blockOwner(ib),MPI_COMM_WORLD,ierr)
      ENDDO multiBlock

      IF(primeNode()) THEN

      CALL mergeMinmax(mergemm,mm)

!$$$$$$$$$$$$$$$
!     Printout part
!$$$$$$$$$$$$$$$
!#######################################################################
!     Quantities minmax
      OPEN(98,FILE=chStat,FORM='formatted',POSITION='append')
      WRITE(98,'(A)') dollars
      WRITE(98,'(A,I6)')'$$ Current iteration: ',it
      WRITE(98,'(A)') dollars
      WRITE(98,'(A)')
     & '---  Quantity ranges and position'//
     & '  -------------------------------'
      WRITE(98,'(A)')'|     Density|    Velocity'//
     &               '|  Int.energy|    Enthalpy| Temperature|'
      WRITE(98,'(A,F12.4,4(A,F12.1),A)')
     &            '|',mergemm%roMaxVal      ,
     &            '|',mergemm%velMaxVal     ,
     &            '|',mergemm%energyMaxVal  ,
     &            '|',mergemm%enthalpyMaxVal,
     &            '|',mergemm%tempMaxVal    ,'|'
      WRITE(98,'(5(A,4I3),A)')
     &            '|',mergemm%roMaxLoc,
     &            '|',mergemm%velMaxLoc     ,
     &            '|',mergemm%energyMaxLoc  ,
     &            '|',mergemm%enthalpyMaxLoc,
     &            '|',mergemm%tempMaxLoc    ,'|'
      WRITE(98,'(A,F12.4,4(A,F12.1),A)')
     &            '|',mergemm%roMinVal      ,
     &            '|',mergemm%velMinVal     ,
     &            '|',mergemm%energyMinVal  ,
     &            '|',mergemm%enthalpyMinVal,
     &            '|',mergemm%tempMinVal    ,'|'
      WRITE(98,'(5(A,4I3),A)')
     &            '|',mergemm%roMinLoc      ,
     &            '|',mergemm%velMinLoc     ,
     &            '|',mergemm%energyMinLoc  ,
     &            '|',mergemm%enthalpyMinLoc,
     &            '|',mergemm%tempMinLoc    ,'|'
      WRITE(98,'(A)') dashs
      WRITE(98,*)

!#######################################################################
!     Quantities on monitoring points
      IF ((num_Mnt.ne.0)) THEN
      WRITE(98,'(A)')
     & '---  Monitoring points  ---------'//
     & '---------------------------------'
                        DO i=1,num_Mnt
                  DO ib=1,nb
                  CALL arraypoint
            IF (mnt(i)%blockIndex.eq.ib) THEN
      WRITE(98,'(A,I2,A,3I4,A,I4)')
     & '---  Point ',i,'  ------------------',mnt(i)%i,mnt(i)%j,mnt(i)%k,
     & ' on block',mnt(i)%blockIndex
      WRITE(98,'(A)')'|     Density|    Velocity'//
     &               '|  Int.energy|    Enthalpy| Temperature|'
      WRITE(98,'(A,F12.4,4(A,F12.1),A)')
     &            '|',mnt(i)%sv(ro                           ),
     &            '|',mnt(i)%sv(SQRT(ru**2+rv**2+rw**2)/ro   ),
     &            '|',mnt(i)%sv(re/ro-0.5d0*(u**2+v**2+w**2) ),
     &            '|',mnt(i)%sv(h-0.5d0*(u**2+v**2+w**2)     ),
     &            '|',mnt(i)%sv(t                            ),'|'
      WRITE(98,'(A)')'|  Velocity U|  Velocity V'//
     &               '|  Velocity W|    Pressure|SpeedofSound|'
      WRITE(98,'(5(A,F12.1),A)')
     &            '|',mnt(i)%sv(ru/ro                        ),
     &            '|',mnt(i)%sv(rv/ro                        ),
     &            '|',mnt(i)%sv(rw/ro                        ),
     &            '|',mnt(i)%sv(p                            ),
     &            '|',mnt(i)%sv(c                            ),'|'
      WRITE(98,'(5(A,3I4),A)')
      WRITE(98,'(A)') dashs
            ENDIF
                  ENDDO;ENDDO
      WRITE(98,*)
      ENDIF

!#######################################################################
!     quantity conservation check
      WRITE(98,'(A)')
     & '---  Quantity conservation  -----'//
     & '---------------------------------'
      WRITE(98,'(a40,E12.5)')
     & 'Mass balance',rocons
      WRITE(98,'(a40,E12.5)')
     & 'Overall Mass',recons
      WRITE(98,'(a40,E12.5)')
     & 'Concentration of First species',rfscons(1)

      WRITE(98,'(A)') dashs

      WRITE(98,*)

!#######################################################################
!     parametric quantities
      WRITE(98,'(A)')
     & '---  parameters  ----------------'//
     & '---------------------------------'
!     42getviscosity
      WRITE(98,'(a40,F12.3)')
     & 'Viscosity ratio(%, Turbulent/Laminar)',visc_Ratio
      WRITE(98,'(a40,F12.3)')
     & 'Mean dynamic viscosity (kg/m s * e-5)',meanViscosity
      WRITE(98,'(a40,F12.3)')
     & 'Mean conductivity (unit:?)',meanConductivity
      WRITE(98,'(a40,F12.3)')
     & 'Mean Heat release (unit:J?)',mergemm%qchemSumVal/nn
      WRITE(98,'(a40,F12.3)')
     & 'Maximun heat release (unit:J?)',mergemm%qChemMaxVal

      WRITE(98,'(A)') dashs

      WRITE(98,*)

!#######################################################################
!     Cautions
      cautionflag=0
      WRITE(98,'(A)')
     & '---  Cautions  ------------------'//
     & '---------------------------------'
!     20expvar
      IF(tempExcess.eq.1) THEN
            WRITE(98,'(a)') 'Temperature: Caution on high temperature.'
            cautionflag=1
      ENDIF
      IF(newtonRapson.eq.1) THEN
            WRITE(98,'(a)') 'Temperature: Newton method failed.'
            cautionflag=1
      ENDIF
!     31AUSM
      IF(AUSMFlag.eq.1) THEN
            WRITE(98,'(a)') 'AUSM: speed of sound is different.'
            cautionflag=1
      ENDIF

!     70addrhs
      IF(wrongConcentr) THEN
            WRITE(98,'(a)') 'Concentration: Negative concentration occured.'
            cautionflag=1
      ENDIF

      IF(voidNode) THEN
            WRITE(98,'(a)') 'Concentration: You have void node. i.e. zero concentration.'
            cautionflag=1
      ENDIF

      WRITE(98,'(A)') dashs

      IF(cautionflag.eq.0) THEN
            BACKSPACE(98)
            BACKSPACE(98)
            BACKSPACE(98)
      ENDIF

      WRITE(98,*)
      CLOSE(98)

      ENDIF

      END SUBROUTINE
