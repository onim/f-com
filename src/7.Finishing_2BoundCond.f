!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!       Wrapper for multiblock interface data exchange
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE BCSLT
      USE grid_Data,                ONLY:geo_gl
      USE conserved_Quantity,       ONLY:cons_gl

      IMPLICIT NONE

      CALL BCSLT1

      CALL BCForInterface
      CALL BCForInterfaceMPI

      CALL ForcedBC

      ENDSUBROUTINE



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!       HOLE SONIC&SUBSONIC INJECTION BOUNDARY CONDITION
!       ALL WALLS : NON-SLIP CONDITION
!                   Adiabatic
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

      SUBROUTINE BCSLT1
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE conserved_Quantity,       ONLY:consIS,consOS
      USE control_Data,             ONLY:nb,ns,nx,ny,nz,ib
      USE boundary_Data_OOP,        ONLY:bf
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:reverse,reverses,arraypoint
      USE Fundamental_Constants

      IMPLICIT NONE

      INTEGER::facet,is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN

      CALL arraypoint
      DO facet=1,6
         SELECT CASE (bf(ib,facet)%faceType)
         CASE(ft_interface,ft_interface_MPI)
            CONTINUE
         CASE(ft_inlet)
!           get values from preset
!           blockface forcedboundarycondition quantities
            consOS(ib,facet)%ro_=bf(ib,facet)%fBC(1)%quant%ro_(1,1,1)
            consOS(ib,facet)%ru_=bf(ib,facet)%fBC(1)%quant%ru_(1,1,1)
            consOS(ib,facet)%rv_=bf(ib,facet)%fBC(1)%quant%rv_(1,1,1)
            consOS(ib,facet)%rw_=bf(ib,facet)%fBC(1)%quant%rw_(1,1,1)
            consOS(ib,facet)%re_=bf(ib,facet)%fBC(1)%quant%re_(1,1,1)
            DO is=1,ns
            consOS(ib,facet)%rfs_(:,:,:,is)=bf(ib,facet)%fBC(1)%quant%rfs_(1,1,1,is)
            ENDDO
         CASE(ft_outlet)
!           zero gradient condition
            SELECT CASE (facet)
            CASE(fo_west)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(1,:,:),1,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(1,:,:),1,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(1,:,:),1,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(1,:,:),1,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(1,:,:),1,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(1,:,:,:),1,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(1,:,:),1,2)

            CASE(fo_east)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(nx,:,:),1,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(nx,:,:),1,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(nx,:,:),1,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(nx,:,:),1,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(nx,:,:),1,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(nx,:,:,:),1,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(nx,:,:),1,2)

            CASE(fo_south)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(:,1,:),2,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(:,1,:),2,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(:,1,:),2,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(:,1,:),2,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(:,1,:),2,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(:,1,:,:),2,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(:,1,:),2,2)

            CASE(fo_north)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(:,ny,:),2,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(:,ny,:),2,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(:,ny,:),2,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(:,ny,:),2,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(:,ny,:),2,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(:,ny,:,:),2,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(:,ny,:),2,2)

            CASE(fo_back)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(:,:,1),3,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(:,:,1),3,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(:,:,1),3,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(:,:,1),3,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(:,:,1),3,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(:,:,1,:),3,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(:,:,1),3,2)

            CASE(fo_front)
            consOS(ib,facet)%ro_=SPREAD(consIS(ib,facet)%ro_(:,:,nz),3,2)
            consOS(ib,facet)%ru_=SPREAD(consIS(ib,facet)%ru_(:,:,nz),3,2)
            consOS(ib,facet)%rv_=SPREAD(consIS(ib,facet)%rv_(:,:,nz),3,2)
            consOS(ib,facet)%rw_=SPREAD(consIS(ib,facet)%rw_(:,:,nz),3,2)
            consOS(ib,facet)%re_=SPREAD(consIS(ib,facet)%re_(:,:,nz),3,2)
            consOS(ib,facet)%rfs_=SPREAD(consIS(ib,facet)%rfs_(:,:,nz,:),3,2)
            consOS(ib,facet)%t_=SPREAD(consIS(ib,facet)%t_(:,:,nz),3,2)

            ENDSELECT

         CASE(ft_wall)
            consOS(ib,facet)%ro_=reverse(consIS(ib,facet)%ro_)

            consOS(ib,facet)%ru_=reverse(consIS(ib,facet)%ru_)*(-1.0d0)
            consOS(ib,facet)%rv_=reverse(consIS(ib,facet)%rv_)*(-1.0d0)
            consOS(ib,facet)%rw_=reverse(consIS(ib,facet)%rw_)*(-1.0d0)

            consOS(ib,facet)%re_=reverse(consIS(ib,facet)%re_)
            consOS(ib,facet)%rfs_=reverses(consIS(ib,facet)%rfs_)

            consOS(ib,facet)%t_=reverse(consIS(ib,facet)%t_)
         CASE(ft_symmetric)
            consOS(ib,facet)%ro_=reverse(consIS(ib,facet)%ro_)

            consOS(ib,facet)%ru_=reverse(consIS(ib,facet)%ru_)*(1.0d0-bf(ib,facet)%normx*2.0d0)
            consOS(ib,facet)%rv_=reverse(consIS(ib,facet)%rv_)*(1.0d0-bf(ib,facet)%normy*2.0d0)
            consOS(ib,facet)%rw_=reverse(consIS(ib,facet)%rw_)*(1.0d0-bf(ib,facet)%normz*2.0d0)

            consOS(ib,facet)%re_=reverse(consIS(ib,facet)%re_)
            consOS(ib,facet)%rfs_=reverses(consIS(ib,facet)%rfs_)

            consOS(ib,facet)%t_=reverse(consIS(ib,facet)%t_)

         CASE DEFAULT
            CALL ErrorStop(7200)
         ENDSELECT
      ENDDO

      ENDIF; ENDDO multiblock

      END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Boundary Condition For Interface
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

      SUBROUTINE BCForInterface
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE conserved_Quantity,       ONLY:consIS,consOS
      USE control_Data,             ONLY:nb,nx,ny,nz,nx_gl,ny_gl,nz_gl
      USE boundary_Data_OOP,        ONLY:bf
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:arraypoint
      USE Fundamental_Constants

      IMPLICIT NONE

      INTEGER::ib,f,tB,tF,m,j
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN

      CALL arraypoint(ib)

      DO f=fo_west,fo_east
      IF (bf(ib,f)%faceType.eq.ft_interface) THEN

         tB=bf(ib,f)%connBlock
         tF=bf(ib,f)%connFace

!        Matching case
         IF    (ny.eq.ny_gl(tB)) THEN
            consOS(ib,f)%ro_=consIS(tB,tF)%ro_
            consOS(ib,f)%ru_=consIS(tB,tF)%ru_
            consOS(ib,f)%rv_=consIS(tB,tF)%rv_
            consOS(ib,f)%rw_=consIS(tB,tF)%rw_
            consOS(ib,f)%re_=consIS(tB,tF)%re_
            consOS(ib,f)%rfs_=consIS(tB,tF)%rfs_
            consOS(ib,f)%t_=consIS(tB,tF)%t_

!        Non-matching case - coarse
         ELSEIF(ny.lt.ny_gl(tB)) THEN
            m=ny_gl(tB)/ny
            IF (m*ny.ne.ny_gl(tB)) CALL ErrorStop(7200)
            DO j=1,ny
            consOS(ib,f)%ro_(:,j,:)=SUM(consIS(tB,tF)%ro_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%ru_(:,j,:)=SUM(consIS(tB,tF)%ru_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rv_(:,j,:)=SUM(consIS(tB,tF)%rv_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rw_(:,j,:)=SUM(consIS(tB,tF)%rw_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%re_(:,j,:)=SUM(consIS(tB,tF)%re_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rfs_(:,j,:,:)=SUM(consIS(tB,tF)%rfs_(:,m*j-m+1:m*j,:,:),DIM=2)/m
            consOS(ib,f)%t_(:,j,:)=SUM(consIS(tB,tF)%t_(:,m*j-m+1:m*j,:),DIM=2)/m
            ENDDO

!        Non-matching case - fine
         ELSEIF(ny.gt.ny_gl(tB)) THEN
            m=ny/ny_gl(tB)
            IF (m*ny_gl(tB).ne.ny) CALL ErrorStop(7200)
            DO j=1,ny
            consOS(ib,f)%ro_(:,j,:)=consIS(tB,tF)%ro_(:,(j-1)/m+1,:)
            consOS(ib,f)%ru_(:,j,:)=consIS(tB,tF)%ru_(:,(j-1)/m+1,:)
            consOS(ib,f)%rv_(:,j,:)=consIS(tB,tF)%rv_(:,(j-1)/m+1,:)
            consOS(ib,f)%rw_(:,j,:)=consIS(tB,tF)%rw_(:,(j-1)/m+1,:)
            consOS(ib,f)%re_(:,j,:)=consIS(tB,tF)%re_(:,(j-1)/m+1,:)
            consOS(ib,f)%rfs_(:,j,:,:)=consIS(tB,tF)%rfs_(:,(j-1)/m+1,:,:)
            consOS(ib,f)%t_(:,j,:)=consIS(tB,tF)%t_(:,(j-1)/m+1,:)
            ENDDO
         ENDIF
      ENDIF
      ENDDO


      DO f=fo_south,fo_north
      IF (bf(ib,f)%faceType.eq.ft_interface) THEN

         tB=bf(ib,f)%connBlock
         tF=bf(ib,f)%connFace

!        Matching case
         IF    (nx.eq.nx_gl(tB)) THEN
            consOS(ib,f)%ro_=consIS(tB,tF)%ro_
            consOS(ib,f)%ru_=consIS(tB,tF)%ru_
            consOS(ib,f)%rv_=consIS(tB,tF)%rv_
            consOS(ib,f)%rw_=consIS(tB,tF)%rw_
            consOS(ib,f)%re_=consIS(tB,tF)%re_
            consOS(ib,f)%rfs_=consIS(tB,tF)%rfs_
            consOS(ib,f)%t_=consIS(tB,tF)%t_

!        Non-matching case - coarse
         ELSEIF(nx.lt.nx_gl(tB)) THEN
            m=nx_gl(tB)/nx
            IF (m*nx.ne.nx_gl(tB)) CALL ErrorStop(7200)
            DO j=1,nx
            consOS(ib,f)%ro_(j,:,:)=SUM(consIS(tB,tF)%ro_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%ru_(j,:,:)=SUM(consIS(tB,tF)%ru_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rv_(j,:,:)=SUM(consIS(tB,tF)%rv_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rw_(j,:,:)=SUM(consIS(tB,tF)%rw_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%re_(j,:,:)=SUM(consIS(tB,tF)%re_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rfs_(j,:,:,:)=SUM(consIS(tB,tF)%rfs_(m*j-m+1:m*j,:,:,:),DIM=1)/m
            consOS(ib,f)%t_(j,:,:)=SUM(consIS(tB,tF)%t_(m*j-m+1:m*j,:,:),DIM=1)/m
            ENDDO

!        Non-matching case - fine
         ELSEIF(nx.gt.nx_gl(tB)) THEN
            m=nx/nx_gl(tB)
            IF (m*nx_gl(tB).ne.nx) CALL ErrorStop(7200)
            DO j=1,nx
            consOS(ib,f)%ro_(j,:,:)=consIS(tB,tF)%ro_((j-1)/m+1,:,:)
            consOS(ib,f)%ru_(j,:,:)=consIS(tB,tF)%ru_((j-1)/m+1,:,:)
            consOS(ib,f)%rv_(j,:,:)=consIS(tB,tF)%rv_((j-1)/m+1,:,:)
            consOS(ib,f)%rw_(j,:,:)=consIS(tB,tF)%rw_((j-1)/m+1,:,:)
            consOS(ib,f)%re_(j,:,:)=consIS(tB,tF)%re_((j-1)/m+1,:,:)
            consOS(ib,f)%rfs_(j,:,:,:)=consIS(tB,tF)%rfs_((j-1)/m+1,:,:,:)
            consOS(ib,f)%t_(j,:,:)=consIS(tB,tF)%t_((j-1)/m+1,:,:)
            ENDDO
         ENDIF
      ENDIF
      ENDDO

      DO f=fo_back,fo_front
      IF (bf(ib,f)%faceType.eq.ft_interface) THEN

         tB=bf(ib,f)%connBlock
         tF=bf(ib,f)%connFace

!        Matching case
         consOS(ib,f)%ro_=consIS(tB,tF)%ro_
         consOS(ib,f)%ru_=consIS(tB,tF)%ru_
         consOS(ib,f)%rv_=consIS(tB,tF)%rv_
         consOS(ib,f)%rw_=consIS(tB,tF)%rw_
         consOS(ib,f)%re_=consIS(tB,tF)%re_
         consOS(ib,f)%rfs_=consIS(tB,tF)%rfs_
         consOS(ib,f)%t_=consIS(tB,tF)%t_
!        No non-matching case
      ENDIF
      ENDDO

      ENDIF; ENDDO multiBlock

      END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Boundary Condition For Interface with MPI
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

      SUBROUTINE BCForInterfaceMPI
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE conserved_Quantity,       ONLY:conservedQuantity,consIS,consOS
      USE control_Data,             ONLY:ne,nb,
     &                                   nx,ny,nz,nx_gl,ny_gl,nz_gl
      USE boundary_Data_OOP,        ONLY:bf
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:arraypoint
      USE Fundamental_Constants

      IMPLICIT NONE

      INTEGER::ib,ib_opp,f,sN,tN,tB,tF,m,j,nTrans,x,y,z
      TYPE(conservedQuantity)::dummy
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      IF(.not.ALLOCATED(bdc_send)) THEN

      ALLOCATE(bdc_send(nb,6),bdc_recv(nb,6))

      sliceInformation: DO ib=1,nb
      DO f=fo_west,fo_east
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace

            CALL bdc_send(ib,f)%alloc((/2,ny_gl(ib)+2*nGS,nz_gl(ib)+2*nGS/),ne+1,tB,tF)
            CALL bdc_recv(ib,f)%alloc((/2,ny_gl(tB)+2*nGS,nz_gl(tB)+2*nGS/),ne+1,tB,tF)
         ENDIF
      ENDDO

      DO f=fo_south,fo_north
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace

            CALL bdc_send(ib,f)%alloc((/nx_gl(ib)+2*nGS,2,nz_gl(ib)+2*nGS/),ne+1,tB,tF)
            CALL bdc_recv(ib,f)%alloc((/nx_gl(tB)+2*nGS,2,nz_gl(tB)+2*nGS/),ne+1,tB,tF)
         ENDIF
      ENDDO

      ENDDO sliceInformation

      ENDIF

!     make transaction
      transaction: DO ib=1,nb

      DO f=fo_west,fo_front
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            sN=bf(ib,f)%myNode
            tN=bf(ib,f)%connNode
            tB=bf(ib,f)%connBlock
            tF=bf(ib,f)%connFace

            IF (myBlockIs(ib)) THEN
               CALL bdc_send(ib,f)%pack(consIS(ib,f)%ro_,
     &                                  consIS(ib,f)%ru_,
     &                                  consIS(ib,f)%rv_,
     &                                  consIS(ib,f)%rw_,
     &                                  consIS(ib,f)%re_,
     &                                  consIS(ib,f)%rfs_,
     &                                  consIS(ib,f)%t_)
               CALL sendBoundaryValue(tN, bdc_send(ib,f))
            ENDIF

            IF (myBlockIs(tB)) THEN
               CALL recvBoundaryValue(sN, bdc_recv(tb,tf))
            ENDIF
         ENDIF
      ENDDO
      ENDDO transaction

!     unpack transactioned data
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN

      CALL arraypoint(ib)
      DO f=fo_west,fo_east
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock


            x=bdc_recv(ib,f)%nx
            y=bdc_recv(ib,f)%ny
            z=bdc_recv(ib,f)%nz
!              cf) x==nx+2*nGS

            IF (ALLOCATED(dummy%ro_)) THEN
               DEALLOCATE(dummy%ro_,dummy%ru_,dummy%rv_,dummy%rw_,
     &                    dummy%re_,dummy%rfs_,dummy%t_)
            ENDIF
            ALLOCATE(dummy%ro_(nGS,1-nGS:y-nGS,1-nGS:z-nGS),
     &               dummy%ru_(nGS,1-nGS:y-nGS,1-nGS:z-nGS),
     &               dummy%rv_(nGS,1-nGS:y-nGS,1-nGS:z-nGS),
     &               dummy%rw_(nGS,1-nGS:y-nGS,1-nGS:z-nGS),
     &               dummy%re_(nGS,1-nGS:y-nGS,1-nGS:z-nGS),
     &               dummy%rfs_(nGS,1-nGS:y-nGS,1-nGS:z-nGS,bdc_recv(ib,f)%ne-6),
     &               dummy%t_(nGS,1-nGS:y-nGS,1-nGS:z-nGS))
            CALL bdc_recv(ib,f)%unpack(
     &         dummy%ro_,
     &         dummy%ru_,
     &         dummy%rv_,
     &         dummy%rw_,
     &         dummy%re_,
     &         dummy%rfs_,
     &         dummy%t_)

!        Matching case
         IF    (ny.eq.ny_gl(tB)) THEN
            consOS(ib,f)%ro_=dummy%ro_
            consOS(ib,f)%ru_=dummy%ru_
            consOS(ib,f)%rv_=dummy%rv_
            consOS(ib,f)%rw_=dummy%rw_
            consOS(ib,f)%re_=dummy%re_
            consOS(ib,f)%rfs_=dummy%rfs_
            consOS(ib,f)%t_=dummy%t_

!        Non-matching case - coarse
         ELSEIF(ny.lt.ny_gl(tB)) THEN
            m=ny_gl(tB)/ny
            IF (m*ny.ne.ny_gl(tB)) CALL ErrorStop(7200)
            DO j=1,ny
            consOS(ib,f)%ro_(:,j,:)=SUM(dummy%ro_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%ru_(:,j,:)=SUM(dummy%ru_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rv_(:,j,:)=SUM(dummy%rv_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rw_(:,j,:)=SUM(dummy%rw_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%re_(:,j,:)=SUM(dummy%re_(:,m*j-m+1:m*j,:),DIM=2)/m
            consOS(ib,f)%rfs_(:,j,:,:)=SUM(dummy%rfs_(:,m*j-m+1:m*j,:,:),DIM=2)/m
            consOS(ib,f)%t_(:,j,:)=SUM(dummy%t_(:,m*j-m+1:m*j,:),DIM=2)/m
            ENDDO

!        Non-matching case - fine
         ELSEIF(ny.gt.ny_gl(tB)) THEN
            m=ny/ny_gl(tB)
            IF (m*ny_gl(tB).ne.ny) CALL ErrorStop(7200)
            DO j=1,ny
            consOS(ib,f)%ro_(:,j,:)=dummy%ro_(:,(j-1)/m+1,:)
            consOS(ib,f)%ru_(:,j,:)=dummy%ru_(:,(j-1)/m+1,:)
            consOS(ib,f)%rv_(:,j,:)=dummy%rv_(:,(j-1)/m+1,:)
            consOS(ib,f)%rw_(:,j,:)=dummy%rw_(:,(j-1)/m+1,:)
            consOS(ib,f)%re_(:,j,:)=dummy%re_(:,(j-1)/m+1,:)
            consOS(ib,f)%rfs_(:,j,:,:)=dummy%rfs_(:,(j-1)/m+1,:,:)
            consOS(ib,f)%t_(:,j,:)=dummy%t_(:,(j-1)/m+1,:)
            ENDDO
         ENDIF
      ENDIF
      ENDDO




      DO f=fo_south,fo_north
         IF (bf(ib,f)%faceType.eq.ft_interface_MPI) THEN
            tB=bf(ib,f)%connBlock

            x=bdc_recv(ib,f)%nx
            y=bdc_recv(ib,f)%ny
            z=bdc_recv(ib,f)%nz

            IF (ALLOCATED(dummy%ro_)) THEN
               DEALLOCATE(dummy%ro_,dummy%ru_,dummy%rv_,dummy%rw_,
     &                    dummy%re_,dummy%rfs_,dummy%t_)
            ENDIF
            ALLOCATE(dummy%ro_(1-nGS:x-nGS,nGS,1-nGS:z-nGS),
     &               dummy%ru_(1-nGS:x-nGS,nGS,1-nGS:z-nGS),
     &               dummy%rv_(1-nGS:x-nGS,nGS,1-nGS:z-nGS),
     &               dummy%rw_(1-nGS:x-nGS,nGS,1-nGS:z-nGS),
     &               dummy%re_(1-nGS:x-nGS,nGS,1-nGS:z-nGS),
     &               dummy%rfs_(1-nGS:x-nGS,nGS,1-nGS:z-nGS,bdc_recv(ib,f)%ne-6),
     &               dummy%t_(1-nGS:x-nGS,nGS,1-nGS:z-nGS))
            CALL bdc_recv(ib,f)%unpack(
     &         dummy%ro_,
     &         dummy%ru_,
     &         dummy%rv_,
     &         dummy%rw_,
     &         dummy%re_,
     &         dummy%rfs_,
     &         dummy%t_)

!        Matching case
         IF    (nx.eq.nx_gl(tB)) THEN
            consOS(ib,f)%ro_=dummy%ro_
            consOS(ib,f)%ru_=dummy%ru_
            consOS(ib,f)%rv_=dummy%rv_
            consOS(ib,f)%rw_=dummy%rw_
            consOS(ib,f)%re_=dummy%re_
            consOS(ib,f)%rfs_=dummy%rfs_
            consOS(ib,f)%t_=dummy%t_

!        Non-matching case - coarse
         ELSEIF(nx.lt.nx_gl(tB)) THEN
            m=nx_gl(tB)/nx
            IF (m*nx.ne.nx_gl(tB)) CALL ErrorStop(7200)
            DO j=1,nx
            consOS(ib,f)%ro_(j,:,:)=SUM(dummy%ro_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%ru_(j,:,:)=SUM(dummy%ru_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rv_(j,:,:)=SUM(dummy%rv_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rw_(j,:,:)=SUM(dummy%rw_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%re_(j,:,:)=SUM(dummy%re_(m*j-m+1:m*j,:,:),DIM=1)/m
            consOS(ib,f)%rfs_(j,:,:,:)=SUM(dummy%rfs_(m*j-m+1:m*j,:,:,:),DIM=1)/m
            consOS(ib,f)%t_(j,:,:)=SUM(dummy%t_(m*j-m+1:m*j,:,:),DIM=1)/m
            ENDDO

!        Non-matching case - fine
         ELSEIF(nx.gt.nx_gl(tB)) THEN
            m=nx/nx_gl(tB)
            IF (m*nx_gl(tB).ne.nx) CALL ErrorStop(7200)
            DO j=1,nx
            consOS(ib,f)%ro_(j,:,:)=dummy%ro_((j-1)/m+1,:,:)
            consOS(ib,f)%ru_(j,:,:)=dummy%ru_((j-1)/m+1,:,:)
            consOS(ib,f)%rv_(j,:,:)=dummy%rv_((j-1)/m+1,:,:)
            consOS(ib,f)%rw_(j,:,:)=dummy%rw_((j-1)/m+1,:,:)
            consOS(ib,f)%re_(j,:,:)=dummy%re_((j-1)/m+1,:,:)
            consOS(ib,f)%rfs_(j,:,:,:)=dummy%rfs_((j-1)/m+1,:,:,:)
            consOS(ib,f)%t_(j,:,:)=dummy%t_((j-1)/m+1,:,:)
            ENDDO
         ENDIF
      ENDIF
      ENDDO

      ENDIF; ENDDO multiBlock

      END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Forced Boundary Condition
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

      SUBROUTINE ForcedBC
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE conserved_Quantity,       ONLY:consOS
      USE control_Data,             ONLY:nb
      USE boundary_Data_OOP,        ONLY:bf
      USE Message_Passing_Interface

      IMPLICIT NONE

      INTEGER::ib,facet,iFBC,iNL,i,j,k
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!     For each surface
      multiBlock: DO ib=1,nb; IF (myBlockIs(ib)) THEN
         DO facet=1,6
!     If there exist forced boundary condition(s)
            IF (bf(ib,facet)%nFBC.ne.0) THEN

!     For each forced boundary condition and point
               DO iFBC=1,bf(ib,facet)%nFBC
               DO iNL=1,bf(ib,facet)%fBC(iFBC)%nnode
                     i=bf(ib,facet)%fBC(iFBC)%nodelist(iNL)%i
                     j=bf(ib,facet)%fBC(iFBC)%nodelist(iNL)%j
                     k=bf(ib,facet)%fBC(iFBC)%nodelist(iNL)%k
                     consOS(ib,facet)%ro_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%ro_(1,1,1)
                     consOS(ib,facet)%ru_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%ru_(1,1,1)
                     consOS(ib,facet)%rv_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%rv_(1,1,1)
                     consOS(ib,facet)%rw_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%rw_(1,1,1)
                     consOS(ib,facet)%re_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%re_(1,1,1)
                     consOS(ib,facet)%rfs_(i,j,k,:)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%rfs_(1,1,1,:)
                     consOS(ib,facet)%t_(i,j,k)=
     &                      bf(ib,facet)%fBC(iFBC)%quant%t_(1,1,1)
               ENDDO;ENDDO
            ENDIF
         ENDDO
      ENDIF; ENDDO multiBlock

      END SUBROUTINE

