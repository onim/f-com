!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
PURE SUBROUTINE ChemLi(yfs,t)
!***********************************************************************
!
!  Reference: S. C. Li and F. A. Williams,
!             Reaction mechanisms for methane ignition
!
!  Participating species
!
!   O2
!   CH4
!   CO
!   H2O
!   H
!
!   OH
!   HO2
!   H2O2
!   CH3
!   HCO
!
!   HCHO
!   N2
!
! Reaction steps
!    CH4 +   O2 ->  HO2 +  CH3
!    CH4 +  HO2 -> H2O2 +  CH3
!    CH4 +   OH ->  H2O +  CH3
!    CH3 +   O2 -> HCHO +   OH
!   HCHO +   OH ->  HCO +  H2O
!
!    HCO +   O2 ->   CO +  HO2
!    HCO +    M ->   CO +    H + M
!    H + O2 + M ->  HO2 +        M
!   H2O2 +    M ->  2OH +        M

!
!***********************************************************************
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE Fundamental_Constants,    ONLY:ns,RUni
   USE Control_Data,             ONLY:nr, dt
   USE Reaction_Data
   USE thermocoeff,              ONLY:NASAPolynomial9
   USE Thermal_Property,         ONLY:wMol,rs

   IMPLICIT NONE

   INTEGER,PARAMETER:: &
      O2   = 1, &
      CH4  = 2, &
      CO   = 3, &
      H2O  = 4, &
      H    = 5, &
      OH   = 6, &
      HO2  = 7, &
      H2O2 = 8, &
      CH3  = 9, &
      HCO  = 10, &
      HCHO = 11, &
      N2   = 12

   REAL(8),INTENT(INOUT),DIMENSION(ns)::yfs
   REAL(8),INTENT(IN)::t

   REAL(8)::dtRe,dtReSum,yfssum
   REAL(8),PARAMETER::unitConvert=1.0d-6, &
                      eps        =0.0d-50
   REAL(8),DIMENSION(nr)::rateCoeff,reactionRate
   REAL(8),DIMENSION(ns)::inityfs,dyfs

   INTEGER::is

   !$$$$$$$$$$$$$$$
   !     Main
   !$$$$$$$$$$$$$$$
   dtReSum=0.0d0 ! time interval used already in reaction progress
   yfssum=SUM(yfs)
   inityfs=yfs


   !***************
   !     Rate coefficients
   !***************
   rateCoeff(1)=cf(1)            *exp(-ef(1)/RUni/t)*unitConvert
   rateCoeff(2)=cf(2)            *exp(-ef(2)/RUni/t)*unitConvert
   rateCoeff(3)=cf(3)*t**zetaf(3)*exp(-ef(3)/RUni/t)*unitConvert
   rateCoeff(4)=cf(4)            *exp(-ef(4)/RUni/t)*unitConvert
   rateCoeff(5)=cf(5)*t**zetaf(5)*exp(-ef(5)/RUni/t)*unitConvert
   rateCoeff(6)=cf(6)            *exp(-ef(6)/RUni/t)*unitConvert
   rateCoeff(7)=cf(7)*t**zetaf(7)*exp(-ef(7)/RUni/t)*unitConvert
   rateCoeff(8)=cf(8)*t**zetaf(8)*exp(-ef(8)/RUni/t)*unitConvert**2
   rateCoeff(9)=cf(9)            *exp(-ef(9)/RUni/t)*unitConvert

   DO WHILE (dtReSum.ne.dt)
      ! Assume no temperature change during reaction in one time step

      !***************
      !     Reaction rates
      !***************
      !    CH4 +   O2 ->  HO2 +  CH3
      reactionRate(1)=rateCoeff(1)*yfs(CH4)*yfs(O2)
      !    CH4 +  HO2 -> H2O2 +  CH3
      reactionRate(2)=rateCoeff(2)*yfs(CH4)*yfs(HO2)
      !    CH4 +   OH ->  H2O +  CH3
      reactionRate(3)=rateCoeff(3)*yfs(CH4)*yfs(OH)
      !    CH3 +   O2 -> HCHO +   OH
      reactionRate(4)=rateCoeff(4)*yfs(CH3)*yfs(O2)
      !   HCHO +   OH ->  HCO +  H2O
      reactionRate(5)=rateCoeff(5)*yfs(HCHO)*yfs(OH)
      !
      !    HCO +   O2 ->   CO +  HO2
      reactionRate(6)=rateCoeff(6)*yfs(HCO)*yfs(O2)
      !    HCO +    M ->   CO +    H + M
      reactionRate(7)=rateCoeff(7)*yfs(HCO)*yfssum
      !    H + O2 + M ->  HO2 +        M
      reactionRate(8)=rateCoeff(8)*yfs(H)*yfs(O2)*yfssum
      !   H2O2 +    M ->  2OH +        M
      reactionRate(9)=rateCoeff(9)*yfs(H2O2)*yfssum

#define rr reactionRate
      dyfs(O2)   = -rr(1)-rr(6)-rr(8)-rr(4)
      dyfs(CH4)  = -rr(1)-rr(2)-rr(3)
      dyfs(CO)   =  rr(6)+rr(7)
      dyfs(H2O)  =  rr(3)+rr(5)
      dyfs(H)    =  rr(7)-rr(8)
      dyfs(OH)   = -rr(3)-rr(5)+rr(9)+rr(9)+rr(4)
      dyfs(HO2)  =  rr(1)-rr(2)+rr(6)+rr(8)
      dyfs(H2O2) =  rr(2)-rr(9)
      dyfs(CH3)  =  rr(1)+rr(2)+rr(3)-rr(4)
      dyfs(HCO)  =  rr(5)-rr(6)-rr(7)
      dyfs(HCHO) = -rr(5)+rr(4)
      dyfs(N2)   = 0.0d0
#undef rr

      dtRe=dt*1.0d0 ! reaction time interval guess

      do is=1,ns
         IF(dyfs(is).lt.0.0d0) THEN
            dtRe=MIN(dtRe,yfs(is)/dyfs(is)*(-0.2d0))
         ENDIF
      ENDDO

      IF(dtRe.ge.dt-dtReSum) THEN
         dtRe=dt-dtReSum
         dtReSum=dt
      ELSE
         dtReSum=dtReSum+dtRe
      ENDIF
      yfs=yfs+dyfs*dtRe
   ENDDO

   END SUBROUTINE


