C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
C
C                       USING CHEMKIN-II CODE
C                     CALCULATE CHEMICAL REACTION
C
C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE ChemPeters
C$$$$$$$$$$$$$$$
C     Header
C$$$$$$$$$$$$$$$
      USE grid_Data,                      ONLY:gj
      USE conserved_Quantity,             ONLY:yfs,qChem,rfs,
     +                                         temperature => t
      USE thermal_Property,               ONLY:td,TdOpt,hf,wMol,TBnd
      USE flux_difference,                ONLY:dre,drfs
      USE fundamental_Constants,          ONLY:RMOL,RCAL
      USE reaction_data,                  ONLY:cf,zetaf,ef,
     +                                         cf0,zetaf0,ef0,
     +                                         acent,ts1,ts2,ts3
      USE control_Data,                   ONLY:ns,nr,nx,ny,nz,
     +                                         dt

      IMPLICIT NONE

      REAL(8)::TMOLE,
     +      t,rkf0,pred,fcent,ctro,ntro,ftro
      REAL(8),DIMENSION(nr)::
     +      RKF, RPF, RPB, domg
      REAL(8),DIMENSION(ns)::
     +      dyfs
      REAL(8),DIMENSION(ns+1)::
     +      helmFE
      REAL(8),DIMENSION(3)::
     +      dHelmFE,eqk

      INTEGER::i,j,k,is
C$$$$$$$$$$$$$$$
C     Main
C$$$$$$$$$$$$$$$
C***********************************************************************
C***CHEMICAL REACTION
c
C     Reference: Peters, Turbulent combustion
C
C     Participating species
c
c        H2
c        H
c        O2
c        H2O
c        CH4
c
c        CO
c        CO2
c        N2
C***********************************************************************

      DO k=1,nz; DO j=1,ny; DO i=1,nx

      t=temperature(i,j,k)

      TMOLE=SUM(YFS(i,j,k,:))

C***************
C     Get Helmholtz free energy
C***************
      DO is=1,ns+1
      SELECT CASE (is)
c                 H2, H, O2, H2O, CO, CO2
            CASE ( 1, 2,  3,   4,  6,   7)
            IF (t.lt.TBnd(is)) Then
                  helmFE(is)=
     +             td(is,1,1)/(-2.0d0) /t**2
     +            +td(is,2,1)          *(1.0d0+LOG(t))/t
     +            +td(is,3,1)          *(1.0d0-LOG(t))
     +            +td(is,4,1)/(-2.0d0) *t
     +            +td(is,5,1)/(-6.0d0) *t**2
     +            +td(is,6,1)/(-12.0d0)*t**3
     +            +td(is,7,1)/(-20.0d0)*t**4
     +            +td(is,8,1)          /t
     +            -td(is,9,1)
     +            -1.0d0
            ELSE
                  helmFE(is)=
     +             td(is,1,2)/(-2.0d0) /t**2
     +            +td(is,2,2)          *(1.0d0+LOG(t))/t
     +            +td(is,3,2)          *(1.0d0-LOG(t))
     +            +td(is,4,2)/(-2.0d0) *t
     +            +td(is,5,2)/(-6.0d0) *t**2
     +            +td(is,6,2)/(-12.0d0)*t**3
     +            +td(is,7,2)/(-20.0d0)*t**4
     +            +td(is,8,2)          /t
     +            -td(is,9,2)
     +            -1.0d0
            ENDIF
c                 OH
            CASE ( 9)
            IF (t.lt.1000.0d0) Then
                  helmFE(9)=
     +             TdOpt(1,1,1)/(-2.0d0) /t**2
     +            +TdOpt(1,2,1)          *(1.0d0+LOG(t))/t
     +            +TdOpt(1,3,1)          *(1.0d0-LOG(t))
     +            +TdOpt(1,4,1)/(-2.0d0) *t
     +            +TdOpt(1,5,1)/(-6.0d0) *t**2
     +            +TdOpt(1,6,1)/(-12.0d0)*t**3
     +            +TdOpt(1,7,1)/(-20.0d0)*t**4
     +            +TdOpt(1,8,1)          /t
     +            -TdOpt(1,9,1)
     +            -1.0d0
            ELSE
                  helmFE(9)=
     +             TdOpt(1,1,2)/(-2.0d0) /t**2
     +            +TdOpt(1,2,2)          *(1.0d0+LOG(t))/t
     +            +TdOpt(1,3,2)          *(1.0d0-LOG(t))
     +            +TdOpt(1,4,2)/(-2.0d0) *t
     +            +TdOpt(1,5,2)/(-6.0d0) *t**2
     +            +TdOpt(1,6,2)/(-12.0d0)*t**3
     +            +TdOpt(1,7,2)/(-20.0d0)*t**4
     +            +TdOpt(1,8,2)          /t
     +            -TdOpt(1,9,2)
     +            -1.0d0
            ENDIF

            CASE DEFAULT
                  CONTINUE
      ENDSELECT
      ENDDO

C***************
C     Get difference of Helmholtz free energy of some reactions
C***************
c                      OH +      H2 =       H +     H2O
      dHelmFE(1)=helmFE(9)+helmFE(1)-helmFE(2)-helmFE(4)
c                      CO +     H2O =     CO2 +      H2
      dHelmFE(2)=helmFE(6)+helmFE(4)-helmFE(7)-helmFE(1)
c                      O2 +           3H2 =            2H +          2H2O
      dHelmFE(3)=helmFE(3)+3.0d0*helmFE(1)-2.0d0*helmFE(2)-2.0d0*helmFE(4)

C***************
C     Get equilibrium constant
C***************
      eqk=exp(-dHelmFE)

c      rkf=cf*t**zetaf*exp(-ef/rcal/t)
      rkf=cf*1.0d-6*t**zetaf*exp(-ef/rcal/t)

c        wI =k_1          *        CH4 *          H
      rpf(1)=rkf(1)       *yfs(i,j,k,5)*yfs(i,j,k,2)
c       wII =k_2   /   K3 *          H /         H2
      rpf(2)=rkf(2)/eqk(1)*yfs(i,j,k,2)/yfs(i,j,k,1)
c                           *(         CO *        H2O
     +                      *(yfs(i,j,k,6)*yfs(i,j,k,4)
c                            -        CO2 *         H2 /  KII )
     +                       -yfs(i,j,k,7)*yfs(i,j,k,1)/eqk(2))
c      wIII =k_3          *          H *         O2 * M
      rpf(3)=rkf(3)       *yfs(i,j,k,2)*yfs(i,j,k,3)*TMOLE
c       wIV =k_4          *          H /         H2^3
      rpf(4)=rkf(4)       *yfs(i,j,k,2)/yfs(i,j,k,1)**3
c                           *(         O2 *         H2^3
     +                      *(yfs(i,j,k,3)*yfs(i,j,k,1)**3
c                            -          H^2  *        H2O^2  /  KIV )
     +                       -yfs(i,j,k,2)**2*yfs(i,j,k,4)**2/eqk(3))
c
      domg=rpf
c
      dyfs(1)=4.0d0*domg(1)+domg(2)+domg(3)-3.0d0*domg(4)
      dyfs(2)=2.0d0*domg(4)                -2.0d0*domg(1)-2.0d0*domg(3)
      dyfs(3)=                             -      domg(4)
      dyfs(4)=2.0d0*domg(4)                -      domg(1)-      domg(2)
      dyfs(5)=                             -      domg(1)
      dyfs(6)=      domg(1)                -      domg(2)
      dyfs(7)=      domg(2)
      dyfs(8)=0.0d0


      drfs(i,j,k,:)=drfs(i,j,k,:)-dyfs*wMol*gj(i,j,k)

      qChem(i,j,k)=-1.0d0*sum(DYFS*wMol*HF(i,j,k,:))

      DRE(i,j,k)=DRE(i,j,k)-qChem(i,j,k)*GJ(i,j,k)

      ENDDO;ENDDO;ENDDO


      END SUBROUTINE


