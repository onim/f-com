!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE ExpandPointSimple
! Expand coordinate onto ghost nodes
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:ib, nx, ny, nz
   USE fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

   INTEGER::i

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

   ! Simple expansion
   ASSOCIATE (px => bl(ib)%p(:,:,:)%g%xd, &
              py => bl(ib)%p(:,:,:)%g%yd, &
              pz => bl(ib)%p(:,:,:)%g%zd)
      !$$$$$ WEST
      px(nGS,:,:) =  px(1+nGS,:,:) &
                   -(px(2+nGS,:,:) -px(1+nGS,:,:))
      py(nGS,:,:) =  py(1+nGS,:,:) &
                   -(py(2+nGS,:,:) -py(1+nGS,:,:))
      pz(nGS,:,:) =  pz(1+nGS,:,:) &
                   -(pz(2+nGS,:,:) -pz(1+nGS,:,:))

      DO i=2,nGS
         px(nGS+1-i,:,:) = px(nGS,:,:) &
                         -(px(i+nGS,:,:) -px(nGS+1,:,:))
         py(nGS+1-i,:,:) = py(nGS,:,:) &
                         -(py(i+nGS,:,:) -py(nGS+1,:,:))
         pz(nGS+1-i,:,:) = pz(nGS,:,:) &
                         -(pz(i+nGS,:,:) -pz(nGS+1,:,:))
      ENDDO
      !$$$$$


      !$$$$$ EAST
      px(nx+nGS+1,:,:) =  px(nx+nGS,:,:) &
                        -(px(nx+nGS-1,:,:) -px(nx+nGS,:,:))
      py(nx+nGS+1,:,:) =  py(nx+nGS,:,:) &
                        -(py(nx+nGS-1,:,:) -py(nx+nGS,:,:))
      pz(nx+nGS+1,:,:) =  pz(nx+nGS,:,:) &
                        -(pz(nx+nGS-1,:,:) -pz(nx+nGS,:,:))

      DO i=2,nGS
         px(nx+ngs+i,:,:) = px(nx+nGS+1,:,:) &
                          -(px(nx+nGS+1-i,:,:) -px(nx+nGS,:,:))
         py(nx+ngs+i,:,:) = py(nx+nGS+1,:,:) &
                          -(py(nx+nGS+1-i,:,:) -py(nx+nGS,:,:))
         pz(nx+ngs+i,:,:) = pz(nx+nGS+1,:,:) &
                          -(pz(nx+nGS+1-i,:,:) -pz(nx+nGS,:,:))
      ENDDO
      !$$$$$


      !$$$$$ SOUTH
      px(:,nGS,:) =  px(:,1+nGS,:) &
                   -(px(:,2+nGS,:) -px(:,1+nGS,:))
      py(:,nGS,:) =  py(:,1+nGS,:) &
                   -(py(:,2+nGS,:) -py(:,1+nGS,:))
      pz(:,nGS,:) =  pz(:,1+nGS,:) &
                   -(pz(:,2+nGS,:) -pz(:,1+nGS,:))

      DO i=2,nGS
         px(:,nGS+1-i,:) = px(:,nGS,:) &
                         -(px(:,i+nGS,:) -px(:,nGS+1,:))
         py(:,nGS+1-i,:) = py(:,nGS,:) &
                         -(py(:,i+nGS,:) -py(:,nGS+1,:))
         pz(:,nGS+1-i,:) = pz(:,nGS,:) &
                         -(pz(:,i+nGS,:) -pz(:,nGS+1,:))
      ENDDO
      !$$$$$

      !$$$$$ NORTH
      px(:,ny+nGS+1,:) =  px(:,ny+nGS,:) &
                        -(px(:,ny+nGS-1,:) -px(:,ny+nGS,:))
      py(:,ny+nGS+1,:) =  py(:,ny+nGS,:) &
                        -(py(:,ny+nGS-1,:) -py(:,ny+nGS,:))
      pz(:,ny+nGS+1,:) =  pz(:,ny+nGS,:) &
                        -(pz(:,ny+nGS-1,:) -pz(:,ny+nGS,:))

      DO i=2,nGS
         px(:,ny+ngs+i,:) = px(:,ny+nGS+1,:) &
                          -(px(:,ny+nGS+1-i,:) -px(:,ny+nGS,:))
         py(:,ny+ngs+i,:) = py(:,ny+nGS+1,:) &
                          -(py(:,ny+nGS+1-i,:) -py(:,ny+nGS,:))
         pz(:,ny+ngs+i,:) = pz(:,ny+nGS+1,:) &
                          -(pz(:,ny+nGS+1-i,:) -pz(:,ny+nGS,:))
      ENDDO
      !$$$$$


      !$$$$$ BACK
      px(:,:,nGS) =  px(:,:,1+nGS) &
                   -(px(:,:,2+nGS) -px(:,:,1+nGS))
      py(:,:,nGS) =  py(:,:,1+nGS) &
                   -(py(:,:,2+nGS) -py(:,:,1+nGS))
      pz(:,:,nGS) =  pz(:,:,1+nGS) &
                   -(pz(:,:,2+nGS) -pz(:,:,1+nGS))

      DO i=2,nGS
         px(:,:,nGS+1-i) = px(:,:,nGS) &
                         -(px(:,:,i+nGS) -px(:,:,nGS+1))
         py(:,:,nGS+1-i) = py(:,:,nGS) &
                         -(py(:,:,i+nGS) -py(:,:,nGS+1))
         pz(:,:,nGS+1-i) = pz(:,:,nGS) &
                         -(pz(:,:,i+nGS) -pz(:,:,nGS+1))
      ENDDO
      !$$$$$


      !$$$$$ FRONT
      px(:,:,nz+nGS+1) =  px(:,:,nz+nGS) &
                        -(px(:,:,nz+nGS-1) -px(:,:,nz+nGS))
      py(:,:,nz+nGS+1) =  py(:,:,nz+nGS) &
                        -(py(:,:,nz+nGS-1) -py(:,:,nz+nGS))
      pz(:,:,nz+nGS+1) =  pz(:,:,nz+nGS) &
                        -(pz(:,:,nz+nGS-1) -pz(:,:,nz+nGS))

      DO i=2,nGS
         px(:,:,nz+nGS+i) = px(:,:,nz+nGS+1) &
                          -(px(:,:,nz+nGS+1-i) -px(:,:,nz+nGS))
         py(:,:,nz+nGS+i) = py(:,:,nz+nGS+1) &
                          -(py(:,:,nz+nGS+1-i) -py(:,:,nz+nGS))
         pz(:,:,nz+nGS+i) = pz(:,:,nz+nGS+1) &
                          -(pz(:,:,nz+nGS+1-i) -pz(:,:,nz+nGS))
      ENDDO
      !$$$$$

   END ASSOCIATE

END SUBROUTINE ExpandPointSimple





!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE ExpandPointCopy
! Copy coordinate information onto ghost nodes from neighboring block
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl, T_GeometryData
   USE control_Data,             ONLY:nx,ny,nz,nx_gl,ny_gl,nz_gl,ib
   USE Boundary_Data_OOP,        ONLY:bf
   USE fundamental_Constants


IMPLICIT NONE

   !     cB    conBlock
   INTEGER::i,j,cB,m
   TYPE(T_GeometryData),DIMENSION(:),ALLOCATABLE::dist

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   !     Copy from adjacent block
   !     The code should be modified if the block has a singular point. e.g. O-grid
   !        or inconsistent block matching
   !
   !     face orientation:             fo_back
   !                       fo_north     /
   !               _______________________
   !              |                       |
   !      fo_west |         block         | fo_east
   !              |                       |
   !              |_______________________|
   !     eta          /    fo_south
   !     ^        fo_front
   !     |--> xi
   !    /
   !  zeta

   ASSOCIATE (px => bl(ib)%p(:,:,:)%g%xd, &
              py => bl(ib)%p(:,:,:)%g%yd, &
              pz => bl(ib)%p(:,:,:)%g%zd)

   IF(bf(ib,fo_west)%faceType.eq.ft_interface .or. &
      bf(ib,fo_west)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_west)%connBlock

      ! Matching case
      IF    (ny.eq.ny_gl(cb)) THEN
         DO i=1,nGS
            px(i,:,:)=bl(cB)%p(nx_gl(cB)+i,:,:)%g%xd
            py(i,:,:)=bl(cB)%p(nx_gl(cB)+i,:,:)%g%yd
            pz(i,:,:)=bl(cB)%p(nx_gl(cB)+i,:,:)%g%zd
         ENDDO

      ! Non-matching case - coarse
      ELSEIF(ny.lt.ny_gl(cb)) THEN
         m=ny_gl(cB)/ny
         DO i=1,nGS
            ! Averaging for innerpoint
            DO j=1+nGS,ny+nGS
               px(i,j,:)=SUM(bl(cB)%p(nx_gl(cb)+i,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%xd,DIM=1)/m
               py(i,j,:)=SUM(bl(cB)%p(nx_gl(cb)+i,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%yd,DIM=1)/m
               pz(i,j,:)=SUM(bl(cB)%p(nx_gl(cb)+i,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%zd,DIM=1)/m
               ! DIM=1 is dimension for eta(There is only one element in xi direction)
            ENDDO

            ! Simple expansion for ghostpoint
            px(i,(/nGS  ,ny+nGS+1/),:)= px(i,(/1+nGS,ny+nGS/),:) &
                                      -(px(i,(/2+nGS,ny-1+nGS/),:)-px(i,(/1+nGS,ny+nGS/),:))
            py(i,(/nGS  ,ny+nGS+1/),:)= py(i,(/1+nGS,ny+nGS/),:) &
                                      -(py(i,(/2+nGS,ny-1+nGS/),:)-py(i,(/1+nGS,ny+nGS/),:))
            pz(i,(/nGS  ,ny+nGS+1/),:)= pz(i,(/1+nGS,ny+nGS/),:) &
                                      -(pz(i,(/2+nGS,ny-1+nGS/),:)-pz(i,(/1+nGS,ny+nGS/),:))
            px(i,(/nGS-1,ny+nGS+2/),:)= px(i,(/nGS,ny+nGS+1/),:) &
                                      -(px(i,(/2+nGS,ny-1+nGS/),:)-px(i,(/1+nGS,ny+ngs/),:))
            py(i,(/nGS-1,ny+nGS+2/),:)= py(i,(/nGS,ny+nGS+1/),:) &
                                      -(py(i,(/2+nGS,ny-1+nGS/),:)-py(i,(/1+nGS,ny+ngs/),:))
            pz(i,(/nGS-1,ny+nGS+2/),:)= pz(i,(/nGS,ny+nGS+1/),:) &
                                      -(pz(i,(/2+nGS,ny-1+nGS/),:)-pz(i,(/1+nGS,ny+ngs/),:))
            ! Valid only when nGS=2. For bigger nGS, you need to write more code for them.
         ENDDO

      ! Non-matching case - fine
      ELSEIF(ny.gt.ny_gl(cb)) THEN
         m=ny/ny_gl(cB)
         IF (ALLOCATED(dist)) DEALLOCATE(dist)
         ALLOCATE(dist(nz+2*nGS))
         DO i=1,nGS
            DO j=1,ny+2*ngs
               dist%xd= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+2,:)%g%xd &
                       -bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS  ,:)%g%xd
               dist%yd= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+2,:)%g%yd &
                       -bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS  ,:)%g%yd
               dist%zd= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+2,:)%g%zd &
                       -bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS  ,:)%g%zd
               px(i,j,:)= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+1,:)%g%xd &
                         +dist%xd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               py(i,j,:)= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+1,:)%g%yd &
                         +dist%yd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               pz(i,j,:)= bl(cB)%p(nx_gl(cb)+i,(j-1-nGS)/m+nGS+1,:)%g%zd &
                         +dist%zd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
            ENDDO
         ENDDO
      ENDIF

   ENDIF

   IF(bf(ib,fo_east)%faceType.eq.ft_interface .or. &
      bf(ib,fo_east)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_east)%connBlock

      ! Matching case
      IF    (ny.eq.ny_gl(cb)) THEN
         DO i=1,nGS
            px(nx+i+nGS,:,:)=bl(cB)%p(i+nGS,:,:)%g%xd
            py(nx+i+nGS,:,:)=bl(cB)%p(i+nGS,:,:)%g%yd
            pz(nx+i+nGS,:,:)=bl(cB)%p(i+nGS,:,:)%g%zd
         ENDDO

      ! Non-matching case - coarse
      ELSEIF(ny.lt.ny_gl(cb)) THEN
         m=ny_gl(cB)/ny
         DO i=1,nGS
            ! Averaging for innerpoint
            DO j=1+nGS,ny+nGS
               px(nx+i+nGS,j,:)=SUM(bl(cB)%p(i+nGS,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%xd,DIM=1)/m
               py(nx+i+nGS,j,:)=SUM(bl(cB)%p(i+nGS,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%yd,DIM=1)/m
               pz(nx+i+nGS,j,:)=SUM(bl(cB)%p(i+nGS,m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,:)%g%zd,DIM=1)/m
               ! DIM=1 is dimension for eta(There is only one element in xi direction)
            ENDDO

            ! Simple expansion for ghostpoint
            px(nx+i+nGS,(/nGS  ,ny+nGS+1/),:)= px(nx+i+nGS,(/1+nGS,ny+nGS/),:) &
                                             -(px(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-px(nx+i+nGS,(/1+nGS,ny+nGS/),:))
            py(nx+i+nGS,(/nGS  ,ny+nGS+1/),:)= py(nx+i+nGS,(/1+nGS,ny+nGS/),:) &
                                             -(py(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-py(nx+i+nGS,(/1+nGS,ny+nGS/),:))
            pz(nx+i+nGS,(/nGS  ,ny+nGS+1/),:)= pz(nx+i+nGS,(/1+nGS,ny+nGS/),:) &
                                             -(pz(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-pz(nx+i+nGS,(/1+nGS,ny+nGS/),:))
            px(nx+i+nGS,(/nGS-1,ny+nGS+2/),:)= px(nx+i+nGS,(/nGS,ny+nGS+1/),:) &
                                             -(px(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-px(nx+i+nGS,(/1+nGS,ny+ngs/),:))
            py(nx+i+nGS,(/nGS-1,ny+nGS+2/),:)= py(nx+i+nGS,(/nGS,ny+nGS+1/),:) &
                                             -(py(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-py(nx+i+nGS,(/1+nGS,ny+ngs/),:))
            pz(nx+i+nGS,(/nGS-1,ny+nGS+2/),:)= pz(nx+i+nGS,(/nGS,ny+nGS+1/),:) &
                                             -(pz(nx+i+nGS,(/2+nGS,ny-1+nGS/),:)-pz(nx+i+nGS,(/1+nGS,ny+ngs/),:))
            ! Valid only when nGS=2. For bigger nGS, you need to write more code for them.
         ENDDO

      ! Non-matching case - fine
      ELSEIF(ny.gt.ny_gl(cb)) THEN
         m=ny/ny_gl(cB)
         IF (ALLOCATED(dist)) DEALLOCATE(dist)
         ALLOCATE(dist(nz+2*nGS))
         DO i=1,nGS
            DO j=1,ny+2*ngs
               dist%xd= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+2,:)%g%xd &
                       -bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS  ,:)%g%xd
               dist%yd= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+2,:)%g%yd &
                       -bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS  ,:)%g%yd
               dist%zd= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+2,:)%g%zd &
                       -bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS  ,:)%g%zd
               px(nx+nGS+i,j,:)= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+1,:)%g%xd &
                            +dist%xd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               py(nx+nGS+i,j,:)= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+1,:)%g%yd &
                            +dist%yd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               pz(nx+nGS+i,j,:)= bl(cB)%p(i+nGS,(j-1-nGS)/m+nGS+1,:)%g%zd &
                            +dist%zd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
            ENDDO
         ENDDO
      ENDIF
   ENDIF

   IF(bf(ib,fo_south)%faceType.eq.ft_interface .or. &
      bf(ib,fo_south)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_south)%connBlock

      ! Matching case
      IF    (nx.eq.nx_gl(cb)) THEN
         DO i=1,nGS
            px(:,i,:)=bl(cB)%p(:,ny_gl(cB)+i,:)%g%xd
            py(:,i,:)=bl(cB)%p(:,ny_gl(cB)+i,:)%g%yd
            pz(:,i,:)=bl(cB)%p(:,ny_gl(cB)+i,:)%g%zd
         ENDDO

      ! Non-matching case - coarse
      ELSEIF(nx.lt.nx_gl(cb)) THEN
         m=nx_gl(cB)/nx
         DO i=1,nGS
            ! Averaging for innerpoint
            DO j=1+nGS,nx+nGS
               px(j,i,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,ny_gl(cb)+i,:)%g%xd,DIM=1)/m
               py(j,i,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,ny_gl(cb)+i,:)%g%yd,DIM=1)/m
               pz(j,i,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,ny_gl(cb)+i,:)%g%zd,DIM=1)/m
               ! DIM=1 is dimension for eta(There is only one element in xi direction)
            ENDDO

            ! Simple expansion for ghostpoint
            px((/nGS  ,nx+nGS+1/),i,:)= px((/1+nGS,nx+nGS/),i,:) &
                                      -(px((/2+nGS,nx-1+nGS/),i,:)-px((/1+nGS,nx+nGS/),i,:))
            py((/nGS  ,nx+nGS+1/),i,:)= py((/1+nGS,nx+nGS/),i,:) &
                                      -(py((/2+nGS,nx-1+nGS/),i,:)-py((/1+nGS,nx+nGS/),i,:))
            pz((/nGS  ,nx+nGS+1/),i,:)= pz((/1+nGS,nx+nGS/),i,:) &
                                      -(pz((/2+nGS,nx-1+nGS/),i,:)-pz((/1+nGS,nx+nGS/),i,:))
            px((/nGS-1,nx+nGS+2/),i,:)= px((/  nGS,nx+nGS+1/),i,:) &
                                      -(px((/2+nGS,nx-1+nGS/),i,:)-px((/1+nGS,nx+ngs/),i,:))
            py((/nGS-1,nx+nGS+2/),i,:)= py((/  nGS,nx+nGS+1/),i,:) &
                                      -(py((/2+nGS,nx-1+nGS/),i,:)-py((/1+nGS,nx+ngs/),i,:))
            pz((/nGS-1,nx+nGS+2/),i,:)= pz((/  nGS,nx+nGS+1/),i,:) &
                                      -(pz((/2+nGS,nx-1+nGS/),i,:)-pz((/1+nGS,nx+ngs/),i,:))
            ! Valid only when nGS=2. For bigger nGS, you need to write more code for them.
         ENDDO

      ! Non-matching case - fine
      ELSEIF(nx.gt.nx_gl(cb)) THEN
         m=nx/nx_gl(cB)
         IF (ALLOCATED(dist)) DEALLOCATE(dist)
         ALLOCATE(dist(nz+2*nGS))
         DO i=1,nGS
            DO j=1,nx+2*ngs
               dist%xd= bl(cB)%p((j-1-nGS)/m+nGS+2,ny_gl(cB)+i,:)%g%xd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,ny_gl(cB)+i,:)%g%xd
               dist%yd= bl(cB)%p((j-1-nGS)/m+nGS+2,ny_gl(cB)+i,:)%g%yd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,ny_gl(cB)+i,:)%g%yd
               dist%zd= bl(cB)%p((j-1-nGS)/m+nGS+2,ny_gl(cB)+i,:)%g%zd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,ny_gl(cB)+i,:)%g%zd
               px(j,i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,ny_gl(cB)+i,:)%g%xd &
                         +dist%xd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               py(j,i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,ny_gl(cB)+i,:)%g%yd &
                         +dist%yd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               pz(j,i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,ny_gl(cB)+i,:)%g%zd &
                         +dist%zd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
            ENDDO
         ENDDO
      ENDIF

   ENDIF

   IF(bf(ib,fo_north)%faceType.eq.ft_interface .or. &
      bf(ib,fo_north)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_north)%connBlock

      ! Matching case
      IF    (nx.eq.nx_gl(cb)) THEN
         DO i=1,nGS
            px(:,ny+i+nGS,:)=bl(cB)%p(:,i+nGS,:)%g%xd
            py(:,ny+i+nGS,:)=bl(cB)%p(:,i+nGS,:)%g%yd
            pz(:,ny+i+nGS,:)=bl(cB)%p(:,i+nGS,:)%g%zd
         ENDDO

      ! Non-matching case - coarse
      ELSEIF(nx.lt.nx_gl(cb)) THEN
         m=nx_gl(cB)/nx
         DO i=1,nGS
            ! Averaging for innerpoint
            DO j=1+nGS,nx+nGS
               px(j,ny+i+nGS,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,i+nGS,:)%g%xd,DIM=1)/m
               py(j,ny+i+nGS,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,i+nGS,:)%g%yd,DIM=1)/m
               pz(j,ny+i+nGS,:)=SUM(bl(cB)%p(m*(j-nGS-1)+nGS+1:m*(j-nGS)+nGS,i+nGS,:)%g%zd,DIM=1)/m
               ! DIM=1 is dimension for eta(There is only one element in xi direction)
            ENDDO

            ! Simple expansion for ghostpoint
            px((/nGS  ,nx+nGS+1/),ny+i+nGS,:)= px((/1+nGS,nx+nGS/),ny+i+nGS,:) &
                                             -(px((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-px((/1+nGS,nx+nGS/),ny+i+nGS,:))
            py((/nGS  ,nx+nGS+1/),ny+i+nGS,:)= py((/1+nGS,nx+nGS/),ny+i+nGS,:) &
                                             -(py((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-py((/1+nGS,nx+nGS/),ny+i+nGS,:))
            pz((/nGS  ,nx+nGS+1/),ny+i+nGS,:)= pz((/1+nGS,nx+nGS/),ny+i+nGS,:) &
                                             -(pz((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-pz((/1+nGS,nx+nGS/),ny+i+nGS,:))
            px((/nGS-1,nx+nGS+2/),ny+i+nGS,:)= px((/nGS,nx+nGS+1/),ny+i+nGS,:) &
                                             -(px((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-px((/1+nGS,nx+ngs/),ny+i+nGS,:))
            py((/nGS-1,nx+nGS+2/),ny+i+nGS,:)= py((/nGS,nx+nGS+1/),ny+i+nGS,:) &
                                             -(py((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-py((/1+nGS,nx+ngs/),ny+i+nGS,:))
            pz((/nGS-1,nx+nGS+2/),ny+i+nGS,:)= pz((/nGS,nx+nGS+1/),ny+i+nGS,:) &
                                             -(pz((/2+nGS,nx-1+nGS/),ny+i+nGS,:)-pz((/1+nGS,nx+ngs/),ny+i+nGS,:))
            ! Valid only when nGS=2. For bigger nGS, you need to write more code for them.
         ENDDO

      ! Non-matching case - fine
      ELSEIF(nx.gt.nx_gl(cb)) THEN
         m=nx/nx_gl(cB)
         IF (ALLOCATED(dist)) DEALLOCATE(dist)
         ALLOCATE(dist(nz+2*nGS))
         DO i=1,nGS
            DO j=1,nx+2*ngs
               dist%xd= bl(cB)%p((j-1-nGS)/m+nGS+2,i+nGS,:)%g%xd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,i+nGS,:)%g%xd
               dist%yd= bl(cB)%p((j-1-nGS)/m+nGS+2,i+nGS,:)%g%yd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,i+nGS,:)%g%yd
               dist%zd= bl(cB)%p((j-1-nGS)/m+nGS+2,i+nGS,:)%g%zd &
                       -bl(cB)%p((j-1-nGS)/m+nGS  ,i+nGS,:)%g%zd
               px(j,ny+nGS+i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,i+nGS,:)%g%xd &
                           +dist%xd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               py(j,ny+nGS+i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,i+nGS,:)%g%yd &
                           +dist%yd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
               pz(j,ny+nGS+i,:)= bl(cB)%p((j-1-nGS)/m+nGS+1,i+nGS,:)%g%zd &
                           +dist%zd*((MOD(j-1-nGS,m)+0.5d0)/m-0.5d0)*0.5d0
            ENDDO
         ENDDO

      ENDIF

   ENDIF

   !     no nonmatching interface in zeta(k) direction
   IF(bf(ib,fo_back)%faceType.eq.ft_interface .or. &
      bf(ib,fo_back)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_back)%connBlock
      DO i=1,nGS
         px(:,:,i)=bl(cB)%p(:,:,nz_gl(cB)+i)%g%xd
         py(:,:,i)=bl(cB)%p(:,:,nz_gl(cB)+i)%g%yd
         pz(:,:,i)=bl(cB)%p(:,:,nz_gl(cB)+i)%g%zd
      ENDDO
   ENDIF

   IF(bf(ib,fo_front)%faceType.eq.ft_interface .or. &
      bf(ib,fo_front)%faceType.eq.ft_interface_MPI) THEN
      cB=bf(ib,fo_front)%connBlock
      DO i=1,nGS
         px(:,:,nz+nGS+i)=bl(cB)%p(:,:,i+nGS)%g%xd
         py(:,:,nz+nGS+i)=bl(cB)%p(:,:,i+nGS)%g%yd
         pz(:,:,nz+nGS+i)=bl(cB)%p(:,:,i+nGS)%g%zd
      ENDDO
   ENDIF

   END ASSOCIATE

END SUBROUTINE ExpandPointCopy



