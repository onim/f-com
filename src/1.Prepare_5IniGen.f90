!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE IniGen
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_BlockPointData,         ONLY:bl
   USE control_Data,             ONLY:roRef,ruRef,reRef,chInit,iC,pI,iniCnd, &
                                      nx,ny,nz,nx_gl,ny_gl,nz_gl,nb,ib
   USE Message_Passing_Interface

IMPLICIT NONE

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   CALL IniGen_ReadCondition

   !***INITIAL CONDITION DATA GENERATION
   !   iniCnd=.true. : READ INTERMEDIATE DATA AND CONTINUE CALCULATION
   !   OTHERWISE: START CALCULATION FROM IT=1
   IF(iniCnd) THEN
      CALL IniGen_Generation
      CALL IniGen_ReadPrevResult
      IF (IamPrimeNode()) PRINT *,'Initial condition loading from ', &
                               TRIM(chInit),' successful.'
   ELSE
      CALL IniGen_Generation
      IF (IamPrimeNode()) PRINT *,'Initial condition generation successful.'
   ENDIF

   _forAllBlocks_
      IF(.not.IamPrimeNode() .and. .not.myBlockIs(ib)) THEN
         DEALLOCATE(bl(ib)%p)
      ENDIF
   _endAllBlocks_
END SUBROUTINE IniGen

