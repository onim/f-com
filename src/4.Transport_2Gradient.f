!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     VISCOUS TERM  (CENTRAL DIFFERENCE)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE qnt_gradient(dimen)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data,             ONLY:ns
      USE conserved_Quantity,       ONLY:u_, v_, w_, t_, fs_
      USE transportModule,          ONLY:diffx,diffy,diffz,
     &                                   face,faces

      IMPLICIT NONE

      INTEGER,INTENT(IN)::dimen

      INTEGER::is

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!**********************************************************************
!***Physical quantities difference i.e. grad(c_i, v, T) (INNER REGION)
!**********************************************************************
!     CENTRAL DIFFERENCE - 1st derivatives
!     VELOCITIES, TEMPERATURE, MASS FRACTION
      face%dux=diffx(u_,dimen)
      face%duy=diffy(u_,dimen)
      face%duz=diffz(u_,dimen)

      face%dvx=diffx(v_,dimen)
      face%dvy=diffy(v_,dimen)
      face%dvz=diffz(v_,dimen)

      face%dwx=diffx(w_,dimen)
      face%dwy=diffy(w_,dimen)
      face%dwz=diffz(w_,dimen)

      face%dtx=diffx(t_,dimen)
      face%dty=diffy(t_,dimen)
      face%dtz=diffz(t_,dimen)

      DO is=1,ns

      faces(:,:,:,is)%dsx=diffx(fs_(:,:,:,is),dimen)
      faces(:,:,:,is)%dsy=diffy(fs_(:,:,:,is),dimen)
      faces(:,:,:,is)%dsz=diffz(fs_(:,:,:,is),dimen)

      ENDDO

      END SUBROUTINE

