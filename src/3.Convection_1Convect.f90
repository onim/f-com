!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE CONVECT
!     convection terms on right hand side of
!     EULER EQUATION DIFFERENCE
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE M_ConvectModule
   USE Control_Data,             ONLY:nx,ny,nz,ib
   USE M_BlockPointData,         ONLY:bl
   USE Fundamental_Constants,    ONLY:nGS

IMPLICIT NONE

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!$OMP PARALLEL DO PRIVATE(i,j,k)
   _forAllPoints_
#define f bl(ib)%p(i,j,k)%f
      ! Initialize
      f%dro =0.0d0
      f%dru =0.0d0
      f%drv =0.0d0
      f%drw =0.0d0
      f%dre =0.0d0
      f%drfs=0.0d0
#undef f
   _endAllPoints_
!$OMP END PARALLEL DO

   CALL conv%alloc

   eachDimension: DO dimen=1,3
      setDimensionShift: SELECT CASE (dimen)
         CASE (1)
            ish=1
            jsh=0
            ksh=0
         CASE (2)
            ish=0
            jsh=1
            ksh=0
         CASE (3)
            ish=0
            jsh=0
            ksh=1
      ENDSELECT setDimensionShift

#define ind 100-ish,9-jsh,12-ksh
      CALL conv%getPrimitive
      CALL conv%calcCellValue
      CALL conv%calcFluxRate
      CALL conv%calcFaceArea
      CALL conv%calcFlux
#undef ind
      !***********************************************************************
      !***INVISCID FLUX DIFFERENCE AT EACH INTERNAL NODE
      !     the flux exists on the right boundary of the node.(CONVENTION)
      !***********************************************************************
!$OMP PARALLEL DO PRIVATE(i,j,k)
      _forInnerPoints_
#define f bl(ib)%p(i,j,k)%f
#define flux conv%flux
         f%dro =f%dro +flux(i,j,k,1) -flux(i-ish,j-jsh,k-ksh,1)
         f%dru =f%dru +flux(i,j,k,2) -flux(i-ish,j-jsh,k-ksh,2)
         f%drv =f%drv +flux(i,j,k,3) -flux(i-ish,j-jsh,k-ksh,3)
         f%drw =f%drw +flux(i,j,k,4) -flux(i-ish,j-jsh,k-ksh,4)
         f%dre =f%dre +flux(i,j,k,5) -flux(i-ish,j-jsh,k-ksh,5)
         f%drfs=f%drfs+flux(i,j,k,6:)-flux(i-ish,j-jsh,k-ksh,6:)
#undef f
#undef flux
      _endInnerPoints_
!$OMP END PARALLEL DO
   ENDDO eachDimension

END SUBROUTINE
