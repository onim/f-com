!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!                       OUTPUT RESULT
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE OUTRSL
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE control_Data
      USE conserved_Quantity
      USE grid_Data
      USE Message_Passing_Interface
      USE AuxSubprogram,            ONLY:arraypoint


      IMPLICIT NONE

      INTEGER::i,j,k,is
      CHARACTER*40 chrslt
      CHARACTER*6 itrchr

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
      gathering: DO ib=1,nb
         IF(primeNode().and.(.not. myBlockIs(ib))) THEN
            CALL MPI_RECV(cons_gl(ib)%ro_,
     &                    SIZE(cons_gl(ib)%ro_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%ru_,
     &                    SIZE(cons_gl(ib)%ru_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%rv_,
     &                    SIZE(cons_gl(ib)%rv_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%rw_,
     &                    SIZE(cons_gl(ib)%rw_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%re_,
     &                    SIZE(cons_gl(ib)%re_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%t_,
     &                    SIZE(cons_gl(ib)%t_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%p_,
     &                    SIZE(cons_gl(ib)%p_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%c_,
     &                    SIZE(cons_gl(ib)%c_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%qchem_,
     &                    SIZE(cons_gl(ib)%qchem_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
            CALL MPI_RECV(cons_gl(ib)%rfs_,
     &                    SIZE(cons_gl(ib)%rfs_),
     &                    MPI_REAL8,blockOwner(ib),0,
     &                    MPI_COMM_WORLD,MPIStatus,ierr)
         ELSEIF((.not.primeNode()).and.myBlockIs(ib)) THEN
            CALL MPI_SEND(cons_gl(ib)%ro_,
     &                    SIZE(cons_gl(ib)%ro_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%ru_,
     &                    SIZE(cons_gl(ib)%ru_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%rv_,
     &                    SIZE(cons_gl(ib)%rv_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%rw_,
     &                    SIZE(cons_gl(ib)%rw_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%re_,
     &                    SIZE(cons_gl(ib)%re_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%t_,
     &                    SIZE(cons_gl(ib)%t_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%p_,
     &                    SIZE(cons_gl(ib)%p_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%c_,
     &                    SIZE(cons_gl(ib)%c_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%qchem_,
     &                    SIZE(cons_gl(ib)%qchem_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
            CALL MPI_SEND(cons_gl(ib)%rfs_,
     &                    SIZE(cons_gl(ib)%rfs_),
     &                    MPI_REAL8,root,0,
     &                    MPI_COMM_WORLD,ierr)
         ENDIF
      ENDDO gathering

      IF(primeNode()) THEN
!***PROCESS RESULTS OUTPUT
!   determine result output filenames by iteration number.
      IF(it.gt.999999) THEN
            WRITE(6,*) 'Too much iteration number, change source code'
      ENDIF

!     convert iteration number into character type
      WRITE(itrchr,'(I6)') it

!     replace spaces with 0
      itrchr=repeat('0',scan(itrchr,' ',BACK=.TRUE.))
     &     //TRIM(ADJUSTL(itrchr))

!     output filename
      chrslt='rsl'//itrchr//'.dat'
      OPEN(60,FILE=chrslt,FORM='UNFORMATTED')

!     output some parameters used for making vtk format file.
      WRITE(60) IT,dqMax,dqNorm,TIME

      multiBlock: DO ib=1,nb
         DO k=1,nz_gl(ib); DO j=1,ny_gl(ib); DO i=1,nx_gl(ib)
            WRITE(60)
     &  cons_gl(ib)%RO_(i,j,k),cons_gl(ib)%RU_(i,j,k),cons_gl(ib)%RV_(i,j,k),cons_gl(ib)%RW_(i,j,k),cons_gl(ib)%RE_(i,j,k),
     &  cons_gl(ib)%T_(i,j,k),  cons_gl(ib)%p_(i,j,k),
     &  SQRT(cons_gl(ib)%ru_(i,j,k)**2+cons_gl(ib)%rv_(i,j,k)**2+cons_gl(ib)%rw_(i,j,k)**2)
     &     /cons_gl(ib)%c_(i,j,k)/cons_gl(ib)%ro_(i,j,k),
     &  cons_gl(ib)%qChem_(i,j,k),
     &  (cons_gl(ib)%RFS_(i,j,k,is),is=1,ns)
         ENDDO;ENDDO;ENDDO
      ENDDO multiBlock

      CLOSE(60)

      ENDIF

      END SUBROUTINE

