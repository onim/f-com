C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
C
C                       USING CHEMKIN-II CODE
C                     CALCULATE CHEMICAL REACTION
C
C$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
c     before execution, check variable name and module usage.

      SUBROUTINE CHEMIC
C$$$$$$$$$$$$$$$
C     Header
C$$$$$$$$$$$$$$$
      USE grid_Data,                      ONLY:gj
      USE conserved_Quantity,             ONLY:yfs,qChem,rfs,
     +                                         temperature => t
      USE thermal_Property,               ONLY:td,hf,wMol,TBnd
      USE flux_difference,                ONLY:dre,drfs
      USE fundamental_Constants,          ONLY:RMOL,RCAL
      USE reaction_data,                  ONLY:cf,zetaf,ef,
     +                                         cf0,zetaf0,ef0,
     +                                         acent,ts1,ts2,ts3
      USE control_Data,                   ONLY:ns,nr,nx,ny,nz,
     +                                         dt

      IMPLICIT NONE

      REAL(8)::TMOLE,tmole1,tmole2,tmole4,
     +      t,rkf0,pred,fcent,ctro,ntro,ftro
      REAL(8),DIMENSION(nr)::
     +      DGIP, RKF, RPF, RPB, EQK, DOMG
      REAL(8),DIMENSION(ns)::
     +      gip
      REAL(8)::DYFS(nx,ny,nz,ns)

      INTEGER::i,j,k,is
C$$$$$$$$$$$$$$$
C     Main
C$$$$$$$$$$$$$$$
C***********************************************************************
C***CHEMICAL REACTION
C***********************************************************************

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx

      t=temperature(i,j,k)

      TMOLE=SUM(YFS(i,j,k,:))
c
      tmole1=tmole-yfs(i,j,k,8)-yfs(i,j,k,1)
      tmole2=tmole+5.4d0*yfs(i,j,k,8)
      tmole4=tmole-2.0d0*(yfs(i,j,k,8)+yfs(i,j,k,1))-yfs(i,j,k,10)
c
      DO is=1,ns
      IF (t.lt.TBnd(is)) Then

      gip(is)=
     +       td(is,1,1)         *(1.0d0-LOG(t))
     +      -td(is,2,1)/(2.0d0) *t
     +      -td(is,3,1)/(6.0d0) *t**2
     +      -td(is,4,1)/(12.0d0)*t**3
     +      -td(is,5,1)/(20.0d0)*t**4
     +      +td(is,6,1)         /t
     +      -td(is,7,1)

      ELSE

      gip(is)=
     +       td(is,1,2)         *(1.0d0-LOG(t))
     +      -td(is,2,2)/(2.0d0) *t
     +      -td(is,3,2)/(6.0d0) *t**2
     +      -td(is,4,2)/(12.0d0)*t**3
     +      -td(is,5,2)/(20.0d0)*t**4
     +      +td(is,6,2)         /t
     +      -td(is,7,2)

      ENDIF
      ENDDO

      dgip(1)=gip(1)-gip(2)+gip(5)-gip(8)
      dgip(2)=-gip(2)-gip(3)+gip(4)+gip(5)
      dgip(3)=gip(1)-gip(2)+gip(4)-gip(5)
      dgip(4)=gip(2)+gip(3)-gip(6)
      dgip(5)=gip(2)+gip(3)-gip(6)
      dgip(6)=gip(2)+gip(3)-gip(6)
      dgip(7)=gip(2)+gip(3)-gip(6)
      dgip(8)=-gip(3)+gip(5)+gip(6)-gip(8)
      dgip(9)=-gip(3)+gip(5)+gip(6)-gip(8)
      dgip(10)=gip(2)-2.0d0*gip(5)+gip(6)
      dgip(11)=-gip(1)+gip(2)-gip(3)+gip(6)
      dgip(12)=gip(2)-gip(4)+gip(6)-gip(8)
      dgip(13)=-gip(3)+gip(4)-gip(5)+gip(6)
      dgip(14)=-gip(4)+2.0d0*gip(5)-gip(8)
      dgip(15)=-gip(1)+2.0d0*gip(2)
      dgip(16)=-gip(1)+2.0d0*gip(2)
      dgip(17)=-gip(1)+2.0d0*gip(2)
      dgip(18)=gip(2)+gip(5)-gip(8)
      dgip(19)=gip(2)+gip(4)-gip(5)
      dgip(20)=-gip(3)+2.0d0*gip(4)
      dgip(21)=-gip(3)+2.0d0*gip(6)-gip(7)
      dgip(22)=-gip(3)+2.0d0*gip(6)-gip(7)
      dgip(23)=2.0d0*gip(5)-gip(7)
      dgip(24)=-gip(1)+gip(2)-gip(6)+gip(7)
      dgip(25)=gip(2)-gip(5)+gip(7)-gip(8)
      dgip(26)=gip(4)-gip(5)-gip(6)+gip(7)
      dgip(27)=gip(5)-gip(6)+gip(7)-gip(8)
c
      rkf(1)=cf(1)*1.0d-6*t**zetaf(1)*dexp(-ef(1)/rcal/t)
      rkf(2)=cf(2)*1.0d-6*t**zetaf(2)*dexp(-ef(2)/rcal/t)
      rkf(3)=cf(3)*1.0d-6*t**zetaf(3)*dexp(-ef(3)/rcal/t)
      rkf(4)=cf(4)*1.0d-6*t**zetaf(4)*dexp(-ef(4)/rcal/t)
      rkf0=cf0(4)*1.0d-12*t**zetaf0(4)*dexp(-ef0(4)/rcal/t)
      pred=rkf0*tmole4/rkf(4)
c      fcent=(1-acent(4))*dexp(-t/ts3(4))
c     +        +acent(4)*dexp(-t/ts1(4))+dexp(-ts2(4)/t)
c      ctro=-0.4d0-0.67d0*dlog(fcent)
c      ntro= 0.75d0-1.27d0*dlog(fcent)
c      ftro=fcent*dexp(1/(1+((dlog(pred)+ctro)
c     +        /(ntro-0.14d0*(dlog(pred)+ctro)))**2))
      ftro=1.0d0
      rkf(4)=rkf(4)*pred/(1+pred)*ftro
      rkf(5)=cf(5)*1.0d-6*t**zetaf(5)*dexp(-ef(5)/rcal/t)
      rkf0=cf0(5)*1.0d-12*t**zetaf0(5)*dexp(-ef0(5)/rcal/t)
      pred=rkf0*yfs(i,j,k,10)/rkf(5)
c      fcent=(1-acent(5))*dexp(-t/ts3(5))
c     +        +acent(5)*dexp(-t/ts1(5))+dexp(-ts2(5)/t)
c      ctro=-0.4d0-0.67d0*dlog(fcent)
c      ntro= 0.75d0-1.27d0*dlog(fcent)
c      ftro=fcent*dexp(1/(1+((dlog(pred)+ctro)
c     +        /(ntro-0.14d0*(dlog(pred)+ctro)))**2))
      ftro=1.0d0
      rkf(5)=rkf(5)*pred/(1+pred)*ftro
      rkf(6)=cf(6)*1.0d-6*t**zetaf(6)*dexp(-ef(6)/rcal/t)
      rkf0=cf0(6)*1.0d-12*t**zetaf0(6)*dexp(-ef0(6)/rcal/t)
      pred=rkf0*yfs(i,j,k,1)/rkf(6)
c      fcent=(1-acent(6))*dexp(-t/ts3(6))
c     +        +acent(6)*dexp(-t/ts1(6))+dexp(-ts2(6)/t)
c      ctro=-0.4d0-0.67d0*dlog(fcent)
c      ntro= 0.75d0-1.27d0*dlog(fcent)
c      ftro=fcent*dexp(1/(1+((dlog(pred)+ctro)
c     +        /(ntro-0.14d0*(dlog(pred)+ctro)))**2))
      ftro=1.0d0
      rkf(6)=rkf(6)*pred/(1+pred)*ftro
      rkf(7)=cf(7)*1.0d-6*t**zetaf(7)*dexp(-ef(7)/rcal/t)
      rkf0=cf0(7)*1.0d-12*t**zetaf0(7)*dexp(-ef0(7)/rcal/t)
      pred=rkf0*yfs(i,j,k,8)/rkf(7)
c      fcent=(1-acent(7))*dexp(-t/ts3(7))
c     +        +acent(7)*dexp(-t/ts1(7))+dexp(-ts2(7)/t)
c      ctro=-0.4d0-0.67d0*dlog(fcent)
c      ntro= 0.75d0-1.27d0*dlog(fcent)
c      ftro=fcent*dexp(1/(1+((dlog(pred)+ctro)
c     +        /(ntro-0.14d0*(dlog(pred)+ctro)))**2))
      ftro=1.0d0
      rkf(7)=rkf(7)*pred/(1+pred)*ftro
      rkf(8)=cf(8)*1.0d-6*t**zetaf(8)*dexp(-ef(8)/rcal/t)
      rkf(9)=cf(9)*1.0d-6*t**zetaf(9)*dexp(-ef(9)/rcal/t)
      rkf(10)=cf(10)*1.0d-6*t**zetaf(10)*dexp(-ef(10)/rcal/t)
      rkf(11)=cf(11)*1.0d-6*t**zetaf(11)*dexp(-ef(11)/rcal/t)
      rkf(12)=cf(12)*1.0d-6*t**zetaf(12)*dexp(-ef(12)/rcal/t)
      rkf(13)=cf(13)*1.0d-6*t**zetaf(13)*dexp(-ef(13)/rcal/t)
      rkf(14)=cf(14)*1.0d-6*t**zetaf(14)*dexp(-ef(14)/rcal/t)
      rkf(15)=cf(15)*1.0d-12*t**zetaf(15)*dexp(-ef(15)/rcal/t)
      rkf(16)=cf(16)*1.0d-12*t**zetaf(16)*dexp(-ef(16)/rcal/t)
      rkf(17)=cf(17)*1.0d-12*t**zetaf(17)*dexp(-ef(17)/rcal/t)
      rkf(18)=cf(18)*1.0d-12*t**zetaf(18)*dexp(-ef(18)/rcal/t)
      rkf(19)=cf(19)*1.0d-12*t**zetaf(19)*dexp(-ef(19)/rcal/t)
      rkf(20)=cf(20)*1.0d-12*t**zetaf(20)*dexp(-ef(20)/rcal/t)
      rkf(21)=cf(21)*1.0d-6*t**zetaf(21)*dexp(-ef(21)/rcal/t)
      rkf(22)=cf(22)*1.0d-6*t**zetaf(22)*dexp(-ef(22)/rcal/t)
      rkf(23)=cf(23)*1.0d-6*t**zetaf(23)*dexp(-ef(23)/rcal/t)
      rkf0=cf0(23)*1.0d-12*t**zetaf0(23)*dexp(-ef0(23)/rcal/t)
      pred=rkf0*tmole/rkf(23)
      fcent=(1-acent(23))*dexp(-t/ts3(23))
     +        +acent(23)*dexp(-t/ts1(23))+dexp(-ts2(23)/t)
      ctro=-0.4d0-0.67d0*dlog(fcent)
      ntro= 0.75d0-1.27d0*dlog(fcent)
      ftro=fcent*dexp(1/(1+((dlog(pred)+ctro)
     +        /(ntro-0.14d0*(dlog(pred)+ctro)))**2))
      rkf(23)=rkf(23)*pred/(1+pred)*ftro
      rkf(24)=cf(24)*1.0d-6*t**zetaf(24)*dexp(-ef(24)/rcal/t)
      rkf(25)=cf(25)*1.0d-6*t**zetaf(25)*dexp(-ef(25)/rcal/t)
      rkf(26)=cf(26)*1.0d-6*t**zetaf(26)*dexp(-ef(26)/rcal/t)
      rkf(27)=cf(27)*1.0d-6*t**zetaf(27)*dexp(-ef(27)/rcal/t)
c
      eqk(1)=dexp(-dgip(1))
      eqk(2)=dexp(-dgip(2))
      eqk(3)=dexp(-dgip(3))
      eqk(4)=dexp(-dgip(4))/(rmol*t)
      eqk(5)=dexp(-dgip(5))/(rmol*t)
      eqk(6)=dexp(-dgip(6))/(rmol*t)
      eqk(7)=dexp(-dgip(7))/(rmol*t)
      eqk(8)=dexp(-dgip(8))
      eqk(9)=dexp(-dgip(9))
      eqk(10)=dexp(-dgip(10))
      eqk(11)=dexp(-dgip(11))
      eqk(12)=dexp(-dgip(12))
      eqk(13)=dexp(-dgip(13))
      eqk(14)=dexp(-dgip(14))
      eqk(15)=dexp(-dgip(15))/(rmol*t)
      eqk(16)=dexp(-dgip(16))/(rmol*t)
      eqk(17)=dexp(-dgip(17))/(rmol*t)
      eqk(18)=dexp(-dgip(18))/(rmol*t)
      eqk(19)=dexp(-dgip(19))/(rmol*t)
      eqk(20)=dexp(-dgip(20))/(rmol*t)
      eqk(21)=dexp(-dgip(21))
      eqk(22)=dexp(-dgip(22))
      eqk(23)=dexp(-dgip(23))/(rmol*t)
      eqk(24)=dexp(-dgip(24))
      eqk(25)=dexp(-dgip(25))
      eqk(26)=dexp(-dgip(26))
      eqk(27)=dexp(-dgip(27))
c
      rpf(1)=rkf(1)*yfs(i,j,k,1)*yfs(i,j,k,5)
      rpf(2)=rkf(2)*yfs(i,j,k,4)*yfs(i,j,k,5)
      rpf(3)=rkf(3)*yfs(i,j,k,1)*yfs(i,j,k,4)
      rpf(4)=rkf(4)*yfs(i,j,k,2)*yfs(i,j,k,3)
      rpf(5)=rkf(5)*yfs(i,j,k,2)*yfs(i,j,k,3)
      rpf(6)=rkf(6)*yfs(i,j,k,2)*yfs(i,j,k,3)
      rpf(7)=rkf(7)*yfs(i,j,k,2)*yfs(i,j,k,3)
      rpf(8)=rkf(8)*yfs(i,j,k,5)*yfs(i,j,k,6)
      rpf(9)=rkf(9)*yfs(i,j,k,5)*yfs(i,j,k,6)
      rpf(10)=rkf(10)*yfs(i,j,k,2)*yfs(i,j,k,6)
      rpf(11)=rkf(11)*yfs(i,j,k,2)*yfs(i,j,k,6)
      rpf(12)=rkf(12)*yfs(i,j,k,2)*yfs(i,j,k,6)
      rpf(13)=rkf(13)*yfs(i,j,k,4)*yfs(i,j,k,6)
      rpf(14)=rkf(14)*yfs(i,j,k,5)**2
      rpf(15)=rkf(15)*yfs(i,j,k,2)**2*tmole1
      rpf(16)=rkf(16)*yfs(i,j,k,1)*yfs(i,j,k,2)**2
      rpf(17)=rkf(17)*yfs(i,j,k,2)**2*yfs(i,j,k,8)
      rpf(18)=rkf(18)*yfs(i,j,k,2)*yfs(i,j,k,5)*tmole2
      rpf(19)=rkf(19)*yfs(i,j,k,2)*yfs(i,j,k,4)*tmole2
      rpf(20)=rkf(20)*yfs(i,j,k,4)**2*tmole
      rpf(21)=rkf(21)*yfs(i,j,k,6)**2
      rpf(22)=rkf(22)*yfs(i,j,k,6)**2
      rpf(23)=rkf(23)*yfs(i,j,k,5)**2
      rpf(24)=rkf(24)*yfs(i,j,k,2)*yfs(i,j,k,7)
      rpf(25)=rkf(25)*yfs(i,j,k,2)*yfs(i,j,k,7)
      rpf(26)=rkf(26)*yfs(i,j,k,4)*yfs(i,j,k,7)
      rpf(27)=rkf(27)*yfs(i,j,k,5)*yfs(i,j,k,7)
c
      rpb(1)=rkf(1)*eqk(1)*yfs(i,j,k,2)*yfs(i,j,k,8)
      rpb(2)=rkf(2)*eqk(2)*yfs(i,j,k,2)*yfs(i,j,k,3)
      rpb(3)=rkf(3)*eqk(3)*yfs(i,j,k,2)*yfs(i,j,k,5)
      rpb(4)=rkf(4)*eqk(4)*yfs(i,j,k,6)
      rpb(5)=rkf(5)*eqk(5)*yfs(i,j,k,6)
      rpb(6)=rkf(6)*eqk(6)*yfs(i,j,k,6)
      rpb(7)=rkf(7)*eqk(7)*yfs(i,j,k,6)
      rpb(8)=rkf(8)*eqk(8)*yfs(i,j,k,3)*yfs(i,j,k,8)
      rpb(9)=rkf(9)*eqk(9)*yfs(i,j,k,3)*yfs(i,j,k,8)
      rpb(10)=rkf(10)*eqk(10)*yfs(i,j,k,5)**2
      rpb(11)=rkf(11)*eqk(11)*yfs(i,j,k,1)*yfs(i,j,k,3)
      rpb(12)=rkf(12)*eqk(12)*yfs(i,j,k,4)*yfs(i,j,k,8)
      rpb(13)=rkf(13)*eqk(13)*yfs(i,j,k,3)*yfs(i,j,k,5)
      rpb(14)=rkf(14)*eqk(14)*yfs(i,j,k,4)*yfs(i,j,k,8)
      rpb(15)=rkf(15)*eqk(15)*yfs(i,j,k,1)*tmole1
      rpb(16)=rkf(16)*eqk(16)*yfs(i,j,k,1)**2
      rpb(17)=rkf(17)*eqk(17)*yfs(i,j,k,1)*yfs(i,j,k,8)
      rpb(18)=rkf(18)*eqk(18)*yfs(i,j,k,8)*tmole2
      rpb(19)=rkf(19)*eqk(19)*yfs(i,j,k,5)*tmole2
      rpb(20)=rkf(20)*eqk(20)*yfs(i,j,k,3)*tmole
      rpb(21)=rkf(21)*eqk(21)*yfs(i,j,k,3)*yfs(i,j,k,7)
      rpb(22)=rkf(22)*eqk(22)*yfs(i,j,k,3)*yfs(i,j,k,7)
      rpb(23)=rkf(23)*eqk(23)*yfs(i,j,k,7)
      rpb(24)=rkf(24)*eqk(24)*yfs(i,j,k,1)*yfs(i,j,k,6)
      rpb(25)=rkf(25)*eqk(25)*yfs(i,j,k,5)*yfs(i,j,k,8)
      rpb(26)=rkf(26)*eqk(26)*yfs(i,j,k,5)*yfs(i,j,k,6)
      rpb(27)=rkf(27)*eqk(27)*yfs(i,j,k,6)*yfs(i,j,k,8)
c
      domg=rpf-rpb
c
      dyfs(i,j,k,1)=-domg(1)-domg(3)+domg(11)+domg(15)+domg(16)
     +        +domg(17)+domg(24)
      dyfs(i,j,k,2)=domg(1)+domg(2)+domg(3)-domg(4)-domg(5)
     +         -domg(6)-domg(7)-domg(10)-domg(11)-domg(12)
     +         -2.0d0*domg(15)-2.0d0*domg(16)-2.0d0*domg(17)
     +         -domg(18)-domg(19)-domg(24)-domg(25)
      dyfs(i,j,k,3)=domg(2)-domg(4)-domg(5)-domg(6)-domg(7)
     +        +domg(8)+domg(9)+domg(11)+domg(13)+domg(20)
     +        +domg(21)+domg(22)
      dyfs(i,j,k,4)=-domg(2)-domg(3)+domg(12)-domg(13)+domg(14)
     +         -domg(19)-2.0d0*domg(20)-domg(26)
      dyfs(i,j,k,5)=-domg(1)-domg(2)+domg(3)-domg(8)-domg(9)
     +        +2.0d0*domg(10)+domg(13)-2.0d0*domg(14)-domg(18)
     +        +domg(19)-2.0d0*domg(23)+domg(25)+domg(26)
     +         -domg(27)
      dyfs(i,j,k,6)=domg(4)+domg(5)+domg(6)+domg(7)-domg(8)
     +         -domg(9)-domg(10)-domg(11)-domg(12)-domg(13)
     +         -2.0d0*domg(21)-2.0d0*domg(22)+domg(24)
     +        +domg(26)+domg(27)
      dyfs(i,j,k,7)=domg(21)+domg(22)+domg(23)-domg(24)-domg(25)
     +         -domg(26)-domg(27)
      dyfs(i,j,k,8)=domg(1)+domg(8)+domg(9)+domg(12)+domg(14)
     +        +domg(18)+domg(25)+domg(27)
      dyfs(i,j,k,9)=0.0d0
      dyfs(i,j,k,10)=0.0d0

      drfs(i,j,k,:)=drfs(i,j,k,:)-dyfs(i,j,k,:)*wMol*gj(i,j,k)

      qChem(i,j,k)=-1.0d0*sum(DYFS(i,j,k,:)*wMol*HF(i,j,k,:))

      DRE(i,j,k)=DRE(i,j,k)-qChem(i,j,k)*GJ(i,j,k)

          ENDDO
        ENDDO
      ENDDO


      END SUBROUTINE


