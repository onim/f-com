!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Modules
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!     Suffixes
!     _           means additional(ghost) node value
!     _gl         means global variable

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!**** flux difference
      MODULE Flux_Difference
      IMPLICIT NONE
      SAVE

      TYPE fluxdifference
      REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::
     &      dro_, dru_, drv_, drw_, dre_
      REAL(8),ALLOCATABLE,DIMENSION(:,:,:,:)::
     &      drfs_
      ENDTYPE

      TYPE(fluxdifference),ALLOCATABLE,TARGET,DIMENSION(:)::fdif_gl

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      dro_, dru_, drv_, drw_, dre_
      REAL(8),POINTER,DIMENSION(:,:,:,:)::
     &      drfs_

      REAL(8),POINTER,DIMENSION(:,:,:)::
     &      dro, dru, drv, drw, dre
      REAL(8),POINTER,DIMENSION(:,:,:,:)::
     &      drfs
      END MODULE

