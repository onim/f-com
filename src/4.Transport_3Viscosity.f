!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     VISCOUS TERM  (CENTRAL DIFFERENCE)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE getViscosity
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE transportModule,          ONLY:rci22,getViscSpecies,
     &                                   pt,pts

      USE thermal_Property,         ONLY:sig,tek,wMol,vmol,dmol
      USE conserved_Quantity,       ONLY:T_,yfs_,ro_
      USE control_Data,             ONLY:nx,ny,nz,
     &                                   num_nonRadical,
     &                                   nonRadical
      USE Status_Check,              ONLY:meanViscosity
      USE fundamental_Constants,    ONLY:nGS

      IMPLICIT NONE

      INTEGER::i,j,k,is,iss,nr,nrr
      REAL(8)::phi

!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
!$OMP PARALLEL DO PRIVATE(i,j,k,is,iss,nr,nrr,phi) REDUCTION(+:meanViscosity)
      do k=1-nGS,nz+nGS
        do j=1-nGS,ny+nGS
          do i=1-nGS,nx+nGS
!**********************************************************************
!   >>>LAMINAR VISCOSITY from non-radical species<<<

!     viscosities of each non-radicals
      DO is=1,num_nonRadical
            nr=nonRadical(is)
            pts(i,j,k,nr)%VISM=getViscSpecies
     &            (t_(i,j,k), sig(nr), rci22(t_(i,j,k),tek(nr)), wMol(nr))
      ENDDO


!     viscosity of mixture
!     Combustion, Eq. 5.18
      pt(i,j,k)%visc=0.0d0
      DO is=1,num_nonRadical
        phi=0.0d0
        nr=nonRadical(is)

        DO iss=1,num_nonRadical
          nrr=nonRadical(iss)
          IF (nrr.ne.nr) THEN
            phi=phi+
     &        YFS_(i,j,k,nrr)
     &          *(1.0D0+SQRT(pts(i,j,k,nr)%VISM/pts(i,j,k,nrr)%VISM)
     &          *VMOL(nr,nrr)**2*DMOL(nr,nrr))
          ELSE
            phi=phi+
     &          yfs_(i,j,k,nrr)
          ENDIF
        ENDDO

        pt(i,j,k)%VISC=
     &  pt(i,j,k)%VISC+YFS_(i,j,k,nr)*pts(i,j,k,nr)%VISM/phi

      ENDDO

      meanViscosity=meanViscosity+pt(i,j,k)%visc

          ENDDO
        ENDDO
      ENDDO
!$OMP END PARALLEL DO


      meanViscosity=meanViscosity
     &              /((nx+2*nGS)*(ny+2*nGS)*(nz+2*nGS))*1.0d5

      END SUBROUTINE

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!     Momentum diffusion (CENTRAL DIFFERENCE)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      SUBROUTINE momentumDiffusion(dimen)
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
      USE transportModule,          ONLY:pt, face

      USE control_Data,             ONLY:nx,ny,nz
      USE conserved_Quantity,       ONLY:ro_
      USE grid_Data,                ONLY:sgsDlt_
      USE Status_Check,              ONLY:visc_Ratio
      USE fundamental_Constants,    ONLY:nGS
      USE TurbulenceModeling

      IMPLICIT NONE

      INTEGER,INTENT(IN)::dimen
      INTEGER::i,j,k,ish,jsh,ksh
      REAL(8)::sxx,syy,szz,sxy,syz,sxz,
     &         txx,tyy,tzz,txy,tyz,txz,
     &         visc,vist

      REAL(8)::turbulentPressure=0.0d0
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$

      ish=0
      jsh=0
      ksh=0
      SELECT CASE(dimen)
      CASE(1)
            ish=1
      CASE(2)
            jsh=1
      CASE(3)
            ksh=1
      END SELECT

      visc_Ratio=0.0d0

      IF( TurbModel.eq.isLES .and.
     &   (LESModel.eq.isDynamic .or.
     &    LESModel.eq.isCompressibleDynamic)) THEN
            CALL Filter(face, sgsdlt_)
      ENDIF

!$OMP PARALLEL DO
!$OMP+ PRIVATE(i,j,k,vist,visc,sxx,syy,szz,sxy,sxz,syz,
!$OMP+                               txx,tyy,tzz,txy,txz,tyz)

      do k=0,nz
        do j=0,ny
          do i=0,nx

      CALL face(i,j,k)%strainRate

      vist=getVisT(face(i,j,k),sgsDlt_(i,j,k))*ro_(i,j,k)

      visc=0.5d0*(pt(i,j,k)%visc+pt(i+ish,j+jsh,k+ksh)%visc)

!     deviatoric strain rate on the boundary
      SXX=2.D0*(2.D0*face(i,j,k)%DUX-face(i,j,k)%DVY-face(i,j,k)%DWZ)/3.D0
      SYY=2.D0*(2.D0*face(i,j,k)%DVY-face(i,j,k)%DWZ-face(i,j,k)%DUX)/3.D0
      SZZ=2.D0*(2.D0*face(i,j,k)%DWZ-face(i,j,k)%DVY-face(i,j,k)%DUX)/3.D0
      SXY=(face(i,j,k)%DUY+face(i,j,k)%DVX)
      SXZ=(face(i,j,k)%DWX+face(i,j,k)%DUZ)
      SYZ=(face(i,j,k)%DVZ+face(i,j,k)%DWY)


!   >>>STRESS TENSOR<<<
      TXX=(visc+vist)*sxx + turbulentPressure
      TYY=(visc+vist)*syy + turbulentPressure
      TZZ=(visc+vist)*szz + turbulentPressure
      TXY=(visc+vist)*SXY
      TXZ=(visc+vist)*SXZ
      TYZ=(visc+vist)*SYZ

      visc_Ratio=MAX(visc_Ratio, vist/visc*1.0d2)
      face(i,j,k)%vist=vist

!     Input stress result into flux terms
      face(i,j,k)%RUFX=TXX
      face(i,j,k)%RVFX=TXY
      face(i,j,k)%RWFX=TXZ

      face(i,j,k)%RUFY=TXY
      face(i,j,k)%RVFY=TYY
      face(i,j,k)%RWFY=TYZ

      face(i,j,k)%RUFZ=TXZ
      face(i,j,k)%RVFZ=TYZ
      face(i,j,k)%RWFZ=TZZ

          ENDDO
        ENDDO
      ENDDO
!$OMP END PARALLEL DO


      END SUBROUTINE
