!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
SUBROUTINE IniGen_Generation
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
   USE control_Data,             ONLY:ib,nb,ns,itrBgn,itrEnd,maxItr, &
                                      nx,ny,nz,nx_gl,ny_gl,nz_gl
   USE M_BlockPointData,         ONLY:bl,initQuant
   USE Message_Passing_Interface

IMPLICIT NONE

   INTEGER::is
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   _forMyBlocks_
   ASSOCIATE(c=>bl(ib)%p(:,:,:)%c)
      SELECT CASE (ib)
      CASE(9,10,19,20,29,30)
         c%ro=initQuant%ro
         c%ru=0.0d0
         c%rv=0.0d0
         c%rw=0.0d0
         c%re=initQuant%re &
              -0.5d0*(initQuant%ru**2 &
                     +initQuant%rv**2 &
                     +initQuant%rw**2)/initQuant%ro
         c%t =initQuant%t
            DO is=1,ns
         c%rfs(is)=initQuant%rfs(is)
            ENDDO

      CASE DEFAULT
         c%ro=initQuant%ro
         c%ru=initQuant%ru
         c%rv=initQuant%rv
         c%rw=initQuant%rw
         c%re=initQuant%re
         c%t =initQuant%t
            DO is=1,ns
         c%rfs(is)=initQuant%rfs(is)
            ENDDO
      END SELECT
   END ASSOCIATE
   _endMyBlocks_

   itrBgn=1
   itrEnd=maxItr

END SUBROUTINE
