!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
MODULE Boundary_Data_OOP
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
   USE fundamental_Constants,    ONLY:ijkPosition
   USE M_BlockPointData,         ONLY:T_ConservedQuantity

IMPLICIT NONE
   SAVE
!$$$$$$$$$$$$$$$$$$$$$
!     Derived data type definition
!$$$$$$$$$$$$$$$$$$$$$

!$$$$$
   TYPE DirichletBC
!           Set a specific part of boundary value as a fixed one
!           Can be used for inlet / partial injection
!$$$$$
      TYPE(T_ConservedQuantity)::quant
      INTEGER::nnode
      TYPE(ijkPosition),DIMENSION(:),ALLOCATABLE::nodelist
      LOGICAL::uniform
   CONTAINS
      PROCEDURE::calcNodeList
   ENDTYPE

!$$$$$
   TYPE blockFace
!$$$$$
   !---------------------|
   ! interfaceBlockFace  |
   !     inletBlockFace  |
   !    outletBlockFace  | => blockFace
   !      wallBlockFace  |
   !  periodicBlockFace  |
   !---------------------|
      INTEGER::         &
         faceType=0,    &
         myNode=0,      &
         connNode=0,    &
         connBlock=0,   &
         connFace=0,    &
         nFBC=0,        &
         inletType=0,   &
         outletType=0
      INTEGER::xil, xiu, yil, yiu, zil, ziu, &
               xol, xou, yol, you, zol, zou
      LOGICAL::isSlipwall=.false.
      REAL(8)::wallTemperature=0
      REAL(8),DIMENSION(:,:,:),ALLOCATABLE::normx, normy, normz
      TYPE(DirichletBC),DIMENSION(:),ALLOCATABLE::fBC
   CONTAINS
      PROCEDURE::setBoundaryExtent
   ENDTYPE

   ! faceType numbering
   ! 1 for connected (interface, periodic)
   ! 2 for interface_MPI
   ! 3 for inlet
   ! 4 for outlet
   ! 5 for wall
   !
   ! dimension of blockFace
   ! 1 for west  -- x =  1
   ! 2 for east  -- x = nx
   ! 3 for south -- y =  1
   ! 4 for north -- y = ny
   ! 5 for back  -- z =  1
   ! 6 for front -- z = nz
   !
   ! boundary extent: [xyd]/[io]/[lu]
   ! xyz: direction
   ! io: inside/outside
   ! lu: lower/upper extent

   TYPE(blockFace),ALLOCATABLE::bf(:,:)

!$$$$$$$$$$$$$$$
CONTAINS
!     Class subprograms
!           1. Calculate node list from vertex
!$$$$$$$$$$$$$$$
SUBROUTINE calcNodeList(this,pBC)
   USE control_Data,       ONLY:partialBoundaryCondition

IMPLICIT NONE

   CLASS(DirichletBC)::this
   TYPE(partialBoundaryCondition)::pBC

   INTEGER::i,j,k,inode

   inode=0

   DO k=pBC%pxyz1(3),pBC%pxyz2(3)
   DO j=pBC%pxyz1(2),pBC%pxyz2(2)
   DO i=pBC%pxyz1(1),pBC%pxyz2(1);
      inode=inode+1
      this%nodelist(inode)%i=i
      this%nodelist(inode)%j=j
      this%nodelist(inode)%k=k
   ENDDO;ENDDO;ENDDO

END SUBROUTINE


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
PURE SUBROUTINE setBoundaryExtent(this, nx,ny,nz,nGS,facet)
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$$$$$$$$$$$$$$$
!     Header
!$$$$$$$$$$$$$$$
#include '0.Script_02Definitions.h'
IMPLICIT NONE

   CLASS(blockFace),INTENT(INOUT)::this
   INTEGER,INTENT(IN)::nx,ny,nz,nGS,facet
!$$$$$$$$$$$$$$$
!     Main
!$$$$$$$$$$$$$$$
   SELECT CASE(facet)
      CASE (1)
        this%xil=1+nGS
         this%yil=1
         this%zil=1
        this%xiu=2*nGS
         this%yiu=ny+2*nGS
         this%ziu=nz+2*nGS

           this%xol=1
            this%yol=1
            this%zol=1
           this%xou=nGS
            this%you=ny+2*nGS
            this%zou=nz+2*nGS
      CASE (2)
        this%xil=nx+1
         this%yil=1
         this%zil=1
        this%xiu=nx+nGS
         this%yiu=ny+2*nGS
         this%ziu=nz+2*nGS

           this%xol=nx+1+nGS
            this%yol=1
            this%zol=1
           this%xou=nx+2*nGS
            this%you=ny+2*nGS
            this%zou=nz+2*nGS
      CASE (3)
         this%xil=1
        this%yil=1+nGS
         this%zil=1
         this%xiu=nx+2*nGS
        this%yiu=2*nGS
         this%ziu=nz+2*nGS

            this%xol=1
           this%yol=1
            this%zol=1
            this%xou=nx+2*nGS
           this%you=nGS
            this%zou=nz+2*nGS
      CASE (4)
         this%xil=1
        this%yil=ny+1
         this%zil=1
         this%xiu=nx+2*nGS
        this%yiu=ny+nGS
         this%ziu=nz+2*nGS

            this%xol=1
           this%yol=ny+1+nGS
            this%zol=1
            this%xou=nx+2*nGS
           this%you=ny+2*nGS
            this%zou=nz+2*nGS
      CASE (5)
         this%xil=1
         this%yil=1
        this%zil=1+nGS
         this%xiu=nx+2*nGS
         this%yiu=ny+2*nGS
        this%ziu=2*nGS

            this%xol=1
            this%yol=1
           this%zol=1
            this%xou=nx+2*nGS
            this%you=ny+2*nGS
           this%zou=nGS
      CASE (6)
         this%xil=1
         this%yil=1
        this%zil=nz+1
         this%xiu=nx+2*nGS
         this%yiu=ny+2*nGS
        this%ziu=nz+nGS

            this%xol=1
            this%yol=1
           this%zol=nz+1+nGS
            this%xou=nx+2*nGS
            this%you=ny+2*nGS
           this%zou=nz+2*nGS
   END SELECT
END SUBROUTINE


END MODULE





